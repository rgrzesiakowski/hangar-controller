import logging
import os
import typing as tp

from hangar_controller.hangar_state import HangarState

MQTT_BROKER_ADDRESS = str(os.environ.get('MQTT_BROKER_ADDRESS', 'mqtt'))
MQTT_BROKER_PORT = 1883

PENTACOMP_API_MOCK_ADDRESS = str(os.environ.get('PENTACOMP_API_MOCK_ADDRESS',
                                                '0.0.0.0'))

AZURE_DEVICE_ID = str(os.environ.get('AZURE_DEVICE_ID', 'cr-hangar-1'))
AZURE_DEVICE_TYPE = 'hangar'
AZURE_SHARED_ACCESS_KEY = str(os.environ.get('AZURE_SHARED_ACCESS_KEY',
                                             'lpdlGunl33r2ubYcNlZIAX6b2QojLxY/s0wEnWhW+gg='))
AZURE_IOTHUB_URL = 'IoTHub-AV.azure-devices.net'
AZURE_CONNECTION_STRING = 'HostName={};DeviceId={};SharedAccessKey={}'.format(
    AZURE_IOTHUB_URL, AZURE_DEVICE_ID, AZURE_SHARED_ACCESS_KEY)
AZURE_STORAGE_ACCOUNT = str(os.environ.get('AZURE_STORAGE_ACCOUNT', None))
AZURE_STORAGE_KEY = str(os.environ.get('AZURE_STORAGE_KEY', None))
AZURE_STORAGE_KWARGS = {}
AZURE_LOGS_CONTAINER = 'logs'
AZURE_SEND_ALERTS_AND_LOGS = str(os.environ.get('AZURE_SEND_ALERTS_AND_LOGS',
                                                'FALSE')) == 'TRUE'

PENTACOMP_API_CARGO_USER_INFO = 'http://airveincrudapi.azurewebsites.net/api/CargoUserInfo'
PENTACOMP_API_CARGO_USER_ACTION = 'https://airveincrudapi.azurewebsites.net/api/CargoUserAction'

HANGAR_ID = AZURE_DEVICE_ID
HANGAR_POS_LAT = float(os.environ.get('HANGAR_POS_LAT', 50.116382))
HANGAR_POS_LON = float(os.environ.get('HANGAR_POS_LON', 22.013737))
HANGAR_POS_ALT = float(os.environ.get('HANGAR_POS_ALT', 230.0))
HANGAR_TYPE: tp.Optional[str] = None
HANGAR_IS_REAL = str(os.environ.get('HANGAR_IS_REAL', 'FALSE')) == 'TRUE'

_init_bcs_slots = os.environ.get('BCS_MOCK_INIT_BATTERIES_SLOTS')
BCS_MOCK_INIT_BATTERIES_SLOTS = _init_bcs_slots.split(',') if _init_bcs_slots else []
BCS_MOCK_SINGLE_HANGAR = os.environ.get('BCS_MOCK_SINGLE_HANGAR', 'FALSE') == 'TRUE'

INIT_DRONE_ID = os.environ.get('INIT_DRONE_ID', None)

if isinstance(INIT_DRONE_ID, str):
    HANGAR_INIT_STATE = HangarState.HANGAR_FULL
elif INIT_DRONE_ID is None:
    HANGAR_INIT_STATE = HangarState.HANGAR_EMPTY

LOGGING_LEVEL = logging.DEBUG

CARGO_BACKEND = "USER_PANEL"  # API or USER_PANEL
