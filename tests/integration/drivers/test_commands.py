from collections import Callable
from typing import Optional, List, Any, Type
from unittest.mock import MagicMock

from hangar_controller.devices.drivers.drivers_controller import \
    DriversController
from hangar_controller.devices.drivers.messages.responses import \
    CollectiveResponseEvaluation
from settingsd.drivers.stm_communication_settings import \
    ResponseMessage
from tests.mocks.drives_mock import DriversMock
from tests.testcase_wrapper import NoLoggingTestCase


class TestCommands(NoLoggingTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.diagnostics = MagicMock()
        cls.error_mock = MagicMock()
        cls.queue_mock = MagicMock()
        cls.modbus_io = MagicMock()

        # this value should be overridden for each test
        cls.stm_mock: Optional[Type[DriversMock]] = None

    def setUp(self) -> None:
        self.stm_mock = self.stm_mock()
        self.stm_mock.subscribe()
        self.drivers = DriversController(MagicMock(), MagicMock())
        self.drivers.start()

    def tearDown(self) -> None:
        DriversController.__it__ = None
        self.stm_mock.stop_drivers()
        self.drivers._mqtt_client.close_connection()
        self.drivers.stop()

    def normal_call(self, controller_method: Callable,
                    args: Optional[List[Any]] = None):
        # given
        try:
            if args:
                controller_method(*args)
            else:
                controller_method()
        except BaseException as exc:
            self.fail(exc)

    def timeout(self, controller_method: Callable,
                args: Optional[List[Any]] = None):
        # given
        self.stm_mock.sending_timeout_response = True

        # when
        if args:
            response = controller_method(*args)
        else:
            response = controller_method()

        if isinstance(response, tuple):
            response = response[0]

        # then
        self.assertFalse(response.evaluation)
        self.assertEqual(ResponseMessage.TIMEOUT,
                         response.evaluation_reason)

    def wrong_value(self, controller_method: Callable,
                    args: Optional[List[Any]] = None):
        # given
        self.stm_mock.sending_wrong_value_response = True

        # when
        if args:
            response = controller_method(*args)
        else:
            response = controller_method()

        if isinstance(response, tuple):
            response = response[0]

        # then
        self.assertFalse(response.evaluation)
        self.assertEqual(ResponseMessage.WRONG_VALUE,
                         response.evaluation_reason)

    def refused(self, controller_method: Callable,
                args: Optional[List[Any]] = None):
        # given
        self.stm_mock.sending_refused_response = True

        # when
        if args:
            response = controller_method(*args)
        else:
            response = controller_method()

        if isinstance(response, tuple):
            response = response[0]

        # then
        self.assertFalse(response.evaluation)
        self.assertEqual(ResponseMessage.REFUSED,
                         response.evaluation_reason)

    def unexpected_response(self, controller_method: Callable,
                            args: Optional[List[Any]] = None):
        # given
        self.stm_mock.sending_unexpected_responses = True

        # when
        if args:
            response: CollectiveResponseEvaluation = controller_method(*args)
        else:
            response: CollectiveResponseEvaluation = controller_method()

        if isinstance(response, tuple):
            response = response[0]

        # then
        self.assertFalse(response.evaluation)
        self.assertEqual(ResponseMessage.TIMEOUT,
                         response.evaluation_reason)
