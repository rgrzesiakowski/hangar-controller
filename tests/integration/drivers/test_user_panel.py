from settingsd.drivers.stm_user_panel_settings import UserPanelViews
from tests.integration.drivers.test_commands import TestCommands
from tests.mocks.stm_user_panel_mock import STMUserPanelMock


class TestSTMUserPanel(TestCommands):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.stm_mock = STMUserPanelMock

    def test_get_pin_normal_call(self):
        self.normal_call(self.drivers.user_panel.get_pin)

    def test_get_pin_timeout(self):
        self.timeout(self.drivers.user_panel.get_pin)

    def test_get_pin_refused(self):
        self.refused(self.drivers.user_panel.get_pin)

    def test_get_pin_wrong_value(self):
        self.wrong_value(self.drivers.user_panel.get_pin)

    def test_set_view_normal_call(self):
        self.normal_call(self.drivers.user_panel.set_view,
                         [UserPanelViews.CARGO_CORRECT])

    def test_set_view_timeout(self):
        self.timeout(self.drivers.user_panel.set_view,
                     [UserPanelViews.CARGO_CORRECT])

    def test_set_view_refused(self):
        self.refused(self.drivers.user_panel.set_view,
                     [UserPanelViews.CARGO_CORRECT])

    def test_set_view_wrong_value(self):
        self.wrong_value(self.drivers.user_panel.set_view,
                         [UserPanelViews.CARGO_CORRECT])

    def test_set_view_unexpected_value(self):
        self.unexpected_response(self.drivers.user_panel.set_view,
                                 [UserPanelViews.CARGO_CORRECT])
