from hangar_controller.devices.drivers.diagnostics.drivers_diagnostics import DriversDiagnostics
from tests.testcase_wrapper import NoLoggingTestCase


class TestDriversDiagnostics(NoLoggingTestCase):
    def setUp(self) -> None:
        self.drivers_diagnostics = DriversDiagnostics()

    def test___init__(self):
        self.assertEqual({}, self.drivers_diagnostics.values)

    def test___repr__(self):
        # when
        self.drivers_diagnostics.values = {'a': 1, 'b': 2}
        self.assertEqual('<DriversDiagnostics values={\'a\': 1, \'b\': 2}>',
                         self.drivers_diagnostics.__repr__())

    def test_update(self):
        # given
        self.assertEqual({}, self.drivers_diagnostics.values)
        
        # when
        self.drivers_diagnostics.update(a=1, b=2)
        
        # then
        self.assertEqual({'a': 1, 'b': 2}, self.drivers_diagnostics.values)
