from hangar_controller.devices.drivers.messages.commands.climate import \
    ClimateGetStates
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.climate.auto_manual_response import \
    AutoManualResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestClimateGetStates(NoLoggingTestCase):
    def setUp(self) -> None:
        self.climate_get_states = ClimateGetStates()

    def test___init__(self):
        expected_messages = [
            Acknowledgment(),
            AutoManualResponse(positive_messages=None),
            *[EnableDisableResponse(positive_messages=None), ] * 4
        ]
        responses = create_response_list(ClimateGetStates.RESPONSE_TOPICS,
                                         ClimateGetStates.TIMEOUTS,
                                         expected_messages)
        self.assertEqual(call_topic(ClimateGetStates.TOPIC),
                         self.climate_get_states.topic)
        self.assertEqual(responses, self.climate_get_states.responses)
