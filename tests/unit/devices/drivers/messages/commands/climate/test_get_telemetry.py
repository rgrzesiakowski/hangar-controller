from hangar_controller.devices.drivers.messages.commands.climate import \
    ClimateGetTelemetry
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestClimateGetTelemetry(NoLoggingTestCase):
    def setUp(self) -> None:
        self.climate_get_telemetry = ClimateGetTelemetry()

    def test___init__(self):
        expected_messages = [Acknowledgment(),
                             *[ValueResponse(), ] * (len(
                                 ClimateGetTelemetry.RESPONSE_TOPICS) - 1)]
        responses = create_response_list(ClimateGetTelemetry.RESPONSE_TOPICS,
                                         ClimateGetTelemetry.TIMEOUTS,
                                         expected_messages)
        self.assertEqual(call_topic(ClimateGetTelemetry.TOPIC),
                         self.climate_get_telemetry.topic)
        self.assertEqual(responses, self.climate_get_telemetry.responses)
