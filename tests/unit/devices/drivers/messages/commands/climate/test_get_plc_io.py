from hangar_controller.devices.drivers.messages.commands.climate import \
    ClimateGetPLCIO
from hangar_controller.devices.drivers.messages.message_utils import \
    create_response_list, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestClimateGetPLCIO(NoLoggingTestCase):
    def setUp(self) -> None:
        self.climate_get_plc_io = ClimateGetPLCIO()

    def test___init__(self):
        expected_messages = [Acknowledgment(),
                             *[ValueResponse(), ] * (len(
                                 ClimateGetPLCIO.RESPONSE_TOPICS) - 1)]
        responses = create_response_list(ClimateGetPLCIO.RESPONSE_TOPICS,
                                         ClimateGetPLCIO.TIMEOUTS,
                                         expected_messages)
        self.assertEqual(call_topic(ClimateGetPLCIO.TOPIC),
                         self.climate_get_plc_io.topic)
        self.assertEqual(responses, self.climate_get_plc_io.responses)
