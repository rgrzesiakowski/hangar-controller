from hangar_controller.devices.drivers.messages.commands.climate import \
    ClimateResetPLC
from hangar_controller.devices.drivers.messages.message_utils import call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from tests.testcase_wrapper import NoLoggingTestCase


class TestClimateResetPLC(NoLoggingTestCase):
    def setUp(self) -> None:
        self.reset_plc = ClimateResetPLC()

    def test___init__(self):
        responses = [create_ack_response(ClimateResetPLC.TOPIC)]

        self.assertEqual(call_topic(ClimateResetPLC.TOPIC),
                         self.reset_plc.topic)
        self.assertEqual(responses, self.reset_plc.responses)
