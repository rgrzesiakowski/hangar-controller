from hangar_controller.devices.drivers.messages.commands.climate \
    import SetRoofHeaterHysteresis
from hangar_controller.devices.drivers.messages.message_utils import \
    temperature_payload, set_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from tests.testcase_wrapper import NoLoggingTestCase


class TestSetRoofHeaterHysteresis(NoLoggingTestCase):
    def setUp(self) -> None:
        self.set_roof_heater_hysteresis = SetRoofHeaterHysteresis(123)

    def test___init__(self):
        # given
        payload = temperature_payload(123)
        expected_messages = [Acknowledgment()]
        responses = create_response_list(
            SetRoofHeaterHysteresis.RESPONSE_TOPICS,
            SetRoofHeaterHysteresis.TIMEOUTS,
            expected_messages)
        self.assertEqual(set_topic(SetRoofHeaterHysteresis.TOPIC),
                         self.set_roof_heater_hysteresis.topic)
        self.assertEqual(responses, self.set_roof_heater_hysteresis.responses)
        self.assertEqual(payload, self.set_roof_heater_hysteresis.payload)
