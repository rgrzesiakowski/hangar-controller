from hangar_controller.devices.drivers.messages.commands.climate import \
    ClimateGetParameters
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from settingsd.drivers.stm_communication_settings import CommandMessage
from tests.testcase_wrapper import NoLoggingTestCase


class TestClimateGetStates(NoLoggingTestCase):
    def setUp(self) -> None:
        self.climate_get_states: ClimateGetParameters = ClimateGetParameters()

    def test___init__(self):
        expected_messages = [Acknowledgment(),
                             *[ValueResponse(), ] * 6]
        responses = create_response_list(ClimateGetParameters.RESPONSE_TOPICS,
                                         ClimateGetParameters.TIMEOUTS,
                                         expected_messages)
        self.assertEqual(call_topic(ClimateGetParameters.TOPIC),
                         self.climate_get_states.topic)
        self.assertEqual(responses, self.climate_get_states.responses)
        self.assertEqual(CommandMessage.GET, self.climate_get_states.payload)
