from hangar_controller.devices.drivers.messages.commands.climate import SetFan
from hangar_controller.devices.drivers.messages.message_utils import \
    create_response_list, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestSetFan(NoLoggingTestCase):
    def setUp(self) -> None:
        self.set_fan = SetFan(SetFan.ENABLE)

    def test___init__(self):
        expected_messages = [Acknowledgment(),
                             EnableDisableResponse(SetFan.ENABLE)]
        responses = create_response_list(SetFan.RESPONSE_TOPICS,
                                         SetFan.TIMEOUTS,
                                         expected_messages)

        self.assertEqual(call_topic(SetFan.TOPIC), self.set_fan.topic)
        self.assertEqual(responses, self.set_fan.responses)
