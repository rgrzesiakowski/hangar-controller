from hangar_controller.devices.drivers.messages.commands.climate \
    import SetHangarHeaterTemperature
from hangar_controller.devices.drivers.messages.message_utils import \
    temperature_payload, set_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from tests.testcase_wrapper import NoLoggingTestCase


class TestSetHangarHeaterTemperature(NoLoggingTestCase):
    def setUp(self) -> None:
        self.set_hangar_heater_temperature = SetHangarHeaterTemperature(123)

    def test___init__(self):
        # given
        expected_messages = [Acknowledgment()]
        responses = create_response_list(
            SetHangarHeaterTemperature.RESPONSE_TOPICS,
            SetHangarHeaterTemperature.TIMEOUTS,
            expected_messages)
        payload = temperature_payload(123)
        self.assertEqual(set_topic(SetHangarHeaterTemperature.TOPIC),
                         self.set_hangar_heater_temperature.topic)
        self.assertEqual(responses,
                         self.set_hangar_heater_temperature.responses)
        self.assertEqual(payload, self.set_hangar_heater_temperature.payload)
