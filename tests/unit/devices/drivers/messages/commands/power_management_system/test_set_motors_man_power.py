from hangar_controller.devices.drivers.messages.commands.power_management_system import \
    PMSSetMotorsManPower
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSTimeouts, PMSStates
from tests.testcase_wrapper import NoLoggingTestCase


class TestSetMotorsManPower(NoLoggingTestCase):
    def setUp(self) -> None:
        self.set_motors_man_power = PMSSetMotorsManPower(
            PMSSetMotorsManPower.ENABLE)

    def test___init__(self):
        ack_response = create_ack_response(PMSSetMotorsManPower.TOPIC)
        state_response = Response(
            state_change_topic(Controllers.STM_PMS, PMSStates.MOTORS_MAN_POWER),
            PMSTimeouts.MOTORS_MAN_POWER,
            EnableDisableResponse(PMSSetMotorsManPower.ENABLE))

        responses = [ack_response, state_response]

        self.assertEqual(call_topic(PMSSetMotorsManPower.TOPIC),
                         self.set_motors_man_power.topic)
        self.assertEqual(responses, self.set_motors_man_power.responses)
