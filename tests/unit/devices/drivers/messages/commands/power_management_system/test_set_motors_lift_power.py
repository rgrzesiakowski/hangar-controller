from hangar_controller.devices.drivers.messages.commands.power_management_system import \
    PMSSetMotorsLiftPower
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSTimeouts, PMSStates
from tests.testcase_wrapper import NoLoggingTestCase


class TestSetMotorsLiftPower(NoLoggingTestCase):
    def setUp(self) -> None:
        self.set_motors_lift_power = PMSSetMotorsLiftPower(
            PMSSetMotorsLiftPower.ENABLE)

    def test___init__(self):

        ack_response = create_ack_response(PMSSetMotorsLiftPower.TOPIC)
        lift_response = Response(
            state_change_topic(Controllers.STM_PMS,
                               PMSStates.MOTORS_LIFT_POWER),
            PMSTimeouts.MOTORS_LIFT_POWER,
            EnableDisableResponse(PMSSetMotorsLiftPower.ENABLE))

        responses = [ack_response, lift_response]

        self.assertEqual(call_topic(PMSSetMotorsLiftPower.TOPIC),
                         self.set_motors_lift_power.topic)
        self.assertEqual(responses, self.set_motors_lift_power.responses)
