from hangar_controller.devices.drivers.messages.commands.power_management_system import \
    PMSSetMotorsPosPower
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSTimeouts, PMSStates
from tests.testcase_wrapper import NoLoggingTestCase


class TestSetMotorsPosPower(NoLoggingTestCase):
    def setUp(self) -> None:
        self.set_motors_pos_power = PMSSetMotorsPosPower(
            PMSSetMotorsPosPower.ENABLE)

    def test___init__(self):

        ack_response = create_ack_response(PMSSetMotorsPosPower.TOPIC)
        state_response = Response(
            state_change_topic(Controllers.STM_PMS,
                               PMSStates.MOTORS_POS_POWER),
            PMSTimeouts.MOTORS_POS_POWER,
            EnableDisableResponse(PMSSetMotorsPosPower.ENABLE))

        responses = [ack_response, state_response]

        self.assertEqual(call_topic(PMSSetMotorsPosPower.TOPIC),
                         self.set_motors_pos_power.topic)
        self.assertEqual(responses, self.set_motors_pos_power.responses)
