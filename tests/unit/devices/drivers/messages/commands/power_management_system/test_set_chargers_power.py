from hangar_controller.devices.drivers.messages.commands.power_management_system import \
    PMSSetChargersPower
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSStates, PMSTimeouts
from tests.testcase_wrapper import NoLoggingTestCase


class TestSetChargersPower(NoLoggingTestCase):
    def setUp(self) -> None:
        self.set_chargers_power = PMSSetChargersPower(
            PMSSetChargersPower.ENABLE)

    def test___init__(self):
        responses = [
            create_ack_response(PMSSetChargersPower.TOPIC),
            Response(state_change_topic(Controllers.STM_PMS,
                                        PMSStates.TRAFO),
                     PMSTimeouts.CHARGERS_POWER,
                     EnableDisableResponse(PMSSetChargersPower.ENABLE))]

        self.assertEqual(call_topic(PMSSetChargersPower.TOPIC),
                         self.set_chargers_power.topic)
        self.assertEqual(responses, self.set_chargers_power.responses)
