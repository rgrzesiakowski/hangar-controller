from hangar_controller.devices.drivers.messages.commands.power_management_system import \
    PMSGetStates
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from settingsd.drivers.stm_communication_settings import CommandMessage
from tests.testcase_wrapper import NoLoggingTestCase


class TestPmsGetStates(NoLoggingTestCase):
    def setUp(self) -> None:
        self.pms_get_states = PMSGetStates()

    def test___init__(self):
        expected_messages = [
            Acknowledgment(),
            *[EnableDisableResponse(None), ] * len(PMSGetStates.PMS_STATES_LIST)
        ]

        responses = create_response_list(PMSGetStates.RESPONSE_TOPICS,
                                         PMSGetStates.TIMEOUTS,
                                         expected_messages)
        self.assertEqual(call_topic(PMSGetStates.TOPIC),
                         self.pms_get_states.topic)
        self.assertEqual(responses, self.pms_get_states.responses)
        self.assertEqual(CommandMessage.GET, self.pms_get_states.payload)
