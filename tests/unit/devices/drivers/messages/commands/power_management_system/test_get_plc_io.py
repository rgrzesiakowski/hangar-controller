from hangar_controller.devices.drivers.messages.commands.power_management_system import \
    PMSGetPLCIO
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from settingsd.drivers.stm_communication_settings import CommandMessage
from tests.testcase_wrapper import NoLoggingTestCase


class TestPMSGetPLCIO(NoLoggingTestCase):
    def setUp(self) -> None:
        self.pms_get_plcio = PMSGetPLCIO()

    def test___init__(self):
        expected_messages = [Acknowledgment(),
                             *[ValueResponse(), ] * (
                                     len(PMSGetPLCIO.RESPONSE_TOPICS) - 1)]

        responses = create_response_list(PMSGetPLCIO.RESPONSE_TOPICS,
                                         PMSGetPLCIO.TIMEOUTS,
                                         expected_messages)
        self.assertEqual(call_topic(PMSGetPLCIO.TOPIC),
                         self.pms_get_plcio.topic)
        self.assertEqual(responses, self.pms_get_plcio.responses)
        self.assertEqual(CommandMessage.GET, self.pms_get_plcio.payload)
