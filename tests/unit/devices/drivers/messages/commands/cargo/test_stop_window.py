from hangar_controller.devices.drivers.messages.commands.cargo import \
    CargoStopWindow
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, response_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.window_response import WindowResponse
from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_cargo_settings import CargoTimeouts
from settingsd.drivers.stm_communication_settings import ACK_TIMEOUT
from tests.testcase_wrapper import NoLoggingTestCase


class TestStopWindow(NoLoggingTestCase):
    def setUp(self) -> None:
        self.stop_window = CargoStopWindow()

    def test___init__(self):
        ack_response = Response(response_topic(CargoStopWindow.TOPIC), ACK_TIMEOUT,
                                Acknowledgment())
        window_response = Response(CargoStopWindow.STATE_CHANGE_TOPIC,
                                   CargoTimeouts.WINDOW_STOP,
                                   WindowResponse(WindowResponse.STOP))
        responses = [ack_response, window_response]
        self.assertEqual(call_topic(CargoStopWindow.TOPIC), self.stop_window.topic)
        self.assertEqual(responses, self.stop_window.responses)
