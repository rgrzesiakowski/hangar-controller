from hangar_controller.devices.drivers.messages.commands.cargo import \
    CargoResetControllerWithPin
from hangar_controller.devices.drivers.messages.message_utils import call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from tests.testcase_wrapper import NoLoggingTestCase


class TestCargoResetControllerGetPIN(NoLoggingTestCase):
    def setUp(self) -> None:
        self.reset_ctrl = CargoResetControllerWithPin('1234')

    def test___init__(self):
        responses = [create_ack_response(CargoResetControllerWithPin.TOPIC)]
        self.assertEqual(call_topic(CargoResetControllerWithPin.TOPIC),
                         self.reset_ctrl.topic)
        self.assertEqual(responses, self.reset_ctrl.responses)
