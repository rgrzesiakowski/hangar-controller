from hangar_controller.devices.drivers.messages.commands.cargo import \
    CargoTare
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_cargo_settings import CargoControlCommands
from settingsd.drivers.stm_communication_settings import ACK_TIMEOUT, \
    Controllers
from tests.testcase_wrapper import NoLoggingTestCase


class TestCargoTare(NoLoggingTestCase):
    def setUp(self) -> None:
        self.cargo_tare = CargoTare()

    def test__init__(self):
        ack_response = create_ack_response(CargoTare.TOPIC)
        weight_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoControlCommands.CARGO_WEIGHT),
            ACK_TIMEOUT, ValueResponse())

        responses = [ack_response, weight_response]

        self.assertEqual(call_topic(CargoTare.TOPIC),
                         self.cargo_tare.topic)
        self.assertEqual(responses, self.cargo_tare.responses)
