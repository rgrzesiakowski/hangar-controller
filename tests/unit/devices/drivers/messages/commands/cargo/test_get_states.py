from hangar_controller.devices.drivers.messages.commands.cargo import \
    CargoGetStates
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.cargo_shift_state_response import \
    CargoShiftStateResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.window_response import WindowResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestCargoGetStates(NoLoggingTestCase):
    def setUp(self) -> None:
        self.cargo_get_states = CargoGetStates()

    def test___init__(self):
        expected_messages = [
            Acknowledgment(),
            WindowResponse(None),
            CargoShiftStateResponse(None),
            *[EnableDisableResponse(None), ] * 12,
        ]

        responses = create_response_list(CargoGetStates.RESPONSE_TOPICS,
                                         CargoGetStates.TIMEOUTS,
                                         expected_messages)
        self.assertEqual(call_topic(CargoGetStates.TOPIC),
                         self.cargo_get_states.topic)
        self.assertEqual(responses, self.cargo_get_states.responses)
