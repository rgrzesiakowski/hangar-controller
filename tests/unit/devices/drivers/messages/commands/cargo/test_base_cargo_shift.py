from hangar_controller.devices.drivers.messages.commands.cargo import \
    CargoBaseShiftCheckConditions
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.shift_response import ShiftResponse
from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_cargo_settings import CargoTimeouts, \
    CargoStates
from settingsd.drivers.stm_communication_settings import Controllers
from tests.testcase_wrapper import NoLoggingTestCase


class TestBaseCargoShift(NoLoggingTestCase):
    def setUp(self) -> None:
        self.command = CargoBaseShiftCheckConditions()

    def test___init__(self):
        ack_response = create_ack_response(CargoBaseShiftCheckConditions.TOPIC)
        shift_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_BASE_CHECK_COND,
            ShiftResponse(ShiftResponse.BASE))
        idle_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_BASE_CHECK_COND,
            ShiftResponse(ShiftResponse.IDLE))
        responses = [ack_response, idle_response, shift_response]
        self.assertEqual(call_topic(CargoBaseShiftCheckConditions.TOPIC),
                         self.command.topic)
        self.assertEqual(responses, self.command.responses)
