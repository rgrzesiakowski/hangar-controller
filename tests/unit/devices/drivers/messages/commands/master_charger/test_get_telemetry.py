from hangar_controller.devices.drivers.messages.commands.master_charger import \
    MasterChargerGetTelemetry
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, create_response_list, battery_measurement, sub_controller_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.list_response import ListResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.master_charger.battery_uid_response import \
    BatteryUIDResponse
from settingsd.drivers.stm_communication_settings import CommandMessage, \
    SubControllers, MULTIPLE_RESPONSE_TIMEOUT
from settingsd.drivers.stm_master_charger_settings import \
    Measurements
from tests.testcase_wrapper import NoLoggingTestCase


class TestMasterChargerGetTelemetry(NoLoggingTestCase):
    def setUp(self) -> None:
        self.master_charger_get_telemetry = MasterChargerGetTelemetry('charger_id')

    def test___init__(self):
        topic = sub_controller_topic(MasterChargerGetTelemetry.COMMAND,
                                     'charger_id')
        topics = [
            battery_measurement('charger_id', SubControllers.BATTERY,
                                Measurements.UID),
            battery_measurement('charger_id', SubControllers.BATTERY,
                                Measurements.TEMPERATURE),
            battery_measurement('charger_id', SubControllers.BATTERY,
                                Measurements.VOLTAGE),
            battery_measurement('charger_id', SubControllers.CHARGER,
                                Measurements.BMS_TEMPERATURE),
            battery_measurement('charger_id', SubControllers.CHARGER,
                                Measurements.CURRENT),
        ]
        timeouts = [MULTIPLE_RESPONSE_TIMEOUT, ] * len(topics)
        expected_messages = [BatteryUIDResponse(),
                             *[ListResponse(), ] * 4]
        responses = create_response_list(topics, timeouts, expected_messages)
        ack_response = create_ack_response(topic)
        responses.append(ack_response)

        self.assertEqual(call_topic(topic),
                         self.master_charger_get_telemetry.topic)
        self.assertEqual(responses, self.master_charger_get_telemetry.responses)
        self.assertEqual(CommandMessage.GET,
                         self.master_charger_get_telemetry.payload)
