from hangar_controller.devices.drivers.messages.commands.user_panel import \
    UserPanelSetView
from hangar_controller.devices.drivers.messages.message_utils import call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import \
    create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.view_response import \
    ViewResponse
from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_user_panel_settings import UserPanelViews, \
    UserPanelTimeouts
from tests.testcase_wrapper import NoLoggingTestCase


class TestUserPanelSetView(NoLoggingTestCase):
    def setUp(self) -> None:
        self.set_view = UserPanelSetView(UserPanelViews.THANK_YOU)

    def test___init__(self):
        ack_response = create_ack_response(UserPanelSetView.TOPIC)
        state_change_response = Response(UserPanelSetView.STATE_CHANGE,
                                         UserPanelTimeouts.VIEW,
                                         ViewResponse(
                                             UserPanelViews.THANK_YOU))
        responses = [ack_response, state_change_response]
        self.assertEqual(call_topic(UserPanelSetView.TOPIC),
                         self.set_view.topic)
        self.assertEqual(responses, self.set_view.responses)
