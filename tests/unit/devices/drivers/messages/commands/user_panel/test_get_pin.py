from hangar_controller.devices.drivers.messages.commands.user_panel import \
    UserPanelGetPin
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import \
    create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.pin_response import \
    PINResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.view_response import \
    ViewResponse
from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_user_panel_settings import UserPanelTimeouts, \
    ConfirmationViews
from tests.testcase_wrapper import NoLoggingTestCase


class TestUserPanelGetPin(NoLoggingTestCase):
    def setUp(self) -> None:
        self.get_pin = UserPanelGetPin()

    def test___init__(self):
        ack_response = create_ack_response(UserPanelGetPin.TOPIC)
        view_change = Response(UserPanelGetPin.VIEW_STATE,
                               UserPanelTimeouts.VIEW,
                               ViewResponse(ConfirmationViews.CARGO_PIN))
        pin_response = Response(UserPanelGetPin.PIN_STATE,
                                UserPanelTimeouts.CARGO_PIN,
                                PINResponse())
        responses = [ack_response, view_change, pin_response]
        self.assertEqual(call_topic(UserPanelGetPin.TOPIC), self.get_pin.topic)
        self.assertEqual(responses, self.get_pin.responses)
