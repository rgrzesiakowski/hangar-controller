from hangar_controller.devices.drivers.messages.commands.roof import RoofGetStates
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.roof.roof_state_response import RoofStateResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestRoofGetStates(NoLoggingTestCase):
    def setUp(self) -> None:
        self.roof_get_states = RoofGetStates()

    def test___init__(self):
        expected_messages = [
            Acknowledgment(),
            RoofStateResponse(positive_messages=None),
            *[EnableDisableResponse(positive_messages=None), ] * 9
        ]
        responses = create_response_list(RoofGetStates.RESPONSE_TOPICS,
                                         RoofGetStates.TIMEOUTS,
                                         expected_messages)
        self.assertEqual(call_topic(RoofGetStates.TOPIC),
                         self.roof_get_states.topic)
        self.assertEqual(responses, self.roof_get_states.responses)
