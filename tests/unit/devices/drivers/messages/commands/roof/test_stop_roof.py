from hangar_controller.devices.drivers.messages.commands.roof import RoofStop
from hangar_controller.devices.drivers.messages.message_utils import call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.roof.roof_state_response import RoofStateResponse
from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_roof_settings import RoofControlTimeouts
from tests.testcase_wrapper import NoLoggingTestCase


class TestStopRoof(NoLoggingTestCase):
    def setUp(self) -> None:
        self.command = RoofStop()

    def test___init__(self):
        self.assertEqual(call_topic(RoofStop.TOPIC), self.command.topic)

        responses = [
            create_ack_response(RoofStop.TOPIC),
            Response(RoofStop.STATE_CHANGE,
                     RoofControlTimeouts.ROOF_STOP,
                     RoofStateResponse(RoofStateResponse.IDLE))
        ]
        self.assertEqual(responses, self.command.responses)
