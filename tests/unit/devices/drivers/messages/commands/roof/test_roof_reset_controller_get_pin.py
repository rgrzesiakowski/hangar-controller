from hangar_controller.devices.drivers.messages.commands.roof import \
    RoofResetControllerGetPin
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, pin_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_communication_settings import PIN_TIMEOUT
from tests.testcase_wrapper import NoLoggingTestCase


class TestRoofResetControllerGetPIN(NoLoggingTestCase):
    def setUp(self) -> None:
        self.reset_ctrl = RoofResetControllerGetPin()

    def test___init__(self):
        responses = [
            Response(pin_topic(RoofResetControllerGetPin.TOPIC), PIN_TIMEOUT,
                     ValueResponse())]
        self.assertEqual(call_topic(RoofResetControllerGetPin.TOPIC),
                         self.reset_ctrl.topic)
        self.assertEqual(responses, self.reset_ctrl.responses)
