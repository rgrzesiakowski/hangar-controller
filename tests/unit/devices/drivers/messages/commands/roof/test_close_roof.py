from hangar_controller.devices.drivers.messages.commands.roof import RoofClose
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, response_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.roof.roof_state_response import RoofStateResponse
from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_roof_settings import RoofControlTimeouts
from tests.testcase_wrapper import NoLoggingTestCase


class TestCloseRoof(NoLoggingTestCase):
    def setUp(self) -> None:
        self.command = RoofClose()

    def test___init__(self):
        ack_response = Response(response_topic(RoofClose.TOPIC),
                                RoofControlTimeouts.ROOF_ACK, Acknowledgment())
        roof_closing = Response(RoofClose.STATE_CHANGE,
                                RoofControlTimeouts.ROOF_CLOSE,
                                RoofStateResponse(RoofStateResponse.IDLE))
        roof_state = Response(RoofClose.STATE_CHANGE,
                              RoofControlTimeouts.ROOF_CLOSE,
                              RoofStateResponse(RoofStateResponse.CLOSE))
        responses = [ack_response, roof_closing, roof_state]
        self.assertEqual(call_topic(RoofClose.TOPIC), self.command.topic)
        self.assertEqual(responses, self.command.responses)
