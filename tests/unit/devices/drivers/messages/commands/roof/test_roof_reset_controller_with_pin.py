from hangar_controller.devices.drivers.messages.commands.roof import \
    RoofResetControllerWithPin
from hangar_controller.devices.drivers.messages.message_utils import call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from tests.testcase_wrapper import NoLoggingTestCase


class TestRoofResetControllerGetPIN(NoLoggingTestCase):
    def setUp(self) -> None:
        self.reset_ctrl = RoofResetControllerWithPin('1234')

    def test___init__(self):
        responses = [create_ack_response(RoofResetControllerWithPin.TOPIC)]
        self.assertEqual(call_topic(RoofResetControllerWithPin.TOPIC),
                         self.reset_ctrl.topic)
        self.assertEqual(responses, self.reset_ctrl.responses)
