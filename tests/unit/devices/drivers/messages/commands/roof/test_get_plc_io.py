from hangar_controller.devices.drivers.messages.commands.roof import RoofGetPLCIO
from hangar_controller.devices.drivers.messages.message_utils import \
    call_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestClimateGetPLCIO(NoLoggingTestCase):
    def setUp(self) -> None:
        self.roof_get_plc_io = RoofGetPLCIO()

    def test___init__(self):
        expected_messages = [Acknowledgment(),
                             *[ValueResponse(), ] * (
                                     len(RoofGetPLCIO.RESPONSE_TOPICS) - 1)]
        responses = create_response_list(RoofGetPLCIO.RESPONSE_TOPICS,
                                         RoofGetPLCIO.TIMEOUTS,
                                         expected_messages)
        self.assertEqual(call_topic(RoofGetPLCIO.TOPIC),
                         self.roof_get_plc_io.topic)
        self.assertEqual(responses, self.roof_get_plc_io.responses)
