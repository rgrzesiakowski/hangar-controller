from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.abstract_message_manager import AbstractMessageManager
from tests.testcase_wrapper import NoLoggingTestCase


class TestAbstractMessageManager(NoLoggingTestCase):
    def setUp(self) -> None:
        self.manager = AbstractMessageManager()

    def test___init__(self):
        self.assertEqual([], self.manager.registered_messages)

    def test_registered_topics(self):
        # given
        message = MagicMock()
        message.topic = 'topic'
        self.manager.registered_messages = [message]
        
        # when
        topics = self.manager.registered_topics
        
        # then
        self.assertEqual(['topic'], topics)

    def test_is_topic_registered(self):
        # given
        self.test_registered_topics()
        
        # when
        topic_registered = self.manager.is_topic_registered('topic')

        # then
        self.assertTrue(topic_registered)

    def test_is_topic_registered_not_registered(self):
        # given
        self.test_registered_topics()

        # when
        topic_registered = self.manager.is_topic_registered('different_topic')

        # then
        self.assertFalse(topic_registered)

    def test__register(self):
        # given
        self.assertEqual([], self.manager.registered_messages)
        message = MagicMock()
        message.topic = 'topic'

        # when
        self.manager._register(message)

        # then
        self.assertEqual([message], self.manager.registered_messages)

    def test_register_messages(self):
        # given
        message1, message2 = MagicMock(), MagicMock()
        messages = [message1, message2]
        
        # when
        self.manager.register_messages(messages)
        
        # then
        self.assertEqual(messages, self.manager.registered_messages)

    def test_unregister(self):
        # given
        message1, message2 = MagicMock(), MagicMock()
        messages = [message1, message2]
        self.manager.register_messages(messages)

        # when
        self.manager.unregister_message(message1)

        # then
        self.assertEqual([message2], self.manager.registered_messages)

    def test_check_is_topic_registered(self):
        # given
        self.manager.is_topic_registered = MagicMock(return_value=True)
        
        # when
        self.manager.check_is_topic_registered('topic')
        
        # then
        self.manager.is_topic_registered.assert_called_with('topic')

    def test_check_is_topic_registered_not_registered(self):
        # given
        self.manager.is_topic_registered = MagicMock(return_value=False)

        # when
        self.assertRaises(ValueError,
                          self.manager.check_is_topic_registered, 'topic')

        # then
        self.manager.is_topic_registered.assert_called_with('topic')

    def test_get_messages_by_topic(self):
        # given
        self.manager.check_is_topic_registered = MagicMock(return_value=True)
        message1, message2 = MagicMock(), MagicMock()
        message2.topic = 'topic2'
        messages = [message1, message2]
        self.manager.register_messages(messages)

        # when
        message = self.manager.get_messages_by_topic('topic2')
        
        # then
        self.assertEqual([message2], message)

    def test_get_message_by_topic(self):
        # given
        self.manager.get_messages_by_topic = MagicMock(
            return_value=['m1', 'm2'])
        
        # when
        message = self.manager.get_message_by_topic('topic')

        # then
        self.manager.get_messages_by_topic.assert_called_with('topic')
        self.assertEqual('m1', message)

    def test_receive(self):
        self.assertRaises(NotImplementedError,
                          self.manager.receive, MagicMock())
