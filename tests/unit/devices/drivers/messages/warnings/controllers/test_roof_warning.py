from hangar_controller.devices.drivers.messages.message_utils import \
    warning_topic
from hangar_controller.devices.drivers.messages.warnings.controllers.roof_warning import RoofWarningMessage
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers
from tests.testcase_wrapper import NoLoggingTestCase


class TestRoofWarningMessage(NoLoggingTestCase):
    def setUp(self) -> None:
        self.roof_warning_message = RoofWarningMessage()

    def test___init__(self):
        self.assertEqual(warning_topic(Controllers.STM_ROOF),
                         self.roof_warning_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerWarningCodes.STM_ROOF,
            self.roof_warning_message.warning_code)
