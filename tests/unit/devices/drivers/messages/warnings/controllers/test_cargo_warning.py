from hangar_controller.devices.drivers.messages.message_utils import \
    warning_topic
from hangar_controller.devices.drivers.messages.warnings.controllers.cargo_warning import CargoWarningMessage
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers
from tests.testcase_wrapper import NoLoggingTestCase


class TestCargoWarningMessage(NoLoggingTestCase):
    def setUp(self) -> None:
        self.cargo_warning_message = CargoWarningMessage()

    def test___init__(self):
        self.assertEqual(warning_topic(Controllers.STM_CARGO),
                         self.cargo_warning_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerWarningCodes.STM_CARGO,
            self.cargo_warning_message.warning_code)
