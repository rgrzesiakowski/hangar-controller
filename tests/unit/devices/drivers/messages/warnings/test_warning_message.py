from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.warnings.warning_message import WarningMessage
from tests.testcase_wrapper import NoLoggingTestCase


class TestWarningMessage(NoLoggingTestCase):
    def setUp(self) -> None:
        self.warning_message = WarningMessage('topic', MagicMock())

    def test___init__(self):
        self.assertEqual('topic', self.warning_message.topic)
