from hangar_controller.devices.drivers.messages.measurements.climate.roof_right_temperature \
    import RoofRightTemperature
from tests.testcase_wrapper import NoLoggingTestCase


class TestRoofRightTemperature(NoLoggingTestCase):
    def setUp(self) -> None:
        self.roof_right_temperature = RoofRightTemperature()

    def test___init__(self):
        self.assertEqual(RoofRightTemperature.TOPIC,
                         self.roof_right_temperature.topic)
        self.assertEqual(float, self.roof_right_temperature.type_)
