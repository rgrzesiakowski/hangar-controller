from hangar_controller.devices.drivers.messages.measurements.climate.hangar_upper_temperature import HangarUpperTemperature
from tests.testcase_wrapper import NoLoggingTestCase


class TestHangarUpperTemperature(NoLoggingTestCase):
    def setUp(self) -> None:
        self.hangar_upper_temperature = HangarUpperTemperature()

    def test___init__(self):
        self.assertEqual(HangarUpperTemperature.TOPIC,
                         self.hangar_upper_temperature.topic)
        self.assertEqual(float, self.hangar_upper_temperature.type_)
