from hangar_controller.devices.drivers.messages.measurements.climate.hangar_average_humidity import HangarAverageHumidity
from tests.testcase_wrapper import NoLoggingTestCase


class TestHangarAverageHumidity(NoLoggingTestCase):
    def setUp(self) -> None:
        self.hangar_average_humidity = HangarAverageHumidity()

    def test___init__(self):
        self.assertEqual(HangarAverageHumidity.TOPIC,
                         self.hangar_average_humidity.topic)
        self.assertEqual(float, self.hangar_average_humidity.type_)
