from hangar_controller.devices.drivers.messages.measurements.climate.hangar_average_temperature import \
    HangarAverageTemperature
from tests.testcase_wrapper import NoLoggingTestCase


class TestHangarAverageTemperature(NoLoggingTestCase):
    def setUp(self) -> None:
        self.hangar_average_temperature = HangarAverageTemperature()

    def test___init__(self):
        self.assertEqual(HangarAverageTemperature.TOPIC,
                         self.hangar_average_temperature.topic)
        self.assertEqual(float, self.hangar_average_temperature.type_)
