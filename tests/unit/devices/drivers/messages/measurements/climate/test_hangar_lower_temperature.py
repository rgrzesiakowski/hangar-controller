from hangar_controller.devices.drivers.messages.measurements.climate.hangar_lower_temperature import HangarLowerTemperature
from tests.testcase_wrapper import NoLoggingTestCase


class TestHangarLowerTemperature(NoLoggingTestCase):
    def setUp(self) -> None:
        self.hangar_lower_temperature = HangarLowerTemperature()

    def test___init__(self):
        self.assertEqual(HangarLowerTemperature.TOPIC,
                         self.hangar_lower_temperature.topic)
        self.assertEqual(float, self.hangar_lower_temperature.type_)
