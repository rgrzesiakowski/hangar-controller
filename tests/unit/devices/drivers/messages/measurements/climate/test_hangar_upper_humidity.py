from hangar_controller.devices.drivers.messages.measurements.climate.hangar_upper_humidity import HangarUpperHumidity
from tests.testcase_wrapper import NoLoggingTestCase


class TestHangarUpperHumidity(NoLoggingTestCase):
    def setUp(self) -> None:
        self.hangar_upper_humidity = HangarUpperHumidity()

    def test___init__(self):
        self.assertEqual(HangarUpperHumidity.TOPIC,
                         self.hangar_upper_humidity.topic)
        self.assertEqual(float, self.hangar_upper_humidity.type_)
