from hangar_controller.devices.drivers.messages.measurements.master_charger.charger_temperature import ChargerTemperature
from hangar_controller.devices.drivers.messages.message_utils import \
    battery_measurement
from settingsd.drivers.stm_communication_settings import SubControllers
from settingsd.drivers.stm_master_charger_settings import Measurements
from tests.testcase_wrapper import NoLoggingTestCase


class TestChargerTemperature(NoLoggingTestCase):
    def setUp(self) -> None:
        self.charger_temperature = ChargerTemperature('battery_id')
        self.topic = battery_measurement('battery_id', SubControllers.CHARGER,
                                         Measurements.TEMPERATURE)

    def test___init__(self):
        self.assertEqual(self.topic, self.charger_temperature.topic)
