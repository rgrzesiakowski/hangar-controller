from hangar_controller.devices.drivers.messages.measurements.master_charger.charger_current import ChargerCurrent
from hangar_controller.devices.drivers.messages.message_utils import \
    battery_measurement
from settingsd.drivers.stm_communication_settings import SubControllers
from settingsd.drivers.stm_master_charger_settings import Measurements
from tests.testcase_wrapper import NoLoggingTestCase


class TestChargerCurrent(NoLoggingTestCase):
    def setUp(self) -> None:
        self.charger_current = ChargerCurrent('battery_id')
        self.topic = battery_measurement('battery_id', SubControllers.CHARGER,
                                         Measurements.CURRENT)

    def test___init__(self):
        self.assertEqual(self.topic, self.charger_current.topic)
