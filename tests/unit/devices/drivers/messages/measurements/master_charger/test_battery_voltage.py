from hangar_controller.devices.drivers.messages.measurements.master_charger.battery_voltage import \
    BatteryVoltage
from hangar_controller.devices.drivers.messages.message_utils import \
    battery_measurement
from settingsd.drivers.stm_communication_settings import SubControllers
from settingsd.drivers.stm_master_charger_settings import Measurements
from tests.testcase_wrapper import NoLoggingTestCase


class TestBatteryVoltage(NoLoggingTestCase):
    def setUp(self) -> None:
        self.battery_voltage = BatteryVoltage('battery_id')
        self.topic = battery_measurement('battery_id', SubControllers.BATTERY,
                                         Measurements.VOLTAGE)

    def test___init__(self):
        self.assertEqual(self.topic, self.battery_voltage.topic)
