from hangar_controller.devices.drivers.messages.measurements.master_charger.battery_temperature import BatteryTemperature
from hangar_controller.devices.drivers.messages.message_utils import \
    battery_measurement
from settingsd.drivers.stm_communication_settings import SubControllers
from settingsd.drivers.stm_master_charger_settings import Measurements
from tests.testcase_wrapper import NoLoggingTestCase


class TestBatteryTemperature(NoLoggingTestCase):
    def setUp(self) -> None:
        self.battery_temperature = BatteryTemperature('battery_id')
        self.topic = battery_measurement('battery_id', SubControllers.BATTERY,
                                         Measurements.TEMPERATURE)

    def test___init__(self):
        self.assertEqual(self.topic, self.battery_temperature.topic)
