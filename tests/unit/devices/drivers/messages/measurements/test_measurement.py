from hangar_controller.devices.drivers.messages.measurements.measurement import Measurement
from tests.testcase_wrapper import NoLoggingTestCase


class TestMeasurement(NoLoggingTestCase):
    def setUp(self) -> None:
        self.topic = 'topic'
        self.measurement = Measurement(self.topic)

    def test___init__(self):
        self.assertEqual(self.topic, self.measurement.topic)
        self.assertIs(self.measurement.type_, float)
