from hangar_controller.devices.drivers.messages.errors.acknowledgment.unexpected_response_error import UnexpectedResponseError
from tests.testcase_wrapper import NoLoggingTestCase


class TestUnexpectedResponseError(NoLoggingTestCase):
    def setUp(self) -> None:
        self.unexpected_response_error = UnexpectedResponseError()

    def test___init__(self):
        self.assertIsInstance(self.unexpected_response_error, Exception)
