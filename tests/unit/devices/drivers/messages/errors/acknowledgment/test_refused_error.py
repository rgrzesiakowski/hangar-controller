from hangar_controller.devices.drivers.messages.errors.acknowledgment.refused_error import RefusedError
from tests.testcase_wrapper import NoLoggingTestCase


class TestRefusedError(NoLoggingTestCase):
    def setUp(self) -> None:
        self.refused_error = RefusedError('description')

    def test___init__(self):
        self.assertIsInstance(self.refused_error, Exception)
