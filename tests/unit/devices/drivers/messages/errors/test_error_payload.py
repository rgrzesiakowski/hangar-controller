from unittest.mock import MagicMock, Mock

from hangar_controller.devices.drivers.messages.errors.error_message import \
    ErrorPayload
from tests.testcase_wrapper import NoLoggingTestCase


class TestErrorPayload(NoLoggingTestCase):
    def setUp(self):
        self.mock_error_code = Mock(peripheral='TSTPE',
                                    command_control='TSTCC',
                                    telemetry='TSTTE')

    def test_init(self):
        ep = ErrorPayload()
        self.assertFalse(ep.is_initialized)
        self.assertFalse(ep.is_peripheral)
        self.assertFalse(ep.is_command_control)
        self.assertFalse(ep.is_telemetry)
        self.assertIsNone(ep.code)
        self.assertIsNone(ep.subcode)
        self.assertIsNone(ep.bitflags)

    def test_parse_peripheral_code(self):
        ep = ErrorPayload()
        payload = ep._parse_code('TSTPE[FILLER]', self.mock_error_code)
        self.assertEqual(payload, '[FILLER]')
        self.assertTrue(ep.is_peripheral)
        self.assertFalse(ep.is_command_control)
        self.assertFalse(ep.is_telemetry)
        self.assertEqual(ep.code, 'TSTPE')

    def test_parse_command_control_code(self):
        ep = ErrorPayload()
        payload = ep._parse_code('TSTCC[FILLER]', self.mock_error_code)
        self.assertEqual(payload, '[FILLER]')
        self.assertTrue(ep.is_command_control)
        self.assertFalse(ep.is_peripheral)
        self.assertFalse(ep.is_telemetry)
        self.assertEqual(ep.code, 'TSTCC')

    def test_parse_telemetry_code(self):
        ep = ErrorPayload()
        payload = ep._parse_code('TSTTE[FILLER]', self.mock_error_code)
        self.assertEqual(payload, '[FILLER]')
        self.assertTrue(ep.is_telemetry)
        self.assertFalse(ep.is_peripheral)
        self.assertFalse(ep.is_command_control)
        self.assertEqual(ep.code, 'TSTTE')

    def test_parse_wrong_code(self):
        ep = ErrorPayload()
        self.assertRaises(ValueError,
                          ep._parse_code,
                          'ERROR[FILLER]', self.mock_error_code)

    def test_parse_code_when_error_codes_set_to_none(self):
        ep = ErrorPayload()
        self.assertRaises(ValueError,
                          ep._parse_code,
                          'ERROR[FILLER]', Mock(peripheral=None,
                                                command_control=None,
                                                telemetry=None))

    def test_parse_fields_only_subcode_present(self):
        ep = ErrorPayload()
        ep._parse_fields('42')
        self.assertEqual(ep.subcode, 42)
        self.assertEqual(ep.bitflags, 0)

    def test_parse_fields(self):
        ep = ErrorPayload()
        ep._parse_fields('42,3')
        self.assertEqual(ep.subcode, 42)
        self.assertEqual(ep.bitflags, 3)

    def test_parse_fields_non_parsable_payload(self):
        ep = ErrorPayload()
        self.assertRaises(ValueError, ep._parse_fields, 'abc')

    def test_parse_fields_too_many_values_in_payload(self):
        ep = ErrorPayload()
        self.assertRaises(ValueError, ep._parse_fields, '1,2,3')

    def test_parse_payload(self):
        ep = ErrorPayload()
        ep.parse('TSTCC1,2', self.mock_error_code)
        self.assertTrue(ep.is_initialized)
        self.assertTrue(ep.is_command_control)
        self.assertFalse(ep.is_peripheral)
        self.assertFalse(ep.is_telemetry)
        self.assertEqual(ep.code, 'TSTCC')
        self.assertEqual(ep.subcode, 1)
        self.assertEqual(ep.bitflags, 2)

    def test_parse_wrong_payload(self):
        ep = ErrorPayload()
        self.assertRaises(ValueError,
                          ep.parse,
                          'ERROR', self.mock_error_code)
        self.assertFalse(ep.is_initialized)
        self.assertFalse(ep.is_command_control)
        self.assertFalse(ep.is_peripheral)
        self.assertFalse(ep.is_telemetry)
        self.assertIsNone(ep.code)
        self.assertIsNone(ep.subcode)
        self.assertIsNone(ep.bitflags)
