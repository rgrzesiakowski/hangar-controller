from hangar_controller.devices.drivers.messages.errors.controllers.user_panel_error import UserPanelErrorMessage
from hangar_controller.devices.drivers.messages.message_utils import error_topic
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers
from tests.testcase_wrapper import NoLoggingTestCase


class TestUserPanelErrorMessage(NoLoggingTestCase):
    def setUp(self) -> None:
        self.user_panel_error_message = UserPanelErrorMessage()

    def test___init__(self):
        self.assertEqual(error_topic(Controllers.STM_USER_PANEL),
                         self.user_panel_error_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerErrorCodes.STM_USER_PANEL,
            self.user_panel_error_message.error_code)
