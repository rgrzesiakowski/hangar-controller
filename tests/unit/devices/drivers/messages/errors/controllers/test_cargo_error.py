from hangar_controller.devices.drivers.messages.errors.controllers.cargo_error import CargoErrorMessage
from hangar_controller.devices.drivers.messages.message_utils import error_topic
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers
from tests.testcase_wrapper import NoLoggingTestCase


class TestCargoErrorMessage(NoLoggingTestCase):
    def setUp(self) -> None:
        self.cargo_error_message = CargoErrorMessage()

    def test___init__(self):
        self.assertEqual(error_topic(Controllers.STM_CARGO),
                         self.cargo_error_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerErrorCodes.STM_CARGO,
            self.cargo_error_message.error_code)
