from hangar_controller.devices.drivers.messages.errors.controllers.pms_error import PMSErrorMessage
from hangar_controller.devices.drivers.messages.message_utils import error_topic
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers
from tests.testcase_wrapper import NoLoggingTestCase


class TestPMSErrorMessage(NoLoggingTestCase):
    def setUp(self) -> None:
        self.pms_error_message = PMSErrorMessage()

    def test___init__(self):
        self.assertEqual(error_topic(Controllers.STM_PMS),
                         self.pms_error_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerErrorCodes.STM_PMS,
            self.pms_error_message.error_code)
