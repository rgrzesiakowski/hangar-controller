from hangar_controller.devices.drivers.messages.errors.controllers.climate_error import ClimateErrorMessage
from hangar_controller.devices.drivers.messages.message_utils import error_topic
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers
from tests.testcase_wrapper import NoLoggingTestCase


class TestClimateErrorMessage(NoLoggingTestCase):
    def setUp(self) -> None:
        self.climate_error_message = ClimateErrorMessage()

    def test___init__(self):
        self.assertEqual(error_topic(Controllers.STM_CLIMATE),
                         self.climate_error_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerErrorCodes.STM_CLIMATE,
            self.climate_error_message.error_code)
