from hangar_controller.devices.drivers.messages.errors.error_code import \
    ErrorCodes
from tests.testcase_wrapper import NoLoggingTestCase


class TestErrorCodes(NoLoggingTestCase):
    def setUp(self) -> None:
        self.peripheral = 'PE'
        self.command_control = 'CC'
        self.telemetry = 'TE'
        self.error_codes = ErrorCodes(peripheral=self.peripheral,
                                      command_control=self.command_control,
                                      telemetry=self.telemetry)

    def test___init__(self):
        self.assertEqual(self.error_codes.peripheral, self.peripheral)
        self.assertEqual(self.error_codes.command_control,
                         self.command_control)
        self.assertEqual(self.error_codes.telemetry, self.telemetry)
