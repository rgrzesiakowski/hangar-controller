from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.message_utils import call_topic
from hangar_controller.devices.drivers.messages.state_readers.readers.read_lift_state import ReadLiftState, TOPIC
from hangar_controller.devices.drivers.messages.state_readers.responses.state_read_response import \
    StateReadResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestReadLiftState(NoLoggingTestCase):
    def setUp(self) -> None:
        self.wobit_reader = MagicMock()
        self.read_lift_state = ReadLiftState(self.wobit_reader)

    def test___init__(self):
        self.assertEqual(call_topic(TOPIC), self.read_lift_state.topic)

    def test__get_lift_position(self):
        # given
        diagnostics = MagicMock()
        diagnostics.get_lift_position = MagicMock(
            return_value='position'
        )

        # when
        result = self.read_lift_state._get_lift_position(diagnostics)

        # then
        diagnostics.get_lift_position.assert_called()
        self.assertEqual('position', result)

    def test_get_responses(self):
        # given
        self.read_lift_state._get_lift_position = MagicMock()

        # when
        result = self.read_lift_state.get_responses()

        # then
        self.assertIsInstance(result, list)
        self.assertEqual(1, len(result))
        self.assertIsInstance(result[0], StateReadResponse)
