from unittest.mock import MagicMock, patch

from hangar_controller.devices.drivers.messages.state_readers.readers.abstract_state_read import AbstractStateRead
from hangar_controller.devices.drivers.messages.state_readers.state_read_manager import \
    StateReadManager
from tests.testcase_wrapper import NoLoggingTestCase


class TestStateReadManager(NoLoggingTestCase):
    @patch.object(StateReadManager, '_init_state_reads')
    def setUp(self, init_state_reads_mock) -> None:
        self.init_state_reads = init_state_reads_mock
        self.diagnostics = MagicMock()
        self.send_message_call = MagicMock()
        self.state_read_manager = StateReadManager(self.diagnostics,
                                                   self.send_message_call)

    def test___init__(self):
        self.assertEqual(self.diagnostics,
                         self.state_read_manager.wobit_diagnostics_reader)
        self.assertEqual(self.send_message_call,
                         self.state_read_manager.send_message_call)
        self.init_state_reads.assert_called()

    def test__init_state_reads(self):
        # given
        self.state_read_manager.register_messages = MagicMock()

        # when
        self.state_read_manager._init_state_reads()

        # then
        self.state_read_manager.register_messages.assert_called()

    def test_receive(self):
        # given
        state_read = MagicMock(spec=AbstractStateRead)
        self.state_read_manager.get_message_by_topic = MagicMock(
            return_value=state_read)
        message = MagicMock()

        # when
        self.state_read_manager.receive(message)

        # then
        self.state_read_manager.get_message_by_topic.assert_called()
        state_read.publish_responses.assert_called()
