from hangar_controller.devices.drivers.messages.responses.expected_messages.plc_io_response \
    import PLCIOResponse
from settingsd.drivers.stm_communication_settings import ResponseMessage
from tests.testcase_wrapper import NoLoggingTestCase


class TestPLCIOResponse(NoLoggingTestCase):
    def setUp(self) -> None:
        self.plc_io_response = PLCIOResponse()

    def test___init__(self):
        self.assertIsNone(self.plc_io_response.expected_messages)
        self.assertIsNone(self.plc_io_response.positive_messages)

    def test_evaluate_message_wrong_value(self):
        # given
        message = '0 | 0 | 1 | 0 | 1 | 1 | 4 | 0'

        # when
        self.plc_io_response.evaluate_message(message)

        # then
        self.assertFalse(self.plc_io_response.evaluation)
        self.assertEqual(ResponseMessage.UNEXPECTED_RESPONSE,
                         self.plc_io_response.evaluation_reason)

    def test_evaluate_message_too_much_inputs(self):
        # given
        message = '0 | 0 | 1 | 0 | 1 | 1 | 0 | 1 | 0'

        # when
        self.plc_io_response.evaluate_message(message)

        # then
        self.assertFalse(self.plc_io_response.evaluation)
        self.assertEqual(ResponseMessage.UNEXPECTED_RESPONSE,
                         self.plc_io_response.evaluation_reason)

    def test_evaluate_message(self):
        # given
        message = '0 | 0 | 1 | 0 | 1 | 1 | 0 | 1'

        # when
        self.plc_io_response.evaluate_message(message)

        # then
        self.assertTrue(self.plc_io_response.evaluation)
