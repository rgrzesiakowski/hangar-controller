from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestEnableDisableResponse(NoLoggingTestCase):
    def setUp(self) -> None:
        self.enable_disable_response = EnableDisableResponse(
            [EnableDisableResponse.ENABLE])

    def test___init__(self):
        self.assertEqual([EnableDisableResponse.ENABLE],
                         self.enable_disable_response.positive_messages)
