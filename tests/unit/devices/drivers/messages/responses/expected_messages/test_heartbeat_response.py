from hangar_controller.devices.drivers.messages.responses.expected_messages.heartbeat_response import HeartbeatResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestHeartbeatResponse(NoLoggingTestCase):
    def setUp(self) -> None:
        self.heartbeat_response = HeartbeatResponse()

    def test___init__(self):
        self.assertEqual(HeartbeatResponse.TOCK,
                         self.heartbeat_response.expected_messages)
        self.assertEqual(HeartbeatResponse.TOCK,
                         self.heartbeat_response.positive_messages)
