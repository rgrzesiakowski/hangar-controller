from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from tests.testcase_wrapper import NoLoggingTestCase


class TestAcknowledgment(NoLoggingTestCase):
    def setUp(self) -> None:
        self.acknowledgment = Acknowledgment()

    def test___init__(self):
        expected_messages = [Acknowledgment.ACK, Acknowledgment.WRONG_VALUE,
                             Acknowledgment.REFUSED]
        self.assertEqual(expected_messages,
                         self.acknowledgment.expected_messages)
        self.assertEqual(Acknowledgment.ACK,
                         self.acknowledgment.positive_messages)
