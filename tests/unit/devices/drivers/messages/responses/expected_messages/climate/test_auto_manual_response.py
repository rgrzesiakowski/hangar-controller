from hangar_controller.devices.drivers.messages.responses.expected_messages.climate.auto_manual_response import \
    AutoManualResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestAutoManualResponse(NoLoggingTestCase):
    def setUp(self) -> None:
        self.auto_manual_response = AutoManualResponse(AutoManualResponse.AUTO)

    def test___init__(self):
        self.assertEqual(AutoManualResponse.AUTO,
                         self.auto_manual_response.positive_messages)
