from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.shift_response import ShiftResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestShiftResponse(NoLoggingTestCase):
    def setUp(self) -> None:
        self.shift_response = ShiftResponse([ShiftResponse.WORK])

    def test___init__(self):
        self.assertEqual([ShiftResponse.WORK],
                         self.shift_response.positive_messages)
