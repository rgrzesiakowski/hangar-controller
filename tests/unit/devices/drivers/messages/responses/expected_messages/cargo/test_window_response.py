from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.window_response import WindowResponse
from tests.testcase_wrapper import NoLoggingTestCase


class TestWindowResponse(NoLoggingTestCase):
    def setUp(self) -> None:
        self.window_response = WindowResponse([WindowResponse.OPEN])

    def test___init__(self):
        self.assertEqual([WindowResponse.OPEN],
                         self.window_response.positive_messages)
