from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.view_response import \
    ViewResponse
from settingsd.drivers.stm_user_panel_settings import UserPanelViews
from tests.testcase_wrapper import NoLoggingTestCase


class TestViewResponse(NoLoggingTestCase):
    def setUp(self) -> None:
        self.view_response = ViewResponse(
            UserPanelViews.CARGO_CORRECT)

    def test___init__(self):
        self.assertEqual(UserPanelViews.CARGO_CORRECT,
                         self.view_response.positive_messages)
