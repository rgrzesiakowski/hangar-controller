from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.user_confirmation import \
    UserConfirmation
from tests.testcase_wrapper import NoLoggingTestCase


class TestConfirmCargoResponse(NoLoggingTestCase):
    def setUp(self) -> None:
        self.response = UserConfirmation(
            UserConfirmation.BUTTON_PRESSED)

    def test___init__(self):
        self.assertEqual(UserConfirmation.BUTTON_PRESSED,
                         self.response.positive_messages)
