from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.responses import Response
from settingsd.drivers.stm_communication_settings import ResponseMessage
from tests.testcase_wrapper import NoLoggingTestCase


class TestResponse(NoLoggingTestCase):
    def setUp(self) -> None:
        self.topic = 'topic'
        self.timeout = 1
        self.expected_message = MagicMock()
        self.response = Response(self.topic, self.timeout,
                                 self.expected_message)

    def test___init__(self):
        self.assertEqual(self.topic, self.response.topic)
        self.assertEqual(self.response._payload, self.response._expected_message.expected_messages)

    def test_wait_for_message(self):
        # given
        self.response._timer = MagicMock()
        self.response._timer.wait = MagicMock(return_value=True)
        self.response.prepare_response = MagicMock(return_value={'dict'})

        # when
        evaluation = self.response.wait_for_message()

        # then
        self.response._timer.wait.assert_called()
        self.response.prepare_response.assert_called()
        self.assertEqual({'dict'}, evaluation)

    def test_prepare_response(self):
        # given
        self.response.update_message_with_evaluation = MagicMock()
        self.response.update_message_with_timer_status = MagicMock()

        # when
        message = self.response.prepare_response()

        # then
        self.response.update_message_with_evaluation.assert_called()
        self.response.update_message_with_timer_status.assert_called()

    def test_update_message_with_timer_status_no_timeout(self):
        # given
        self.response._timer_status = True
        initial_message = {ResponseMessage.EVALUATION: True,
                           ResponseMessage.TOPIC: self.topic,
                           ResponseMessage.PAYLOAD: 'msg'}
        expected_message = initial_message.copy()

        # when
        self.response.update_message_with_timer_status(initial_message)

        # then
        self.assertEqual(expected_message, initial_message)

    def test_update_message_with_evaluation_reason_eval_False(self):
        # given
        self.response._evaluation = False
        self.response._evaluation_reason = \
            ResponseMessage.UNEXPECTED_RESPONSE

        initial_message = MagicMock()
        initial_message.evaluation = True
        initial_message.evaluation_reason = None

        # when
        self.response.update_message_with_evaluation(initial_message)

        # then
        self.assertFalse(initial_message.evaluation)
        self.assertEqual(ResponseMessage.UNEXPECTED_RESPONSE,
                         initial_message.evaluation_reason)

    def test_update_message_with_evaluation_reason(self):
        # given
        self.response._expected_message.evaluation = True
        initial_message = {ResponseMessage.EVALUATION: True,
                           ResponseMessage.TOPIC: self.topic,
                           ResponseMessage.PAYLOAD: 'msg'}
        expected_message = initial_message.copy()

        # when
        self.response.update_message_with_evaluation(initial_message)

        # then
        self.assertEqual(expected_message, initial_message)

    def test_receive(self):
        # given
        message = MagicMock()
        self.response._timer = MagicMock()
        self.response.evaluate = MagicMock()
        self.response._evaluation = True

        # when
        status = self.response.receive(message)

        # then
        self.response._timer.stop.assert_called()
        self.response.evaluate.assert_called_with(message)
        self.assertTrue(status)
