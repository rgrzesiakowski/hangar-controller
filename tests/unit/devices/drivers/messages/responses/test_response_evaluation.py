from hangar_controller.devices.drivers.messages.responses import \
    ResponseEvaluation
from tests.testcase_wrapper import NoLoggingTestCase


class TestResponseEvaluation(NoLoggingTestCase):
    def setUp(self) -> None:
        self.response_evaluation = ResponseEvaluation(True, 'topic', 'payload')

    def test___init__(self):
        self.assertTrue(self.response_evaluation.evaluation)
        self.assertEqual(self.response_evaluation.topic, 'topic')
        self.assertEqual(self.response_evaluation.payload, 'payload')
        self.assertIsNone(self.response_evaluation.evaluation_reason)

    def test_update_evaluation(self):
        # given

        # when
        self.response_evaluation.update_evaluation(False)

        # then
        self.assertFalse(self.response_evaluation.evaluation)

    def test_update_evaluation_reason(self):
        # given

        # when
        self.response_evaluation.update_evaluation_reason('reason')

        # then
        self.assertEqual('reason', self.response_evaluation.evaluation_reason)
