from unittest.mock import MagicMock, call

from hangar_controller.devices.drivers.messages.responses import ResponseTimer
from tests.testcase_wrapper import NoLoggingTestCase


class TestResponseTimer(NoLoggingTestCase):
    def setUp(self) -> None:
        self.timeout = 1
        self.response_timer = ResponseTimer(self.timeout)

    def test___init__(self):
        self.assertEqual(self.timeout, self.response_timer.timeout)

    def test_stop(self):
        # given
        self.response_timer.lock = MagicMock()

        # when
        self.response_timer.stop()

        # then
        self.response_timer.lock.release.assert_called()

    def test_start(self):
        # given
        self.response_timer.lock = MagicMock()
        self.response_timer.lock.acquire = MagicMock(return_value=True)

        # when
        acquired = self.response_timer.wait()

        # then
        self.response_timer.lock.acquire.assert_has_calls([
            call(), call(timeout=self.timeout)])

        self.assertTrue(acquired)
