from unittest.mock import MagicMock

from tests.testcase_wrapper import NoLoggingTestCase
from settingsd.drivers.stm_user_panel_settings import UserPanelViews
from hangar_controller.devices.drivers.controller_components import UserPanel
from hangar_controller.devices.drivers.messages.commands.user_panel import \
    UserPanelGetPin, UserPanelSetView


class TestUserPanel(NoLoggingTestCase):
    def setUp(self) -> None:
        self.user_panel = UserPanel(MagicMock())

    def test_get_pin(self):
        # given
        self.user_panel._commands_manager = MagicMock()
        expected_response = MagicMock()
        expected_pin_response = MagicMock()
        expected_response.get_response_by_topic = MagicMock(
            return_value=expected_pin_response)
        expected_pin_response.payload = 'pin'
        self.user_panel._commands_manager.send_command_and_wait = MagicMock(
            return_value=expected_response
        )

        # when
        resp, pin = self.user_panel.get_pin()

        # then
        self.user_panel._commands_manager.send_command_and_wait. \
            assert_called_with(UserPanelGetPin())
        self.assertEqual(expected_response, resp)
        self.assertEqual('pin', pin)

    def test_set_view(self):
        # given
        self.user_panel._commands_manager = MagicMock()

        # when
        self.user_panel.set_view(UserPanelViews.CARGO_CORRECT)

        # then
        self.user_panel._commands_manager.send_command_and_wait. \
            assert_called_with(UserPanelSetView(UserPanelViews.CARGO_CORRECT))
