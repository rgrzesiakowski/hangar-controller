from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from settingsd.drivers.stm_pms_settings import PMSComponent
from tests.testcase_wrapper import NoLoggingTestCase
from hangar_controller.devices.drivers.controller_components import \
    PowerManagementSystem
from hangar_controller.devices.drivers.messages.commands.power_management_system import \
    PMSGetPLCIO, PMSGetStates, PMSSetChargersPower, PMSSetIRLockPower, \
    PMSSetMotorsLiftPower, PMSSetMotorsManPower, PMSSetMotorsPosPower, \
    PMSSetPressurePower, PMSSetAirCompressorPower


class TestPowerManagementSystem(NoLoggingTestCase):
    def setUp(self) -> None:
        self.pms = PowerManagementSystem(MagicMock())

        self.cases = {
            PMSComponent.TRAFO: PMSSetChargersPower,
            PMSComponent.POSITIONING_MOTORS: PMSSetMotorsPosPower,
            PMSComponent.MANIPULATOR_MOTORS: PMSSetMotorsManPower,
            PMSComponent.LIFT_MOTORS: PMSSetMotorsLiftPower,
            PMSComponent.PRESSURE: PMSSetPressurePower,
            PMSComponent.AIR_COMPRESSOR: PMSSetAirCompressorPower,
            PMSComponent.IR_LOCK: PMSSetIRLockPower
        }

    def test_pms_components_power_enable(self):
        self.pms._commands_manager = MagicMock()

        for case in self.cases:
            with self.subTest(msg=f'case: {case}'):
                self.pms.set_component_power(case, True)

                self.pms._commands_manager.send_command_and_wait.assert_called_with(
                    self.cases[case](EnableDisableResponse.ENABLE)
                )
            self.pms._commands_manager.reset_mock()

    def test_pms_components_power_disable(self):
        self.pms._commands_manager = MagicMock()

        for case in self.cases:
            with self.subTest(msg=f'case: {case}'):
                self.pms.set_component_power(case, False)

                self.pms._commands_manager.send_command_and_wait.assert_called_with(
                    self.cases[case](EnableDisableResponse.DISABLE)
                )
            self.pms._commands_manager.reset_mock()

    def test_pms_get_states(self):
        # given
        self.pms._commands_manager = MagicMock()
        self.pms._commands_manager.send_command_and_wait = MagicMock(
            return_value='states'
        )

        # when
        states = self.pms.get_states()

        # then
        self.pms._commands_manager.send_command_and_wait. \
            assert_called_with(PMSGetStates())
        self.assertEqual('states', states)

    def test_pms_get_plc_io(self):
        # given
        self.pms._commands_manager = MagicMock()
        self.pms._commands_manager.send_command_and_wait = MagicMock(
            return_value='plc_io'
        )

        # when
        plc_io = self.pms.get_plc_io()

        # then
        self.pms._commands_manager.send_command_and_wait. \
            assert_called_with(PMSGetPLCIO())
        self.assertEqual('plc_io', plc_io)
