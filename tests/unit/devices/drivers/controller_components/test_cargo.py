from unittest.mock import MagicMock

from tests.testcase_wrapper import NoLoggingTestCase
from hangar_controller.devices.drivers.controller_components import Cargo
from hangar_controller.devices.drivers.messages.commands.cargo import \
    CargoBaseShift, CargoCloseWindow, CargoGetPLCIO, CargoGetStates, \
    CargoOpenWindow, CargoWorkShift


class TestCargo(NoLoggingTestCase):
    def setUp(self) -> None:
        self.cargo = Cargo(MagicMock())

    def test_open_window(self):
        # given
        self.cargo._commands_manager = MagicMock()

        # when
        self.cargo.open_window()

        # then
        self.cargo._commands_manager.send_command_and_wait. \
            assert_called_with(CargoOpenWindow())

    def test_close_window(self):
        # given
        self.cargo._commands_manager = MagicMock()

        # when
        self.cargo.close_window()

        # then
        self.cargo._commands_manager.send_command_and_wait. \
            assert_called_with(CargoCloseWindow())

    def test_base_shift(self):
        # given
        self.cargo._commands_manager = MagicMock()

        # when
        self.cargo.base_shift()

        # then
        self.cargo._commands_manager.send_command_and_wait. \
            assert_called_with(CargoBaseShift())

    def test_work_shift(self):
        # given
        self.cargo._commands_manager = MagicMock()

        # when
        self.cargo.work_shift()

        # then
        self.cargo._commands_manager.send_command_and_wait. \
            assert_called_with(CargoWorkShift())

    def test_cargo_get_states(self):
        # given
        self.cargo._commands_manager = MagicMock()

        # when
        self.cargo.get_states()

        # then
        self.cargo._commands_manager.send_command_and_wait. \
            assert_called_with(CargoGetStates())

    def test_cargo_get_plc_io(self):
        # given
        self.cargo._commands_manager = MagicMock()

        # when
        self.cargo.get_plc_io()

        # then
        self.cargo._commands_manager.send_command_and_wait. \
            assert_called_with(CargoGetPLCIO())
