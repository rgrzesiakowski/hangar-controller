from unittest.mock import MagicMock

from tests.testcase_wrapper import NoLoggingTestCase
from hangar_controller.devices.drivers.controller_components import \
    MasterCharger
from hangar_controller.devices.drivers.messages.commands.master_charger import \
    MasterChargerGetChargersNumber, MasterChargerGetPLCIO, \
    MasterChargerGetStates, MasterChargerGetTelemetry, MasterChargerSetPower


class TestMasterCharger(NoLoggingTestCase):
    def setUp(self) -> None:
        self.master_charger = MasterCharger(MagicMock())

    def test_set_charger_power(self):
        # given
        self.master_charger._commands_manager = MagicMock()

        # when
        self.master_charger.set_charger_power('1', True)

        # then
        self.master_charger._commands_manager.send_command_and_wait. \
            assert_called_with(
                MasterChargerSetPower('1', MasterChargerSetPower.ENABLED))

    def test_disable_charger_power(self):
        # given
        self.master_charger._commands_manager = MagicMock()

        # when
        self.master_charger.set_charger_power('1', False)

        # then
        self.master_charger._commands_manager.send_command_and_wait. \
            assert_called_with(
                MasterChargerSetPower('1', MasterChargerSetPower.DISABLED))

    def test_mc_get_states(self):
        # given
        self.master_charger._commands_manager = MagicMock()

        # when
        self.master_charger.get_states('1')

        # then
        self.master_charger._commands_manager.send_command_and_wait. \
            assert_called_with(MasterChargerGetStates('1'))

    def test_mc_get_telemetry(self):
        # given
        self.master_charger._commands_manager = MagicMock()

        # when
        self.master_charger.get_telemetry('1')

        # then
        self.master_charger._commands_manager.send_command_and_wait. \
            assert_called_with(MasterChargerGetTelemetry('1'))

    def test_mc_get_plc_io(self):
        # given
        self.master_charger._commands_manager = MagicMock()

        # when
        self.master_charger.get_plc_io()

        # then
        self.master_charger._commands_manager.send_command_and_wait. \
            assert_called_with(MasterChargerGetPLCIO())
