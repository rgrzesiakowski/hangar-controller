from unittest.mock import MagicMock

from tests.testcase_wrapper import NoLoggingTestCase
from hangar_controller.devices.drivers.controller_components import Climate
from hangar_controller.devices.drivers.messages.commands.climate import SetAC, \
    SetFan, SetHangarHeater, SetHangarHeaterTemperature


class TestClimate(NoLoggingTestCase):
    def setUp(self) -> None:
        self.climate = Climate(MagicMock())

    def test_set_ac(self):
        # given
        self.climate._commands_manager = MagicMock()

        # when
        self.climate.set_ac(True)

        # then
        self.climate._commands_manager.send_command_and_wait. \
            assert_called_with(SetAC(SetAC.ENABLE))

    def test_set_fan(self):
        # given
        self.climate._commands_manager = MagicMock()

        # when
        self.climate.set_fan(True)

        # then
        self.climate._commands_manager.send_command_and_wait. \
            assert_called_with(SetFan(SetFan.ENABLE))

    def test_set_hangar_heater(self):
        # given
        self.climate._commands_manager = MagicMock()

        # when
        self.climate.set_hangar_heater(True)

        # then
        self.climate._commands_manager.send_command_and_wait. \
            assert_called_with(SetHangarHeater(SetHangarHeater.ENABLE))

    def test_set_hangar_heater_temperature(self):
        # given
        self.climate._commands_manager = MagicMock()

        # when
        self.climate.set_hangar_heater_temperature(25.1)

        # then
        self.climate._commands_manager.send_command_and_wait. \
            assert_called_with(SetHangarHeaterTemperature(25.1))
