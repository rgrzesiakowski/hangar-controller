from unittest.mock import MagicMock

from tests.testcase_wrapper import NoLoggingTestCase
from hangar_controller.devices.drivers.controller_components import Roof
from hangar_controller.devices.drivers.messages.commands.roof import \
    RoofClose, RoofOpen, RoofStop


class TestRoof(NoLoggingTestCase):
    def setUp(self) -> None:
        self.roof = Roof(MagicMock())

    def test_stop_roof(self):
        # given
        self.roof._commands_manager = MagicMock()

        # when
        self.roof.stop()

        # then
        self.roof._commands_manager.send_command_and_wait. \
            assert_called_with(RoofStop())

    def test_open_roof(self):
        # given
        self.roof._commands_manager = MagicMock()

        # when
        self.roof.open()

        # then
        self.roof._commands_manager.send_command_and_wait. \
            assert_called_with(RoofOpen())

    def test_close_roof(self):
        # given
        self.roof._commands_manager = MagicMock()

        # when
        self.roof.close()

        # then
        self.roof._commands_manager.send_command_and_wait. \
            assert_called_with(RoofClose())
