from concurrent.futures import ThreadPoolExecutor
from unittest.mock import MagicMock, call, patch

from hangar_controller.devices.drivers.diagnostics.drivers_diagnostics import \
    DriversDiagnostics
from hangar_controller.devices.drivers.drivers_controller import \
    DriversController
from hangar_controller.devices.drivers.messages.abstract_message_manager import \
    AbstractMessageManager
from hangar_controller.devices.drivers.messages.commands.commands_manager import \
    CommandsManager
from hangar_controller.devices.drivers.messages.measurements.measurement import \
    Measurement
from hangar_controller.devices.drivers.messages.measurements.measurements_manager import \
    MeasurementsManager
from hangar_controller.devices.drivers.messages.responses import ResponseManager
from hangar_controller.devices.drivers.messages.state_readers.state_read_manager import \
    StateReadManager
from tests.testcase_wrapper import NoLoggingTestCase


class TestDriversController(NoLoggingTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.client = MagicMock()
        cls.error_handler = MagicMock()
        cls.task_queue = MagicMock()
        cls.diagnostics = MagicMock()

    # @patch.object(StateReadManager, '_init_state_reads')
    @patch(
        'hangar_controller.devices.drivers.hangar_drivers_mqtt_client.HangarDriversMQTTClient.__new__',
        return_value=MagicMock())
    def setUp(self, mqtt_mock) -> None:
        self.controller = DriversController(MagicMock(), MagicMock())
        self.controller._task_queue = MagicMock()
        self.mqtt_mock = mqtt_mock

    def tearDown(self):
        DriversController.__it__ = None

    def test___init__(self):
        self.assertIsInstance(self.controller._diagnostics, DriversDiagnostics)
        self.assertIsInstance(self.controller.executor, ThreadPoolExecutor)
        self.assertIsInstance(self.controller._measurements_manager,
                              MeasurementsManager)
        self.assertIsInstance(self.controller._response_manager,
                              ResponseManager)
        self.assertIsInstance(self.controller._commands_manager,
                              CommandsManager)
        self.assertIsInstance(self.controller._state_read_manager,
                              StateReadManager)

    def test_diagnostics(self):
        self.assertEqual(self.controller._diagnostics,
                         self.controller.diagnostics)

    def test_receive_message_response(self):
        # given
        self.controller._response_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._response_manager.is_topic_registered = MagicMock(
            return_value=True
        )
        self.controller._measurements_manager = MagicMock(
            spec=AbstractMessageManager)
        message = MagicMock()
        message.topic = MagicMock()

        # when
        self.controller._receive_message(message)

        # then
        self.controller._response_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._response_manager.receive.assert_called_with(
            message
        )
        self.controller._measurements_manager.receive.assert_not_called()

    def test_receive_message_measurement(self):
        # given
        self.controller._response_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._response_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._measurements_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=True
        )
        message = MagicMock(spec=Measurement)
        message.topic = MagicMock()

        # when
        self.controller._receive_message(message)

        # then
        self.controller._response_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._response_manager.receive.assert_not_called()
        self.controller._measurements_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._measurements_manager.receive.assert_called_with(
            message
        )

    def test_receive_message_error(self):
        # given
        self.controller._response_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._response_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._measurements_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._error_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=True
        )
        self.controller._measurements_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        message = MagicMock(spec=Measurement)
        message.topic = MagicMock()

        # when
        self.controller._receive_message(message)

        # then
        self.controller._response_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._response_manager.receive.assert_not_called()
        self.controller._measurements_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._measurements_manager.receive.assert_not_called()
        self.controller._error_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._error_manager.receive.assert_called()

    def test_receive_message_state_read(self):
        # given
        self.controller._response_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._response_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._measurements_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._error_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._state_read_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._state_read_manager.is_topic_registered = MagicMock(
            return_value=True
        )
        message = MagicMock(spec=Measurement)
        message.topic = MagicMock()

        # when
        self.controller._receive_message(message)

        # then
        self.controller._response_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._response_manager.receive.assert_not_called()
        self.controller._measurements_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._measurements_manager.receive.assert_not_called()
        self.controller._state_read_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._state_read_manager.receive.assert_called()

    def test_run_empty_message(self):
        # given
        def stop_controller(*args):
            self.controller.stop()

        self.controller.message_queue = MagicMock()
        self.controller.message_queue.get = MagicMock(
            side_effect=[None, 'msg'])
        self.controller._receive_message = MagicMock(
            side_effect=stop_controller)

        # when
        self.controller.run()

        # then
        self.controller.message_queue.get.assert_has_calls([call(), call()])
        self.controller._receive_message.assert_called_once_with('msg')

    def test_run(self):
        # given
        def stop_controller(*args):
            self.controller.stop()

        self.controller.message_queue = MagicMock()
        self.controller.message_queue.get = MagicMock(return_value='msg')
        self.controller._receive_message = MagicMock(
            side_effect=stop_controller)

        # when
        self.controller.run()

        # then
        self.controller.message_queue.get.assert_called()
        self.controller._receive_message.assert_called_with('msg')

    def test_start_heartbeat(self):
        # given
        self.controller._commands_manager.heartbeat_sender = MagicMock()

        # when
        self.controller.start_heartbeat()

        # then
        self.controller._commands_manager.heartbeat_sender.start.assert_called()

    def test_stop_heartbeat(self):
        # given
        self.controller._commands_manager.heartbeat_sender = MagicMock()

        # when
        self.controller.stop_heartbeat()

        # then
        self.controller._commands_manager.heartbeat_sender.stop.assert_called()
