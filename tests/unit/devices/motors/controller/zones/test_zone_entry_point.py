from hangar_controller.devices.motors.controller.zones.zone_entry_point import \
    ZoneEntryPositions
from settingsd.motors import parameters_settings
from tests.testcase_wrapper import NoLoggingTestCase


class TestZoneEntryPositions(NoLoggingTestCase):
    def setUp(self) -> None:
        self.zone_entry_positions: ZoneEntryPositions = ZoneEntryPositions()

    def test_zone_entry_positions_has_all_fields(self):
        self.assertEqual(self.zone_entry_positions.CARGO_SAFE_HEIGHT,
                         parameters_settings.Positions.CARGO_SAFE_MOVE_HEIGHT)
        self.assertEqual(self.zone_entry_positions.MOTOR_Y_BACK,
                         parameters_settings.Positions.MOTOR_Y_BACK)
        self.assertEqual(self.zone_entry_positions.CARGO_WINDOW_PICK_UP,
                         parameters_settings.Positions.CARGO_WINDOW_PICK_UP)
