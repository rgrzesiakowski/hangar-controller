from hangar_controller.devices.motors.controller.zones.hub_zones import HubZones
from hangar_controller.devices.motors.controller.zones.zone import Zone
from tests.testcase_wrapper import NoLoggingTestCase


class TestHubZones(NoLoggingTestCase):
    def setUp(self) -> None:
        self.hub_zones: HubZones = HubZones()

    def test_HubZones_has_all_fields(self):
        self.assertIsInstance(self.hub_zones.BATTERY_SLOTS, Zone)
        self.assertIsInstance(self.hub_zones.CARGO_WINDOW_FOR_PICKING_UP, Zone)
        self.assertIsInstance(self.hub_zones.CARGO_DRONE, Zone)
        self.assertIsInstance(self.hub_zones.BATTERY_DRONE, Zone)
        self.assertIsInstance(self.hub_zones.HOME, Zone)
        self.assertIsInstance(self.hub_zones.CALIBRATION, Zone)
