from unittest.mock import MagicMock, call

from hangar_controller.devices.motors.controller.observers.diagnostics import Diagnostics
from hangar_controller.devices.motors.controller.observers.motor_diagnostics import \
    MotorDiagnostics
from settingsd.motors.modbus_constants import ModbusResponseStatus
from tests.testcase_wrapper import NoLoggingTestCase


class TestDiagnostics(NoLoggingTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.motor1 = MagicMock()
        cls.motor1.motor_name = 'motor1'
        cls.motor2 = MagicMock()
        cls.motor2.motor_name = 'motor2'
        cls.motors = [cls.motor1, cls.motor2]
        cls.error_handler = MagicMock()

    def setUp(self) -> None:
        self.diagnostics = Diagnostics(self.motors, self.error_handler)

    def test___init__(self):
        self.assertFalse(self.diagnostics._stop_diagnostics)
        self.assertIsInstance(self.diagnostics._diagnostics, dict)

    def test_diagnostics(self):
        self.assertEqual(self.diagnostics.diagnostics,
                         self.diagnostics._diagnostics)

    def test__init_diagnostics(self):
        # given

        # when
        diagnostics = self.diagnostics._init_diagnostics()

        # then
        self.assertEqual(2, len(diagnostics))
        self.assertIsInstance(diagnostics['motor1'], MotorDiagnostics)
        self.assertIsInstance(diagnostics['motor2'], MotorDiagnostics)

    def test__check_modbus_response_status_modbus_error(self):
        # given

        # when
        status = self.diagnostics._check_modbus_response_status(
            ModbusResponseStatus.CONNECTION_ERROR
        )

        # then
        self.assertFalse(status)
        self.diagnostics._error_handler.modbus_error.assert_called_with(
            ModbusResponseStatus.CONNECTION_ERROR
        )

    def test__check_modbus_response_status(self):
        # given

        # when
        status = self.diagnostics._check_modbus_response_status(
            ModbusResponseStatus.STATUS_OK
        )

        # then
        self.assertTrue(status)
        self.diagnostics._error_handler.assert_not_called()

    def test_update_motor_reading_modbus_error(self):
        # given
        motor_read = MagicMock(
            return_value=('status', 'value')
        )
        self.diagnostics._check_modbus_response_status = MagicMock(
            return_value=False
        )
        diagnostic_update = MagicMock()

        # when
        status_ok = self.diagnostics._update_motor_reading(motor_read,
                                                           diagnostic_update)

        # then
        self.assertFalse(status_ok)
        motor_read.assert_called()
        self.diagnostics._check_modbus_response_status.assert_called_with(
            'status')
        diagnostic_update.assert_not_called()

    def test_update_motor_reading(self):
        # given
        motor_read = MagicMock(
            return_value=('status', 'value')
        )
        self.diagnostics._check_modbus_response_status = MagicMock(
            return_value=True
        )
        diagnostic_update = MagicMock()

        # when
        self.diagnostics._update_motor_reading(motor_read, diagnostic_update)

        # then
        motor_read.assert_called()
        self.diagnostics._check_modbus_response_status.assert_called_with(
            'status')
        diagnostic_update.assert_called()

    def test_update_diagnostics_limit_switch_in_modbus_error(self):
        # given
        diagnostics1 = self.diagnostics.diagnostics['motor1']
        diagnostics2 = self.diagnostics.diagnostics['motor2']
        self.diagnostics._update_motor_reading = MagicMock(return_value=True)

        calls = [
            call(True, self.motor1.read_limit_switch_in,
                 diagnostics1.update_limit_switch_in),
            call(True, self.motor1.read_limit_switch_out,
                 diagnostics1.update_limit_switch_out),
            call(True, self.motor1._read_position,
                 diagnostics1.update_position),
            call(True, self.motor2.read_limit_switch_in,
                 diagnostics2.update_limit_switch_in),
            call(True, self.motor2.read_limit_switch_out,
                 diagnostics2.update_limit_switch_out),
            call(True, self.motor2._read_position, diagnostics2.update_position)
        ]

        # when
        self.diagnostics.update_diagnostics()

        # then
        self.diagnostics._update_motor_reading.has_calls(calls)

    def test_stop(self):
        # when
        self.diagnostics.stop()

        # then
        self.assertTrue(self.diagnostics._stop_diagnostics)

    def test_run(self):
        # given
        def mock_stop(*args):
            self.diagnostics._stop_diagnostics = True

        self.diagnostics._frequency = 1000
        self.diagnostics.update_diagnostics = MagicMock()
        self.diagnostics.update_diagnostics.side_effect = mock_stop

        # when
        self.diagnostics.start()

        # then
        self.diagnostics.update_diagnostics.assert_called()
