from unittest.mock import MagicMock

from hangar_controller.devices.motors.controller.task_queue.task import Task
from tests.testcase_wrapper import NoLoggingTestCase


class TestTask(NoLoggingTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.callback = MagicMock()
        cls.arguments = ['123', 'hello']
        cls.lock_release = MagicMock()

    def setUp(self) -> None:
        self.queue_task: Task = Task(self.callback, self.arguments,
                                     self.lock_release)

    def test___init__args_empty_list_when_None(self):
        # given
        queue_task: Task = Task(self.callback, lock_release=self.lock_release)

        # when

        # then
        self.assertIsInstance(queue_task._args, list)
        self.assertEqual(0, len(queue_task._args))

    def test_queue_locking_lock_exists(self):
        # given

        # when

        # then
        self.assertTrue(self.queue_task.queue_locking)

    def test_queue_locking_no_lock(self):
        # given
        self.queue_task._lock_release = None

        # when

        # then
        self.assertFalse(self.queue_task.queue_locking)

    def test_process(self):
        # given

        # when
        self.queue_task.run()

        # then
        self.callback.assert_called_with(*self.arguments)
