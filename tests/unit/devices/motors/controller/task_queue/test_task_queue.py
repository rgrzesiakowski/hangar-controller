import time
from threading import Lock
from unittest.mock import MagicMock

from hangar_controller.devices.motors.controller.task_queue.empty_task import EmptyTask
from hangar_controller.devices.motors.controller.task_queue.task import Task
from hangar_controller.devices.motors.controller.task_queue.task_queue import TaskQueue
from tests.testcase_wrapper import NoLoggingTestCase


class TestQueue(NoLoggingTestCase):
    def setUp(self) -> None:
        self.queue: TaskQueue = TaskQueue()

    def tearDown(self) -> None:
        self.queue.release_lock()

    def test___init__(self):
        # given
        test_lock = Lock()

        # when

        # then
        self.assertEqual(type(self.queue.lock), type(test_lock))

    def test_lock(self):
        # given

        # when

        # then
        self.assertEqual(self.queue.lock, self.queue._lock)

    def test_add_task(self, locking=False):
        # given
        callback = print
        arguments = ['123']
        locking = locking

        # when
        self.queue.add_task(callback, arguments, locking)
        task: Task = self.queue.get()

        # then
        self.assertEqual(callback, task._callback)
        self.assertEqual(arguments, task._args)
        self.assertEqual(locking, task.queue_locking)

    def test_add_empty_task(self):
        # when
        self.queue.add_empty_task()

        # then
        task = self.queue.get()
        self.assertIsInstance(task, EmptyTask)

    def test_wait_for_unlock(self):
        # given
        self.queue._lock = MagicMock()
        self.queue.lock.acquire = MagicMock()

        # when
        self.queue.wait_for_unlock()

        # then
        self.queue.lock.acquire.assert_called()

    def test_release_lock_when_not_locked_doesnt_call_release(self):
        # given
        self.queue._lock = MagicMock()
        self.queue.lock.locked = MagicMock(return_value=False)
        self.queue.lock.release = MagicMock()

        # when
        self.queue.release_lock()

        # then
        self.queue.lock.release.assert_not_called()

    def test_release_lock_when_not_locked_doesnt_change_unlock_state(self):
        # given

        # when
        self.queue.release_lock()

        # then
        self.assertFalse(self.queue.lock.locked())

    def test_release_lock(self):
        # given
        self.queue.lock.acquire()
        self.assertTrue(self.queue.lock.locked())

        # when
        self.queue.release_lock()

        # then
        self.assertFalse(self.queue.lock.locked())

    def test_all_tasks_started_when_queue_unlocked(self):
        # given
        next_task = MagicMock()
        start_time = time.time()

        # when
        self.queue.add_task(time.sleep, [0.1], False)
        self.queue.add_task(next_task, ['123'], False)
        self.queue.process_task()
        self.queue.process_task()
        processing_time = time.time() - start_time

        # then
        self.assertAlmostEqual(0.0, processing_time, places=1)

    def test_process_task_no_locking(self):
        # given
        callback = time.sleep
        arguments = [0.1]
        locking = False

        # when
        self.queue.add_task(callback, arguments, locking)
        self.queue.process_task()

        # then
        self.assertFalse(self.queue.lock.locked())
