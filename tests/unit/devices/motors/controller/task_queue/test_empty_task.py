from unittest.mock import patch

from hangar_controller.devices.motors.controller.task_queue.empty_task import EmptyTask, \
    empty_callback
from hangar_controller.devices.motors.controller.task_queue.task import Task
from tests.testcase_wrapper import NoLoggingTestCase


class TestEmptyTask(NoLoggingTestCase):
    def setUp(self) -> None:
        self.empty_task = EmptyTask()

    @patch.object(Task, '__init__')
    def test___init__(self, mock_init):
        # given
        mock_init.return_value = None

        # when
        EmptyTask()

        # then
        mock_init.assert_called_with(empty_callback)
