from unittest.mock import MagicMock, call

from tests.testcase_wrapper import NoLoggingTestCase
from settingsd.motors import parameters_settings
from hangar_controller.devices.motors.controller.motors_controllers. \
    positioning_motors_controller import PositioningMotorController, \
    X_PLATFORM, Y_PLATFORM


class TestPositioningMotorController(NoLoggingTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.modbus_io = MagicMock()
        cls.error_handler = MagicMock()
        cls.parameters_reader = MagicMock()
        cls.task_queue = MagicMock()
        cls.parameters_reader.read_positioning_motors_placement = MagicMock(
            return_value=[[0, 0], [1, 2]]
        )

    def setUp(self) -> None:
        self.controller = PositioningMotorController(
            self.modbus_io, self.error_handler,
            self.task_queue
        )

    def test___init__(self):
        self.assertEqual(2, len(self.controller.motors))
        self.assertEqual(X_PLATFORM, self.controller.motors[0].motor_name)
        self.assertEqual(Y_PLATFORM, self.controller.motors[1].motor_name)

    def test_calibrate_x(self):
        # given
        motor_x_params_name = parameters_settings.DrivesNames.X_PLATFORM
        self.controller._parameters_reader = MagicMock()
        self.controller._parameters_reader.read_calibration_speed = MagicMock(
            return_value=12.12
        )
        calls = [
            call(self.controller.calibrate, [[X_PLATFORM], [12.12]]),
        ]
        # when
        self.controller.calibrate_x()

        # then
        self.controller._parameters_reader.read_calibration_speed.assert_called_with(
            motor_x_params_name
        )
        self.task_queue.add_task.assert_has_calls(calls)
        self.task_queue.wait_until_done.assert_called()

    def test_calibrate_y(self):
        # given
        motor_y_params_name = parameters_settings.DrivesNames.Y_PLATFORM
        self.controller._parameters_reader.read_calibration_speed = MagicMock(
            return_value=12.12
        )
        calls = [
            call(self.controller.calibrate, [[Y_PLATFORM], [12.12]]),
        ]
        # when
        self.controller.calibrate_y()

        # then
        self.controller._parameters_reader.read_calibration_speed.assert_called_with(
            motor_y_params_name
        )
        self.task_queue.add_task.assert_has_calls(calls)
        self.task_queue.wait_until_done.assert_called()

    def test_calibrate_all(self):
        # given
        motor_x_params_name = parameters_settings.DrivesNames.X_PLATFORM
        motor_y_params_name = parameters_settings.DrivesNames.Y_PLATFORM
        self.controller._parameters_reader.read_calibration_speed = MagicMock(
            return_value=12.12
        )
        calls = [
            call(motor_x_params_name), call(motor_y_params_name)
        ]
        # when
        self.controller.calibrate_all()

        # then
        self.task_queue.add_task.assert_called_with(
            self.controller.calibrate,
            [[X_PLATFORM, Y_PLATFORM], [12.12, 12.12]])
        self.controller._parameters_reader.read_calibration_speed.assert_has_calls(calls)
        self.task_queue.wait_until_done.assert_called()

    def test_position_drone(self):
        # given
        self.controller.calibrate_all = MagicMock()
        self.controller._parameters_reader.read_platform_middle = MagicMock(
            return_value=['positions']
        )
        self.controller.filter_positions = MagicMock()
        self.controller.task_queue.add_task = MagicMock()
        self.controller.task_queue.wait_until_done = MagicMock()

        # when
        self.controller.position_drone()

        # then
        self.controller.calibrate_all.assert_called()
        self.controller._parameters_reader.read_platform_middle.assert_called()
        self.controller.filter_positions.assert_called()
        self.task_queue.add_task.assert_called()
        self.task_queue.wait_until_done.assert_called()

    def test_release_drone(self):
        # given
        self.controller._parameters_reader.read_platform_home = MagicMock(
            return_value=['positions']
        )
        self.controller.task_queue.add_task = MagicMock()
        self.controller.task_queue.wait_until_done = MagicMock()

        # when
        self.controller.release_drone()

        # then
        self.controller._parameters_reader.read_platform_home.assert_called()
        self.task_queue.add_task.assert_called()
        self.task_queue.wait_until_done.assert_called()
