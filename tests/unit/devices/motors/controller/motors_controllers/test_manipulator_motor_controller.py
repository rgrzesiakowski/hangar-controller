from unittest.mock import MagicMock, call

from hangar_controller.devices.motors.controller.motors_controllers.manipulator_motor_controller import \
    ManipulatorMotorController, MOTOR_X, MOTOR_Y, MOTOR_Z
from hangar_controller.devices.motors.controller.zones.hub_zones import \
    HubZones
from hangar_controller.devices.motors.controller.zones.zone_entry_point import \
    ZoneEntryPositions
from settingsd.motors import parameters_settings
from settingsd.motors.parameters_settings import Positions
from tests.testcase_wrapper import NoLoggingTestCase


class TestManipulatorMotorController(NoLoggingTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.modbus_io = MagicMock()
        cls.error_handler = MagicMock()
        cls.parameters_reader = MagicMock()
        cls.task_queue = MagicMock()
        cls.parameters_reader.read_manipulator_motors_placement = MagicMock(
            return_value=[[1, 1, 1], [0, 1, 2]]
        )

    def setUp(self) -> None:
        self.controller = ManipulatorMotorController(
            self.modbus_io, self.error_handler,
            self.task_queue
        )

    def test___init__(self):
        self.assertEqual(3, len(self.controller.motors))
        self.assertEqual(MOTOR_X, self.controller.motors[0].motor_name)
        self.assertEqual(MOTOR_Y, self.controller.motors[1].motor_name)
        self.assertEqual(MOTOR_Z, self.controller.motors[2].motor_name)

    def test__update_current_zone(self):
        # given
        zone = MagicMock()

        # when
        self.controller._update_current_zone(zone)

        # then
        self.assertEqual(zone, self.controller.current_zone)

    def test__move_by_path_moving_cargo_window_home(self):
        # given
        self.controller._pick_up_cargo = MagicMock()
        movement_path = [ZoneEntryPositions.CARGO_WINDOW_PICK_UP]

        # when
        self.controller._move_by_path(movement_path)

        # then
        self.controller._pick_up_cargo.assert_called()

    def test__exit_current_zone(self):
        # given
        self.controller.current_zone = MagicMock()
        self.controller._move_by_path = MagicMock()

        # when
        self.controller._exit_current_zone()

        # then
        self.controller._move_by_path.assert_called_with(
            self.controller.current_zone.exit_path
        )

    def test__move_motors_xz_y(self):
        # given
        self.controller.filter_positions = MagicMock(
            side_effect=['first', 'second'])
        self.controller.task_queue.add_task = MagicMock()

        # when
        self.controller._move_motors_xz_y_position(MagicMock())

        # then
        self.controller.filter_positions.assert_called()
        self.controller.task_queue.add_task.assert_called()

    def test__move_home(self):
        # given
        self.controller._parameters_reader.read_home_position = MagicMock(
            return_value='position')
        self.controller.filter_positions = MagicMock(return_value='position_f')
        self.controller.task_queue.add_task = MagicMock()

        # when
        self.controller._move_home()

        # then
        self.controller._parameters_reader.read_home_position.assert_called()
        self.controller.filter_positions.assert_called()
        self.controller.task_queue.add_task.assert_called()

    def test__move_cargo_window_home(self):
        # given
        self.controller._parameters_reader.read_cargo_window_pick_up = MagicMock(
            return_value='position')
        self.controller.filter_positions = MagicMock()
        self.controller.task_queue.add_task = MagicMock()

        # when
        self.controller._pick_up_cargo()

        # then
        self.controller._parameters_reader.read_cargo_window_pick_up.assert_called()
        self.controller.filter_positions.assert_called_once()
        self.controller.task_queue.add_task.assert_called_once()

    def test_calibrate_x(self):
        # given
        motor_x_params_name = parameters_settings.DrivesNames.X
        self.controller._parameters_reader.read_calibration_speed = MagicMock(
            return_value=12.12
        )
        self.task_queue.add_task = MagicMock()
        calls = [
            call(self.controller.calibrate, [[MOTOR_X], [12.12]]),
            call(self.controller._update_current_zone, [HubZones.CALIBRATION])
        ]
        # when
        self.controller.calibrate_x()

        # then
        self.controller._parameters_reader.read_calibration_speed.assert_called_with(
            motor_x_params_name
        )
        self.task_queue.add_task.assert_has_calls(calls)
        self.task_queue.wait_until_done.assert_called()

    def test_calibrate_y(self):
        # given
        motor_y_params_name = parameters_settings.DrivesNames.Y
        self.controller._parameters_reader.read_calibration_speed = MagicMock(
            return_value=12.12
        )
        self.task_queue.add_task = MagicMock()
        calls = [
            call(self.controller.calibrate, [[MOTOR_Y], [12.12]]),
            call(self.controller._update_current_zone, [HubZones.CALIBRATION])
        ]
        # when
        self.controller.calibrate_y()

        # then
        self.controller._parameters_reader.read_calibration_speed.assert_called_with(
            motor_y_params_name
        )
        self.task_queue.add_task.assert_has_calls(calls)
        self.task_queue.wait_until_done.assert_called()

    def test_calibrate_z(self):
        # given
        motor_z_params_name = parameters_settings.DrivesNames.Z
        self.controller._parameters_reader.read_calibration_speed = MagicMock(
            return_value=12.12
        )
        self.task_queue.add_task = MagicMock()
        calls = [
            call(self.controller.calibrate, [[MOTOR_Z], [12.12]]),
            call(self.controller._update_current_zone, [HubZones.CALIBRATION])
        ]

        # when
        self.controller.calibrate_z()

        # then
        self.controller._parameters_reader.read_calibration_speed.assert_called_with(
            motor_z_params_name
        )
        self.task_queue.add_task.assert_has_calls(calls)
        self.task_queue.wait_until_done.assert_called()

    def test_go_to_battery_slot(self):
        # given
        self.controller._exit_current_zone = MagicMock()
        self.controller._parameters_reader.read_battery_slot_position = MagicMock(
            return_value='pos'
        )
        self.controller._move_motors_z_x_y_position = MagicMock()
        self.controller.task_queue.add_task = MagicMock()

        # when
        self.controller.go_to_battery_slot(1)

        # then
        self.controller._exit_current_zone.assert_called()
        self.controller._parameters_reader.read_battery_slot_position.assert_called()
        self.controller._move_motors_z_x_y_position.assert_called()
        self.controller.task_queue.add_task.assert_called()
        self.task_queue.wait_until_done.assert_called()

    def test_go_home(self):
        # given
        self.controller._exit_current_zone = MagicMock()
        self.controller.calibrate_all = MagicMock()
        self.controller._move_home = MagicMock()
        self.controller.task_queue.add_task = MagicMock()

        # when
        self.controller.go_home()

        # then
        self.controller._exit_current_zone.assert_called()
        self.controller.calibrate_all.assert_called()
        self.controller._move_home.assert_called()
        self.task_queue.wait_until_done.assert_called()

    def test_go_cargo_window_home(self):
        # given
        self.controller._exit_current_zone = MagicMock()
        self.controller._parameters_reader.read_cargo_window = MagicMock(
        )
        self.controller._move_motors_x_z_y_position = MagicMock()

        # when
        self.controller.go_cargo_window_pick_up()

        # then
        self.controller._parameters_reader.read_cargo_window.assert_called()
        self.controller._move_motors_x_z_y_position.assert_called()
        self.controller._exit_current_zone.assert_called()
        self.task_queue.wait_until_done.assert_called()

    def test_go_cargo_drone(self):
        # given
        self.controller._exit_current_zone = MagicMock()
        self.controller._parameters_reader.read_cargo_drone = MagicMock(
        )
        self.controller._move_motors_x_z_y_position = MagicMock()

        # when
        self.controller.go_cargo_drone()

        # then
        self.controller._parameters_reader.read_cargo_drone.assert_called()
        self.controller._move_motors_x_z_y_position.assert_called()
        self.controller._exit_current_zone.assert_called()
        self.task_queue.wait_until_done.assert_called()

    def test_go_battery_drone(self):
        positions = [Positions.BATTERY_DRONE_TAKE,
                     Positions.BATTERY_DRONE_PUT]

        for pos in positions:
            self.controller._exit_current_zone = MagicMock()
            self.controller._move_motors_xz_y = MagicMock()
            self.controller._parameters_reader = MagicMock()

            with self.subTest():
                self.controller.go_battery_drone(pos)

                self.controller._parameters_reader.read_battery_drone.assert_called()
                self.controller._exit_current_zone.assert_called()
                self.task_queue.wait_until_done.assert_called()
