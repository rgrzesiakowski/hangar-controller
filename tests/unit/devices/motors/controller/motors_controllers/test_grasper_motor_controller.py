from unittest.mock import MagicMock

from hangar_controller.devices.motors.controller.motors_controllers.grasper_motor_controller \
    import GrasperMotorController, MOTOR_GRASPER
from settingsd.motors import parameters_settings
from tests.testcase_wrapper import NoLoggingTestCase


class TestGrasperMotorController(NoLoggingTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.modbus_io = MagicMock()
        cls.error_handler = MagicMock()
        cls.parameters_reader = MagicMock()
        cls.task_queue = MagicMock()
        cls.parameters_reader.read_grasper_motor_placement = MagicMock(
            return_value=[[0], [1]]
        )

    def setUp(self) -> None:
        self.controller = GrasperMotorController(
            self.modbus_io, self.error_handler,
            self.task_queue
        )

    def test___init__(self):
        self.assertEqual(1, len(self.controller.motors))
        self.assertEqual(MOTOR_GRASPER, self.controller.motors[0].motor_name)

    def test_calibrate_grasper(self):
        # given
        self.controller._parameters_reader.read_calibration_speed = MagicMock(
            return_value=123
        )

        # when
        self.controller.calibrate_grasper()

        # then
        self.controller._parameters_reader.read_calibration_speed. \
            assert_called_with(
            parameters_settings.DrivesNames.GRASPER
        )
        self.task_queue.add_task.assert_called_with(
            self.controller.calibrate, [[MOTOR_GRASPER], [123]]
        )
        self.task_queue.wait_until_done.assert_called()
