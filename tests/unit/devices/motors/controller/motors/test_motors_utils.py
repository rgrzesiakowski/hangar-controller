from unittest.mock import MagicMock

from hangar_controller.devices.motors.controller.wobit_motors import motors_utils
from hangar_controller.devices.motors.controller.wobit_motors.wobit_motor import \
    WobitMotor
from settingsd.motors import modbus_constants
from tests.testcase_wrapper import NoLoggingTestCase


class TestMotorUtils(NoLoggingTestCase):
    def setUp(self) -> None:
        self.motor_lift = MagicMock(spec=WobitMotor)
        self.motor_lift.motor_name = \
            modbus_constants.Wobit.BatteriesMotorsNames.MOTOR_LIFT
        self.motor_x = MagicMock(spec=WobitMotor)
        self.motor_x.motor_name = \
            modbus_constants.Wobit.BatteriesMotorsNames.MOTOR_X
        self.motor_y = MagicMock(spec=WobitMotor)
        self.motor_y.motor_name = \
            modbus_constants.Wobit.BatteriesMotorsNames.MOTOR_Y
        self.motor_z = MagicMock(spec=WobitMotor)
        self.motor_z.motor_name = \
            modbus_constants.Wobit.BatteriesMotorsNames.MOTOR_Z
        self.motors = [self.motor_x, self.motor_y, self.motor_z,
                       self.motor_lift]

    def test_get_motor_by_name(self):
        # given
        motor_name = modbus_constants.Wobit.BatteriesMotorsNames.MOTOR_LIFT

        # when
        motor_found = motors_utils.get_motor_by_name(self.motors, motor_name)

        # then
        self.assertEqual(self.motor_lift, motor_found)

    def test_get_motor_by_name_not_found(self):
        # given
        motor_name = 'bad name'

        # when
        self.assertRaises(ValueError, motors_utils.get_motor_by_name,
                          self.motors, motor_name)

    def test_get_motors_by_names(self):
        # given
        motor_names = [modbus_constants.Wobit.BatteriesMotorsNames.MOTOR_LIFT,
                       modbus_constants.Wobit.BatteriesMotorsNames.MOTOR_X]

        # when
        motor_found = motors_utils.get_motors_by_names(self.motors, motor_names)

        # then
        self.assertEqual([self.motor_lift, self.motor_x], motor_found)
