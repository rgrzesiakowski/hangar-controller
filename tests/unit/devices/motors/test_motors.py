import time
from unittest.mock import MagicMock

from tests.testcase_wrapper import NoLoggingTestCase
from hangar_controller.devices.motors.motors import Motors
from hangar_controller.devices.motors.errors.error_handler import ErrorHandler
from hangar_controller.devices.motors.parameters.parameters_reader import \
    ParametersReader
from hangar_controller.devices.motors.controller.task_queue.task_queue import \
    TaskQueue


class TestMotors(NoLoggingTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.parameters_reader = ParametersReader()

    def setUp(self) -> None:
        self.controller = Motors(MagicMock(), MagicMock())
        self.controller._task_queue.wait_until_done = MagicMock()

    def test___init__(self):
        self.assertFalse(self.controller._stop_controller)
        self.assertIsInstance(self.controller._task_queue, TaskQueue)
        self.assertIsInstance(self.controller._error_handler, ErrorHandler)

    def test_stop(self):
        # given
        self.controller._task_queue = MagicMock()

        # when
        self.controller.stop()

        # then
        self.assertTrue(self.controller._stop_controller)
        self.controller._task_queue.exit_procedure.assert_called()

    def test_run(self):
        # given
        def stop_controller():
            self.controller.stop()

        self.controller._task_queue.exit_procedure = MagicMock()
        self.controller._task_queue.process_task = MagicMock(
            side_effect=stop_controller)

        # when
        self.controller.start()
        time.sleep(0.1)

        # then
        self.controller._task_queue.process_task.assert_called_once()

    def test_start_diagnostics(self):
        # given
        self.controller.start_diagnostics = MagicMock()

        # when
        self.controller.start_diagnostics()

        # then
        self.controller.start_diagnostics.assert_called()

    def test_stop_diagnostics(self):
        # given
        self.controller.stop_diagnostics = MagicMock()

        # when
        self.controller.stop_diagnostics()

        # then
        self.controller.stop_diagnostics.assert_called()
