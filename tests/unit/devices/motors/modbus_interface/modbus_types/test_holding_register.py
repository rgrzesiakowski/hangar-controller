from unittest.mock import MagicMock

from hangar_controller.devices.motors.modbus_interface.modbus_types.holding_register import \
    HoldingRegister
from settingsd.motors.modbus_constants import ModbusResponseStatus
from tests.testcase_wrapper import NoLoggingTestCase


class TestHoldingRegister(NoLoggingTestCase):
    def setUp(self) -> None:
        self.holding_register = HoldingRegister('open_roof', 0x0002)

    def test_init(self):
        self.assertEqual(self.holding_register._name, 'open_roof')
        self.assertEqual(self.holding_register._address, 0x0002)
        self.assertIsNone(self.holding_register.value)

    def test_update(self):
        # given
        modbus_io = MagicMock()
        modbus_io.read_holding_registers = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, [14])
        )

        # when
        status = self.holding_register.update(modbus_io)

        # then
        modbus_io.read_holding_registers.assert_called_with(
            self.holding_register._address, 1)
        self.assertEqual(status, ModbusResponseStatus.STATUS_OK)
        self.assertEqual(self.holding_register._value, 14)

    def test_write(self):
        # given
        modbus_io = MagicMock()
        modbus_io.write_holding_register = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, None)
        )

        # when
        status = self.holding_register.write(modbus_io, 4)

        # then
        modbus_io.write_holding_register.assert_called_with(
            self.holding_register._address, 4)
        self.assertEqual(status, ModbusResponseStatus.STATUS_OK)
        self.assertEqual(self.holding_register._value, 4)

    def test_write_error(self):
        # given
        modbus_io = MagicMock()
        modbus_io.write_holding_register = MagicMock(
            return_value=(ModbusResponseStatus.CONNECTION_ERROR, None)
        )

        # when
        status = self.holding_register.write(modbus_io, 13)

        # then
        modbus_io.write_holding_register.assert_called_with(
            self.holding_register._address, 13)
        self.assertEqual(status,
                         ModbusResponseStatus.CONNECTION_ERROR)
        self.assertIsNone(self.holding_register._value)
