from unittest.mock import MagicMock

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.real_register import \
    RealRegister
from settingsd.motors import modbus_constants
from settingsd.motors.modbus_constants import ModbusResponseStatus
from tests.testcase_wrapper import NoLoggingTestCase


class TestRealRegister(NoLoggingTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.modbus_io = MagicMock(spec=ModbusIO)

    def setUp(self) -> None:
        self.real_register = RealRegister(
            modbus_constants.Wobit.RealRegistersNames.Mx_POS_ACT,
            0x10020)

    def test_init(self):
        self.assertEqual(self.real_register._name, 'M_POS_ACT')
        self.assertEqual(self.real_register._address, 0x10020)
        self.assertIsNone(self.real_register.value)

    def test_update(self):
        # given
        self.modbus_io.read_real_register = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK,
                          [15729, 16722])
        )

        # when
        status = self.real_register.update(self.modbus_io)

        # then
        self.modbus_io.read_real_register.assert_called_with(
            self.real_register._address, count=1)
        self.assertEqual(status, ModbusResponseStatus.STATUS_OK)
        self.assertAlmostEqual(self.real_register._value, 13.14, places=1)

    def test_read_values(self):
        # given
        self.modbus_io.read_real_register = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK,
                          [15729, 16722, 15729, 16722])
        )

        # when
        status, values = self.real_register.read_values(self.modbus_io, count=2)

        # then
        self.modbus_io.read_real_register.assert_called_with(
            self.real_register.address, count=2
        )
        self.assertAlmostEqual(values[0], 13.14, places=2)
        self.assertAlmostEqual(values[1], 13.14, places=2)

    def test_read_values_connection_error(self):
        # given
        self.modbus_io.read_real_register = MagicMock(
            return_value=(ModbusResponseStatus.CONNECTION_ERROR,
                          None),
        )

        # when
        status, values = self.real_register.read_values(self.modbus_io, count=2)

        # then
        self.modbus_io.read_real_register.assert_called_with(
            self.real_register.address, count=2
        )
        self.assertEqual(ModbusResponseStatus.CONNECTION_ERROR, status)
        self.assertIsNone(values)

    def test_write(self):
        # given
        modbus_io = MagicMock()
        modbus_io.write_real_register = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, None)
        )

        # when
        status = self.real_register.write(modbus_io, 13.14)

        # then
        modbus_io.write_real_register.assert_called_with(
            self.real_register.address, 13.14
        )

        self.assertEqual(status, ModbusResponseStatus.STATUS_OK)
        self.assertEqual(self.real_register._value, 13.14)

    def test_write_error(self):
        # given
        modbus_io = MagicMock()
        modbus_io.write_real_register = MagicMock(
            return_value=(ModbusResponseStatus.CONNECTION_ERROR, None)
        )

        # when
        status = self.real_register.write(modbus_io, 13.14)

        # then
        modbus_io.write_real_register.assert_called_with(
            self.real_register._address, 13.14)
        self.assertEqual(status, ModbusResponseStatus.CONNECTION_ERROR)
        self.assertIsNone(self.real_register._value)
