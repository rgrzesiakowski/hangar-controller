from unittest.mock import MagicMock

from pymodbus.exceptions import ConnectionException

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from settingsd.motors import modbus_constants
from tests.testcase_wrapper import NoLoggingTestCase


class TestModbusUtils(NoLoggingTestCase):
    def test_try_reading_bits_no_bits(self):
        # given
        response = None

        # when

        # then
        with self.assertRaises(ConnectionException):
            modbus_utils.try_reading_bits(response)

    def test_try_reading_bits(self):
        # given
        response = MagicMock()
        response.bits = [True, False]

        # when
        bits = modbus_utils.try_reading_bits(response)

        # then
        self.assertEqual(bits, [True, False])

    def test_try_reading_registers_no_field(self):
        # given
        response = None

        # when

        # then
        with self.assertRaises(ConnectionException):
            modbus_utils.try_reading_registers(response)

    def test_try_reading_registers(self):
        # given
        response = MagicMock()
        response.registers = [1, 2, 3]

        # when
        registers = modbus_utils.try_reading_registers(response)

        # then
        self.assertEqual(registers, [1, 2, 3])

    def test_check_response_status_ok(self):
        # given
        status = modbus_constants.ModbusResponseStatus.STATUS_OK

        # when
        check = modbus_utils.check_response_status(status)

        # then
        self.assertTrue(check)

    def test_check_response_status_not_ok(self):
        # given
        status = modbus_constants.ModbusResponseStatus.CONNECTION_ERROR

        # when
        check = modbus_utils.check_response_status(status)

        # then
        self.assertFalse(check)

    def test_filter_response_by_status_ok(self):
        # given
        status = modbus_constants.ModbusResponseStatus.STATUS_OK

        # when
        response = modbus_utils.filter_response_by_status(status, True)

        # then
        self.assertTrue(response)

    def test_filter_response_by_status_not_ok(self):
        # given
        status = modbus_constants.ModbusResponseStatus.CONNECTION_ERROR

        # when
        response = modbus_utils.filter_response_by_status(status, True)

        # then
        self.assertIsNone(response)
