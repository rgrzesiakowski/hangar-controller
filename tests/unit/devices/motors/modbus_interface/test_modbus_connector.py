from unittest.mock import MagicMock

from hangar_controller.devices.motors.modbus_interface.modbus_connector import ModbusConnector
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from settingsd import devices_modbus_connection
from settingsd.motors import modbus_constants
from tests.testcase_wrapper import NoLoggingTestCase


class TestModbusConnector(NoLoggingTestCase):
    def setUp(self) -> None:
        self.modbus_comm = ModbusConnector(
            devices_modbus_connection.WOBIT_BATTERIES_CONNECTION,
            modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START
        )

    def test_init(self):
        self.assertIsInstance(self.modbus_comm.modbus_io, ModbusIO)
        self.assertFalse(self.modbus_comm._connected)

    def test_modbus_io(self):
        self.assertEqual(self.modbus_comm._modbus_io,
                         self.modbus_comm.modbus_io)

    def test_connect(self):
        # given
        self.modbus_comm._modbus_io.connect_and_test = MagicMock()

        # when
        self.modbus_comm.connect()

        # then
        self.modbus_comm._modbus_io.connect_and_test.assert_called()

    def test_check_connection(self):
        # given
        self.modbus_comm._modbus_io.check_connection = MagicMock()

        # when
        self.modbus_comm.check_connection()

        # then
        self.modbus_comm._modbus_io.check_connection.assert_called()
