from typing import Type
from unittest.mock import MagicMock

from hangar_controller.devices.motors.modbus_interface.device_data_holders.abstract_modbus_controller \
    import AbstractModbusController
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress
from hangar_controller.devices.motors.modbus_interface.modbus_types.discrete_input import \
    DiscreteInput
from hangar_controller.devices.motors.modbus_interface.modbus_types.discrete_output import \
    DiscreteOutput
from hangar_controller.devices.motors.modbus_interface.modbus_types.holding_register import \
    HoldingRegister
from settingsd.motors import modbus_constants
from settingsd.motors.modbus_constants import ModbusResponseStatus
from tests.testcase_wrapper import NoLoggingTestCase


class AbstractModbusControllerImplementation(
        AbstractModbusController):  # pragma: no cover

    def __init__(self, address_list: list,
                 address_type: Type[AbstractIOAddress],
                 modbus_io: ModbusIO):
        super().__init__(address_list, address_type, modbus_io)


class TestAbstractModbusController(NoLoggingTestCase):
    def setUp(self) -> None:
        self.modbus_io = MagicMock()
        self.modbus_controller = AbstractModbusControllerImplementation(
            modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES,
            HoldingRegister,
            self.modbus_io)

    def test_init(self):
        self.assertIsInstance(self.modbus_controller.data_list, list)

    def test_check_io_address_writable_address_none(self):
        # given
        io_address = None

        # when
        status = self.modbus_controller.check_io_address_writable(io_address)

        # then
        self.assertEqual(ModbusResponseStatus.VALUE_ERROR, status)

    def test_check_io_address_writable_read_only(self):
        # given
        name, address = \
            modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES[0]
        io_address = DiscreteInput(name, address)

        # when
        status = self.modbus_controller.check_io_address_writable(io_address)

        # then
        self.assertEqual(ModbusResponseStatus.VALUE_ERROR, status)

    def test_check_io_address_writable(self):
        # given
        name, address = \
            modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES[0]
        io_address = DiscreteOutput(name, address)

        # when
        status = self.modbus_controller.check_io_address_writable(io_address)

        # then
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)

    def test_write_to_io_address_error(self):
        # given
        io_address = DiscreteOutput('s', 0)
        self.modbus_controller.check_io_address_writable = MagicMock(
            return_value=ModbusResponseStatus.VALUE_ERROR
        )
        io_address.write = MagicMock()

        # when
        status = self.modbus_controller.write_to_io_address(io_address, 4)

        # then
        self.assertEqual(ModbusResponseStatus.VALUE_ERROR, status)
        io_address.write.assert_not_called()

    def test_write_to_io_address(self):
        # given
        io_address = DiscreteOutput('s', 0)
        io_address.write = MagicMock(
            return_value=ModbusResponseStatus.STATUS_OK
        )

        # when
        status = self.modbus_controller.write_to_io_address(io_address, 4)

        # then
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        io_address.write.assert_called_with(self.modbus_io, 4)

    def test_write_by_channel_name(self):
        # given
        io_address = DiscreteOutput('s', 0)
        self.modbus_controller.get_io_address_by_name = MagicMock(
            return_value=io_address
        )
        self.modbus_controller.write_to_io_address = MagicMock(
            return_value=ModbusResponseStatus.STATUS_OK
        )

        # when
        status = self.modbus_controller.write_by_channel_name('s', 4)

        # then
        self.modbus_controller.get_io_address_by_name.assert_called_with('s')
        self.modbus_controller.write_to_io_address.assert_called_with(
            io_address, 4
        )
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)

    def test_write_by_address(self):
        # given
        io_address = DiscreteOutput('s', 0)
        self.modbus_controller.get_io_address_by_address = MagicMock(
            return_value=io_address
        )
        self.modbus_controller.write_to_io_address = MagicMock(
            return_value=ModbusResponseStatus.STATUS_OK
        )

        # when
        status = self.modbus_controller.write_by_address(0, 4)

        # then
        self.modbus_controller.get_io_address_by_address. \
            assert_called_with(0)
        self.modbus_controller.write_to_io_address.assert_called_with(
            io_address, 4
        )
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
