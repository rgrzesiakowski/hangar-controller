from unittest.mock import MagicMock

from hangar_controller.devices.motors.modbus_interface.device_data_holders.real_register_controller \
    import RealRegisterController
from hangar_controller.devices.motors.modbus_interface.modbus_types.real_register import \
    RealRegister
from settingsd.motors import modbus_constants
from tests.testcase_wrapper import NoLoggingTestCase


class TestRealRegisterReader(NoLoggingTestCase):
    def setUp(self) -> None:
        self.modbus_io = MagicMock()
        self.modbus_data = RealRegisterController(
            self.modbus_io,
            modbus_constants.WobitMotor1Addresses.REAL_REGISTERS)

    def test_init(self):
        self.assertIsInstance(self.modbus_data.data_list, list)
        self.assertIsInstance(self.modbus_data.data_list[0], RealRegister)
        self.assertIsInstance(self.modbus_data.modbus_io, MagicMock)
