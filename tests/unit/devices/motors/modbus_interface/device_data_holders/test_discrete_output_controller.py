from hangar_controller.devices.motors.modbus_interface.device_data_holders.discrete_output_controller \
    import DiscreteOutputController
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.discrete_output import \
    DiscreteOutput
from settingsd import devices_modbus_connection
from settingsd.motors import modbus_constants
from tests.testcase_wrapper import NoLoggingTestCase


class TestDiscreteOutputController(NoLoggingTestCase):
    def setUp(self) -> None:
        self.modbus_io = ModbusIO(
            devices_modbus_connection.WOBIT_BATTERIES_CONNECTION,
            modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES[0][
                1]
        )
        self.modbus_controller = DiscreteOutputController(
            self.modbus_io,
            modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES)

    def test_init(self):
        self.assertIsInstance(self.modbus_controller.data_list, list)
        self.assertIsInstance(self.modbus_controller.data_list[0],
                              DiscreteOutput)
        self.assertIsInstance(self.modbus_controller.modbus_io, ModbusIO)
