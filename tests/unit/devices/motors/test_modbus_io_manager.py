from unittest.mock import MagicMock

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.modbus_io_manager import ModbusIOManager
from tests.testcase_wrapper import NoLoggingTestCase


class TestModbusIOManager(NoLoggingTestCase):
    def setUp(self) -> None:
        self.parameters_reader = MagicMock()
        self.manager = ModbusIOManager()

    def test___init__(self):
        self.assertIsNone(self.manager._modbus_positioning_io, ModbusIO)
        self.assertIsNone(self.manager._modbus_batteries_io, ModbusIO)

    def test_batteries(self):
        self.assertEqual(self.manager.batteries,
                         self.manager._modbus_batteries_io)

    def test_positioning(self):
        self.assertEqual(self.manager.positioning,
                         self.manager._modbus_positioning_io)

    def test_connect_batteries_wobit(self):
        # given
        self.manager._modbus_batteries_io = MagicMock()
        self.manager._modbus_batteries_io.connect_and_test = MagicMock(
            return_value='status')

        # when
        status = self.manager.connect_batteries_wobit()

        # then
        self.assertEqual('status', status)

    def test_connect_positioning_wobit(self):
        # given
        self.manager._modbus_positioning_io = MagicMock()
        self.manager._modbus_positioning_io.connect_and_test = MagicMock(
            return_value='status')

        # when
        status = self.manager.connect_positioning_wobit()

        # then
        self.assertEqual('status', status)
