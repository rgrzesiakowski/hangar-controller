from unittest import mock
import typing as tp

from hangar_controller.put_and_take_to_hangar_backend.abstract_backend import \
    AbstractBackend
from tests.testcase_wrapper import NoLoggingTestCase


class AbstractBackendImplementation(AbstractBackend):
    def _cargo_is_invalid_callback(self):
        pass

    def _cargo_is_too_heavy_callback(self):
        pass

    def _user_should_insert_cargo_callback(self):
        pass

    def _user_should_taken_out_cargo_callback(self):
        pass


class TestAbstractBackend(NoLoggingTestCase):
    def setUp(self) -> None:
        self.test_abstract_backend = AbstractBackendImplementation()

    def test_terminating(self):
        self.assertFalse(self.test_abstract_backend._terminating)

        self.test_abstract_backend.terminate()

        self.assertTrue(self.test_abstract_backend._terminating)
