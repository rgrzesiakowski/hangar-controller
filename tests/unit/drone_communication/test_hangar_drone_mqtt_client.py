import typing as tp
from unittest import mock
from unittest.mock import MagicMock

from paho.mqtt.client import Client
from satella.coding.concurrent import CallableGroup

from hangar_controller.drone_communication import HangarDroneMQTTClient
from hangar_controller.drone_communication.messages import BaseDroneMessage
from tests.testcase_wrapper import NoLoggingTestCase


class TestHangarDroneMQTTClient(NoLoggingTestCase):
    def setUp(self) -> None:
        self.mqtt_client_patcher = mock.patch('paho.mqtt.client.Client.__new__')
        self.mqtt_client_mock = self.mqtt_client_patcher.start()
        self.mqtt_client_mock.return_value = MagicMock(spec=Client)

        self.message_validators_patcher = mock.patch(
            'satella.coding.concurrent.CallableGroup.__new__')
        self.message_validators_mock = self.message_validators_patcher.start()
        self.message_validators_mock.return_value = MagicMock(spec=CallableGroup)

        self.message_handler_patcher = mock.patch('satella.coding.concurrent.CallableGroup.__new__')
        self.message_handler_mock = self.message_handler_patcher.start()
        self.message_handler_mock.return_value = MagicMock(spec=CallableGroup)

        self.hangar_drone_mqtt_client = HangarDroneMQTTClient()
        self.hangar_drone_mqtt_client._publish_message = mock.Mock()

    def test_send_messages(self):
        test_base_drone_message = MagicMock(spec=BaseDroneMessage)
        test_base_drone_message.get_data = lambda: {'command': 'test-command'}

        self.hangar_drone_mqtt_client.send_message(test_base_drone_message)
        self.hangar_drone_mqtt_client._publish_message.assert_called()

    def test_add_validator(self):
        # given
        mock_validator = MagicMock(spec=tp.Callable)

        # when
        self.hangar_drone_mqtt_client.add_validator(mock_validator)

        # then
        self.hangar_drone_mqtt_client.message_validators.add.assert_called_with(mock_validator)

    def test_add_handler(self):
        # given
        mock_handler = MagicMock(spec=tp.Callable)

        # when
        self.hangar_drone_mqtt_client.add_handler(mock_handler)

        # then
        self.hangar_drone_mqtt_client.message_handlers.add.assert_called_with(mock_handler)
