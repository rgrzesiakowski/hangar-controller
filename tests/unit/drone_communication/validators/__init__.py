from unittest import mock

from hangar_controller.drone_communication.validators import *
from hangar_controller.drone_communication.validators import _validate_schema
from tests.testcase_wrapper import NoLoggingTestCase


class TestHandlers(NoLoggingTestCase):
    def setUp(self) -> None:
        self.generic_command = {
            "command": 'generic_command_name',
            "parameters": {}
        }

    def test_validate_schema_for_command(self):
        @_validate_schema(EXPECTED_COMMAND_SCHEMA, False)
        def validate_command(cmd: dict) -> bool:
            return True

        self.assertTrue(validate_command(self.generic_command))

        self.generic_command['command'] = 1
        self.assertFalse(validate_command(self.generic_command))

        del self.generic_command['command']
        self.assertFalse(validate_command(self.generic_command))

    def test_validate_schema_for_parameters(self):
        @_validate_schema({'cargo_id': str})
        def validate_command(cmd: dict) -> bool:
            return True

        self.generic_command["parameters"]['cargo_id'] = 'test-cargo-id'
        self.assertTrue(validate_command(self.generic_command))

        del self.generic_command["parameters"]['cargo_id']
        self.assertFalse(validate_command(self.generic_command))

    @mock.patch('hangar_controller.drone_communication.validators.KNOWN_COMMAND',
                ('generic_command_name',))
    def test_validate_known_command_should_return_true(self):
        self.assertTrue(validate_is_known_command(self.generic_command))

    @mock.patch('hangar_controller.drone_communication.validators.KNOWN_COMMAND',
                ('invalid_generic_command_name',))
    def test_validate_unknown_command_should_return_false(self):
        self.assertFalse(validate_is_known_command(self.generic_command))

    def test_validate_incorrect_command_should_return_false(self):
        self.assertTrue(validate_command_schema(self.generic_command))
