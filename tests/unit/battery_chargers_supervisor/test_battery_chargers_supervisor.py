import logging

from unittest.mock import patch, Mock, MagicMock

from hangar_controller.battery_chargers_supervisor import \
    BatteryChargersSupervisor
from hangar_controller.battery_chargers_supervisor.battery_chargers_supervisor import \
    NoChargerError
from hangar_controller.devices.drivers.drivers_controller import \
    DriversController
from hangar_controller.executor import HangarData
from settingsd.drivers.stm_master_charger_settings import MasterChargerStates

from tests.testcase_wrapper import NoLoggingTestCase

logger = logging.getLogger('__name__')


class TestBatteryChargersSupervisor(NoLoggingTestCase):
    def setUp(self) -> None:
        dc = DriversController(Mock(), Mock())
        dc._master_charger.get_available_chargers = Mock(
            return_value=(Mock(), ['0', '1', '2', '4', '5'])
        )
        azure_patcher = patch('hangar_controller.battery_chargers_supervisor.battery_chargers_supervisor.AzureIoTHubClient',
                              return_value=MagicMock())
        azure_patcher.start()

        with patch.object(BatteryChargersSupervisor, 'update_battery_data'):
            self.bcs = BatteryChargersSupervisor()

    def tearDown(self) -> None:
        BatteryChargersSupervisor.__it__ = None
        HangarData.__it__ = None

        patch.stopall()

    def test_update_battery_data(self):
        self.bcs._master_charger.get_states = Mock(
            side_effect=[(Mock(), {'battery': 'charging'}),  # 0
                         (Mock(), {'battery': 'charging'}),  # 1
                         (Mock(), {'battery': 'unplugged'}),  # 2
                         (Mock(), {'battery': 'charged'}),  # 4
                         (Mock(), {'battery': 'charged'})]  # 5
        )
        self.bcs._master_charger.get_telemetry = Mock(
            side_effect=[(Mock(), {'battery': {'uid': 'uid1:uid2:uid3'}}),  # 0
                         (Mock(), {'battery': {'uid': 'uid4:uid5:uid6'}}),  # 1
                         (Mock(), {'battery': {'uid': 'uid7:uid8:uid9'}}),  # 4
                         (Mock(), {'battery': {'uid': 'uid10:uid11:uid12'}})]  # 5
        )
        expected_batteries = ['uid1:uid2:uid3', 'uid4:uid5:uid6',
                              'uid7:uid8:uid9', 'uid10:uid11:uid12']
        expected_charged_slots = ['4', '5']

        self.bcs.update_battery_data()

        self.assertListEqual(expected_charged_slots,
                             self.bcs.charged_batteries_slots)
        with HangarData():
            self.assertEqual(
                len(expected_charged_slots), HangarData().available_battery)
            self.assertListEqual(expected_batteries, HangarData().batteries)

    def test_get_charged_battery_should_return_charger_and_battery_from_charged_batteries(self):
        self.bcs.charged_batteries_slots = ['0', '2', '4']  # should get last item
        expected_telemetry = {
            'charger': {
                'bms_temperature': [293, 293, 293],
                'charge_current': [3.1, 3.1, 3.1]
            },
            'battery': {
                'uid': "uid1:uid2:uid3",
                'temperature': [290, 290, 290],
                'voltage': [25.2, 25.2, 25.2]
            }
        }
        self.bcs._master_charger.get_telemetry = Mock(return_value=(Mock(), expected_telemetry))

        expected_output = ('4', expected_telemetry['battery']['uid'])

        output = self.bcs.get_charged_battery()

        self.bcs._master_charger.get_telemetry.assert_called_once_with('4')
        self.assertTupleEqual(expected_output, output)

    def test_get_charger_id_by_state_should_return_charger_id_for_first_matched_state_when_called(self):
        self.bcs._master_charger.get_states = Mock(
            side_effect=[(Mock(), {'battery': 'charging'}),  # 0
                         (Mock(), {'battery': 'charging'}),  # 1
                         (Mock(), {'battery': 'unplugged'}),  # 2
                         (Mock(), {'battery': 'charged'}),  # 4
                         (Mock(), {'battery': 'charged'})]  # 5
        )

        expected_output = '4'

        output = self.bcs.get_charger_id_by_state(MasterChargerStates.CHARGED)

        self.assertEqual(self.bcs._master_charger.get_states.call_count, 4)
        self.assertEqual(expected_output, output)

    def test_get_charger_id_by_state_should_raise_NoChargerError_when_no_charger_is_found(self):
        self.bcs._master_charger.get_states = Mock(
            side_effect=[(Mock(), {'battery': 'charging'}) for _ in range(5)]
        )

        with self.assertRaises(NoChargerError):
            self.bcs.get_charger_id_by_state(MasterChargerStates.CHARGED)

    def test_add_battery_to_hangar_should_add_battery_uid_to_hangar_data_when_called(self):
        with HangarData():
            self.assertEqual(0, len(HangarData().batteries))

        self.bcs._master_charger.get_telemetry = Mock(
            return_value=(Mock(), {'battery': {'uid': 'uid1:uid2:uid3'}})
        )

        output = self.bcs.add_battery_to_slot('0')

        with HangarData():
            self.assertIn('uid1:uid2:uid3', HangarData().batteries)
            self.assertEqual(1, len(HangarData().batteries))
        self.assertEqual('uid1:uid2:uid3', output)

    def test_add_battery_to_hangar_should_NOT_add_battery_uid_if_its_already_registered(self):
        with HangarData():
            self.assertEqual(0, len(HangarData().batteries))

        self.bcs._master_charger.get_telemetry = Mock(
            return_value=(Mock(), {'battery': {'uid': 'uid1:uid2:uid3'}})
        )

        self.bcs.add_battery_to_slot('0')
        output = self.bcs.add_battery_to_slot('0')  # second call shouldn't make duplicates

        self.assertEqual('uid1:uid2:uid3', output)
        with HangarData():
            batteries = HangarData().batteries
        self.assertIn('uid1:uid2:uid3', batteries)
        self.assertEqual(1, len(batteries))

    def test_remove_battery_from_hangar_should_remove_battery_from_hangar_data(self):
        with HangarData():
            HangarData().batteries = ['uid1:uid2:uid3', 'uid4:uid5:uid6']

        self.bcs.remove_battery_from_hangar('uid4:uid5:uid6')

        with HangarData():
            self.assertNotIn('uid4:uid5:uid6', HangarData().batteries)
