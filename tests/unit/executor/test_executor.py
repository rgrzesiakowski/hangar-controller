import threading
import time
import unittest
import logging

from hangar_controller.azure_communication import AzureIoTHubClient
from hangar_controller.executor import ExecutorThread, CannotRun
from hangar_controller.executor.actions import BaseAction
from hangar_controller.executor.state_machine import StateMachine

logger = logging.getLogger(__name__)


class ExecutorTestCase(unittest.TestCase):
    """
    A test case that starts the Executor for each test, and for each tests shuts it down
    """

    def setUp(self):
        StateMachine(AzureIoTHubClient())
        threading.Thread.__init__(ExecutorThread())
        ExecutorThread()._terminating = False
        ExecutorThread().start()

    def tearDown(self):
        ExecutorThread().terminate().join()


class TestExecutor(ExecutorTestCase):

    def test_exec_simple_action(self):
        class MyAction(BaseAction):
            def run(self):
                return 2

            def can_run(self) -> bool:
                return True

        a = MyAction()
        b = MyAction()
        a.schedule()
        b.schedule()
        self.assertEqual(b.result(10.0), 2)

    def test_wait_for_her_concurrently(self):
        class WaitForHer(BaseAction):

            def __init__(self, lock):
                super(WaitForHer, self).__init__()
                self.lock = lock

            def run(self):
                time.sleep(2)
                self.lock.acquire()

            def can_run(self) -> bool:
                return True

        class Action(BaseAction):
            CAN_EXECUTE_CONCURRENTLY_WITH = ['WaitForHer']

            def __init__(self, lock):
                super(Action, self).__init__()
                self.lock = lock

            def run(self):
                time.sleep(2)
                self.lock.release()

            def can_run(self) -> bool:
                return True

        l = threading.Lock()
        l.acquire()

        wfh = WaitForHer(l)
        act = Action(l)

        wfh.schedule()
        act.schedule()
        wfh.result(10.0)

    def test_do_not_run_concurrently(self):
        class WaitForHer(BaseAction):
            def __init__(self, times):
                super(WaitForHer, self).__init__()
                self.times = times

            def run(self):
                self.times['WaitForHer_end'] = time.time()

            def can_run(self) -> bool:
                return True

        class Action(BaseAction):
            def __init__(self, times):
                super(Action, self).__init__()
                self.times = times

            def run(self):
                self.times['Action_START'] = time.time()

            def can_run(self) -> bool:
                return True

        times = {
            "WaitForHer_end": None,
            "Action_START": None
        }

        wfh = WaitForHer(times)
        act = Action(times)

        wfh.schedule()
        act.schedule()

        act.result(10.0)

        self.assertGreaterEqual(
            times['Action_START'], times['WaitForHer_end']
        )

    def test_runs_concurrently_always(self):
        class WaitForHer(BaseAction):

            def __init__(self, lock):
                super(WaitForHer, self).__init__()
                self.lock = lock

            def run(self):
                time.sleep(2)
                self.lock.acquire()

            def can_run(self) -> bool:
                return True

        class Action(BaseAction):
            CAN_EXECUTE_CONCURRENTLY_WITH = [True]

            def run(self):
                time.sleep(2)

            def can_run(self) -> bool:
                return True

        l = threading.Lock()
        l.acquire()

        wfh = WaitForHer(l)
        act = Action()

        wfh.schedule()
        act.schedule()

        act.result(10.0)
        l.release()
        wfh.result(10.0)

    def test_cant_run(self):
        class Action(BaseAction):

            def can_run(self) -> bool:
                return False

            def run(self):
                pass

        act = Action()

        act.schedule()
        self.assertRaises(CannotRun, act.result)
