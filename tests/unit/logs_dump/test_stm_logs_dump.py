import datetime
import heapq
import time

from unittest.mock import patch, call, mock_open, MagicMock, PropertyMock, Mock
from serial.serialposix import Serial, SerialException

from tests.testcase_wrapper import NoLoggingTestCase
from hangar_controller.logs_dump import STMLogsDump
from settingsd.logs_dump.stm_dump_settings import NEXT_CONN_RETRY_DELAY, \
    BAUD_RATE, READ_TIMEOUT

TESTED_MODULE = 'hangar_controller.logs_dump.stm_logs_dump'


class TestSTMLogsDump(NoLoggingTestCase):
    def setUp(self) -> None:
        self.dump = STMLogsDump()

        devices_patch = patch(f'{TESTED_MODULE}.DEVICES',
                              ['dev1', 'dev2', 'dev3'])
        devices_patch.start()

        dump_folder_patch = patch(f'{TESTED_MODULE}.DUMP_FOLDER',
                                  'TEST_FOLDER/')
        dump_folder_patch.start()

        dump_size_patch = patch(f'{TESTED_MODULE}.DUMP_SIZE', 4)
        dump_size_patch.start()

        self.serial_patch = patch(f'{TESTED_MODULE}.Serial')
        self.serial_mock = self.serial_patch.start()

        logger_patch = patch(f'{TESTED_MODULE}.logger')
        self.logger_mock = logger_patch.start()

        open_patch = patch('builtins.open', mock_open())
        open_patch.start()

    def tearDown(self) -> None:
        STMLogsDump.__it__ = None  # patch singleton
        patch.stopall()

    def test_loop_tries_connect_to_disconnected_devices(self):
        self.serial_mock.side_effect = SerialException

        self.dump.loop()

        calls = [call('/dev/dev1', BAUD_RATE, timeout=READ_TIMEOUT),
                 call('/dev/dev2', BAUD_RATE, timeout=READ_TIMEOUT),
                 call('/dev/dev3', BAUD_RATE, timeout=READ_TIMEOUT)]
        self.serial_mock.assert_has_calls(calls)

    def test_loop_data_is_read_from_connected_devices(self):
        self.dump._serial['dev1'] = None
        self.dump._serial['dev2'] = self.serial_mock
        self.dump._serial['dev3'] = self.serial_mock

        self.dump.loop()

        self.assertEqual(self.serial_mock.read_until.call_count, 2)
        self.serial_mock.read_until.assert_called_with(b'\r\n')

    def test_cleanup_dump_all_logs(self):
        self.dump._logs_heap = [(1, 'test', 'LOG'), (2, 'test2', 'LOG2')]

        self.dump.cleanup()

        calls = [call('TEST_FOLDER/stm-19700101.log', 'a'),
                 call().__enter__(),
                 call().write('1 test: LOG\n'),
                 call().write('2 test2: LOG2\n'),
                 call().__exit__(None, None, None)]
        open.assert_has_calls(calls)

    def test_cleanup_close_all_connections(self):
        self.dump._serial['dev1'] = None
        self.dump._serial['dev2'] = self.serial_mock
        self.dump._serial['dev3'] = self.serial_mock

        self.dump.cleanup()

        self.assertEqual(self.serial_mock.close.call_count, 2)

    def test_connection_retry_delays_create_chain(self):
        self.assertIn(0, NEXT_CONN_RETRY_DELAY)
        for value in NEXT_CONN_RETRY_DELAY.values():
            with self.subTest(value=value):
                self.assertIn(value, NEXT_CONN_RETRY_DELAY)

    def test_connect_retry_is_not_run_too_early(self):
        self.serial_mock.side_effect = SerialException

        self.dump._connect('test')  # 1st try
        self.dump._connect('test')  # retry ignored

        self.serial_mock.assert_called_once()

    def test_connect_retry_is_run_after_delay(self):
        self.serial_mock.side_effect = SerialException

        with patch(f'{TESTED_MODULE}.NEXT_CONN_RETRY_DELAY',
                   {0: 0.2, 0.2: 0.2}):
            self.dump._connect('test')  # 1st try
            time.sleep(0.25)
            self.dump._connect('test')  # retry

        self.assertEqual(self.serial_mock.call_count, 2)

    def test_connect_retry_delays_adjust_in_time(self):
        self.serial_mock.side_effect = SerialException

        with patch(f'{TESTED_MODULE}.NEXT_CONN_RETRY_DELAY',
                   {0: 0.1, 0.1: 10, 10: 10}):
            self.dump._connect('test')  # 1st try
            self.dump._connect('test')  # retry ignored
            time.sleep(0.2)
            self.dump._connect('test')  # retry
            time.sleep(0.2)
            self.dump._connect('test')  # retry ignored
            time.sleep(0.2)
            self.dump._connect('test')  # retry ignored

        self.assertEqual(self.serial_mock.call_count, 2)

    def test_connect_retries_are_independent(self):
        self.dump._connect('test1')
        self.dump._connect('test2')
        self.assertEqual(self.serial_mock.call_count, 2)

    def test_read_process_correct_logs(self):
        mock_msg = b'(42) [ IN  / m_m ] >> c i_msg [ m/1/_ ]\r\n'
        out_msg = '42 test: [ IN  / m_m ] >> c i_msg [ m/1/_ ]\n'

        self.dump._serial['test'] = self.serial_mock
        self.serial_mock.in_waiting.return_value = len(mock_msg)
        self.serial_mock.read_until.return_value = mock_msg

        self.dump._read_logs('test')

        self.serial_mock.read_until.assert_called()
        self.logger_mock.error.assert_not_called()

        self.dump.cleanup()

        open.return_value.__enter__().write.assert_called_once_with(out_msg)

    def test_read_ignore_colors(self):
        mock_msg = b'(42) [\x1b[31m IN \x1b[0m / m ] >> c i_msg [ m/1/_ ]\r\n'
        out_msg = '42 test: [ IN  / m ] >> c i_msg [ m/1/_ ]\n'

        self.dump._serial['test'] = self.serial_mock
        self.serial_mock.in_waiting.return_value = len(mock_msg)
        self.serial_mock.read_until.return_value = mock_msg

        self.dump._read_logs('test')

        self.serial_mock.read_until.assert_called()
        self.logger_mock.error.assert_not_called()

        self.dump.cleanup()

        open.return_value.__enter__().write.assert_called_once_with(out_msg)

    def test_read_process_incorrect_logs(self):
        mock_msg = b'STH STH WRONG\r\n'

        self.dump._serial['test'] = self.serial_mock
        self.serial_mock.in_waiting.return_value = len(mock_msg)
        self.serial_mock.read_until.return_value = mock_msg

        self.dump._read_logs('test')

        self.serial_mock.read_until.assert_called()
        self.logger_mock.error.assert_called()

        self.dump.cleanup()

        open.return_value.__enter__().write.assert_not_called()

    def test_read_fails_gracefully_when_interrupted(self):
        self.serial_patch.stop()
        self.dump._serial['test'] = Serial()

        with patch(f'{TESTED_MODULE}.Serial.in_waiting',
                   PropertyMock(side_effect=IOError)):
            self.dump._read_logs('test')

        self.serial_mock.read_until.assert_not_called()
        self.logger_mock.error.assert_called()

        self.dump.cleanup()

        open.return_value.__enter__().write.assert_not_called()

    def test_loop_dumps_single_day_logs_when_batch_size_reached(self):
        logs = [(1, 'test', 'log payload 1'),
                (4, 'test', 'log payload 4'),
                (3, 'test', 'log payload 3'),
                (2, 'test', 'log payload 2')]
        for log in logs:
            heapq.heappush(self.dump._logs_heap, log)

        self.dump.loop()

        calls = [call('TEST_FOLDER/stm-19700101.log', 'a'),
                 call().__enter__(),
                 call().write('1 test: log payload 1\n'),
                 call().write('2 test: log payload 2\n'),
                 call().write('3 test: log payload 3\n'),
                 call().write('4 test: log payload 4\n'),
                 call().__exit__(None, None, None)]
        open.assert_has_calls(calls)

    def test_loop_dumps_multiple_days_logs_when_batch_size_reached(self):
        day_length = int(datetime.timedelta(days=1).total_seconds())

        logs = [(1, 'test', 'log payload 1'),
                (2, 'test', 'log payload 2'),
                (day_length + 1, 'test', 'log payload 3'),
                (2 * day_length + 1, 'test', 'log payload 4')]
        for log in logs:
            heapq.heappush(self.dump._logs_heap, log)

        self.dump.loop()

        calls = [call('TEST_FOLDER/stm-19700101.log', 'a'),
                 call().__enter__(),
                 call().write('1 test: log payload 1\n'),
                 call().write('2 test: log payload 2\n'),
                 call().__exit__(None, None, None),
                 call(f'TEST_FOLDER/stm-19700102.log', 'a'),
                 call().__enter__(),
                 call().write(f'{day_length + 1} test: log payload 3\n'),
                 call().__exit__(None, None, None),
                 call(f'TEST_FOLDER/stm-19700103.log', 'a'),
                 call().__enter__(),
                 call().write(f'{2 * day_length + 1} test: log payload 4\n'),
                 call().__exit__(None, None, None)]
        open.assert_has_calls(calls)
