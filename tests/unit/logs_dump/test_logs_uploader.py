from datetime import datetime
from time import sleep
from unittest.mock import Mock, MagicMock, patch, call

from azure.storage.blob import BlockBlobService

from tests.testcase_wrapper import NoLoggingTestCase
from hangar_controller.logs_dump.logs_uploader import LogsUploader

TESTED_MODULE = 'hangar_controller.logs_dump.logs_uploader'


class TestLogsUploader(NoLoggingTestCase):
    def setUp(self) -> None:
        patch(f'{TESTED_MODULE}.AZURE_DEVICE_ID', 'hangar-1').start()
        patch(f'{TESTED_MODULE}.AZURE_STORAGE_ACCOUNT', 'ASA').start()
        patch(f'{TESTED_MODULE}.AZURE_STORAGE_KEY', 'ASK').start()
        patch(f'{TESTED_MODULE}.AZURE_STORAGE_KWARGS',
              {'is_emulated': True}).start()
        patch(f'{TESTED_MODULE}.AZURE_LOGS_CONTAINER', 'logs').start()
        patch(f'{TESTED_MODULE}.SUPERVISED_FOLDERS', ['log1/', 'log2']).start()
        patch(f'{TESTED_MODULE}.TERMINATION_WATCHDOG_INTERVAL', 0.5).start()
        patch(f'{TESTED_MODULE}.UPLOAD_RETRIES_NUM', 3).start()
        patch(f'{TESTED_MODULE}.UPLOAD_RETRY_INTERVAL', 0.2).start()
        patch(f'{TESTED_MODULE}.UPLOAD_AT_DELAY', 0.2).start()

        self.bbs_mock = patch(f'{TESTED_MODULE}.BlockBlobService',
                              MagicMock(spec=BlockBlobService)).start()

    def tearDown(self) -> None:
        if LogsUploader().is_alive():
            LogsUploader().terminate(force=True)
        LogsUploader.__it__ = None  # Patch singleton
        patch.stopall()

    def test_storage_service_is_created(self):
        LogsUploader().loop()

        self.bbs_mock.assert_called_with('ASA', 'ASK', is_emulated=True)

    def test_thread_watches_for_termination(self):
        LogsUploader().start()
        LogsUploader().terminate()
        sleep(1)

        self.assertFalse(LogsUploader().is_alive())

    def test_correct_files_are_uploaded_after_midnight(self):
        dt_mock = MagicMock(space=datetime)
        dt_mock.utcnow = Mock(side_effect=[datetime(2000, 1, 1),
                                           datetime(2000, 1, 2)])

        def dirs(path):
            ret_values = {
                'log1/': ['log1-20000101.log'],
                'log2': ['log2-20000101.log']
            }
            return ret_values[path]
        os_listdir_mock = Mock(side_effect=dirs)

        with patch(f'{TESTED_MODULE}.datetime', dt_mock), \
             patch(f'{TESTED_MODULE}.os.listdir', os_listdir_mock):
            LogsUploader().loop()

        bbs_calls = [call('logs', 'hangar-1-log1-20000101.log',
                          'log1/log1-20000101.log'),
                     call('logs', 'hangar-1-log2-20000101.log',
                          'log2/log2-20000101.log')]
        self.bbs_mock().create_blob_from_path.assert_has_calls(bbs_calls)

    def test_files_are_uploaded_only_after_midnight(self):
        dt_mock = MagicMock(space=datetime)
        dt_mock.utcnow = Mock(return_value=datetime(2000, 1, 1))

        def dirs(path):
            ret_values = {
                'log1/': ['log1-20000101.log'],
                'log2': ['log2-200001001.log']
            }
            return ret_values[path]

        os_listdir_mock = Mock(side_effect=dirs)

        with patch(f'{TESTED_MODULE}.datetime', dt_mock), \
             patch(f'{TESTED_MODULE}.os.listdir', os_listdir_mock):
            LogsUploader().loop()

        self.bbs_mock().create_blob_from_path.assert_not_called()

    def test_other_files_are_not_uploaded(self):
        dt_mock = MagicMock(space=datetime)
        dt_mock.utcnow = Mock(side_effect=[datetime(2000, 1, 1),
                                           datetime(2000, 1, 2)])

        def dirs(path):
            ret_values = {
                'log1/': ['log1-19991231.log', 'log1-20000101.log',
                          'log1-20000102.log', 'sth_sth.log'],
                'log2': ['log2-20000101.log']
            }
            return ret_values[path]

        os_listdir_mock = Mock(side_effect=dirs)

        with patch(f'{TESTED_MODULE}.datetime', dt_mock), \
             patch(f'{TESTED_MODULE}.os.listdir', os_listdir_mock):
            LogsUploader().loop()

        bbs_calls = [call('logs', 'hangar-1-log1-20000101.log',
                          'log1/log1-20000101.log'),
                     call('logs', 'hangar-1-log2-20000101.log',
                          'log2/log2-20000101.log')]
        self.bbs_mock().create_blob_from_path.assert_has_calls(bbs_calls)

    def test_upload_is_repeated_on_error(self):
        self.bbs_mock().create_blob_from_path.side_effect = [Exception, None]

        dt_mock = MagicMock(space=datetime)
        dt_mock.utcnow = Mock(side_effect=[datetime(2000, 1, 1),
                                           datetime(2000, 1, 2)])

        def dirs(path):
            ret_values = {
                'log1/': [],
                'log2': ['log2-20000101.log']
            }
            return ret_values[path]

        os_listdir_mock = Mock(side_effect=dirs)

        with patch(f'{TESTED_MODULE}.datetime', dt_mock), \
             patch(f'{TESTED_MODULE}.os.listdir', os_listdir_mock):
            LogsUploader().loop()

        bbs_calls = [call('logs', 'hangar-1-log2-20000101.log',
                          'log2/log2-20000101.log')] * 2
        self.bbs_mock().create_blob_from_path.assert_has_calls(bbs_calls)

    def test_upload_is_skipped_if_retries_num_was_exceeded(self):
        self.bbs_mock().create_blob_from_path.side_effect = Exception

        dt_mock = MagicMock(space=datetime)
        dt_mock.utcnow = Mock(side_effect=[datetime(2000, 1, 1),
                                           datetime(2000, 1, 2)])

        def dirs(path):
            ret_values = {
                'log1/': [],
                'log2': ['log2-20000101.log']
            }
            return ret_values[path]

        os_listdir_mock = Mock(side_effect=dirs)

        with patch(f'{TESTED_MODULE}.datetime', dt_mock), \
             patch(f'{TESTED_MODULE}.os.listdir', os_listdir_mock):
            LogsUploader().loop()

        bbs_calls = [call('logs', 'hangar-1-log2-20000101.log',
                          'log2/log2-20000101.log')] * 3
        self.bbs_mock().create_blob_from_path.assert_has_calls(bbs_calls)
