
import logging
from queue import Empty
from time import sleep as true_sleep
from unittest.mock import MagicMock, call, patch

from hangar_controller.alerts import AlertChecker, AlertManager
from hangar_controller.devices.drivers.messages.errors.error_manager import \
    ErrorManager
from hangar_controller.devices.drivers.messages.errors.error_message import (
    ErrorMessage, ErrorPayload)
from tests.testcase_wrapper import NoLoggingTestCase

TESTED_MODULE = 'hangar_controller.alerts.alert_checker'


class TestAlertChecker(NoLoggingTestCase):
    def tearDown(self):
        AlertChecker.__it__ = None  # Patch singleton

    def test_thread_loop_interval(self):
        counter = 0

        def sleep(t):
            nonlocal counter
            counter += 1
            true_sleep(0.005)

        err_manager_mock = MagicMock(spec=ErrorManager)
        err_manager_mock.get_error.side_effect = Empty()

        ac = AlertChecker(MagicMock(spec=AlertManager), err_manager_mock)

        with patch(f'{TESTED_MODULE}.time.sleep', sleep):
            ac.start()
            true_sleep(0.0125)  # add half of interval as race safeguard
            ac.terminate().join()

        # finish third loop even if terminating
        self.assertEqual(counter, 3)
        self.assertEqual(err_manager_mock.get_error.call_count, 3)

    def test_error_is_dispatched_on_arrival(self):
        error_mock = MagicMock(spec=ErrorMessage)

        err_manager_mock = MagicMock(spec=ErrorManager)
        err_manager_mock.get_error.side_effect = [error_mock, Empty()]

        ac = AlertChecker(MagicMock(spec=AlertManager), err_manager_mock)
        ac._dispatch_error = MagicMock(return_value=None)
        with patch(f'{TESTED_MODULE}.CHECK_ALERT_INTERVAL_MS', 5):
            ac.start()
        true_sleep(0.0025)
        ac.terminate().join()

        ac._dispatch_error.assert_called_once_with(error_mock)

    def test_mutliple_errors_are_dispatched_on_arrival(self):
        error_mock = MagicMock(spec=ErrorMessage)

        queued_errors = [error_mock, error_mock, Empty()]
        err_manager_mock = MagicMock(spec=ErrorManager)
        err_manager_mock.get_error.side_effect = queued_errors

        ac = AlertChecker(MagicMock(spec=AlertManager), err_manager_mock)
        ac._dispatch_error = MagicMock(return_value=None)
        with patch(f'{TESTED_MODULE}.CHECK_ALERT_INTERVAL_MS', 5):
            ac.start()
        true_sleep(0.0025)
        ac.terminate().join()

        ac._dispatch_error.assert_has_calls([call(error_mock)] * 2)

    def test_error_is_correctly_dispatched(self):
        error_mock = MagicMock(spec=ErrorMessage,
                               parsed_payload=MagicMock(spec=ErrorPayload))
        error_mock.parsed_payload.code = 'TEST'
        error_mock.parsed_payload.subcode = 42

        mapping_mock = {('TEST', 42): 'ERROR'}

        ac = AlertChecker(MagicMock(spec=AlertManager),
                          MagicMock(spec=ErrorManager))

        with patch(f'{TESTED_MODULE}.errors_mapping',
                   mapping_mock):
            ac._dispatch_error(error_mock)

        ac._alert_manager.register.assert_called_once_with('ERROR')

    def test_error_is_logged_if_cannot_be_dispatched(self):
        error_mock = MagicMock(spec=ErrorMessage,
                               parsed_payload=MagicMock(spec=ErrorPayload))
        error_mock.parsed_payload.code = 'MISPLACED'
        error_mock.parsed_payload.subcode = 42

        mapping_mock = {('CORRECT', 42): 'ERROR'}

        logger_mock = MagicMock(spec=logging.Logger)

        ac = AlertChecker(MagicMock(spec=AlertManager),
                          MagicMock(spec=ErrorManager))

        with patch(f'{TESTED_MODULE}.logging.Logger', logger_mock), \
             patch(f'{TESTED_MODULE}.errors_mapping', mapping_mock):
            ac._dispatch_error(error_mock)

        ac._alert_manager.register.assert_not_called()
        logger_mock.debug.called_with('Unsupported error in queue; '
                                      'Error code: MISPLACED42')

    def test_error_is_ignored_if_mapped_to_none(self):
        error_mock = MagicMock(spec=ErrorMessage,
                               parsed_payload=MagicMock(spec=ErrorPayload))
        error_mock.parsed_payload.code = 'SILENCED'
        error_mock.parsed_payload.subcode = 42
        mapping_mock = {('SILENCED', 42): None}

        ac = AlertChecker(MagicMock(spec=AlertManager),
                          MagicMock(spec=ErrorManager))

        logger_mock = MagicMock(spec=logging.Logger)

        with patch(f'{TESTED_MODULE}.logging.Logger', logger_mock), \
             patch(f'{TESTED_MODULE}.errors_mapping', mapping_mock):
            pass

        ac._alert_manager.register.assert_not_called()
        logger_mock.debug.assert_not_called()
