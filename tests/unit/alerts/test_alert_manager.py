from enum import Enum
from unittest import TestCase
from unittest.mock import patch, Mock, mock_open, MagicMock, call

from hangar_controller.alerts import AlertCodes, AlertManager
from settingsd.alerts.alerts_settings import SAVE_ALERT_INTERVAL_MS


class TestAlertManager(TestCase):
    def setUp(self) -> None:
        patch('builtins.open', mock_open()).start()
        patch('hangar_controller.alerts.alert_manager.'
              'AZURE_SEND_ALERTS_AND_LOGS', True).start()
        with patch.object(AlertManager, '_load_existing_alerts', return_value=set()):
            self.alert_manager = AlertManager()

    def tearDown(self) -> None:
        patch.stopall()

    @patch('builtins.open', mock_open())
    @patch('hangar_controller.alerts.alert_manager.csv.writer')
    def test_save_new_alert(self, mock_csv_writer):
        mock_alert = MagicMock(spec=AlertCodes)
        mock_alert.name = 'Alert_1'
        mock_alert.value = 1

        output = self.alert_manager._save_new_alert(mock_alert, 123456, 'csvpath')
        open.assert_called_with('csvpath', 'a')
        mock_csv_writer().writerow.assert_has_calls([call(['H0000001', 123456])])
        self.assertTrue(output)

    @patch('hangar_controller.alerts.alert_manager.csv.writer')
    @patch('hangar_controller.alerts.alert_manager.csv.reader')
    @patch('hangar_controller.alerts.alert_manager.parse_string_code_to_alert')
    def test_erase_alert_type(self, mock_parse,
                              mock_csv_reader, mock_csv_writer):
        class MockCodes(Enum):
            A1 = 'H1'
            A2 = 'H2'
            A3 = 'H3'
        mock_parse.side_effect = [MockCodes.A1, MockCodes.A2, MockCodes.A1,
            MockCodes.A3]
        mock_csv_reader.return_value = [['H1', 123456],
                                        ['H2', 126789],
                                        ['H1', 126900],
                                        ['H3', 127877],]
        output = self.alert_manager._erase_alert_type(MockCodes.A1, 'csvpath')
        mock_csv_writer().writerows.assert_called_with([['H2', 126789],
                                                        ['H3', 127877]])
        self.assertTrue(output)                                            

    @patch('hangar_controller.alerts.alert_manager.parse_string_code_to_alert')
    @patch('hangar_controller.alerts.alert_manager.csv.reader')
    def test_load_existing_alerts(self, mock_csv_reader, mock_parse):
        mock_alerts = [MagicMock(alert='Alert1'),
                       MagicMock(alert='Alert2')]
        mock_csv_reader.return_value = [['Alert1', 123456],
                                        ['Alert2', 126789]]
        mock_parse.side_effect = mock_alerts

        output = self.alert_manager._load_existing_alerts('csvpath')

        open.assert_called_with('csvpath')
        self.assertEqual(set(mock_alerts), output)

    def test_get_existing_breakdowns(self):
        class MockCodes(Enum):
            A1 = 'A1'
            A2 = 'A2'
            A3 = 'A3'

        mock_breakdowns = {MockCodes.A1: ['a', 'b', 'c'],
                           MockCodes.A2: ['a', 'd'],
                           MockCodes.A3: ['a', 'b', 'd']}
        alerts = [MockCodes.A1, MockCodes.A2, MockCodes.A3]
        expected_output = ['a', 'b', 'c', 'd']

        with patch('hangar_controller.alerts.alert_manager.alerts_breakdowns',
                   mock_breakdowns):
            output = self.alert_manager._get_existing_breakdowns(alerts)

        self.assertCountEqual(expected_output, output)

    @patch.object(AlertManager, '_send_alert_message_to_cloud', Mock())
    @patch.object(AlertManager, '_save_new_alert', Mock())
    @patch.object(AlertManager, '_get_existing_breakdowns', Mock(return_value=['a', 'b']))
    @patch('hangar_controller.alerts.alert_manager.get_timestamp_ms')
    def test_register_should_save_alert_when_timestamp_is_higher_than_interval_value(
            self, mock_timestamp):
        mock_alert = MagicMock(spec=AlertCodes, name='Alert1')

        mock_timestamp.return_value = 1234567890000
        minimum_timestamp_delta = SAVE_ALERT_INTERVAL_MS
        self.alert_manager._last_alerts_timestamp[mock_alert] = \
            1234567890000 - minimum_timestamp_delta

        self.alert_manager.register(mock_alert)

        self.assertSetEqual({mock_alert}, self.alert_manager.existing_alerts)
        self.assertEqual(['a', 'b'], self.alert_manager.existing_breakdowns)
        self.alert_manager._send_alert_message_to_cloud.assert_called_with(
            mock_alert, False)
        self.alert_manager._save_new_alert.assert_called_with(
            mock_alert, 1234567890000
        )

    @patch.object(AlertManager, '_save_new_alert', Mock())
    @patch.object(AlertManager, '_send_alert_message_to_cloud', Mock())
    @patch('hangar_controller.alerts.alert_manager.get_timestamp_ms')
    def test_register_should_NOT_save_alert_when_timestamp_is_lower_than_interval_value(
            self, mock_timestamp):
        mock_alert = MagicMock(spec=AlertCodes, name='Alert1')

        mock_timestamp.return_value = 1234567890000
        minimum_timestamp_delta = SAVE_ALERT_INTERVAL_MS
        self.alert_manager._last_alerts_timestamp[mock_alert] = \
            mock_timestamp() - minimum_timestamp_delta + 1

        output = self.alert_manager.register(mock_alert)

        self.assertFalse(output)
        self.alert_manager._send_alert_message_to_cloud.assert_not_called()
        self.alert_manager._save_new_alert.assert_not_called()

    @patch.object(AlertManager, '_get_existing_breakdowns', Mock(return_value=[]))
    @patch.object(AlertManager, '_send_alert_message_to_cloud')
    @patch.object(AlertManager, '_erase_alert_type')
    def test_repair(self, mock_erase_alert_type, mock_send_alert_msg):
        mock_alert = Mock()
        self.alert_manager.existing_alerts = {mock_alert}

        output = self.alert_manager.repair(mock_alert)

        self.assertTrue(output)
        self.assertFalse(self.alert_manager.existing_alerts)
        self.assertFalse(self.alert_manager.existing_breakdowns)
        mock_send_alert_msg.assert_called_with(mock_alert, True)
        mock_erase_alert_type.assert_called_with(mock_alert)

    @patch('hangar_controller.alerts.alert_manager.AzureIoTHubClient')
    @patch('hangar_controller.alerts.alert_manager.AlertMessage')
    @patch('hangar_controller.alerts.alert_manager.parse_alert_to_string_code')
    def test_send_alert_message_to_cloud_with_repair_False(
            self, mock_parse_alert, mock_alert_msg, mock_azure_client):
        mock_alert = MagicMock(spec=AlertCodes)
        mock_parse_alert.return_value = 'H1'
        mock_breakdowns = {mock_alert: ['a', 'b']}

        with patch('hangar_controller.alerts.alert_manager.alerts_breakdowns',
                   mock_breakdowns):
            self.alert_manager._send_alert_message_to_cloud(mock_alert, False)

        mock_alert_msg.assert_called_with('H1', False, ['a', 'b'])
        mock_azure_client().send_iothub_message.assert_called_with(
            mock_alert_msg())
