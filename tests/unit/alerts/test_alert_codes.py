from unittest import TestCase

from hangar_controller.alerts import AlertCodes, errors_mapping, \
    alerts_breakdowns


class TestAlertCodes(TestCase):
    def test_error_mappings_should_have_value_for_each_alert_code(self):
        alerts_in_errors_mapping = set(errors_mapping.values())
        alerts_in_errors_mapping.remove(None)
        self.assertEqual(len(AlertCodes),
                         len(alerts_in_errors_mapping))
        for code in AlertCodes:
            with self.subTest(msg=f'code: {code}'):
                self.assertIn(code, errors_mapping.values())

    def test_alert_breakdowns_should_have_keys_for_each_alert_code(self):
        self.assertEqual(len(AlertCodes), len(alerts_breakdowns))
        for code in AlertCodes:
            with self.subTest(msg=f'code: {code}'):
                self.assertIn(code, alerts_breakdowns)
