from unittest import mock
from unittest.mock import MagicMock

from hangar_controller.azure_communication.handlers import *
from tests.testcase_wrapper import NoLoggingTestCase


class TestHandlers(NoLoggingTestCase):
    def setUp(self) -> None:
        self.generic_command = {
            "timestamp": 1520601811,
            "operator_id": "cr-operator-1",
            "name": "generic_command_name",
            "parameters": {
                'flight_id': '1',
                'route_id': '115',
                'drone_id': 'cr-drone-1',
                'cargo': True,
                'cargo_id': 'cargo-id',
                'cargo_weight': 1000,
            }
        }

    def test_must_have_command_name_decorator(self):
        correct_command = {'name': 'correct_command_name'}
        incorrect_command = {'name': 'incorrect_command_name'}

        @must_have_command_name('correct_command_name')
        def handle_command(cmd: dict) -> bool:
            return True

        self.assertTrue(handle_command(correct_command))
        self.assertFalse(handle_command(incorrect_command))

    def test_action_command_handler_should_launch_right_action(self):
        action_command_list = (
            (handle_init_flight, InitFlight, 'init_flight'),
            (handle_drone_started, DroneStarted, 'drone_started'),
            (handle_leave_drone, LeaveDrone, 'leave_drone'),
            (handle_receive_drone, ReceiveDrone, 'receive_drone'),
            (handle_drone_landed, DroneLanded, 'drone_landed'),
            (handle_cancel_flight, CancelFlight, 'cancel_flight'),
            (handle_service_finish_flight, ServiceFinishFlight,
             'service_finish_flight'),
            (handle_alive, Alive, 'others.alive')
        )

        state_machine_patcher = mock.patch(
            'hangar_controller.executor.state_machine.StateMachine.__new__',
            return_value=MagicMock(spec=StateMachine)
        )
        state_machine_patcher.start()

        for action_command in action_command_list:
            patch_target = f'hangar_controller.executor.actions.' \
                           f'{action_command[2]}.{action_command[1].__name__}' \
                           f'.__new__'
            action_patcher = mock.patch(patch_target)
            action_mock = action_patcher.start()
            action_mock.return_value = MagicMock(spec=action_command[1])

            with self.subTest(action_command[0].__name__):
                # given
                self.generic_command['name'] = action_command[2].split('.')[-1]

                # when
                action_command[0](self.generic_command)

                # then
                action_mock.return_value.schedule.assert_called()

                action_patcher.stop()

        state_machine_patcher.stop()
