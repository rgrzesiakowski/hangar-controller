from hangar_controller.azure_communication.messages import HangarHeartBeat
from hangar_controller.hangar_state import HangarState
from tests.testcase_wrapper import NoLoggingTestCase


class TestHangarHeartBeat(NoLoggingTestCase):
    def setUp(self) -> None:
        self.test_system_info = {'drone_id': 'cr-drone-1',
                                 'cargo_id': 'test-cargo-1',
                                 'cargo_weight': 1000,
                                 'state': HangarState.DRONE_POWERING.value,
                                 'available_battery': 3}
        self.test_weather_info = {'wind_speed': 2.4,
                                  'wind_direction': 23,
                                  'temperature': 17.6,
                                  'humidity': 43.3,
                                  'pressure': 1013.2}

        self.test_breakdowns = ["breakdown_1", "breakdown_2"]

        HangarHeartBeat.set_system_info_getter(lambda: self.test_system_info)
        HangarHeartBeat.set_weather_info_getter(lambda: self.test_weather_info)
        HangarHeartBeat.set_breakdowns_getter(lambda: self.test_breakdowns)

        self.hangar_heart_beat = HangarHeartBeat()

    def test_system_info_getter(self):
        HangarHeartBeat.cargo_info_getter = None

        self.assertIsNone(HangarHeartBeat.cargo_info_getter)

        HangarHeartBeat.set_system_info_getter(lambda: self.test_system_info)

        # noinspection PyCallingNonCallable
        self.assertEqual(HangarHeartBeat.system_info_getter(), self.test_system_info)

    def test_weather_info_getter(self):
        HangarHeartBeat.cargo_info_getter = None
        self.assertIsNone(HangarHeartBeat.cargo_info_getter)

        HangarHeartBeat.set_weather_info_getter(lambda: self.test_weather_info)

        # noinspection PyCallingNonCallable
        self.assertEqual(HangarHeartBeat.weather_info_getter(), self.test_weather_info)

    def test_breakdowns_getter(self):
        HangarHeartBeat.breakdowns_getter = None
        self.assertIsNone(HangarHeartBeat.breakdowns_getter)

        HangarHeartBeat.set_breakdowns_getter(lambda: self.test_breakdowns)

        # noinspection PyCallingNonCallable
        self.assertEqual(HangarHeartBeat.breakdowns_getter(), self.test_breakdowns)

    def test_prepared_payload_should_have_correct_structure(self):
        payload = self.hangar_heart_beat._prepare_payload()

        self.assertIn('hangar_heart_beat', payload)

        self.assertIn('system', payload['hangar_heart_beat'])
        self.assertIn('weather', payload['hangar_heart_beat'])
        self.assertIn('breakdowns', payload['hangar_heart_beat'])

        cargo_action_system_payload = payload['hangar_heart_beat']['system']
        cargo_action_weather_payload = payload['hangar_heart_beat']['weather']

        self.assertIn('drone_id', cargo_action_system_payload)
        self.assertIn('cargo_id', cargo_action_system_payload)
        self.assertIn('cargo_weight', cargo_action_system_payload)
        self.assertIn('state', cargo_action_system_payload)
        self.assertIn('available_battery', cargo_action_system_payload)

        self.assertIn('wind_speed', cargo_action_weather_payload)
        self.assertIn('wind_direction', cargo_action_weather_payload)
        self.assertIn('temperature', cargo_action_weather_payload)
        self.assertIn('humidity', cargo_action_weather_payload)
        self.assertIn('pressure', cargo_action_weather_payload)

    def test_prepared_payload_keys_should_have_correct_value(self):
        payload = self.hangar_heart_beat._prepare_payload()

        cargo_action_system_payload = payload['hangar_heart_beat']['system']
        cargo_action_weather_payload = payload['hangar_heart_beat']['weather']

        self.assertEqual(self.test_system_info['drone_id'],
                         cargo_action_system_payload['drone_id'])
        self.assertEqual(self.test_system_info['cargo_id'],
                         cargo_action_system_payload['cargo_id'])
        self.assertEqual(self.test_system_info['cargo_weight'],
                         cargo_action_system_payload['cargo_weight'])
        self.assertEqual(self.test_system_info['state'],
                         cargo_action_system_payload['state'])
        self.assertEqual(self.test_system_info['available_battery'],
                         cargo_action_system_payload['available_battery'])

        self.assertEqual(self.test_weather_info['wind_speed'],
                         cargo_action_weather_payload['wind_speed'])
        self.assertEqual(self.test_weather_info['wind_direction'],
                         cargo_action_weather_payload['wind_direction'])
        self.assertEqual(self.test_weather_info['temperature'],
                         cargo_action_weather_payload['temperature'])
        self.assertEqual(self.test_weather_info['humidity'],
                         cargo_action_weather_payload['humidity'])
        self.assertEqual(self.test_weather_info['pressure'],
                         cargo_action_weather_payload['pressure'])

        self.assertEqual(self.test_breakdowns,
                         payload['hangar_heart_beat']['breakdowns'])

    def test_prepared_properties_should_have_correct_structure(self):
        self.hangar_heart_beat = HangarHeartBeat('cr-operator-1')
        properties = self.hangar_heart_beat._prepare_properties()

        self.assertIn('message_type', properties)
        self.assertIn('operator_id', properties)

        self.assertEqual('hangar_heart_beat', properties['message_type'])
        self.assertEqual('cr-operator-1', properties['operator_id'])

    def test_fill_system_info_section(self):
        self.hangar_heart_beat.system_drone_id = None
        self.hangar_heart_beat.system_cargo_id = None
        self.hangar_heart_beat.system_cargo_weight = None
        self.hangar_heart_beat.system_state = None
        self.hangar_heart_beat.system_available_battery = None

        self.hangar_heart_beat._fill_system_info_section()

        self.assertEqual(self.test_system_info['drone_id'],
                         self.hangar_heart_beat.system_drone_id)
        self.assertEqual(self.test_system_info['cargo_id'],
                         self.hangar_heart_beat.system_cargo_id)
        self.assertEqual(self.test_system_info['cargo_weight'],
                         self.hangar_heart_beat.system_cargo_weight)
        self.assertEqual(self.test_system_info['state'],
                         self.hangar_heart_beat.system_state)
        self.assertEqual(self.test_system_info['available_battery'],
                         self.hangar_heart_beat.system_available_battery)

    def test_fill_weather_info_section(self):
        self.hangar_heart_beat.weather_wind_speed = None
        self.hangar_heart_beat.weather_wind_direction = None
        self.hangar_heart_beat.weather_temperature = None
        self.hangar_heart_beat.weather_humidity = None
        self.hangar_heart_beat.weather_pressure = None

        self.hangar_heart_beat._fill_weather_info_section()

        self.assertEqual(self.test_weather_info['wind_speed'],
                         self.hangar_heart_beat.weather_wind_speed)
        self.assertEqual(self.test_weather_info['wind_direction'],
                         self.hangar_heart_beat.weather_wind_direction)
        self.assertEqual(self.test_weather_info['temperature'],
                         self.hangar_heart_beat.weather_temperature)
        self.assertEqual(self.test_weather_info['humidity'],
                         self.hangar_heart_beat.weather_humidity)
        self.assertEqual(self.test_weather_info['pressure'],
                         self.hangar_heart_beat.weather_pressure)

    def test_fill_breakdowns(self):
        self.hangar_heart_beat.breakdowns = None

        self.hangar_heart_beat._fill_breakdowns()

        self.assertEqual(self.test_breakdowns,
                         self.hangar_heart_beat.breakdowns)
