from hangar_controller.azure_communication.messages import DeviceState
from hangar_controller.hangar_state import HangarState
from tests.testcase_wrapper import NoLoggingTestCase


class TestCargoAction(NoLoggingTestCase):
    def setUp(self) -> None:
        self.old_state = HangarState.HANGAR_FULL
        self.new_state = HangarState.DRONE_INIT

        self.device_state = DeviceState(self.old_state, self.new_state)

    def test_prepared_payload_should_have_correct_structure(self):
        payload = self.device_state._prepare_payload()

        self.assertIn('device_state', payload)

        device_state_payload = payload['device_state']
        self.assertIn('old_state', device_state_payload)
        self.assertIn('new_state', device_state_payload)

    def test_prepared_properties_should_have_correct_structure(self):
        properties = self.device_state._prepare_properties()

        self.assertIn('message_type', properties)

        self.assertEqual('device_state', properties['message_type'])
