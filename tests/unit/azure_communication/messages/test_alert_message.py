from unittest import TestCase

from hangar_controller.azure_communication.messages import AlertMessage


class TestAlertMessage(TestCase):
    def setUp(self) -> None:
        self.code = "H0000001"
        self.repair = True
        self.breakdowns = ["breakdown_1", "breakdown_2"]

        self.alert_message = AlertMessage(self.code, self.repair,
                                          self.breakdowns)

    def test_prepared_payload_should_have_correct_structure(self):
        payload = self.alert_message._prepare_payload()

        self.assertIn('alert', payload)

        alert_msg_payload = payload['alert']
        self.assertEqual(3, len(alert_msg_payload))
        self.assertEqual(self.code, alert_msg_payload['code'])
        self.assertEqual(self.repair, alert_msg_payload['repair'])
        self.assertEqual(self.breakdowns, alert_msg_payload['breakdowns'])

    def test_prepared_properties_should_have_correct_structure(self):
        properties = self.alert_message._prepare_properties()

        self.assertIn('message_type', properties)
        self.assertEqual('alert', properties['message_type'])
