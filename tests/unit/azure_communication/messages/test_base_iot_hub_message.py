from unittest.mock import patch

from hangar_controller.azure_communication.messages import BaseIoTHubMessage
from tests.testcase_wrapper import NoLoggingTestCase


class TestBaseIoTHubMessage(NoLoggingTestCase):
    def setUp(self) -> None:
        self.base_iot_hub_message = BaseIoTHubMessage()
        BaseIoTHubMessage.reset_counter()

    def tearDown(self) -> None:
        BaseIoTHubMessage.flight_id_getter = None
        BaseIoTHubMessage.hangar_type_getter = None

    def test_counter_should_increment(self):
        BaseIoTHubMessage.flight_id_getter = lambda: 'test-flight'
        BaseIoTHubMessage.hangar_type_getter = lambda: 'S'

        self.assertEqual(BaseIoTHubMessage.msg_counter, 0)
        BaseIoTHubMessage()
        self.assertEqual(BaseIoTHubMessage.msg_counter, 1)
        BaseIoTHubMessage()
        self.assertEqual(BaseIoTHubMessage.msg_counter, 2)

    def test_counter_reset(self):
        BaseIoTHubMessage.flight_id_getter = lambda: 'test-flight'
        BaseIoTHubMessage.hangar_type_getter = lambda: 'S'

        self.assertEqual(BaseIoTHubMessage.msg_counter, 0)
        BaseIoTHubMessage()
        self.assertEqual(BaseIoTHubMessage.msg_counter, 1)
        BaseIoTHubMessage.reset_counter()
        self.assertEqual(BaseIoTHubMessage.msg_counter, 0)

    def test_counter_without_flight_id_or_hangar_type(self):
        BaseIoTHubMessage.flight_id_getter = lambda: 'test-flight'
        BaseIoTHubMessage.hangar_type_getter = None

        self.assertEqual(BaseIoTHubMessage.msg_counter, 0)
        BaseIoTHubMessage()
        self.assertEqual(BaseIoTHubMessage.msg_counter, 0)

        BaseIoTHubMessage.flight_id_getter = None
        BaseIoTHubMessage.hangar_type_getter = lambda: 'S'

        self.assertEqual(BaseIoTHubMessage.msg_counter, 0)
        BaseIoTHubMessage()
        self.assertEqual(BaseIoTHubMessage.msg_counter, 0)

    @patch('common.get_timestamp_ms', side_effect=(1, 1, 2))
    def test_timestamp_must_be_unique(self, time_time):
        base_msg_1 = BaseIoTHubMessage()
        self.assertEqual(1, base_msg_1.timestamp)

        base_msg_1 = BaseIoTHubMessage()
        self.assertEqual(2, base_msg_1.timestamp)

    def test_get_data_should_return_correct_dict(self):
        message = self.base_iot_hub_message.get_data()

        self.assertIn('payload', message)
        self.assertIn('properties', message)
        self.assertIsInstance(message['payload'], dict)
        self.assertIsInstance(message['properties'], dict)

    def test_set_getters(self):
        self.assertIs(BaseIoTHubMessage.flight_id_getter, None)
        self.assertIs(BaseIoTHubMessage.hangar_type_getter, None)

        BaseIoTHubMessage.set_flight_id_getter(lambda: 'test-flight')
        BaseIoTHubMessage.set_hangar_type_getter(lambda: 'S')

        self.assertEqual(BaseIoTHubMessage.flight_id_getter(), 'test-flight')
        self.assertEqual(BaseIoTHubMessage.hangar_type_getter(), 'S')

    def test_prepared_payload_should_have_correct_keys(self):
        payload = self.base_iot_hub_message._prepare_payload()

        self.assertIn('timestamp', payload)
        self.assertIn('flight_id', payload)
        self.assertIn('message_id', payload)
        self.assertIn('device_type', payload)
        self.assertIn('device_id', payload)

    def test_prepared_properties_should_have_correct_keys(self):
        BaseIoTHubMessage.set_flight_id_getter(lambda: 'test-flight')
        BaseIoTHubMessage.set_hangar_type_getter(lambda: 'S')
        self.base_iot_hub_message = BaseIoTHubMessage()

        expected_properties_keys = ("message_id", "device_type", "flight_id",
                                    "timestamp", "device_id")
        properties = self.base_iot_hub_message._prepare_properties()

        for expected_property_key in expected_properties_keys:
            self.assertIn(expected_property_key, properties)

    def test_prepared_properties_can_not_have_empty_keys(self):
        self.base_iot_hub_message = BaseIoTHubMessage()

        expected_properties_keys = ("message_id", "device_type", "flight_id",
                                    "timestamp", "device_id")
        properties = self.base_iot_hub_message._prepare_properties()

        self.assertNotIn('flight_id', properties)

    def test_clean_up_properties(self):
        ugly_properties = {
            'empty_property': None,
            'correct_property': 'correct',
            'no_str_property': 1000
        }

        clean_properties = self.base_iot_hub_message._clean_up_properties(ugly_properties)
        self.assertEqual({'correct_property': 'correct', 'no_str_property': '1000'},
                         clean_properties)
