from hangar_controller.azure_communication.messages import CommandResponse
from hangar_controller.azure_communication.response_status import ResponseStatus
from tests.testcase_wrapper import NoLoggingTestCase


class TestCommandResponse(NoLoggingTestCase):
    def setUp(self) -> None:
        self.test_command = {
            "timestamp": 1520601811,
            "operator_id": "cr-operator-1",
            "name": "init_flight",
            "parameters": {
                "flight_id": "1",
                "drone_id": "115"
            }
        }
        self.test_command['command_name'] = self.test_command['name']
        self.test_command_status = ResponseStatus.ACKNOWLEDGED

        self.command_response = CommandResponse(self.test_command, self.test_command_status)

    def test_prepared_payload_should_have_correct_structure(self):
        payload = self.command_response._prepare_payload()

        self.assertIn('command_response', payload)

        command_response_payload = payload['command_response']
        self.assertIn('operator_id', command_response_payload)
        self.assertIn('command_name', command_response_payload)
        self.assertIn('command_timestamp', command_response_payload)
        self.assertIn('command_status', command_response_payload)
        self.assertIn('command_parameters', command_response_payload)

    def test_prepared_properties_should_have_correct_structure(self):
        properties = self.command_response._prepare_properties()

        self.assertIn('message_type', properties)
        self.assertIn('operator_id', properties)

        self.assertEqual('command_response', properties['message_type'])
        self.assertEqual(self.test_command['operator_id'], properties['operator_id'])

    def test_fill_cargo_info(self):
        self.command_response.operator_id = None
        self.command_response.command_name = None
        self.command_response.command_timestamp = None
        self.command_response.command_parameters = None
        self.command_response.command_status = None

        self.command_response._fill_message_fields(self.test_command, self.test_command_status)

        self.assertEqual(self.test_command['operator_id'], self.command_response.operator_id)
        self.assertEqual(self.test_command['command_name'], self.command_response.command_name)
        self.assertEqual(self.test_command['timestamp'], self.command_response.command_timestamp)
        self.assertEqual(self.test_command['parameters'], self.command_response.command_parameters)
        self.assertEqual(self.test_command_status.name.lower(),
                         self.command_response.command_status)
