from unittest import mock

from hangar_controller.azure_communication.validators import *
from hangar_controller.azure_communication.validators import _validate_command_name, \
    _validate_schema, _validate_command_schema, _validate_is_known_command, _validate_init_flight, \
    _validate_receive_drone, _validate_drone_landed, _validate_unload_drone, \
    _validate_service_finish_flight
from tests.testcase_wrapper import NoLoggingTestCase


class TestHandlers(NoLoggingTestCase):
    def setUp(self) -> None:
        self.generic_command = {
            "timestamp": 1520601811,
            "operator_id": "cr-operator-1",
            "name": "generic_command_name",
            "parameters": {
                'flight_id': '1',
                'route_id': '115',
                'drone_id': 'cr-drone-1',
                'cargo': True,
                'cargo_id': 'cargo-id',
                'cargo_weight': 1000,
            }
        }

    def test_must_have_valid_command_name_decorator(self):
        correct_command_name = {'name': 'correct_command_name'}
        incorrect_command_name = {'name': 'incorrect_command_name'}

        @_validate_command_name('correct_command_name')
        def validate_command(cmd: dict) -> bool:
            return True

        self.generic_command['name'] = incorrect_command_name
        self.assertTrue(validate_command(incorrect_command_name))

        self.generic_command['name'] = correct_command_name
        self.assertTrue(validate_command(correct_command_name))

        self.assertFalse(validate_command({}))

    def test_validate_schema_for_command(self):
        @_validate_schema(EXPECTED_COMMAND_SCHEMA, False)
        def validate_command(cmd: dict) -> bool:
            return True

        self.assertTrue(validate_command(self.generic_command))

        del self.generic_command['operator_id']
        self.assertFalse(validate_command(self.generic_command))

    def test_validate_schema_for_parameters(self):
        @_validate_schema({'cargo_id': tp.Optional[str], 'route_id': str})
        def validate_command(cmd: dict) -> bool:
            return True

        self.assertTrue(validate_command(self.generic_command))

        del self.generic_command["parameters"]['cargo_id']
        self.assertFalse(validate_command(self.generic_command))

    def test_validate_schema_for_parameters_with_invalid_schema(self):
        @_validate_schema({'cargo_id': tp.Optional[int], 'route_id': str})
        def validate_command(cmd: dict) -> bool:
            return True

        self.assertFalse(validate_command(self.generic_command))

    @mock.patch('hangar_controller.azure_communication.validators.KNOWN_COMMAND',
                ('generic_command_name',))
    def test_validate_known_command_should_return_true(self):
        self.assertTrue(_validate_is_known_command(self.generic_command))

    @mock.patch('hangar_controller.azure_communication.validators.KNOWN_COMMAND',
                ('invalid_generic_command_name',))
    def test_validate_unknown_command_should_return_false(self):
        self.assertFalse(_validate_is_known_command(self.generic_command))

    def test_validate_correct_command_should_return_true(self):
        self.assertTrue(_validate_command_schema(self.generic_command))

        validation_command_list = (
            (_validate_init_flight, 'init_flight'),
            (_validate_receive_drone, 'receive_drone'),
            (_validate_drone_landed, 'drone_landed'),
            (_validate_service_finish_flight, 'service_finish_flight'),
            (_validate_unload_drone, 'unload_drone')
        )

        for validated_command in validation_command_list:
            with self.subTest(f'INVALID: {validated_command[0].__name__}'):
                self.generic_command['name'] = validated_command[1]

                self.assertTrue(validated_command[0](self.generic_command))

    def test_validate_incorrect_command_should_return_false(self):
        self.assertTrue(_validate_command_schema(self.generic_command))

        validation_command_list = (
            (_validate_init_flight, 'init_flight'),
            (_validate_receive_drone, 'receive_drone'),
            (_validate_drone_landed, 'drone_landed'),
            (_validate_service_finish_flight, 'service_finish_flight'),
            (_validate_unload_drone, 'unload_drone')
        )

        for validated_command in validation_command_list:
            with self.subTest(f'INVALID: {validated_command[0].__name__}'):
                self.generic_command['name'] = validated_command[1]

                self.generic_command['parameters'] = {}

                self.assertFalse(validated_command[0](self.generic_command))
