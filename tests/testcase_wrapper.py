import logging
from unittest import TestCase

class NoLoggingTestCase(TestCase):
    def run(self, *args, **kwargs):
        logging_level = logging.root.level
        logging.disable(logging.CRITICAL)

        super().run(*args, **kwargs)

        logging.disable(logging_level - 1) # return to previous level of logging
