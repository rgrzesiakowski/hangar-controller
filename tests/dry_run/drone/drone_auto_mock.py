import json
import time

from hangar_controller.mqtt_client import MQTTClient
from tests.dry_run.drone.messages import message_templates


class DroneAutoMock(MQTTClient):
    def __init__(self):
        super(DroneAutoMock, self).__init__('drone_auto_mock')
        self.client.subscribe('hangar', 2)
        self.end = False

        self.actions = {
            'drone_initializing': lambda: self.send_message(
                'drone_initialized'),
            'cargo_prepare': lambda: self.send_message('cargo_ready'),
            'drone_prepare': lambda: self.send_message('drone_ready'),
            'flight_finish': lambda: self.send_message('flight_ended'),
            'flight_cancel': lambda: self.send_message('flight_canceled'),
            'shutdown_drone': lambda: self._process_shutdown_drone()
        }

    # region Actions
    def _process_shutdown_drone(self):
        print('INFO: Shutdown Drone')
        self.end = True

    # endregion

    def _on_message_callback(self, client, userdata, message):
        raw_message = message.payload.decode("utf-8")
        received_message = json.loads(raw_message)

        if received_message['command'] not in self.actions.keys():
            print(f'Command: {received_message["command"]} not found')
            return

        print(f'Hangar command: {received_message["command"]}')

        self.actions[received_message['command']]()

    def send_message(self, message_name, **kwargs):
        message = json.dumps(message_templates[message_name])
        print(f"D-H: {message}")

        self.client.publish('drone', message, 2)
        time.sleep(2)

    def run(self):
        self.send_message('drone_launched')

        while not self.end:
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                break

        self.close_connection()


if __name__ == '__main__':
    drone_cli = DroneAutoMock()
    drone_cli.run()
