_message_templates_long_names = {
    "drone_launched": {"command": "drone_launched", "parameters": {}},
    "drone_initialized": {"command": "drone_initialized", "parameters": {}},
    "cargo_ready": {"command": "cargo_ready", "parameters": {}},
    "drone_ready": {"command": "drone_ready", "parameters": {}},
    "flight_ended": {"command": "flight_ended", "parameters": {}},
    "flight_canceled": {"command": "flight_canceled", "parameters": {}},
}

message_templates = {
    "dl": _message_templates_long_names["drone_launched"],
    "drone_launched": _message_templates_long_names["drone_launched"],
    "di": _message_templates_long_names["drone_initialized"],
    "drone_initialized": _message_templates_long_names["drone_initialized"],
    "cr": _message_templates_long_names["cargo_ready"],
    "cargo_ready": _message_templates_long_names["cargo_ready"],
    "dr": _message_templates_long_names["drone_ready"],
    "drone_ready": _message_templates_long_names["drone_ready"],
    "fe": _message_templates_long_names["flight_ended"],
    "flight_ended": _message_templates_long_names["flight_ended"],
    "fc": _message_templates_long_names["flight_canceled"],
    "flight_canceled": _message_templates_long_names["flight_canceled"]
}
