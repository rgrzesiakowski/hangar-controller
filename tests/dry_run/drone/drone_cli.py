import json

from hangar_controller.mqtt_client import MQTTClient
from tests.dry_run.drone.messages import message_templates


class DroneCLI(MQTTClient):
    def __init__(self):
        super().__init__('drone_mock')
        self.client.subscribe('hangar', 2)

    def _on_message_callback(self, client, userdata, message):
        raw_message = message.payload.decode("utf-8")
        received_message = json.loads(raw_message)
        print(f'H-D: {received_message}')

    def _send_command(self, command: dict):
        message_text = json.dumps(command)
        print(f'D-H: {message_text}')
        self._publish_message(message_text, 'drone')

    def run(self):
        work = True
        while work:
            try:
                message_input = input('> ')
                if message_input == 'q':
                    self.close_connection()
                    break

                if message_input not in message_templates.keys():
                    print('Unknown message')
                    self.close_connection()
                    continue

                message = message_templates[message_input]

                if bool(message['parameters']):
                    for field in message['parameters']:
                        message['parameters'][field] = input(f'\t{field}: ')

                self._send_command(message)
            except KeyboardInterrupt:
                self.close_connection()
                break


if __name__ == '__main__':
    drone_cli = DroneCLI()
    drone_cli.run()
