import logging
import os

logger = logging.getLogger(__name__)


class AzureBlockBlobServiceMock:
    def __init__(self, account_name=None, account_key=None, sas_token=None,
                 is_emulated=False, protocol='https',
                 endpoint_suffix='core.windows.net', custom_domain=None,
                 request_session=None, connection_string=None,
                 socket_timeout=None):
        logger.debug('AzureBlockBlobServiceMock initialized instead of '
                     'azure.storage.blob.BlockBlobService')

    def create_blob_from_path(self, container_name, blob_name, file_path,
                              content_settings=None, metadata=None,
                              validate_content=False, progress_callback=None,
                              max_connections=2, lease_id=None,
                              if_modified_since=None, if_unmodified_since=None,
                              if_match=None, if_none_match=None, timeout=None):
        for name in ['container_name', 'blob_name', 'file_path']:
            if locals()[name] is None:
                raise ValueError(f'Parameter {name} value can not be None')
        if not os.path.exists(file_path):
            raise FileNotFoundError(f'File {file_path} not found')
        logger.debug(f'File {file_path} saved as {blob_name} '
                     f'in {container_name}')
