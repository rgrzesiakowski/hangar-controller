crumb_actions_command_parameters_templates = {
    "open_roof": None,
    "close_roof": None,
    "open_cargo_window": None,
    "close_cargo_window": None,
    "cargo_feeder_window": None,
    "cargo_feeder_lift": None,
    "position_drone": None,
    "release_drone": None,
    "lift_up": None,
    "lift_down": None,
    "grasper_grasp": None,
    "grasper_release": None,
    "manipulator_cargo_drone": None,
    "manipulator_cargo_feeder_pick_up": None,
    "manipulator_cargo_feeder_put_down": None,
    "manipulator_battery_drone": {"position": ["put", str]},
    "manipulator_battery_slot": {"battery_slot_number": [1, int]},
    "manipulator_home": None,
}

crumb_actions_command_names_aliases = {
    "or": "open_roof",
    "cr": "close_roof",
    "ocw": "open_cargo_window",
    "ccw": "close_cargo_window",
    "cfw": "cargo_feeder_window",
    "cfl": "cargo_feeder_lift",
    "pd": "position_drone",
    "rd": "release_drone",
    "lu": "lift_up",
    "ld": "lift_down",
    "gg": "grasper_grasp",
    "gr": "grasper_release",
    "mcd": "manipulator_cargo_drone",
    "mcfpu": "manipulator_cargo_feeder_pick_up",
    "mcfpd": "manipulator_cargo_feeder_put_down",
    "mbd": "manipulator_battery_drone",
    "mbs": "manipulator_battery_slot",
    "mh": "manipulator_home",
}


diagnostic_actions_command_parameters_templates = {
    "get_weather": None,
    "cargo_tare": None,
}

diagnostic_actions_command_names_aliases = {
    "gw": "get_weather",
    "ct": "cargo_tare",
}

sequence_actions_command_parameters_templates = {
    "calibrate_mechanisms": None,
    "load_cargo": None,
    "unload_cargo": None
}

sequence_actions_command_names_aliases = {
    "cm": "calibrate_mechanisms",
    "lc": "load_cargo",
    "uc": "unload_cargo"
}

control_actions_command_parameters_templates = {
    "alert_repair": {"alert": ["H0000000", str]},
    "enable_service_mode": None,
    "disable_service_mode": {"new_state": ["hangar_empty", str]},
    "set_battery_to_charge": {"slot": ["0", str]},
    "set_forced_battery_slot": {"slot": ["0", str]},
    "set_hangar_state": {"state": ["", str]},
}

control_actions_command_names_aliases = {
    "ar": "alert_repair",
    "esm": "enable_service_mode",
    "dsm": "disable_service_mode",
    "sbtc": "set_battery_to_charge",
    "sfbs": "set_forced_battery_slot",
    "shs": "set_hangar_state",
}

command_parameters_templates = {
    **crumb_actions_command_parameters_templates,
    **diagnostic_actions_command_parameters_templates,
    **sequence_actions_command_parameters_templates,
    **control_actions_command_parameters_templates
}

command_names_aliases = {
    **crumb_actions_command_names_aliases,
    **diagnostic_actions_command_names_aliases,
    **sequence_actions_command_names_aliases,
    **control_actions_command_names_aliases
}
