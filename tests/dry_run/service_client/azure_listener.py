import json
import time

from hangar_controller.mqtt_client import MQTTClient


class AzureMockListener(MQTTClient):
    def __init__(self):
        super().__init__('azure_mock_listener')
        self.client.subscribe('azure_hangar', 2)
        self.client.subscribe('azure_hangar_response', 2)

    def _on_message_callback(self, client, userdata, message):
        message_type = "NULL"
        if message.topic == 'azure_hangar':
            message_type = "MESSAGE"
        if message.topic == "azure_hangar_response":
            message_type = "RESPONSE"
        raw_message = message.payload.decode("utf-8")
        message = json.loads(raw_message)
        print(f'{message_type}: {message}\n')

    def run(self):
        work = True
        while work:
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                self.close_connection()
                break


if __name__ == '__main__':
    azure_mock_listener = AzureMockListener()
    azure_mock_listener.run()
