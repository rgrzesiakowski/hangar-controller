import json

from hangar_controller.mqtt_client import MQTTClient
from tests.dry_run.service_client.service_commands import \
    command_parameters_templates, command_names_aliases


class ServiceCLI(MQTTClient):
    def __init__(self):
        super().__init__('hangar_service_cli')

    def _on_message_callback(self, client, userdata, message):
        pass

    def _send_command(self, command: dict):
        message_text = json.dumps(command)
        self._publish_message(message_text, 'serviceman')

    def run(self):
        work = True
        while work:
            try:
                command_input = input('> ')

                if command_input == 'q':
                    self.close_connection()
                    break

                if command_input in command_names_aliases.keys():
                    command_name = command_names_aliases[command_input]
                else:
                    command_name = command_input

                if command_name not in command_parameters_templates.keys():
                    print('Unknown command')
                    continue

                try:
                    if isinstance(command_parameters_templates[command_name],
                                  dict):
                        parameters = command_parameters_templates[
                            command_name].copy()
                    else:
                        parameters = {}
                except KeyError:
                    print("No parameters schema")
                    continue

                if bool(parameters):
                    for p in parameters:
                        parameter_def = parameters[p]
                        parameter_value = input(
                            f'\t{p} {parameter_def}: '
                        )

                        if parameter_value == "":
                            parameters[p] = parameter_def[0]
                            continue

                        if parameter_def[1] in (int, float, str, dict):
                            parameters[p] = parameter_def[1](parameter_value)
                            continue

                        if parameter_def[1] == bool:
                            if parameter_value.lower() == str(True).lower():
                                parameters[p] = True
                            else:
                                parameters[p] = False

                            continue

                        parameters[p] = parameter_value

                command = {'command': command_name, 'parameters': parameters}
                self._send_command(command)
            except KeyboardInterrupt:
                self.close_connection()
                break


if __name__ == '__main__':
    service_cli = ServiceCLI()
    service_cli.run()
