import logging
from unittest.mock import patch, MagicMock

import settings
from hangar_controller import run
from hangar_controller.devices.modbus_io_manager import ModbusIOManager
from tests.dry_run.azure_communication_mock.azure_iothub_client_mock import \
    AzureIoTHubClientMock
from tests.dry_run.azure_communication_mock.azure_block_blob_service_mock \
    import AzureBlockBlobServiceMock
from tests.dry_run.battery_chargers_supervisor_mock.battery_chargers_supervisor_mock import \
    BatteryChargersSupervisorMock
from tests.dry_run.devices_mocks.drivers_controller_mock import \
    DriversControllerMock
from tests.dry_run.devices_mocks.motors_mock import MotorsMock
from tests.dry_run.devices_mocks.weather_station_controller_mock import \
    WeatherStationControllerMock

logger = logging.getLogger(__name__)


def patch_azure_communication():
    iothub_client_patcher = patch(
        'hangar_controller.azure_communication.AzureIoTHubClient.__new__',
        return_value=AzureIoTHubClientMock()
    )
    iothub_client_patcher.start()

    block_blob_service_patcher = patch(
        'hangar_controller.logs_dump.logs_uploader.BlockBlobService',
        AzureBlockBlobServiceMock
    )
    block_blob_service_patcher.start()

    settings.PENTACOMP_API_CARGO_USER_INFO = \
        f'http://{settings.PENTACOMP_API_MOCK_ADDRESS}:48647/api/CargoUserInfo'
    settings.PENTACOMP_API_CARGO_USER_ACTION = \
        f'http://{settings.PENTACOMP_API_MOCK_ADDRESS}:48647/api/CargoUserAction'

    return iothub_client_patcher, block_blob_service_patcher


def patch_devices():
    motors_controller_patcher = patch(
        'hangar_controller.devices.motors.motors.Motors.__new__',
        return_value=MotorsMock())
    motors_controller_patcher.start()

    drivers_controller_patcher = patch(
        'hangar_controller.devices.drivers.drivers_controller.DriversController.__new__',
        return_value=DriversControllerMock())
    drivers_controller_patcher.start()

    weather_station_controller_patcher = patch(
        'hangar_controller.devices.weather_station.weather_station_controller.WeatherStationController.__new__',
        return_value=WeatherStationControllerMock())
    weather_station_controller_patcher.start()

    modbus_io_manager_patcher = patch(
        'hangar_controller.devices.modbus_io_manager.ModbusIOManager.__new__',
        return_value=MagicMock(spec=ModbusIOManager)
    )
    modbus_io_manager_patcher.start()

    stm_logs_dump_devices_patcher = patch(
        'hangar_controller.logs_dump.stm_logs_dump.DEVICES', []
    )
    stm_logs_dump_devices_patcher.start()

    return motors_controller_patcher, drivers_controller_patcher, \
           weather_station_controller_patcher, modbus_io_manager_patcher,\
           stm_logs_dump_devices_patcher


def patch_battery_chargers_supervisor():
    bcs_mock = BatteryChargersSupervisorMock()
    bcs_patcher = patch('hangar_controller.battery_chargers_supervisor.battery_chargers_supervisor.BatteryChargersSupervisor.__new__',
                        return_value=bcs_mock)
    bcs_patcher.start()
    return bcs_patcher


if __name__ == '__main__':
    # region: patch for tests

    # Comment this during real AZURE test
    azure_patchers = patch_azure_communication()

    # Comment this during real HARDWARE test
    # devices_patchers = patch_devices()

    # Comment this when real chargers are present
    bcs_patcher = patch_battery_chargers_supervisor()

    # endregion

    run.run_hangar()

    # region: disable patcher

    # Comment this during real AZURE test
    for patcher in azure_patchers:
        patcher.stop()

    # Comment this during real HARDWARE test
    # for patcher in devices_patchers:
    #     patcher.stop()

    # Comment this when real chargers are present
    bcs_patcher.stop()
    # endregion
