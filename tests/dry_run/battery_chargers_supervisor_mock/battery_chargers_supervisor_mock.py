import logging
from typing import Optional

from hangar_controller.azure_communication import AzureIoTHubClient
from hangar_controller.azure_communication.messages import HangarHeartBeat
from hangar_controller.azure_communication.messages.battery_state import \
    BatteryState
from hangar_controller.executor import HangarData
from hangar_controller.executor.state_machine import StateMachine
from hangar_controller.hangar_state import HangarState
from settings import BCS_MOCK_INIT_BATTERIES_SLOTS, BCS_MOCK_SINGLE_HANGAR
from settingsd.drivers.stm_master_charger_settings import MasterChargerStates

logger = logging.getLogger(__name__)


class BatteryChargersSupervisorMock:
    """This Mock will only work if both hangars are going to have
    properly available batteries set on init. Otherwise it will send
    some bad data to cloud (i.e. 2 hangars send that they have same battery).

    This mock simulates 2 chargers by default (you might add more but follow
    the rules).

    1. Available chargers always start counting from 0. If you want to have
        2 available chargers, their slots HAVE TO be ['0', '1']
    2. Both hangars has to have the same amount of available chargers set in the mock
    3. BCS_MOCK_INIT_BATTERIES_SLOTS has to have slots that are eliminating
        each other between two hangars. I.e. if you init hangar-1 with
        'BCS_MOCK_INIT_BATTERIES_SLOTS=0', then hangar-2 has to be initialized with
        'BCS_MOCK_INIT_BATTERIES_SLOTS=1' (if both are initialized with 2 chargers).
        You can initialize one hangar with 2 batteries: 'BCS_MOCK_INIT_BATTERIES_SLOTS=0,1'
        but remember that second has to have 'BCS_MOCK_INIT_BATTERIES_SLOTS='
    4. 'Present' batteries in hangar are always initialized as CHARGED

    """
    def __init__(self):
        self.hangar_data = HangarData()
        self.azure_client = AzureIoTHubClient()

        self._slot_battery_map = {  # all possible batteries
            '0': "28ff776240170431:28ff776240170431:28ff776240170431",
            '1': "69ff796221370610:69ff796221370610:69ff796221370610"
        }
        self.available_chargers = ['0', '1']
        self.charged_batteries_slots = BCS_MOCK_INIT_BATTERIES_SLOTS

        self._picked_slot = None  # workaround if only single hangar is running
        self.forced_slot: Optional[str] = None

        logger.error("BCS Mock initialized instead of Battery Chargers Supervisor")

    def _pick_forced_slot(self):
        forced_slot = self.forced_slot
        self.forced_slot = None
        try:
            self.charged_batteries_slots.remove(forced_slot)
        except ValueError:
            pass
        return forced_slot

    def get_charged_battery(self):
        if self.forced_slot:
            slot = self._pick_forced_slot()
        else:
            slot = self.charged_batteries_slots.pop(0)
        self._picked_slot = slot
        return slot, self._slot_battery_map[slot]

    def get_charger_id_by_state(self, state):
        if self.forced_slot:
            slot = self.forced_slot
            self.forced_slot = None
            return slot

        if state != MasterChargerStates.CHARGED:
            is_canceled = StateMachine().state == HangarState.CANCELLING_FLIGHT
            if is_canceled or BCS_MOCK_SINGLE_HANGAR:
                return self._picked_slot

            for slot in self.available_chargers:
                if slot not in self.charged_batteries_slots:
                    return slot
        else:
            return self.charged_batteries_slots[0]

    def add_battery_to_slot(self, slot):
        self._picked_slot = None
        with self.hangar_data:
            self.hangar_data.batteries.append(self._slot_battery_map[slot])
        return self._slot_battery_map[slot]

    def remove_battery_from_hangar(self, battery_uid):
        with self.hangar_data:
            self.hangar_data.available_battery = len(self.charged_batteries_slots)
            self.hangar_data.batteries.remove(battery_uid)

    def change_battery_state_to_charged(self, slot: str):
        """Only exists in mock and should be called via service CLI

        """
        with self.hangar_data:
            batteries = self.hangar_data.batteries.copy()

        if self._slot_battery_map[slot] not in batteries:
            logger.error("Battery bound to slot not found in "
                         f"hangar's batteries. Slot: {slot} | "
                         f"battery: {self._slot_battery_map[slot]}")
            return

        if slot in self.charged_batteries_slots:
            logger.error(f"Selected slot is already charged. Slot: {slot}")
            return

        self.charged_batteries_slots.append(slot)
        self.charged_batteries_slots.sort()

        with self.hangar_data:
            self.hangar_data.available_battery = len(self.charged_batteries_slots)

        self.azure_client.send_iothub_message(BatteryState())
        self.azure_client.send_iothub_message(HangarHeartBeat())

        logger.debug(f"Succesfully added battery under slot {slot} to charged")

    def start(self):
        available_battery = len(self.charged_batteries_slots)
        batteries = [self._slot_battery_map[v] for v in BCS_MOCK_INIT_BATTERIES_SLOTS]
        with self.hangar_data:
            self.hangar_data.batteries = batteries
            self.hangar_data.available_battery = available_battery

    def terminate(self):
        return self

    def join(self):
        return
