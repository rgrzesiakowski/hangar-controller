import typing as tp

from unittest import mock
from unittest.mock import Mock

from hangar_controller.devices.weather_station.weather_station_controller import \
    WeatherStationController


class WeatherStationControllerMock:
    def __new__(cls, *args, **kwargs):
        origin_attributes_dict = cls.__dict__.copy()

        mocked_return_values = cls._specific_return_value_for_some_attributes()
        excluded_attributes = cls._excluded_attributes()

        mocked_class_attributes = WeatherStationController.__dict__

        for name in mocked_class_attributes:
            if name.startswith("__"):
                continue

            if name in excluded_attributes:
                continue

            try:
                if name in mocked_return_values:
                    setattr(cls, name, mocked_return_values[name])
                else:
                    setattr(cls, name,
                            mock.create_autospec(mocked_class_attributes[name]))
            except (TypeError, AttributeError):
                pass

        for name in origin_attributes_dict:
            try:
                setattr(cls, name, origin_attributes_dict[name])
            except (TypeError, AttributeError):
                pass

        return super(WeatherStationControllerMock, cls).__new__(cls, *args,
                                                                **kwargs)

    @classmethod
    def _specific_return_value_for_some_attributes(cls):
        mocked_return_values: tp.Dict[str, Mock] = {
            'read_device_state': Mock(return_value={
                'uv_radiation': False,
                'snow_thickness': False,
                'true_wind_speed': True,
                'true_wind_direction': True,
                'altitude': False,
                'visibility': False,
                'luminance': False,
                'solar_radiation': False,
                'pm_1_0__2_5__10': True,
                'gps': False,
                'precipitation': True,
                'compass': False,
                'pressure': True,
                'relative_wind': False,
                'temperature_and_humidity': True
            }),
            'read_wind_direction': Mock(return_value=13),
            'read_wind_speed': Mock(return_value=4.2),
            'read_temperature': Mock(return_value=20.0),
            'read_humidity': Mock(return_value=75.4),
            'read_pressure': Mock(return_value=1011.1)
        }

        return mocked_return_values

    @classmethod
    def _excluded_attributes(cls):
        excluded_attributes = []

        return excluded_attributes
