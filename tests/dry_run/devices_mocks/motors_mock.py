import logging
import typing as tp

from unittest import mock
from unittest.mock import Mock, MagicMock

from hangar_controller.devices.motors.motors import Motors
from hangar_controller.devices.motors.controller.motors_controllers. \
    manipulator_motor_controller import ManipulatorMotorController
from hangar_controller.devices.motors.controller.motors_controllers. \
    grasper_motor_controller import GrasperMotorController
from hangar_controller.devices.motors.controller.motors_controllers. \
    positioning_motors_controller import PositioningMotorController
from hangar_controller.devices.motors.controller.motors_controllers. \
    lift_motor_controller import LiftMotorController
from hangar_controller.devices.motors.controller.diagnostics. \
    wobit_diagnostics_reader import WobitDiagnosticsReader


logger = logging.getLogger(__name__)


class MotorsMock:
    def __new__(cls, *args, **kwargs):
        origin_attributes_dict = cls.__dict__.copy()

        mocked_return_values = cls._specific_return_value_for_some_attributes()
        excluded_attributes = cls._excluded_attributes()

        mocked_class_attributes = Motors.__dict__

        for name in mocked_class_attributes:
            if name.startswith("__"):
                continue

            if name in excluded_attributes:
                continue

            try:
                if name in mocked_return_values:
                    setattr(cls, name, mocked_return_values[name])
                else:
                    setattr(cls, name,
                            mock.create_autospec(mocked_class_attributes[name]))
            except (TypeError, AttributeError):
                pass

        for name in origin_attributes_dict:
            try:
                setattr(cls, name, origin_attributes_dict[name])
            except (TypeError, AttributeError):
                pass

        return super(MotorsMock, cls).__new__(cls, *args, **kwargs)

    @classmethod
    def _specific_return_value_for_some_attributes(cls):
        mocked_return_values: tp.Dict[str, Mock] = {}

        mocked_return_values.update({
            'lift': Mock(return_value=MagicMock(LiftMotorController)),
            'grasper': Mock(return_value=MagicMock(GrasperMotorController)),
            'manipulator': Mock(return_value=MagicMock(
                                    ManipulatorMotorController)),
            'positioning': Mock(return_value=MagicMock(
                                    PositioningMotorController)),
            'diagnostics_reader': Mock(return_value=MagicMock(
                                    WobitDiagnosticsReader))
        })

        return mocked_return_values

    @classmethod
    def _excluded_attributes(cls):
        excluded_attributes = []

        return excluded_attributes

    def start(self) -> None:
        return None
