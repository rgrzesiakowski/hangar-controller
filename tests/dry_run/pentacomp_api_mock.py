import json
from random import randint

from flask import Flask, request, jsonify

WEIGHT_LIMIT = 5000
CARGO_ID = "cr-cargo-1"

cargo_info = {"deviceid": None,
              "action": None,
              "user_type": None,
              "pin": None}

app = Flask(__name__)


@app.route("/api/CargoUserAction", methods=['POST'])
def validate_cargo_user_action():
    data = json.loads(request.get_data())

    response = {"cargo_id": CARGO_ID,
                "weight_limit": WEIGHT_LIMIT,
                "valid": False
                }

    has_required_keys = all(key in data for key in cargo_info.keys())
    if not has_required_keys:
        return jsonify(response), 200

    weight = data.get('weight')

    is_valid_weight = True if weight is None else weight <= WEIGHT_LIMIT
    id_deviceid_valid = data.get('deviceid') == cargo_info['deviceid']
    is_action_valid = data.get('action') == cargo_info['action']
    is_user_type_valid = data.get('user_type') == cargo_info['user_type']

    valid = (id_deviceid_valid and is_action_valid
             and is_user_type_valid and is_valid_weight)

    if data['action'] == 'put_to_hangar':
        is_pin_valid = data['pin'] == cargo_info['pin']
        valid = valid and is_pin_valid

    response['valid'] = valid

    return jsonify(response), 200


@app.route("/api/CargoUserInfo", methods=['GET'])
def get_cargo_user_info():
    deviceid = request.args.get('deviceid')
    if deviceid is None:
        return jsonify({"status": "wrong query"}), 422

    is_action_empty = cargo_info['action'] is None
    has_incorrect_deviceid = cargo_info['deviceid'] != deviceid

    if is_action_empty or has_incorrect_deviceid:
        response = {"action": "none"}
        return jsonify(response), 200

    response = {k: cargo_info[k] for k in cargo_info.keys() if k != 'deviceid'}

    return jsonify(response), 200


@app.route("/api/cargo_info", methods=['POST'])
def create_cargo_user_info():
    data = request.get_json()

    if not all(key in data for key in ('deviceid', 'action', 'user_type')):
        return jsonify(status="wrong keywords"), 400

    if data.get('pin') is None:
        data['pin'] = str(randint(000000, 999999))

    cargo_info.update(data)

    return jsonify(status="ok"), 200


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=48647)
