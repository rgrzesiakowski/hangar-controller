from hangar_controller.devices.drivers.messages.commands.power_management_system import \
    PMSGetPLCIO, PMSGetStates, PMSSetChargersPower, PMSSetIRLockPower, \
    PMSSetMotorsLiftPower, PMSSetMotorsManPower, PMSSetMotorsPosPower, \
    PMSSetPressurePower, PMSSetAirCompressorPower
from settingsd.drivers.stm_communication_settings import Controllers
from tests.mocks.drives_mock import DriversMock


class STMPMSMock(DriversMock):
    def __init__(self):
        super().__init__('STMPMSMock')
        self.commands = [PMSGetPLCIO(), PMSGetStates(),
                         PMSSetChargersPower(PMSSetChargersPower.ENABLE),
                         PMSSetIRLockPower(PMSSetIRLockPower.ENABLE),
                         PMSSetMotorsLiftPower(PMSSetMotorsLiftPower.ENABLE),
                         PMSSetMotorsManPower(PMSSetMotorsManPower.ENABLE),
                         PMSSetMotorsPosPower(PMSSetMotorsPosPower.ENABLE),
                         PMSSetPressurePower(PMSSetPressurePower.ENABLE),
                         PMSSetAirCompressorPower(PMSSetAirCompressorPower.ENABLE)]

    def subscribe(self):
        self.client.subscribe(Controllers.STM_PMS + '/#')
