from hangar_controller.devices.drivers.messages.commands.master_charger import \
    MasterChargerGetPLCIO, MasterChargerGetStates, MasterChargerGetTelemetry, \
    MasterChargerSetPower
from settingsd.drivers.stm_communication_settings import Controllers
from tests.mocks.drives_mock import DriversMock


class STMMasterChargerMock(DriversMock):
    def __init__(self):
        super().__init__('STMMasterChargerMock')
        self.commands = [
            MasterChargerGetPLCIO(), MasterChargerGetStates('1'),
            MasterChargerGetTelemetry('1'),
            MasterChargerSetPower('1',
                                  MasterChargerSetPower.ENABLED),
        ]

    def subscribe(self):
        self.client.subscribe(Controllers.STM_MASTER_CHARGER + '/#')
