from hangar_controller.devices.drivers.messages.commands.climate import \
    ClimateGetParameters, ClimateGetPLCIO, ClimateGetStates, \
    ClimateGetTelemetry, SetAC, SetFan, SetHangarHeater, \
    SetHangarHeaterHysteresis, SetHangarHeaterTemperature, \
    SetHangarHumidityHysteresis, SetHangarHumiditySetPoint, \
    SetRoofHeater, SetRoofHeaterHysteresis, SetRoofHeaterTemperature, \
    SetTypeClimateController
from settingsd.drivers.stm_communication_settings import Controllers
from tests.mocks.drives_mock import DriversMock


class STMClimateMock(DriversMock):
    def __init__(self):
        super().__init__('STMClimateMock')
        self.commands = [
            ClimateGetParameters(), ClimateGetPLCIO(), ClimateGetStates(),
            ClimateGetTelemetry(), SetAC(SetAC.ENABLE), SetFan(SetFan.ENABLE),
            SetHangarHeater(SetHangarHeater.ENABLE),
            SetHangarHeaterHysteresis(15), SetHangarHeaterTemperature(15),
            SetHangarHumidityHysteresis(15), SetHangarHumiditySetPoint(15),
            SetRoofHeater(SetRoofHeater.ENABLE), SetRoofHeaterHysteresis(15),
            SetRoofHeaterTemperature(15),
            SetTypeClimateController(SetTypeClimateController.AUTO),
        ]

    def subscribe(self):
        self.client.subscribe(Controllers.STM_CLIMATE + '/#')
