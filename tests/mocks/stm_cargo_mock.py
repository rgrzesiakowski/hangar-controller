from hangar_controller.devices.drivers.messages.commands.cargo import \
    CargoBaseShift, CargoCloseWindow, CargoGetPLCIO, CargoGetStates, \
    CargoOpenWindow, CargoStopWindow, CargoWorkShift, CargoTare
from settingsd.drivers.stm_communication_settings import Controllers
from tests.mocks.drives_mock import DriversMock


class STMCargoMock(DriversMock):
    def __init__(self):
        super().__init__(Controllers.STM_CARGO)
        self.commands = [CargoBaseShift(), CargoCloseWindow(), CargoGetPLCIO(),
                         CargoGetStates(), CargoOpenWindow(), CargoStopWindow(),
                         CargoWorkShift(), CargoTare()]

    def subscribe(self):
        self.client.subscribe(Controllers.STM_CARGO + '/#')
