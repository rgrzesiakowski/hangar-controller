#!/bin/bash

python3 -m coverage run --source=hangar_controller \
                        -m unittest discover -s tests/unit/ \
&& \
python3 -m coverage report --skip-covered
