#!/bin/bash

# Script that prepares fresh installation of Ubuntu 18.04 to run hangar software:
# 1. Rsync hangar-controller to ~/hangar-controller
# 2. Rsync .env with environment variables to ~/.env
# 3. Run `bash ~/hangar-controller/install.sh` FROM UPBOARD and not SSH

install_upboard_kernel() {
  if [[ "$(uname -v)" =~ ^\#[0-9]+\~upboard.+ ]]; then
    echo "Proper kernel already installed. Kernel installation aborted"
    return 1
  fi
  cd $HOME
  export DEBIAN_FRONTEND=noninteractive
  sudo add-apt-repository -y ppa:aaeon-cm/up
  sudo apt-get update -y
  sudo apt-get autoremove -qy --purge 'linux-.*generic'
  sudo apt-get install -qy linux-image-generic-hwe-18.04-upboard
  sudo apt dist-upgrade -y
}

tmux_config() {
  sudo apt-get install -y tmux
  mkdir -p ~/.tmux/plugins
  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

  {
     echo "set -g @plugin 'tmux-plugins/tpm'"
     echo "set -g @plugin 'tmux-plugins/tmux-sensible'"
     echo "set -g @plugin 'tmux-plugins/tmux-resurrect'"
     echo "run '~/.tmux/plugins/tpm/tpm'"
  } >> ~/.tmux.conf
  sleep 1
  ~/.tmux/plugins/tpm/bin/install_plugins
}

install_zsh() {
  sudo apt-get install -y zsh
  # install oh-my-zsh
  0>/dev/null sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

  echo '[ -f ~/.env ] && export $(xargs < ~/.env)' >> ~/.zshrc
  echo "Set zsh as default shell. Enter password and reboot to confirm "
  chsh -s $(which zsh)

  {
    echo 'export ZSH=$HOME/.oh-my-zsh'
    echo 'ZSH_THEME="agnoster"'
    echo 'source $ZSH/oh-my-zsh.sh'
    echo 'set -o allexport; source ~/.env; set +o allexport'
    echo 'export PYTHONPATH="${PYTHONPATH}:${HOME}/hangar-controller"'
  }  > ~/.zshrc
}

setup_udev() {
  sudo adduser $(whoami) dialout
  sudo cp ~/hangar-controller/settingsd/logs_dump/60-usb-mapping.rules /etc/udev/rules.d/
  sudo udevadm control --reload-rules
  sudo udevadm trigger
}

prepare_project() {

  if [ ! -d ~/hangar-controller ]; then
    read -p "Repository not found in ~/hangar-controller. Press enter when repo is uploaded or ctrl+C to cancel"
  fi

  sudo apt-get update -y && sudo apt-get install -y python3-pip

  cat ~/hangar-controller/requirements.txt \
      ~/hangar-controller/tests/dry_run/requirements-dryrun.txt > requirements_all.txt

  export PIP_INDEX_URL=https://pip.cervirobotics.com/packages/stable/+simple/
  pip3 install -r requirements_all.txt
  rm requirements_all.txt

  sudo apt-get install -y libboost-python-dev

  setup_udev

  sudo mkdir -p /data/logs/stm_logs/
  sudo mkdir -p /data/logs/hangar_logs/
  sudo mkdir -p /data/logs/alerts/ && sudo touch /data/logs/alerts/existing_alerts.csv
  sudo chown -R $USER /data/logs
  cd $HOME
}

set_network() {
  sudo mv ~/hangar-controller/settingsd/installation/netplan_config.yaml /etc/netplan/00-installer-config.yaml
  sudo netplan apply
}

if [ ! -f ~/.resume-after-reboot ]; then

  script="bash ~/hangar-controller/install.sh"
  echo "$script" >> ~/.bashrc
  sudo touch ~/.resume-after-reboot
  install_upboard_kernel && sudo reboot

else

  if [[ "$(uname -v)" =~ ^\#[0-9]+\~upboard.+ ]]; then
    echo "UPboard kernel succesfully installed. Resuming installation post-reboot"
  else
    echo "UPboard kernel not installed properly. Aborting..."
    return 2
  fi

  sudo apt-get install -y sed
  sed -i '/bash ~\/hangar-controller\/install.sh/d' ~/.bashrc
  rm -f ~/.resume-after-reboot

  if [ ! -f ~/.env ]; then
    sudo touch ~/.env
  fi

  tmux_config
  prepare_project
  install_zsh
  set_network
fi
