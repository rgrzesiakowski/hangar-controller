class RoofControlCommands:
    ROOF = 'roof'
    SV_OPEN_LEFT = 'sv_open_left'
    SV_CLOSE_LEFT = 'sv_close_left'
    SV_OPEN_RIGHT = 'sv_open_right'
    SV_CLOSE_RIGHT = 'sv_close_right'
    CPS_MIN_LEFT = 'cps_min_left'
    CPS_MAX_LEFT = 'cps_max_left'
    CPS_MIN_RIGHT = 'cps_min_right'
    CPS_MAX_RIGHT = 'cps_max_right'
    LIGHT_BUZZER = 'light_buzzer'
    OPEN = 'open'
    CLOSE = 'close'
    STOP = 'stop'
    IDLE = 'idle'
    ERROR = 'error'


class Measurements:
    ROOF_LEFT = 'roof_left'


class RoofControlTimeouts:
    ROOF_ACK = 10
    ROOF_OPEN = 20
    ROOF_CLOSE = 20
    ROOF_STOP = 10
