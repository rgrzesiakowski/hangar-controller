class CargoControlCommands:
    WINDOW = 'window'
    SHIFT = 'shift'
    OPEN = 'open'
    CLOSE = 'close'
    STOP = 'stop'
    BASE = 'base'
    WORK = 'work'
    WORK_CONDITIONS = 'work,c'
    BASE_CONDITIONS = 'base,c'
    IDLE = 'idle'
    ERROR = 'error'
    CARGO_WEIGHT = 'cargo_weight'
    CARGO_PRESENCE = 'cargo_presence'


class CargoStates:
    WINDOW = 'window'
    CARGO_SHIFT = 'shift'
    SV_CARGO_WINDOW_OPEN = 'sv_cargo_window_open'
    SV_CARGO_WINDOW_CLOSE = 'sv_cargo_window_close'
    CPS_CARGO_WINDOW_OPEN = 'cps_cargo_window_open'
    CPS_CARGO_WINDOW_CLOSE = 'cps_cargo_window_close'
    RELAY_SAFETY_CURTAIN = 'relay_safety_curtain'
    OUTPUT_SAFETY_CURTAIN = 'output_safety_curtain'
    SAFETY_LIGHT = 'safety_light'
    SV_CARGO_SHIFT = 'sv_cargo_shift'
    CPS_CARGO_SHIFT_BASE = 'cps_cargo_shift_base'
    CPS_CARGO_SHIFT_WORK = 'cps_cargo_shift_work'
    OUTPUT_POS_CARGO_A = 'output_pos_cargo_A'
    OUTPUT_POS_CARGO_B = 'output_pos_cargo_B'


class CargoPresenceStates:
    ABSENT = 'absent'
    PRESENT = 'present'
    INCORRECT = 'incorrect'


class CargoTimeouts:
    WINDOW_OPEN = 20
    WINDOW_CLOSE = 20
    WINDOW_STOP = 1
    WINDOW_ACK = 5
    SHIFT_BASE = 5
    SHIFT_BASE_CHECK_COND = 5
    SHIFT_WORK = 5
    SHIFT_WORK_CHECK_COND = 5
    CARGO_WEIGHT = 15
    CARGO_PRESENCE = 5
