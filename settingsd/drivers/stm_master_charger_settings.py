class MasterChargerControlCommands:
    POWER = 'power'
    BATTERY = 'battery'
    HEARTBEAT = 'heartbeat'
    ENABLE = 'enable'
    DISABLE = 'disable'
    CHARGE = 'charge'
    DISCHARGE = 'discharge'
    STORAGE = 'storage'
    STOP = 'stop'


class MasterChargerStates:
    POWER = 'power'
    BATTERY = 'battery'
    CHARGING = 'charging'
    CHARGED = 'charged'
    UNCHARGED = 'uncharged'
    PLUGGED = 'plugged'
    UNPLUGGED = 'unplugged'
    CHARGERS_NUMBER = 'chargers_num'
    AVAILABLE_CHARGERS = 'available_chargers'


class Measurements:
    UID = 'uid'
    BMS_TEMPERATURE = 'bms_temperature'
    TEMPERATURE = 'temperature'
    VOLTAGE = 'voltage'
    CURRENT = 'charge_current'


class MasterChargerTimeouts:
    POWER = 1
    CHARGING = 5
    CHARGED = 10
