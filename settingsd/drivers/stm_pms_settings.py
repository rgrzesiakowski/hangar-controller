from enum import Enum


class PMSComponent(Enum):
    TRAFO = 'trafo'
    POSITIONING_MOTORS = 'positioning motors'
    MANIPULATOR_MOTORS = 'manipulator motors'
    LIFT_MOTORS = 'lift motors'
    AIR_COMPRESSOR = 'air compressor'
    PRESSURE = 'pressure'
    IR_LOCK = 'IR-Lock'


class PMSControlCommands:
    PRESSURE = 'pressure'
    TRAFO = 'trafo'
    MOTORS_POS_POWER = 'motors_pos_power'
    MOTORS_MAN_POWER = 'motors_man_power'
    MOTORS_LIFT_POWER = 'motors_lift_power'
    IR_LOCK_POWER = 'ir_lock_power'
    AIR_COMPRESSOR_POWER = 'air_compressor_power'


class PMSStates:
    PRESSURE = 'pressure'
    TRAFO = 'trafo'
    MOTORS_POS_POWER = 'motors_pos_power'
    MOTORS_MAN_POWER = 'motors_man_power'
    MOTORS_LIFT_POWER = 'motors_lift_power'
    IR_LOCK_POWER = 'ir_lock_power'
    AIR_COMPRESSOR_POWER = 'air_compressor_power'


class PMSTimeouts:
    PRESSURE = 1
    CHARGERS_POWER = 1
    MOTORS_POS_POWER = 3
    MOTORS_MAN_POWER = 1
    MOTORS_LIFT_POWER = 1
    IR_LOCK_POWER = 1
    AIR_COMPRESSOR = 1
