class ClimateControlCommands:
    TYPE = 'type'
    AC = 'ac'
    HANGAR_HEATER = 'hangar_heater'
    HANGAR_HEAT = 'hangar_heat'
    HANGAR_HYSTERESIS = 'hangar_hysteresis'
    HANGAR_HUMIDITY = 'hangar_humidity'
    ROOF_HEATER = 'roof_heater'
    ROOF_HEAT = 'roof_heat'
    ROOF_HYSTERESIS = 'roof_hysteresis'
    FAN = 'fan'
    AUTO = 'automatic'
    MANUAL = 'manual'


class Measurements:
    ROOF_LEFT = 'roof_left'
    ROOF_RIGHT = 'roof_right'
    HANGAR_UPPER = 'hangar_upper'
    HANGAR_LOWER = 'hangar_lower'
    HANGAR_AVG = 'hangar_avg'
    ROOF_HEATING = 'roof_heating'
    ROOF_HYSTERESIS = 'roof_hysteresis'
    HANGAR_HEATING = 'hangar_heating'
    HANGAR_HYSTERESIS = 'hangar_hysteresis'
    HANGAR_HUMIDIFIER = 'hangar_humidity'


class ClimateControlTimeouts:
    TYPE = 1
    AC = 1
    HANGAR_HEATER = 4
    ROOF_HEATER = 4
    FAN = 4
