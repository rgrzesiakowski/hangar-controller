from enum import Enum
from typing import List, Tuple

ERROR_RESPONSE = 0x80


class ModbusResponseStatus:
    STATUS_OK = 0
    CONNECTION_ERROR = 1
    PARAMETERS_ERROR = 2
    VALUE_ERROR = 3


class MotorErrors:
    MOTOR_LIMIT_REACHED = 0


ADDRESS_LIST_TYPE = List[Tuple[str, int]]


class Wobit:
    class MotorNames:
        pass

    class BatteriesMotorsNames(MotorNames):
        MOTOR_LIFT = 'MOTOR_LIFT'
        MOTOR_X = 'MOTOR_X'
        MOTOR_Y = 'MOTOR_Y'
        MOTOR_Z = 'MOTOR_Z'

    class PositioningMotorsNames(MotorNames):
        MOTOR_X = 'MOTOR_X_PLATFORM'
        MOTOR_Y = 'MOTOR_Y_PLATFORM'
        MOTOR_GRASPER = 'MOTOR_GRASPER'

    class BitRegistersNames:
        OUTPUTS_START = 'OUTPUTS_START'
        Mx_LIMIT_SWITCH_OUT = 'M_LIMIT_SWITCH_OUT'
        Mx_LIMIT_SWITCH_IN = 'M_LIMIT_SWITCH_IN'

        Mx_POWER = 'M_POWER'
        Mx_JOG_PLUS = 'M_JOG_PLUS'
        Mx_JOG_MINUS = 'M_JOG_MINUS'
        USER_REG_READ = 'USER_REG_READ'
        USER_REG_SAVE = 'USER_REG_SAVE'
        USER_REG_RESET = 'USER_REG_RESET'

    class HoldingRegistersNames:
        DATA_TYPE = 'DATA_TYPE'
        PROG_BANK_SEL = 'PROG_BANK_SEL'
        POS_BANK_SEL = 'POS_BANK_SEL'
        PROGRAM_CTRL = 'PROGRAM_CTRL'
        AXIS_CTR_MODE = 'AXIS_CTR_MODE'
        AXIS_VEL_ABS_TRIG = 'AXIS_VEL_ABS_TRIG'
        AXIS_VEL_REL_TRIG = 'AXIS_VEL_REL_TRIG'
        AXIS_POS_ABS_TRIG = 'AXIS_POS_ABS_TRIG'
        AXIS_POS_REL_TRIG = 'AXIS_POS_REL_TRIG'
        M_C_STATUS = 'M_C_STATUS'
        M_STATUS = 'M_STATUS'
        M_ENABLE = 'M_ENABLE'
        M_DISABLE = 'M_DISABLE'
        M_STOP = 'M_STOP'
        WOBIT_ID = 'WOBIT_ID'

    class RealRegistersNames:
        # Actual values and parameters
        Mx_POS_ACT = 'M_POS_ACT'
        Mx_VEL_ACT = 'M_VEL_ACT'
        Mx_ACC = 'M_ACC'
        Mx_DEC = 'M_DEC'
        Mx_VMAX = 'M_VMAX'

        # Movement control
        Mx_HOME = 'M_HOME'
        Mx_VEL_ABS = 'M_VEL_ABS'
        Mx_POS_ABS = 'M_POS_ABS'
        Mx_POS_REL = 'M_POS_REL'
        Mx_POSLIM_L = 'M_POSLIM_L'
        Mx_POSLIM_R = 'M_POSLIM_R'

    class DataTypes(Enum):
        # class for setting data type to controller
        DINT = 1
        REAL = 0

    class AxisControlMode(Enum):
        DIRECT = 0
        TRIGGER = 1

    class ControllerStatus:
        STATUS_OK = 0
        ENCODER_ERROR = 1
        INSTRUCTION_ERROR = 2
        COMMUNICATION_ERROR = 3
        CONNECTION_ERROR = 4

    class MotorStatus:
        DISABLED = 0
        ENABLED = 1
        VELOCITY_MODE = 2
        POSITION_MODE = 3
        AT_POSITION = 4
        POSITION_ERROR = 5
        HOME_MODE = 6
        POSITION_CORRECTION = 8
        L_LIMIT = 9
        R_LIMIT = 10

    class RestoreRegistersType:
        WORK = 0
        TEST = 1

    class UserRegisters:
        WOBIT_ID = 1000


class WobitMotorAddresses:
    WOBIT_ID = None
    DISCRETE_INPUTS_ADDRESSES = []
    DISCRETE_OUTPUTS_ADDRESSES = []
    HOLDING_REGISTERS = []
    REAL_REGISTERS = []

    COMMON_HOLDING_REGISTERS = [
        # WORD registers
        (Wobit.HoldingRegistersNames.DATA_TYPE, 10000),  # R/W
        (Wobit.HoldingRegistersNames.PROG_BANK_SEL, 10001),  # R/W
        (Wobit.HoldingRegistersNames.POS_BANK_SEL, 10002),  # R/W
        (Wobit.HoldingRegistersNames.PROGRAM_CTRL, 10003),  # R/W
        (Wobit.HoldingRegistersNames.AXIS_CTR_MODE, 10004),  # R/W
        (Wobit.HoldingRegistersNames.AXIS_VEL_ABS_TRIG, 10005),  # R/W
        (Wobit.HoldingRegistersNames.AXIS_VEL_REL_TRIG, 10006),  # R/W
        (Wobit.HoldingRegistersNames.AXIS_POS_ABS_TRIG, 10007),  # R/W
        (Wobit.HoldingRegistersNames.AXIS_POS_REL_TRIG, 10008),  # R/W
        (Wobit.HoldingRegistersNames.M_C_STATUS, 10009),
        (Wobit.HoldingRegistersNames.M_ENABLE, 10014),  # W
        (Wobit.HoldingRegistersNames.M_DISABLE, 10015),  # W
        (Wobit.HoldingRegistersNames.M_STOP, 10016),  # W
    ]
    USER_REG_READ = 2100
    USER_REG_SAVE = 2101


class WobitMotor1Addresses(WobitMotorAddresses):
    WOBIT_ID = 1

    DISCRETE_INPUTS_ADDRESSES = [
        (Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_IN, 5000),
        (Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_OUT, 5001),
    ]

    DISCRETE_OUTPUTS_ADDRESSES = [
        (Wobit.BitRegistersNames.Mx_POWER, 2001),
    ]

    HOLDING_REGISTERS = [
        (Wobit.HoldingRegistersNames.M_STATUS, 10010),  # R
    ]

    REAL_REGISTERS = [
        (Wobit.RealRegistersNames.Mx_POS_ACT, 10020),
        (Wobit.RealRegistersNames.Mx_VEL_ACT, 10028),
        (Wobit.RealRegistersNames.Mx_ACC, 10036),
        (Wobit.RealRegistersNames.Mx_DEC, 10044),
        (Wobit.RealRegistersNames.Mx_VMAX, 10052),

        (Wobit.RealRegistersNames.Mx_HOME, 10060),
        (Wobit.RealRegistersNames.Mx_VEL_ABS, 10068),
        (Wobit.RealRegistersNames.Mx_POS_ABS, 10084),
        (Wobit.RealRegistersNames.Mx_POS_REL, 10092),
        (Wobit.RealRegistersNames.Mx_POSLIM_L, 10120),
        (Wobit.RealRegistersNames.Mx_POSLIM_R, 10128),
    ]


class WobitMotor2Addresses(WobitMotorAddresses):
    WOBIT_ID = 2

    DISCRETE_INPUTS_ADDRESSES = [
        (Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_IN, 5002),
        (Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_OUT, 5003),
    ]

    DISCRETE_OUTPUTS_ADDRESSES = [
        (Wobit.BitRegistersNames.Mx_POWER, 2002),
    ]

    HOLDING_REGISTERS = [
        (Wobit.HoldingRegistersNames.M_STATUS, 10011),  # R
    ]

    REAL_REGISTERS = [
        (Wobit.RealRegistersNames.Mx_POS_ACT, 10022),
        (Wobit.RealRegistersNames.Mx_VEL_ACT, 10030),
        (Wobit.RealRegistersNames.Mx_ACC, 10038),
        (Wobit.RealRegistersNames.Mx_DEC, 10046),
        (Wobit.RealRegistersNames.Mx_VMAX, 10054),

        (Wobit.RealRegistersNames.Mx_HOME, 10062),
        (Wobit.RealRegistersNames.Mx_VEL_ABS, 10070),
        (Wobit.RealRegistersNames.Mx_POS_ABS, 10086),
        (Wobit.RealRegistersNames.Mx_POS_REL, 10094),
        (Wobit.RealRegistersNames.Mx_POSLIM_L, 10122),
        (Wobit.RealRegistersNames.Mx_POSLIM_R, 10130),
    ]


class WobitMotor3Addresses(WobitMotorAddresses):
    WOBIT_ID = 3

    DISCRETE_INPUTS_ADDRESSES = [
        (Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_IN, 5004),
        (Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_OUT, 5005),
    ]

    DISCRETE_OUTPUTS_ADDRESSES = [
        (Wobit.BitRegistersNames.Mx_POWER, 2003),
    ]

    HOLDING_REGISTERS = [
        (Wobit.HoldingRegistersNames.M_STATUS, 10012),  # R
    ]

    REAL_REGISTERS = [
        (Wobit.RealRegistersNames.Mx_POS_ACT, 10024),
        (Wobit.RealRegistersNames.Mx_VEL_ACT, 10032),
        (Wobit.RealRegistersNames.Mx_ACC, 10040),
        (Wobit.RealRegistersNames.Mx_DEC, 10048),
        (Wobit.RealRegistersNames.Mx_VMAX, 10056),

        (Wobit.RealRegistersNames.Mx_HOME, 10064),
        (Wobit.RealRegistersNames.Mx_VEL_ABS, 10072),
        (Wobit.RealRegistersNames.Mx_POS_ABS, 10088),
        (Wobit.RealRegistersNames.Mx_POS_REL, 10096),
        (Wobit.RealRegistersNames.Mx_POSLIM_L, 10124),
        (Wobit.RealRegistersNames.Mx_POSLIM_R, 10132),
    ]


class WobitMotor4Addresses(WobitMotorAddresses):
    WOBIT_ID = 4

    DISCRETE_INPUTS_ADDRESSES = [
        (Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_IN, 5006),
        (Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_OUT, 5007),
    ]

    DISCRETE_OUTPUTS_ADDRESSES = [
        (Wobit.BitRegistersNames.Mx_POWER, 2004),
    ]

    HOLDING_REGISTERS = [
        (Wobit.HoldingRegistersNames.M_STATUS, 10013),  # R
    ]

    REAL_REGISTERS = [
        (Wobit.RealRegistersNames.Mx_POS_ACT, 10026),
        (Wobit.RealRegistersNames.Mx_VEL_ACT, 10034),
        (Wobit.RealRegistersNames.Mx_ACC, 10042),
        (Wobit.RealRegistersNames.Mx_DEC, 10050),
        (Wobit.RealRegistersNames.Mx_VMAX, 10058),

        (Wobit.RealRegistersNames.Mx_HOME, 10066),
        (Wobit.RealRegistersNames.Mx_VEL_ABS, 10074),
        (Wobit.RealRegistersNames.Mx_POS_ABS, 10090),
        (Wobit.RealRegistersNames.Mx_POS_REL, 10098),
        (Wobit.RealRegistersNames.Mx_POSLIM_L, 10126),
        (Wobit.RealRegistersNames.Mx_POSLIM_R, 10134),
    ]


ALL_ADDRESSES = [WobitMotor1Addresses, WobitMotor2Addresses,
                 WobitMotor3Addresses, WobitMotor4Addresses]
