import os

FILE_PATH = os.path.dirname(__file__) + '/parameters.yaml'


class Parameters:
    DRIVES = 'drives'
    CONNECTION_0 = 'connection_0'
    CONNECTION_1 = 'connection_1'
    CONTROLLER = 'controller'
    POSITION = 'positions'


class ConnectionParameters:
    PORT = 'port'
    BAUDRATE = 'baudrate'
    STOP_BITS = 'stop_bits'
    PARITY = 'parity'
    DEVICE_ADDRESS = 'device_address'
    TIMEOUT = 'timeout'


class Positions:
    SLOTS = 'slots'
    CARGO_WINDOW = 'cargo_window'
    CARGO_DRONE = 'cargo_drone'
    BATTERY_DRONE_TAKE = 'battery_drone_take'
    BATTERY_DRONE_PUT = 'battery_drone_put'
    LIFT_UP = 'lift_up'
    LIFT_DOWN = 'lift_down'
    HOME = 'home'
    MOTOR_Y_BACK = 'motor_y_back'
    CARGO_WINDOW_PICK_UP = 'cargo_window_pick_up'
    CARGO_SAFE_MOVE_HEIGHT = 'cargo_safe_move_height'
    PLATFORM_MIDDLE = 'platform_middle'
    PLATFORM_HOME = 'platform_home'
    GRASP = 'grasp'
    RELEASE = 'release'


class RelativePositions:
    UP = 'up'
    DOWN = 'down'
    MIDDLE = 'middle'


class Drives:
    RESET_MOTOR_TIME = 'reset_motor_time'
    POLLING_FREQUENCY = 'polling_frequency'


class DrivesNames:
    LIFT = 'lift'
    X = 'x'
    Y = 'y'
    Z = 'z'
    X_PLATFORM = 'x_platform'
    Y_PLATFORM = 'y_platform'
    GRASPER = 'grasper'


class Motor:
    MOTOR_ID = 'motor_id'
    WOBIT_ID = 'wobit_id'
    ACCELERATION = 'acceleration'
    DECELERATION = 'deceleration'
    MAX_VELOCITY = 'max_velocity'
    CALIBRATION_SPEED = 'calibration_speed'
    RESET_OUTPUT_ID = 'reset_output_id'
    ALARM_INPUT_ID = 'alarm_input_id'


class Coordinates:
    X = 'x'
    Y = 'y'
    Z = 'z'
    UP = 'up'
    DOWN = 'down'
    GRASPER = 'grasper'


class CurrentOperators:
    EMPTY = 'empty'
