from datetime import timedelta

from .hangar_dump_settings import DUMP_FOLDER as HANGAR_DUMP_FOLDER
from .stm_dump_settings import DUMP_FOLDER as STM_DUMP_FOLDER

SUPERVISED_FOLDERS = [HANGAR_DUMP_FOLDER, STM_DUMP_FOLDER]

UPLOAD_AT_DELAY = timedelta(minutes=5).seconds

TERMINATION_WATCHDOG_INTERVAL = 5  # seconds

UPLOAD_RETRIES_NUM = 3
UPLOAD_RETRY_INTERVAL = 5  # seconds
