import re

DEVICES = ['roof_controller', 'cargo_controller', 'climate_controller',
           'pms_controller', 'master_charger_controller',
           'user_panel_controller']
BAUD_RATE = 115200

NEXT_CONN_RETRY_DELAY = {0: 2,
                         2: 3,
                         3: 5,
                         5: 10,
                         10: 30,
                         30: 60,
                         60: 300,
                         300: 1800,
                         1800: 1800}  # seconds
READ_TIMEOUT = 1  # seconds
LOOP_TIME = 5

DUMP_SIZE = 64  # rows

LOG_TIMESTAMP_FORMAT = r'^\(\d*\)'
# example: '(1234567)'

LOG_FORMAT = LOG_TIMESTAMP_FORMAT + r' \[[\w\ \/\-]*\] >> .*$'
# example: '(1234) [ UPPERCASE   / snake_case ] >> Freeform [text]'

LOG_COLORS = ['\x1b[31m', '\x1b[0m', '\x1b[36m', '\x1b[33m', '\x1b[32m']

LOG_TIMESTAMP_REGEX = re.compile(LOG_TIMESTAMP_FORMAT)
LOG_REGEX = re.compile(LOG_FORMAT)

DUMP_FOLDER = '/data/logs/stm_logs/'
