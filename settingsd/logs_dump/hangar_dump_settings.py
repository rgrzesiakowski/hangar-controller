LOG_FORMAT = '%(asctime)s [ %(levelname)6s ] [ %(name)s ] %(message)s'
TIME_FORMAT = '%s'

DUMP_FOLDER = '/data/logs/hangar_logs/'
