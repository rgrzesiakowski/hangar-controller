class MessagesSettings:
    TIMEOUT = 1


class TelemetrySettings:
    FREQUENCY = 1
    RECONNECT_TIMES = 3
