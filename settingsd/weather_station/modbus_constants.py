from typing import List, Tuple, Union

ERROR_RESPONSE = 0x80


class ModbusResponseStatus:
    STATUS_OK = 0
    CONNECTION_ERROR = 1
    PARAMETERS_ERROR = 2
    VALUE_ERROR = 3


ADDRESS_LIST_TYPE = List[Tuple[str, int]]


class HoldingRegisters:
    DEVICE_STATE = 0
    WIND_DIRECTION = 1
    COMPASS_HEADING = 10
    PRECIPITATION_TYPE = 11
    GPS_STATUS = 17
    GPS_HEADING = 20
    DEVICE_STATE_2 = 51
    MAX_RELATIVE_WIND_DIRECTION_0_10 = 59
    MIN_RELATIVE_WIND_DIRECTION_0_10 = 60
    MAX_RELATIVE_WIND_DIRECTION_0_2 = 82
    MIN_RELATIVE_WIND_DIRECTION_0_2 = 83


class RealRegisters:
    WIND_SPEED = 2
    TEMPERATURE = 4
    HUMIDITY = 6
    PRESSURE = 8
    PRECIPITATION_INTENSITY = 12
    ACCUMULATED_PRECIPITATION = 14
    GPS_SPEED = 18
    LONGITUDE = 21
    LATITUDE = 23
    PM_2_5_CONCENTRATION = 25
    VISIBILITY = 27
    RADIATION_ILLUMINANCE = 29
    ACCUMULATED_SOLAR_RADIATION = 31
    SOLAR_RADIATION_POWER = 33
    TRUE_WIND_DIRECTION = 35
    ALTITUDE = 37
    TRUE_WIND_SPEED = 39
    ACCUMULATED_SNOW_THICKNESS = 41
    UV_RADIATION = 43
    PM_1_0_CONCENTRATION = 45
    PM_10_CONCENTRATION = 47
    COLOR_TEMPERATURE = 49
    AVERAGE_RELATIVE_WIND_SPEED_0_10 = 52
    MAX_RELATIVE_WIND_SPEED_0_10 = 54
    MIN_RELATIVE_WIND_SPEED_0_10 = 56
    AVG_TRUE_WIND_SPEED_0_10 = 61
    MAX_TRUE_WIND_SPEED_0_10 = 63
    MIN_TRUE_WIND_SPEED_0_10 = 65
    AVG_TRUE_WIND_DIR_0_10 = 67
    MAX_TRUE_WIND_DIR_0_10 = 69
    MIN_TRUE_WIND_DIR_0_10 = 71
    GUST = 73
    AVERAGE_RELATIVE_WIND_SPEED_0_2 = 75
    MAX_RELATIVE_WIND_SPEED_0_2 = 77
    MIN_RELATIVE_WIND_SPEED_0_2 = 79
    AVG_TRUE_WIND_SPEED_0_2 = 84
    MAX_TRUE_WIND_SPEED_0_2 = 86
    MIN_TRUE_WIND_SPEED_0_2 = 88
    AVG_TRUE_WIND_DIR_0_2 = 90
    MAX_TRUE_WIND_DIR_0_2 = 92
    MIN_TRUE_WIND_DIR_0_2 = 94


ADDRESS_TYPE = Union[HoldingRegisters, RealRegisters]


class PrecipitationTypes:
    NO_RAIN = 0
    RAIN = 1


class DeviceStateNames:
    UV_RADIATION = 'uv_radiation'
    SNOW_THICKNESS = 'snow_thickness'
    TRUE_WIND_SPEED = 'true_wind_speed'
    TRUE_WIND_DIRECTION = 'true_wind_direction'
    ALTITUDE = 'altitude'
    VISIBILITY = 'visibility'
    LUMINANCE = 'luminance'
    SOLAR_RADIATION = 'solar_radiation'
    PM_1_0__2_5__10 = 'pm_1_0__2_5__10'
    GPS = 'gps'
    PRECIPITATION = 'precipitation'
    COMPASS = 'compass'
    PRESSURE = 'pressure'
    RELATIVE_WIND = 'relative_wind'
    TEMPERATURE_AND_HUMIDITY = 'temperature_and_humidity'


DEVICE_STATUS_NAMES = [value for key, value in DeviceStateNames.__dict__.items()
                       if not key.startswith('__')]
DEVICE_STATUS_TEMPLATE = dict(
    zip(DEVICE_STATUS_NAMES, [False] * len(DEVICE_STATUS_NAMES)))
