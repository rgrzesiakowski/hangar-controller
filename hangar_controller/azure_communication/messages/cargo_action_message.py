import enum

from hangar_controller.azure_communication.messages.base_iot_hub_message \
    import BaseIoTHubMessage


class CargoActionType(enum.Enum):
    LOADING_DRONE = 'loading_drone'
    UNLOADING_DRONE = 'unloading_drone'
    PUT_TO_HANGAR = 'put_to_hangar'
    TAKE_FROM_HANGAR = 'take_from_hangar'


class CargoActionObjectType(enum.Enum):
    DRONE = 'drone'
    CLIENT = 'client'
    SERVICE = 'service'


class CargoAction(BaseIoTHubMessage):
    def __init__(self, action: CargoActionType, action_object: str,
                 action_object_type: CargoActionObjectType, action_result: bool,
                 cargo_id: str, cargo_weight: int):
        super().__init__()

        self.action = action.value
        self.action_object = action_object
        self.action_object_type = action_object_type.value
        self.cargo_id: str = cargo_id
        self.cargo_weight: int = cargo_weight
        self.action_result = action_result

    def _prepare_payload(self) -> dict:
        payload = super()._prepare_payload()
        payload["cargo_action"] = {
            "action": self.action,
            "action_object": self.action_object,
            "action_object_type": self.action_object_type,
            "cargo_id": self.cargo_id,
            "cargo_weight": self.cargo_weight,
            "action_result": self.action_result
        }

        return payload

    def _prepare_properties(self) -> dict:
        properties = super()._prepare_properties()
        properties["message_type"] = "cargo_action"
        properties["cargo_id"] = self.cargo_id

        return properties
