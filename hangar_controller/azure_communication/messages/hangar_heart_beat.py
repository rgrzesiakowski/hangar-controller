import typing as tp

from hangar_controller.azure_communication.messages.base_iot_hub_message \
    import BaseIoTHubMessage


class HangarHeartBeat(BaseIoTHubMessage):
    system_info_getter: tp.Optional[tp.Callable[[], dict]] = None
    weather_info_getter: tp.Optional[tp.Callable[[], dict]] = None
    breakdowns_getter: tp.Optional[tp.Callable[[], list]] = None

    def __init__(self, operator_id: str = None, **kwargs):
        """Parameter operator_id is allowed only when message is
        prepared as response for alive command and should contain
        operator_id from command.
        """

        super().__init__()

        self.system_drone_id: tp.Optional[str] = kwargs.pop('system_drone_id',
                                                            None)
        self.system_cargo_id: tp.Optional[str] = kwargs.pop('system_cargo_id',
                                                            None)
        self.system_cargo_weight: int = kwargs.pop('system_cargo_weight', 0)
        self.system_state = kwargs.pop('system_state', None)
        self.system_available_battery = kwargs.pop('system_available_battery',
                                                   None)

        self.weather_wind_speed = kwargs.pop('weather_wind_speed', None)
        self.weather_wind_direction = kwargs.pop('weather_wind_direction', None)
        self.weather_temperature = kwargs.pop('weather_temperature', None)
        self.weather_humidity = kwargs.pop('weather_humidity', None)
        self.weather_pressure = kwargs.pop('weather_pressure', None)

        self.breakdowns: tp.List[str] = []
        self._operator_id: tp.Optional[str] = operator_id

        self._fill_system_info_section()
        self._fill_weather_info_section()
        self._fill_breakdowns()

    @staticmethod
    def set_system_info_getter(system_info_getter: tp.Optional[tp.Callable]):
        """Set system_info getter to allow message get information about
        system.

        Your system_info getter should return, you can check necessary
        dict keys into self._fill_system_info_section implementation.
        """

        HangarHeartBeat.system_info_getter = system_info_getter

    @staticmethod
    def set_weather_info_getter(weather_info_getter: tp.Optional[tp.Callable]):
        """Set weather_info getter to allow message get information
        about weather.

        Your weather_info getter should return, you can check necessary
        dict keys into self._fill_weather_info_section implementation.
        """

        HangarHeartBeat.weather_info_getter = weather_info_getter

    @staticmethod
    def set_breakdowns_getter(breakdowns_getter: tp.Optional[tp.Callable]):
        """Set breakdowns getter to get information about zones that
        are currently damaged.

        Your breakdowns_getter should return a list of strings, where
        strings are faulty zones.
        """

        HangarHeartBeat.breakdowns_getter = breakdowns_getter

    def _prepare_payload(self) -> dict:
        payload = super()._prepare_payload()
        payload["hangar_heart_beat"] = {
            "system": {
                "drone_id": self.system_drone_id,
                "cargo_id": self.system_cargo_id,
                "cargo_weight": self.system_cargo_weight,
                "state": self.system_state,
                "available_battery": self.system_available_battery
            },
            "weather": {
                "wind_speed": round(self.weather_wind_speed, 1),
                "wind_direction": self.weather_wind_direction,
                "temperature": round(self.weather_temperature, 1),
                "humidity": round(self.weather_humidity, 1),
                "pressure": round(self.weather_pressure, 1)
            },
            "breakdowns": self.breakdowns
        }

        return payload

    def _prepare_properties(self) -> dict:
        properties = super()._prepare_properties()
        properties["message_type"] = "hangar_heart_beat"
        properties["operator_id"] = self._operator_id

        return properties

    def _fill_system_info_section(self):
        try:
            system_info = HangarHeartBeat.system_info_getter()
        except TypeError:
            return

        self.system_drone_id = system_info.get("drone_id", None)
        self.system_cargo_id = system_info.get("cargo_id", None)
        self.system_cargo_weight = system_info.get("cargo_weight", 0)
        self.system_state = system_info.get("state", None)
        self.system_available_battery = system_info.get("available_battery",
                                                        None)

    def _fill_weather_info_section(self):
        try:
            weather_info = HangarHeartBeat.weather_info_getter()
        except TypeError:
            return

        self.weather_wind_speed = weather_info.get("wind_speed", None)
        self.weather_wind_direction = weather_info.get("wind_direction", None)
        self.weather_temperature = weather_info.get("temperature", None)
        self.weather_humidity = weather_info.get("humidity", None)
        self.weather_pressure = weather_info.get("pressure", None)

    def _fill_breakdowns(self):
        try:
            breakdowns = HangarHeartBeat.breakdowns_getter()
        except TypeError:
            breakdowns = []

        self.breakdowns = breakdowns
