from hangar_controller.azure_communication.messages.base_iot_hub_message \
    import BaseIoTHubMessage
from hangar_controller.azure_communication.response_status import ResponseStatus


class CommandResponse(BaseIoTHubMessage):
    def __init__(self, command: dict, command_status: ResponseStatus):
        super().__init__()

        self.operator_id = None
        self.command_name = None
        self.command_timestamp = None
        self.command_status = None
        self.command_parameters = {}

        self._fill_message_fields(command, command_status)

    def _prepare_payload(self) -> dict:
        payload = super()._prepare_payload()
        payload["command_response"] = {
            "operator_id": self.operator_id,
            "command_name": self.command_name,
            "command_timestamp": self.command_timestamp,
            "command_status": self.command_status,
            "command_parameters": self.command_parameters
        }

        return payload

    def _prepare_properties(self) -> dict:
        properties = super()._prepare_properties()
        properties["message_type"] = "command_response"
        properties["operator_id"] = self.operator_id

        return properties

    def _fill_message_fields(self, command: dict,
                             command_status: ResponseStatus):
        self.operator_id = command.get("operator_id", None)
        self.command_name = command.get("name", None)
        self.command_timestamp = command.get("timestamp", None)
        self.command_parameters = command.get("parameters", {})
        self.command_status = str(command_status.name.lower())
