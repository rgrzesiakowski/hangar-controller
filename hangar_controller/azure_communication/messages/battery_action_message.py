import enum

from hangar_controller.azure_communication.messages.base_iot_hub_message \
    import BaseIoTHubMessage


class BatteryActionType(enum.Enum):
    CONNECTING_DRONE = 'connecting_drone'
    DISCONNECTING_DRONE = 'disconnecting_drone'


class BatteryAction(BaseIoTHubMessage):
    def __init__(self, action: BatteryActionType, action_object: str,
                 battery_id: str):
        super().__init__()

        self.action = action.value
        self.action_object = action_object
        self.battery_id: str = battery_id

    def _prepare_payload(self) -> dict:
        payload = super()._prepare_payload()
        payload["battery_action"] = {
            "action": self.action,
            "action_object": self.action_object,
            "battery_id": self.battery_id,
        }

        return payload

    def _prepare_properties(self) -> dict:
        properties = super()._prepare_properties()
        properties["message_type"] = "battery_action"
        properties["battery_id"] = self.battery_id

        return properties
