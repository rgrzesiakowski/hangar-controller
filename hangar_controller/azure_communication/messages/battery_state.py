import typing as tp

from hangar_controller.azure_communication.messages.base_iot_hub_message \
    import BaseIoTHubMessage


class BatteryState(BaseIoTHubMessage):
    battery_info_getter: tp.Optional[tp.Callable[[], dict]] = None

    def __init__(self, operator_id: str = None):
        """Parameter operator_id is allowed only when message is
        prepared as response for alive_battery command and should
        contain operator_id from command.
        """

        super().__init__()

        self.available_battery: tp.Optional[int] = None
        self.batteries: tp.Optional[tp.List] = None
        self._operator_id: tp.Optional[str] = operator_id

        self._fill_battery_info()

    @staticmethod
    def set_battery_info_getter(battery_info_getter: tp.Optional[tp.Callable]):
        """Set battery_info getter to allow message get information about
        battery.

        Your battery_info getter should return, you can check necessary
        dict keys into self._fill_battery_info implementation.
        """

        BatteryState.battery_info_getter = battery_info_getter

    def _prepare_payload(self) -> dict:
        payload = super()._prepare_payload()
        payload["battery_state"] = {
            "available_battery": self.system_available_battery,
            "batteries": self.batteries
        }

        return payload

    def _prepare_properties(self) -> dict:
        properties = super()._prepare_properties()
        properties["message_type"] = "battery_state"
        properties["operator_id"] = self._operator_id

        return properties

    def _fill_battery_info(self):
        try:
            battery_info = BatteryState.battery_info_getter()
        except TypeError:
            return

        self.system_available_battery = battery_info.get(
            "available_battery", None
        )
        self.batteries = battery_info.get("batteries", None)
