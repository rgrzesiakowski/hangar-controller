import time
import typing as tp

import common
import settings


class BaseIoTHubMessage:
    msg_counter = 0
    last_timestamp = 0
    flight_id_getter: tp.Optional[tp.Callable[[], str]] = None
    hangar_type_getter: tp.Optional[tp.Callable[[], tp.Optional[str]]] = None

    @staticmethod
    def reset_counter():
        """Reset internal message counter"""
        BaseIoTHubMessage.msg_counter = 0

    def __init__(self):
        self.timestamp = common.get_timestamp_ms()
        self.flight_id: tp.Optional[str] = None
        self._hangar_type: tp.Optional[str] = None
        self.message_id: tp.Optional[str] = None
        self.device_type = settings.AZURE_DEVICE_TYPE
        self.device_id = settings.HANGAR_ID

        if self.timestamp == BaseIoTHubMessage.last_timestamp:
            time.sleep(0.01)
            self.timestamp = common.get_timestamp_ms()

        BaseIoTHubMessage.last_timestamp = self.timestamp

        try:
            self.flight_id = BaseIoTHubMessage.flight_id_getter()
            self._hangar_type = BaseIoTHubMessage.hangar_type_getter()
        except TypeError:
            self.flight_id = None
            self._hangar_type = None

        if (self.flight_id is not None) and (self._hangar_type is not None):
            BaseIoTHubMessage.msg_counter += 1
            self.message_id = f'{self._hangar_type}' \
                              f'{BaseIoTHubMessage.msg_counter}'
        else:
            BaseIoTHubMessage.msg_counter = 0

        self._properties_keys = ("message_id", "device_type", "flight_id",
                                 "timestamp", "device_id")
        self._message_template_keys = tuple(self.__dict__.keys())

    def get_data(self) -> dict:
        """Convert self to a dictionary"""

        payload = self._prepare_payload()
        properties = self._prepare_properties()
        properties = self._clean_up_properties(properties)

        data = {"payload": payload, "properties": properties}
        return data

    @staticmethod
    def set_flight_id_getter(flight_id_getter: tp.Optional[tp.Callable]):
        """
        Set flight_id getter to allow message get information about
        flight_id
        """

        BaseIoTHubMessage.flight_id_getter = flight_id_getter

    @staticmethod
    def set_hangar_type_getter(hangar_type_getter: tp.Optional[tp.Callable]):
        """
        Set hangar_type getter to allow message get information about
        hangar_type
        """

        BaseIoTHubMessage.hangar_type_getter = hangar_type_getter

    def _prepare_payload(self) -> dict:
        payload = {
            "timestamp": self.timestamp,
            "flight_id": self.flight_id,
            "message_id": self.message_id,
            "device_type": self.device_type,
            "device_id": self.device_id
        }

        return payload

    def _prepare_properties(self) -> dict:
        properties = {}
        for k, v in self.__dict__.items():
            if v is None:
                continue
            if k in self._properties_keys:
                properties[k] = v

        return properties

    @staticmethod
    def _clean_up_properties(properties: dict) -> dict:
        clean_properties = properties.copy()
        for k, v in properties.items():
            if v is None:
                del clean_properties[k]
                continue

            clean_properties[k] = str(v)

        return clean_properties
