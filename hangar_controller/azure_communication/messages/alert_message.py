from typing import List

from hangar_controller.azure_communication.messages import BaseIoTHubMessage


class AlertMessage(BaseIoTHubMessage):
    def __init__(self, code: str, repair: bool, breakdowns: List[str]):
        super().__init__()

        self.code = code
        self.repair = repair
        self.breakdowns = breakdowns

    def _prepare_payload(self) -> dict:
        payload = super()._prepare_payload()
        payload["alert"] = {
            "code": self.code,
            "repair": self.repair,
            "breakdowns": self.breakdowns
        }

        return payload

    def _prepare_properties(self) -> dict:
        properties = super()._prepare_properties()
        properties["message_type"] = "alert"

        return properties
