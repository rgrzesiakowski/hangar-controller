from hangar_controller.azure_communication.messages.base_iot_hub_message \
    import BaseIoTHubMessage

HangarState = 'hangar_controller.executor.HangarState'


class DeviceState(BaseIoTHubMessage):
    old_state: HangarState
    new_state: HangarState

    def __init__(self, old_state: HangarState, new_state: HangarState):
        super().__init__()

        self.old_state: HangarState = old_state
        self.new_state: HangarState = new_state

    def _prepare_payload(self) -> dict:
        payload = super()._prepare_payload()
        payload["device_state"] = {
            "old_state": self.old_state.value,
            "new_state": self.new_state.value
        }

        return payload

    def _prepare_properties(self) -> dict:
        properties = super()._prepare_properties()
        properties["message_type"] = "device_state"

        return properties
