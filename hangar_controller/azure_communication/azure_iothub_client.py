import json
import logging
import time
import typing as tp
from threading import Thread

import settings
from satella.coding.structures import Singleton
from satella.coding.concurrent import CallableGroup
from iothub_client import IoTHubTransportProvider, IoTHubClient, IoTHubMap, \
    IoTHubMessage, IoTHubClientConfirmationResult, DeviceMethodReturnValue, \
    IoTHubMessageDispositionResult

from hangar_controller.azure_communication.messages.base_iot_hub_message \
    import BaseIoTHubMessage
from hangar_controller.azure_communication.messages.command_response \
    import CommandResponse
from hangar_controller.azure_communication.response_status import ResponseStatus
from hangar_controller.executor import HangarData

logger = logging.getLogger(__name__)


@Singleton
class AzureIoTHubClient:
    def __init__(self):
        self.client: tp.Optional[IoTHubClient] = None
        self.azure_method_name_validator = tp.Optional[tp.Callable]
        self.message_validators = CallableGroup()
        self.message_handlers = CallableGroup()

        self.reset(True)

    def send_iothub_message(self, message: BaseIoTHubMessage):
        """
        Send Azure IoT Hub message

        Message should contain appropriate content (payload, properties)
        according to specification
        """
        message_data = message.get_data()

        message = IoTHubMessage(json.dumps(message_data["payload"]))
        # noinspection PyCallingNonCallable
        properties: IoTHubMap = message.properties()
        for k, v in message_data.get("properties", {}).items():
            if v is None:
                continue
            properties.add_or_update(k, str(v))

        self.client.send_event_async(message, self._send_message_callback, None)

    def register_azure_method_name_validator(self, azure_method_name_validator: tp.Callable):
        self.azure_method_name_validator = azure_method_name_validator

    def add_validator(self, validator: tp.Callable[[dict], bool]):
        """
        Add a validator that accepts a message and returns whether it is correct

        All of your validators must return True for handler to be called upon it
        """
        self.message_validators.add(validator)

    def add_handler(self, handler: tp.Callable[[dict], bool]):
        """
        Register a handler to be called when a correct message is received

        Your handler should return True when correct handled message, else False
        """
        self.message_handlers.add(handler)

    def reset(self, reset_all: bool = False):
        """
        Reset a Azure IoTHub client
        """
        time.sleep(0.5)  # necessary for safe call as thread from IoTHub client callback
        self.client = IoTHubClient(settings.AZURE_CONNECTION_STRING,
                                   IoTHubTransportProvider.MQTT)
        self.client.set_device_method_callback(self._direct_method_callback, 0)

        if reset_all:
            self.client.set_message_callback(self._receive_message_callback, 0)

    def _receive_message_callback(self, message: IoTHubMessage, counter: int):
        """
        Callback called when client receive Cloud to Device message

        The self.reset thread call must be placed as last statement before
        return statement and return statement can not be a call, it must be
        value
        """
        status = ResponseStatus.INVALID_COMMAND

        try:
            message_buffer = message.get_bytearray()
            message_body = message_buffer.decode('utf-8')
        except UnicodeDecodeError:
            logger.debug('Received message payload is unreadable')

            command_response = CommandResponse({}, status)
            self.send_iothub_message(command_response)

            return IoTHubMessageDispositionResult.REJECTED

        command = self._get_command(message_body)
        if not bool(command):  # check that command is empty, bool({}) == False
            command_response = CommandResponse(command, status)
            self.send_iothub_message(command_response)

            return IoTHubMessageDispositionResult.REJECTED

        with HangarData():
            hangar_launch_timestamp = HangarData().launch_timestamp

            if hangar_launch_timestamp is None:
                return IoTHubMessageDispositionResult.ABANDONED

        try:
            command_timestamp = command['timestamp']
        except KeyError:
            logger.debug("Command not have timestamp")
            return IoTHubMessageDispositionResult.REJECTED

        if command_timestamp < hangar_launch_timestamp:
            logger.debug('Command sent before hangar launched')
            return IoTHubMessageDispositionResult.REJECTED

        current_timestamp = int(time.time() * 1000)
        two_minutes_timestamp_delta = 120 * 1000
        if command_timestamp < current_timestamp - two_minutes_timestamp_delta:
            logger.debug('Command sent over 2 minutes ago')
            return IoTHubMessageDispositionResult.REJECTED

        status = self._handle_command(command)
        command_response = CommandResponse(command, status)
        self.send_iothub_message(command_response)

        # call reset() as thread must be a last statement before "return"
        Thread(target=self.reset, args=(True,)).start()

        return IoTHubMessageDispositionResult.ACCEPTED

    def _direct_method_callback(self, method_name: str, payload: str,
                                context: tp.Any) -> DeviceMethodReturnValue:

        status = ResponseStatus.INVALID_COMMAND

        command = self._get_command(payload)
        if not bool(command):  # check that command is empty, bool({}) == False
            result = self._prepare_command_response(command, status)
            return result

        if self.azure_method_name_validator is not None:
            if not self.azure_method_name_validator(command, method_name):
                result = self._prepare_command_response(command, status)
                return result

        status = self._handle_command(command)

        result = self._prepare_command_response(command, status)
        return result

    @staticmethod
    def _get_command(payload: str) -> dict:
        try:
            command: dict = json.loads(payload)
        except (TypeError, json.decoder.JSONDecodeError):
            logger.debug(f'Received command payload is incorrect JSON.'
                         f'\n\tCommand payload: {payload}')
            return {}

        return command

    def _handle_command(self, command: dict) -> ResponseStatus:
        status = ResponseStatus.INVALID_COMMAND

        if all(self.message_validators(command)):
            if not any(self.message_handlers(command)):
                logger.warning(
                    f'Cannot perform received command. Command: {command}')
                status = ResponseStatus.REFUSED
            else:
                logger.debug(f'Received correct command. Command: {command}')
                status = ResponseStatus.ACKNOWLEDGED
        else:
            logger.debug(f'Received command is invalid. Command: {command}')

        return status

    @staticmethod
    def _send_message_callback(message: IoTHubMessage,
                               result, context):  # pragma: no cover

        if result is IoTHubClientConfirmationResult.OK:
            logger.debug('Correctly send IoTHub Message')
            return

        logger.debug(f'Error during sending IoTHub Message.'
                     f'\n\tSend result: {result.name}.'
                     f'\n\tMessage: {message.get_string()}.')

    @staticmethod
    def _prepare_command_response(command: dict, status: ResponseStatus) -> \
            DeviceMethodReturnValue:

        result = DeviceMethodReturnValue()
        command_response = CommandResponse(command, status)

        result.status = status.value
        result.response = json.dumps(command_response.get_data()["payload"])
        return result
