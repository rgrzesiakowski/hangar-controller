from enum import IntEnum


class ResponseStatus(IntEnum):  # pragma: no cover
    DONE = 201
    ACKNOWLEDGED = 200
    REFUSED = 400
    FAILED = 501
    INVALID_COMMAND = 500
