class BaseDroneMessage:
    command_name: str = None

    def __init__(self):
        self.parameters = {}

    def get_data(self) -> dict:
        """Convert self to a dictionary"""

        data = {
            "command": self.command_name,
            "parameters": self.parameters
        }
        return data


class DroneInitializing(BaseDroneMessage):
    command_name = 'drone_initializing'

    def __init__(self, route_id: str, flight_id: str, cargo: bool):
        super().__init__()
        self.parameters['route_id'] = route_id
        self.parameters['flight_id'] = flight_id
        self.parameters['cargo'] = cargo


class DronePrepare(BaseDroneMessage):
    command_name = 'drone_prepare'


class CargoPrepare(BaseDroneMessage):
    command_name = 'cargo_prepare'


class FinishFlight(BaseDroneMessage):
    command_name = 'flight_finish'


class ShutdownDrone(BaseDroneMessage):
    command_name = 'shutdown_drone'


class FlightCancel(BaseDroneMessage):
    command_name = 'flight_cancel'
