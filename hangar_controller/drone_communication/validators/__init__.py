import functools
import logging
import typing as tp

logger = logging.getLogger(__name__)

KNOWN_COMMAND = ('drone_launched', 'drone_initialized', 'cargo_ready',
                 'drone_ready', 'dump_data', 'flight_ended', 'flight_canceled')

EXPECTED_COMMAND_SCHEMA = {'command': str, 'parameters': dict}


# noinspection DuplicatedCode
def _validate_schema(expected_schema: tp.Dict[str, type],
                     check_parameters: bool = True):
    def inner(fun):
        @functools.wraps(fun)
        def inner2(command: dict) -> bool:
            try:
                if check_parameters:
                    fields: dict = command['parameters']
                else:
                    fields: dict = command

                for expected_key in tuple(expected_schema.keys()):
                    if expected_key not in fields.keys():
                        return False

                    expected_value_type: type = expected_schema[expected_key]
                    # noinspection PyTypeHints
                    if not isinstance(fields[expected_key], expected_value_type):
                        return False
                return fun(command)
            except KeyError:
                return False

        return inner2

    return inner


def validate_is_known_command(command: dict) -> bool:
    command_name = command.get('command', None)

    if command_name not in KNOWN_COMMAND:
        return False
    return True


@_validate_schema(EXPECTED_COMMAND_SCHEMA, False)
def validate_command_schema(command: dict) -> bool:
    return True


validators: tp.List[tp.Callable[[dict], bool]] = [validate_is_known_command,
                                                  validate_command_schema]
