import logging
import time

from .alerts import AlertChecker, AlertManager
from .devices.drivers.drivers_controller import DriversController
from .devices.error_queues import ErrorQueue, EventLogQueue
from .devices.modbus_io_manager import ModbusIOManager
from .devices.motors.motors import Motors
from .devices.weather_station.weather_station_controller import \
    WeatherStationController
from .logs_dump import STMLogsDump
from settingsd.drivers.stm_pms_settings import PMSComponent

logger = logging.getLogger(__name__)


def set_up_devices():
    logger.info('[LAUNCHING] [START] Set Up devices')

    error_queue = ErrorQueue()
    event_log_queue = EventLogQueue()

    modbus_io_manager = ModbusIOManager()
    modbus_io_manager.find_and_apply_ports()

    motors = Motors(error_queue, event_log_queue)
    modbus_io_manager.connect_devices()
    motors.update_motors_parameters()
    motors.start()

    drivers = DriversController(error_queue, event_log_queue)
    drivers.start()

    STMLogsDump().start()

    WeatherStationController(modbus_io_manager.weather_station, error_queue)
    time.sleep(2)

    alert_checker = AlertChecker(AlertManager(), drivers.error_manager)
    alert_checker.start()

    logger.info('[LAUNCHING] [END] Set Up devices')


def power_on_executive_subsystems():
    logger.info('[LAUNCHING] [START] Power ON executive subsystems')

    drivers = DriversController()

    components = (
        PMSComponent.TRAFO, PMSComponent.POSITIONING_MOTORS,
        PMSComponent.MANIPULATOR_MOTORS, PMSComponent.LIFT_MOTORS,
        PMSComponent.AIR_COMPRESSOR, PMSComponent.PRESSURE
    )
    for component in components:
        logger.debug(drivers.pms.set_component_power(component, True))
    time.sleep(1)

    logger.info('[LAUNCHING] [END] Power ON executive subsystems')


def calibrate_mechanisms():
    logger.info('[LAUNCHING] [START] Calibrate mechanisms')

    motors = Motors()
    drivers = DriversController()

    logger.debug(motors.grasper.calibrate_grasper())
    logger.debug(motors.grasper.release())
    logger.debug(motors.manipulator.calibrate_manipulator_motors())
    logger.debug(motors.positioning.calibrate_positioning())
    logger.debug(motors.positioning.position_drone())
    logger.debug(drivers.cargo.base_shift())
    logger.debug(motors.lift.calibrate_lift())
    logger.debug(motors.lift.go_down())
    logger.debug(drivers.cargo.close_window())
    logger.debug(drivers.roof.close())
    time.sleep(1)

    logger.info('[LAUNCHING] [END] Calibrate mechanisms')


def tear_down_executive_subsystems():
    logger.info('[DISABLING] [START] Tear Down executive subsystems')

    AlertChecker().terminate().join()

    drivers = DriversController()

    components = (
        PMSComponent.TRAFO, PMSComponent.POSITIONING_MOTORS,
        PMSComponent.MANIPULATOR_MOTORS, PMSComponent.LIFT_MOTORS,
        PMSComponent.AIR_COMPRESSOR, PMSComponent.PRESSURE
    )
    for component in components:
        logger.debug(drivers.pms.set_component_power(component, False))

    ModbusIOManager().weather_station.disconnect()

    logger.info('[DISABLING] [END] Tear Down executive subsystems')
