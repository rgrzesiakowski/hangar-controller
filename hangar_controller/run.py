import logging

from satella.posix import hang_until_sig

from hangar_controller import hangar_logic_utils, hangar_devices_utils

logger = logging.getLogger(__name__)


def run_hangar():
    hangar_logic_utils.setup_loggers()

    hangar_logic_utils.prepare_hangar()
    hangar_logic_utils.add_validators_and_handlers()
    hangar_logic_utils.register_getters()

    hangar_devices_utils.set_up_devices()
    hangar_devices_utils.power_on_executive_subsystems()
    hangar_devices_utils.calibrate_mechanisms()

    enabled_hangar = hangar_logic_utils.enable_hangar()
    executor_thread, iothub_client, drone_client, service_client = enabled_hangar

    cargo_api_backend = hangar_logic_utils.enable_cargo_backend()

    hang_until_sig()

    # region HANGAR_DISABLING
    hangar_logic_utils.lock_actions()

    hangar_devices_utils.tear_down_executive_subsystems()

    hangar_logic_utils.disable_hangar(cargo_api_backend)
    # endregion


if __name__ == '__main__':
    run_hangar()
