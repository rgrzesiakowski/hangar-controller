import logging

import settings
from hangar_controller.azure_communication import AzureIoTHubClient
from hangar_controller.azure_communication.messages import HangarHeartBeat
from .abstract import BaseAction
from hangar_controller.hangar_state import HangarState
from ..state_machine import StateMachine

logger = logging.getLogger(__name__)


class ServiceFinishFlight(BaseAction):
    """Action that is called in response to receiving the service_finish_flight
    command from Azure.
    """

    def __init__(self, drone_id: str, flight_id: str):
        super().__init__()
        self.drone_id = drone_id
        self.flight_id = flight_id

    def run(self):
        with self.hangar_data:
            if self.hangar_data.flight_id is None:
                self.hangar_data.flight_id = self.flight_id
                self.hangar_data.hangar_type = "E"

            self.hangar_data.drone_id = self.drone_id

        StateMachine().transition(HangarState.HANGAR_FULL)
        with self.hangar_data:
            self.hangar_data.flight_id = None
            self.hangar_data.hangar_type = None
        AzureIoTHubClient().send_iothub_message(HangarHeartBeat())

    def can_run(self) -> bool:
        with self.hangar_data:
            hangar_flight_id = self.hangar_data.flight_id
            hangar_drone_id = self.hangar_data.drone_id

        if settings.HANGAR_IS_REAL and hangar_drone_id is None:
            return False

        if hangar_flight_id not in (self.flight_id, None):
            return False

        if hangar_drone_id not in (self.drone_id, None):
            return False

        return True
