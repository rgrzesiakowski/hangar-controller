import logging

from .abstract import BaseAction
from hangar_controller.battery_chargers_supervisor import BatteryChargersSupervisor
from hangar_controller.hangar_state import HangarState
from hangar_controller.drone_communication import HangarDroneMQTTClient
from hangar_controller.drone_communication.messages import DroneInitializing, \
    DronePrepare
from .cargo import LoadingDrone
from ..state_machine import StateMachine
from ...azure_communication import AzureIoTHubClient
from ...azure_communication.messages import BaseIoTHubMessage
from ...azure_communication.messages.battery_action_message import \
    BatteryAction, BatteryActionType
from ...azure_communication.messages.battery_state import BatteryState
from settingsd.motors.parameters_settings import Positions
from settingsd.drivers.stm_pms_settings import PMSComponent

logger = logging.getLogger(__name__)


class InitFlight(BaseAction):
    """Action that is called in response to receiving the init_flight
    command from Azure.
    """

    flight_id: str
    route_id: str
    cargo: bool

    def __init__(self, flight_id: str, route_id: str, cargo: bool):
        super().__init__()
        self.flight_id = flight_id
        self.route_id = route_id
        self.cargo = cargo

        self.bcs = BatteryChargersSupervisor()

    def run(self):
        with self.hangar_data:
            self.hangar_data.flight_id = self.flight_id
            self.hangar_data.sync_route_id = self.route_id
            self.hangar_data.sync_is_flight_with_cargo = self.cargo
            self.hangar_data.hangar_type = 'S'
        BaseIoTHubMessage.reset_counter()

        StateMachine().transition(HangarState.DRONE_POWERING)
        charger_id, battery_id = self.bcs.get_charged_battery()
        slot = int(charger_id)

        logger.debug(self.motors.manipulator.go_to_battery_slot(slot))
        logger.debug(self.motors.grasper.grasp())
        logger.debug(self.motors.manipulator.go_battery_drone(
            Positions.BATTERY_DRONE_PUT))
        logger.debug(self.motors.grasper.release())
        logger.debug(self.motors.manipulator.go_home())

        self.bcs.remove_battery_from_hangar(battery_id)

        with self.hangar_data:
            drone_id = self.hangar_data.drone_id

        battery_action = BatteryAction(
            BatteryActionType.CONNECTING_DRONE, drone_id, battery_id
        )
        AzureIoTHubClient().send_iothub_message(battery_action)
        AzureIoTHubClient().send_iothub_message(BatteryState())

    def can_run(self) -> bool:
        with self.hangar_data:
            init_flight_locked = self.hangar_data.init_flight_lock
            hangar_has_cargo = self.hangar_data.hangar_cargo_id is not None
            available_battery = self.hangar_data.available_battery

        hangar_has_correct_state = \
            StateMachine().state == HangarState.HANGAR_FULL

        hangar_has_charged_battery = available_battery >= 1

        if init_flight_locked:
            return False

        if not self.cargo:
            return hangar_has_correct_state and hangar_has_charged_battery

        return (hangar_has_correct_state
                and hangar_has_cargo
                and hangar_has_charged_battery)


class OnReceivedDroneLaunched(BaseAction):

    def run(self):
        StateMachine().transition(HangarState.DRONE_INIT)

        with self.hangar_data:
            route_id = self.hangar_data.sync_route_id
            flight_id = self.hangar_data.flight_id
            is_flight_with_cargo = self.hangar_data.sync_is_flight_with_cargo

        HangarDroneMQTTClient().send_message(
            DroneInitializing(route_id, flight_id, is_flight_with_cargo)
        )

    def can_run(self) -> bool:
        return StateMachine().state == HangarState.DRONE_POWERING


class OnReceivedDroneInitialized(BaseAction):

    def run(self):
        with self.hangar_data:
            is_flight_with_cargo = self.hangar_data.sync_is_flight_with_cargo

        if is_flight_with_cargo:
            StateMachine().transition(HangarState.CARGO_PREPARING)
            LoadingDrone().schedule()
        else:
            StateMachine().transition(HangarState.DRONE_PREPARING)

            logger.debug(self.drivers.roof.open())
            logger.debug(self.motors.lift.go_up())
            logger.debug(self.motors.positioning.release_drone())
            logger.debug(self.drivers.pms.set_component_power(
                PMSComponent.IR_LOCK, True)
            )

            HangarDroneMQTTClient().send_message(DronePrepare())

    def can_run(self) -> bool:
        return StateMachine().state == HangarState.DRONE_INIT


class OnReceivedCargoReady(BaseAction):

    def run(self):
        StateMachine().transition(HangarState.DRONE_PREPARING)

        logger.debug(self.drivers.roof.open())
        logger.debug(self.motors.lift.go_up())
        logger.debug(self.motors.positioning.release_drone())
        logger.debug(self.drivers.pms.set_component_power(
            PMSComponent.IR_LOCK, True)
        )

        HangarDroneMQTTClient().send_message(DronePrepare())

    def can_run(self) -> bool:
        return StateMachine().state == HangarState.CARGO_PREPARING


class OnReceivedDroneReady(BaseAction):

    def run(self):
        StateMachine().transition(HangarState.DRONE_READY)

    def can_run(self) -> bool:
        return StateMachine().state == HangarState.DRONE_PREPARING
