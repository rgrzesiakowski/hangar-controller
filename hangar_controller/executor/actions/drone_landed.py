import logging
import time
import typing as tp

from hangar_controller.drone_communication import HangarDroneMQTTClient
from hangar_controller.drone_communication.messages import FinishFlight, \
    ShutdownDrone
from hangar_controller.hangar_state import HangarState
from .abstract import BaseAction
from .cargo import UnloadingDrone
from ..state_machine import StateMachine
from ...azure_communication import AzureIoTHubClient
from ...azure_communication.messages.battery_action_message import \
    BatteryAction, BatteryActionType
from ...azure_communication.messages.battery_state import BatteryState
from ...battery_chargers_supervisor import BatteryChargersSupervisor
from settingsd.drivers.stm_master_charger_settings import MasterChargerStates
from settingsd.drivers.stm_pms_settings import PMSComponent
from settingsd.motors.parameters_settings import Positions

logger = logging.getLogger(__name__)


class DroneLanded(BaseAction):
    """Action that is called in response to receiving the drone_landed
    command from Azure.
    """

    cargo_id: str
    cargo_weight: int

    def __init__(self, cargo_id: tp.Optional[str],
                 cargo_weight: tp.Optional[int]):
        super().__init__()
        self.cargo_id = cargo_id
        self.cargo_weight = cargo_weight

    def run(self):
        with self.hangar_data:
            self.hangar_data.drone_cargo_id = self.cargo_id
            self.hangar_data.drone_cargo_weight = self.cargo_weight
            self.hangar_data.drone_id = self.hangar_data.sync_drone_id
            self.hangar_data.sync_drone_id = None

        StateMachine().transition(HangarState.DRONE_RECEIVED)

        logger.debug(self.motors.positioning.position_drone())
        logger.debug(self.drivers.pms.set_component_power(PMSComponent.IR_LOCK,
                                                          False))
        logger.debug(self.motors.lift.go_down())
        logger.debug(self.drivers.roof.close())

        StateMachine().transition(HangarState.FLIGHT_FINISHING)
        HangarDroneMQTTClient().send_message(FinishFlight())

        if self.cargo_id is not None:
            # this may tops fail to execute
            with self.hangar_data:
                self.hangar_data.init_flight_lock = True

            UnloadingDrone().schedule(when=UnloadingDrone.can_unload)

    def can_run(self) -> bool:
        return StateMachine().state == HangarState.WAITING_DRONE


class OnReceivedFlightEnded(BaseAction):

    def run(self):
        HangarDroneMQTTClient().send_message(ShutdownDrone())
        time.sleep(10)

        charger_id = BatteryChargersSupervisor().get_charger_id_by_state(
            MasterChargerStates.UNPLUGGED
        )
        free_battery_slot = int(charger_id)

        logger.debug(self.motors.manipulator.go_battery_drone(
            Positions.BATTERY_DRONE_TAKE))
        logger.debug(self.motors.grasper.grasp())
        logger.debug(self.motors.manipulator.go_to_battery_slot(
            free_battery_slot))
        logger.debug(self.motors.grasper.release())
        logger.debug(self.motors.manipulator.go_home())

        battery_id = BatteryChargersSupervisor().add_battery_to_slot(
            charger_id)
        logger.debug(self.drivers.master_charger.set_charger_power(
            charger_id=charger_id, enabled=True))

        with self.hangar_data:
            drone_id = self.hangar_data.drone_id

        battery_action = BatteryAction(
            BatteryActionType.DISCONNECTING_DRONE, drone_id, battery_id
        )
        AzureIoTHubClient().send_iothub_message(battery_action)
        AzureIoTHubClient().send_iothub_message(BatteryState())

        StateMachine().transition(HangarState.HANGAR_FULL)
        with self.hangar_data:
            self.hangar_data.hangar_type = None
            self.hangar_data.flight_id = None

    def can_run(self) -> bool:
        return StateMachine().state == HangarState.FLIGHT_FINISHING
