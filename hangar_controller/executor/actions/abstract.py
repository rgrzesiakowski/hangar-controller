import abc
import concurrent.futures
import logging
import typing as tp
import uuid
from ..executor import ExecutorThread
from ..hangar_data import HangarData
from ...devices.drivers.drivers_controller import DriversController
from ...devices.motors.motors import Motors
from ...devices.weather_station.weather_station_controller import \
    WeatherStationController

logger = logging.getLogger(__name__)


class BaseAction(concurrent.futures.Future, metaclass=abc.ABCMeta):
    """Base class for an executor-schedulable actions."""

    # list of subclasses of BaseAction, alternatively, you can set the
    # first value to True to always be able co execute concurrently
    CAN_EXECUTE_CONCURRENTLY_WITH: tp.List[tp.Union[str, type]] = []

    action_id: str

    def schedule(self, when=None) -> bool:
        return ExecutorThread().schedule(self, when=when)

    def __init__(self):
        super(BaseAction, self).__init__()
        self.action_id = uuid.uuid4().hex
        self.hangar_data = HangarData()
        self.motors = Motors()
        self.drivers = DriversController()
        self.weather_station = WeatherStationController()

    @abc.abstractmethod
    def run(self):
        """Perform the action. This is performed in a separate thread."""
        pass

    @abc.abstractmethod
    def can_run(self) -> bool:
        """Can this entry, to the best knowing of current system state,
        be ran?
        """

    @classmethod
    def can_execute_action(cls, currently_doing_actions: tp.Set['BaseAction']):

        def class_to_classname(x: tp.Union[str, type]) -> str:
            if not isinstance(x, str):
                return x.__qualname__.split('.')[-1]
            return x

        if not cls.CAN_EXECUTE_CONCURRENTLY_WITH:
            return False

        if cls.CAN_EXECUTE_CONCURRENTLY_WITH[0] == True:
            return True

        can_execute_with = set(map(class_to_classname,
                                   cls.CAN_EXECUTE_CONCURRENTLY_WITH))
        currently_doing = set(map(lambda x: class_to_classname(x.__class__),
                                  currently_doing_actions))

        return can_execute_with.issubset(currently_doing)
