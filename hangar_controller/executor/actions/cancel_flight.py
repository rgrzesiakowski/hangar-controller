import logging
import time

from .abstract import BaseAction
from hangar_controller.hangar_state import HangarState
from .cargo import UnloadingDrone
from ..state_machine import StateMachine
from ...azure_communication import AzureIoTHubClient
from ...azure_communication.messages.battery_action_message import \
    BatteryAction, BatteryActionType
from ...azure_communication.messages.battery_state import BatteryState
from ...drone_communication import HangarDroneMQTTClient
from ...drone_communication.messages import FlightCancel, ShutdownDrone
from ...battery_chargers_supervisor import BatteryChargersSupervisor
from settingsd.drivers.stm_master_charger_settings import MasterChargerStates
from settingsd.motors.parameters_settings import Positions

logger = logging.getLogger(__name__)


class CancelFlight(BaseAction):
    """Action that is called in response to receiving the cancel_flight
    command from Azure.
    """

    def run(self):
        StateMachine().transition(HangarState.CANCELLING_FLIGHT)

        logger.debug(self.motors.positioning.position_drone())
        logger.debug(self.motors.lift.go_down())
        logger.debug(self.drivers.roof.close())

        time.sleep(0.01)

        with self.hangar_data:
            flight_with_cargo = self.hangar_data.sync_is_flight_with_cargo
            self.hangar_data.drone_cargo_id = \
                self.hangar_data.cancel_flight_cargo_id
            self.hangar_data.drone_cargo_weight = \
                self.hangar_data.cancel_flight_cargo_weight

        if flight_with_cargo:
            with self.hangar_data:
                self.hangar_data.init_flight_lock = True

            UnloadingDrone().schedule(when=UnloadingDrone.can_unload)

        HangarDroneMQTTClient().send_message(FlightCancel())

    def can_run(self) -> bool:
        allowed_states = (HangarState.DRONE_POWERING, HangarState.DRONE_INIT,
                          HangarState.CARGO_PREPARING, HangarState.DRONE_READY)

        return StateMachine().state in allowed_states

    @staticmethod
    def can_cancel():
        return StateMachine().state == HangarState.DRONE_READY


class OnReceivedFlightCanceled(BaseAction):
    def run(self):
        HangarDroneMQTTClient().send_message(ShutdownDrone())
        time.sleep(10)

        charger_id = BatteryChargersSupervisor().get_charger_id_by_state(
            MasterChargerStates.UNPLUGGED
        )
        free_battery_slot = int(charger_id)

        logger.debug(self.motors.manipulator.go_battery_drone(
            Positions.BATTERY_DRONE_TAKE))
        logger.debug(self.motors.grasper.grasp())
        logger.debug(self.motors.manipulator.go_to_battery_slot(
            free_battery_slot))
        logger.debug(self.motors.grasper.release())

        battery_id = BatteryChargersSupervisor().add_battery_to_slot(
            charger_id)
        logger.debug(self.drivers.master_charger.set_charger_power(
            charger_id=charger_id, enabled=True))

        with self.hangar_data:
            drone_id = self.hangar_data.drone_id

        battery_action = BatteryAction(
            BatteryActionType.DISCONNECTING_DRONE, drone_id, battery_id
        )
        AzureIoTHubClient().send_iothub_message(battery_action)
        AzureIoTHubClient().send_iothub_message(BatteryState())

        logger.debug(self.motors.manipulator.go_home())

        StateMachine().transition(HangarState.HANGAR_FULL)
        with self.hangar_data:
            self.hangar_data.hangar_type = None
            self.hangar_data.flight_id = None

    def can_run(self) -> bool:
        return StateMachine().state == HangarState.CANCELLING_FLIGHT
