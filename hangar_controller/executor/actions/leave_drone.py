import logging

from .abstract import BaseAction
from ..state_machine import StateMachine
from hangar_controller.hangar_state import HangarState
from settingsd.drivers.stm_pms_settings import PMSComponent

logger = logging.getLogger(__name__)


class LeaveDrone(BaseAction):
    """Action that is called in response to receiving the leave_drone
    command from Azure.
    """

    def run(self):
        StateMachine().transition(HangarState.HANGAR_EMPTY)

        logger.debug(self.drivers.pms.set_component_power(PMSComponent.IR_LOCK,
                                                          False))
        logger.debug(self.motors.lift.go_down())
        logger.debug(self.drivers.roof.close())

        with self.hangar_data:
            self.hangar_data.hangar_type = None
            self.hangar_data.flight_id = None
            self.hangar_data.sync_drone_id = None

    def can_run(self) -> bool:
        can_run = StateMachine().state in (HangarState.PROTECT_DRONE,
                                           HangarState.RECEIVING_DRONE,
                                           HangarState.WAITING_DRONE)

        if can_run:
            ...
            # TODO: abort other actions now

        return can_run
