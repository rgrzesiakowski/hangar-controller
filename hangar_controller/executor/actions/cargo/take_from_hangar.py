import logging
import time
import typing as tp

from settingsd.drivers.stm_communication_settings import ResponseMessage
from settingsd.drivers.stm_user_panel_settings import UserPanelViews, \
    ConfirmationViews
from ..abstract import BaseAction
from hangar_controller.hangar_state import HangarState
from hangar_controller.azure_communication import AzureIoTHubClient
from hangar_controller.azure_communication.messages import CargoAction, \
    CargoActionType, CargoActionObjectType
from .pentacomp_api import request_cargo_user_action
from ...state_machine import StateMachine

logger = logging.getLogger(__name__)


class TakeFromHangar(BaseAction):
    def __init__(self, action_object: tp.Optional[str],
                 caot: CargoActionObjectType, pin: str = None):

        super(TakeFromHangar, self).__init__()
        self.hangar_data.lock_put_and_take_actions()

        self._cargo_action_type = CargoActionType.TAKE_FROM_HANGAR
        self._action_object = action_object
        self._caot = caot
        self._pin = pin

    def run(self):
        self.drivers.user_panel.set_view(UserPanelViews.CARGO_TAKEOUT)
        time.sleep(2)

        self._pin, hangar_should_give_cargo = self._pre_weighting_handling()

        if not hangar_should_give_cargo:
            logger.debug('Hangar cannot give cargo')
            self.hangar_data.unlock_put_and_take_actions()
            return


        with self.hangar_data:
            hangar_cargo_weight = self.hangar_data.hangar_cargo_weight

        _, weight_limit, hangar_can_give_cargo = request_cargo_user_action(
            self._cargo_action_type, self._caot, '', hangar_cargo_weight
        )

        if not hangar_can_give_cargo:
            # TODO: Show CANNOT TAKE HANGAR view (NON EXIST YET)
            self.hangar_data.unlock_put_and_take_actions()
            return

        self.drivers.user_panel.set_view(UserPanelViews.WINDOW_OPENING)
        time.sleep(1)
        self.drivers.cargo.open_window()

        wait_for_take_out = True
        while wait_for_take_out:
            response = self.drivers.user_panel.set_confirmation_view(
                ConfirmationViews.CARGO_TAKE_OUT)

            if response.evaluation:
                wait_for_take_out = False
                continue

            if response.evaluation_reason != ResponseMessage.TIMEOUT:
                # TODO: Show ERROR on user panel
                pass

        time.sleep(1)
        self.drivers.user_panel.set_view(UserPanelViews.WINDOW_CLOSING)
        time.sleep(1)
        self.drivers.cargo.close_window()

        with self.hangar_data:
            cargo_id = self.hangar_data.hangar_cargo_id
            cargo_weight = self.hangar_data.hangar_cargo_weight

        cargo_action_message = CargoAction(
            self._cargo_action_type, self._action_object, self._caot, True,
            cargo_id, cargo_weight
        )

        AzureIoTHubClient().send_iothub_message(cargo_action_message)

        with self.hangar_data:
            self.hangar_data.hangar_cargo_id = None
            self.hangar_data.hangar_cargo_weight = 0

        self.drivers.user_panel.set_view(UserPanelViews.THANK_YOU)
        time.sleep(3)
        self.hangar_data.unlock_put_and_take_actions()

    def _pre_weighting_handling(self) -> tp.Tuple[tp.Optional[str], bool]:
        if self._pin is None:
            pin = None

            wait_for_pin = True
            while wait_for_pin:
                response, pin = self.drivers.user_panel.get_pin()

                if response.evaluation:
                    wait_for_pin = False
                    continue

                if response.evaluation_reason != ResponseMessage.TIMEOUT:
                    # TODO: Show ERROR on user panel
                    pass
        else:
            pin = self._pin

        self.drivers.user_panel.set_view(UserPanelViews.CARGO_PIN_CHECK)

        pin = pin.replace(',', '')

        _, weight_limit, pre_weighting_valid = request_cargo_user_action(
            self._cargo_action_type, self._caot, pin
        )

        if not pre_weighting_valid:
            logger.debug('Cargo pin is invalid')
            self.drivers.user_panel.set_view(UserPanelViews.CARGO_PIN_INCORRECT)

            time.sleep(5)
            return pin, False

        self.drivers.user_panel.set_view(UserPanelViews.CARGO_PIN_CORRECT)
        time.sleep(2)
        return pin, True

    def can_run(self) -> bool:
        with self.hangar_data:
            hangar_has_cargo = self.hangar_data.hangar_cargo_id is not None
            hangar_has_correct_state = StateMachine().state in (
                HangarState.HANGAR_FULL, HangarState.HANGAR_EMPTY)

        can_run = hangar_has_cargo and hangar_has_correct_state

        if not can_run:
            with self.hangar_data:
                self.hangar_data.unlock_put_and_take_actions()

        return can_run
