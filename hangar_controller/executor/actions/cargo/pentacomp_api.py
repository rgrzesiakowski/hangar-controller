import json
import logging
import typing as tp

import requests

import settings
from hangar_controller.azure_communication.messages import CargoActionType, \
    CargoActionObjectType

logger = logging.getLogger(__name__)


def request_cargo_user_action(action_type: CargoActionType,
                              user_type: CargoActionObjectType,
                              pin: str,
                              weight: tp.Optional[int] = None) \
        -> tp.Tuple[tp.Optional[str], tp.Optional[int], bool]:
    request_dict = {
        'deviceid': settings.HANGAR_ID,
        'action': action_type.value,
        'user_type': user_type.value,
        'pin': pin,
    }
    if weight is not None:
        request_dict['weight'] = weight

    url = settings.PENTACOMP_API_CARGO_USER_ACTION
    r = requests.post(url, json.dumps(request_dict))

    if r.status_code != 200:
        raise Exception(f'Received response from CargoUserAction endpoint with '
                        f'{r.status_code} status code.'
                        f'Return value:\n{r.text}\n'
                        f'URL: {url}\n'
                        f'DATA:\n{request_dict}')

    response_dict = r.json()
    logger.debug(f'CargoUserAction RESPONSE: {response_dict}')

    cargo_id = response_dict.get('cargo_id', None)
    weight_limit = response_dict.get('weight_limit', None)
    valid = response_dict['valid']

    if valid is None:
        raise Exception('Received response has incorrect structure')

    return cargo_id, weight_limit, valid
