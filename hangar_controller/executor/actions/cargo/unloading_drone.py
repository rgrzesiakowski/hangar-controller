import logging
import typing as tp

from hangar_controller.azure_communication import AzureIoTHubClient
from hangar_controller.azure_communication.messages import CargoActionType, \
    CargoAction, CargoActionObjectType
from hangar_controller.hangar_state import HangarState
from ..abstract import BaseAction
from ... import HangarData
from ...state_machine import StateMachine

logger = logging.getLogger(__name__)


class UnloadingDrone(BaseAction):
    def __init__(self, cargo_id: tp.Optional[str] = None,
                 cargo_weight: tp.Optional[int] = None):

        super(UnloadingDrone, self).__init__()
        parameters = (cargo_id, cargo_weight)
        if None not in parameters:
            with self.hangar_data:
                self.hangar_data.drone_cargo_id = cargo_id
                self.hangar_data.drone_cargo_weight = cargo_weight
        elif parameters.count(None) != 2:
            logger.error("Parameters")

    def run(self):
        logger.debug(self.drivers.cargo.work_shift())
        logger.debug(self.motors.manipulator.go_cargo_drone())
        logger.debug(self.motors.grasper.grasp())
        logger.debug(self.motors.manipulator.go_cargo_window_put_down())
        logger.debug(self.motors.grasper.release())
        logger.debug(self.motors.manipulator.go_home())
        logger.debug(self.drivers.cargo.base_shift())

        with self.hangar_data:
            self.hangar_data.hangar_cargo_id = \
                self.hangar_data.drone_cargo_id
            self.hangar_data.hangar_cargo_weight = \
                self.hangar_data.drone_cargo_weight

            # Temporary data for possibly cancel_flight, no longer needed
            self.hangar_data.cancel_flight_cargo_id = None
            self.hangar_data.cancel_flight_cargo_weight = 0

            self.hangar_data.drone_cargo_id = None
            self.hangar_data.drone_cargo_weight = 0

            drone_id = self.hangar_data.drone_id
            cargo_id = self.hangar_data.hangar_cargo_id
            cargo_weight = self.hangar_data.hangar_cargo_weight

            self.hangar_data.init_flight_lock = False

        cargo_action_message = CargoAction(
            CargoActionType.UNLOADING_DRONE, drone_id,
            CargoActionObjectType.DRONE, True, cargo_id, cargo_weight
        )

        AzureIoTHubClient().send_iothub_message(cargo_action_message)

    def can_run(self) -> bool:
        return UnloadingDrone.can_unload()

    @staticmethod
    def can_unload() -> bool:
        with HangarData():
            hangar_has_drone = HangarData().drone_id is not None
            hangar_has_no_cargo = HangarData().hangar_cargo_id is None
            hangar_has_correct_state = \
                StateMachine().state == HangarState.HANGAR_FULL

        return hangar_has_no_cargo and hangar_has_correct_state \
               and hangar_has_drone
