import collections
import concurrent.futures
import logging
import time
import threading
import typing as tp

from satella.coding.structures import Singleton
from satella.coding.concurrent import LockedDataset, TerminableThread

from hangar_controller.executor.state_machine import StateMachine

logger = logging.getLogger(__name__)


class CannotRun(Exception):
    """This action will not run, because it's can_run returned False."""


class ExecutorThreadData(LockedDataset):
    def __init__(self):
        from .actions import BaseAction
        super().__init__()
        with self:
            self.todo: tp.Deque[BaseAction] = collections.deque()
            self.currently_doing: tp.Set[BaseAction] = set()
            self.whens: tp.List[tp.Tuple[tp.Callable[[], bool], BaseAction]] = \
                []


@Singleton
class ExecutorThread(TerminableThread):
    def __init__(self):
        super().__init__()
        self.dataset = ExecutorThreadData()

    def cleanup(self):
        with self.dataset as dataset:
            concurrent.futures.wait(dataset.currently_doing)
            dataset.currently_doing = set()

    def loop(self):
        time.sleep(0.5)

        with self.dataset as dataset:
            if not dataset.todo:
                self._add_whens_to_todo_list(dataset)

            self._remove_completed_futures(dataset)

            if not dataset.todo:
                return

            action = dataset.todo[0]
            can_execute = action.can_execute_action(dataset.currently_doing)

            if not can_execute:
                if dataset.currently_doing:
                    return

                can_execute = True

            if can_execute:
                self._execute_action(dataset)

    def schedule(self, action: 'BaseAction',
                 when: tp.Optional[tp.Callable[[], bool]] = None) -> bool:
        """Schedule an action to execute.

        Args:
            action: action to schedule.
            when: optional condition to start processing the action.
                Used to implement deferred actions.

        Returns:
            True if action can run, False otherwise.
        """
        with self.dataset as dataset:
            if when is None:
                dataset.todo.append(action)
                return action.can_run()

            if when():
                dataset.todo.append(action)
            else:
                dataset.whens.append((when, action))

            return True

    @staticmethod
    def _add_whens_to_todo_list(dataset):
        for when, action in dataset.whens:
            if not when():
                continue

            dataset.todo.append(action)
            dataset.whens.remove((when, action))

    @staticmethod
    def _remove_completed_futures(dataset):
        completed, _ = concurrent.futures.wait(
            dataset.currently_doing, 0.5, concurrent.futures.FIRST_COMPLETED
        )
        for completed_future in completed:
            dataset.currently_doing.remove(completed_future)

    def _execute_action(self, dataset):
        action = dataset.todo.popleft()
        if action.set_running_or_notify_cancel():
            if not action.can_run():
                action.set_exception(CannotRun())
                return

            logger.info(f'Running {action}')
            dataset.currently_doing.add(action)
            threading.Thread(target=run_action, args=(action,)).start()


def run_action(action: 'BaseAction') -> None:
    try:
        a = action.run()
    except BaseException as e:
        action.set_exception(e)
    else:
        action.set_result(a)
