import time
import logging
from queue import Empty

from satella.coding.concurrent import TerminableThread
from satella.coding.structures import Singleton

from .alert_manager import AlertManager
from .alert_codes import errors_mapping
from ..devices.drivers.messages.errors.error_manager import ErrorManager
from ..devices.drivers.messages.errors.error_message import ErrorMessage
from settingsd.alerts.alerts_settings import CHECK_ALERT_INTERVAL_MS

logger = logging.getLogger(__name__)


@Singleton
class AlertChecker(TerminableThread):
    def __init__(self, alert_manager: AlertManager,
                 error_manager: ErrorManager):
        super().__init__()
        self._alert_manager = alert_manager
        self._error_manager = error_manager

    def loop(self) -> None:
        time.sleep(CHECK_ALERT_INTERVAL_MS / 1000)
        while True:  # dispatch all queued errors
            try:
                error = self._error_manager.get_error()
                if isinstance(error, ErrorMessage):
                    self._dispatch_error(error)
                else:
                    logger.debug('Message of other type than error in queue\n'
                                 f'\tMessage: {error}')
            except Empty:
                break

    def _dispatch_error(self, error: ErrorMessage) -> None:
        alert_code = (error.parsed_payload.code, error.parsed_payload.subcode)

        try:
            alert = errors_mapping[alert_code]
        except KeyError:
            logger.debug('Unsupported error in queue; Error code: '
                         f'{error.parsed_payload.code}'
                         f'{error.parsed_payload.subcode}')
            return

        if alert is not None:
            # TODO: add repair procedures and silencing of singleton timeouts
            self._alert_manager.register(alert)
