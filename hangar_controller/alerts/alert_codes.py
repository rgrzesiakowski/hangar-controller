from enum import Enum
from typing import List, Dict, Tuple, Optional

from satella.coding.structures import frozendict


class AlertCodes(Enum):
    UNKNOWN = 0
    CLIMATE_CTRL = 1
    CLIMATE_ROOF_HEATING_WINTER = 2
    CLIMATE_ROOF_HEATING = 3
    ROOF_CTRL = 4
    CARGO_CTRL = 5
    CARGO_PUT_TAKE = 6
    HANGAR_PMS = 7
    USER_PANEL = 8
    BATTERY_CHARGING = 9
    BATTERY_IDENTIFICATION = 10
    BATTERY_PARAMETERS = 11
    # TODO: temporary unused
    # BATTERY_CHARGER_CTRL = 12

    @classmethod
    def _missing_(cls, value):
        return cls.UNKNOWN


class BreakdownZones(Enum):
    DRONE_ARRIVAL = 'drone_arrival'
    DRONE_DEPARTURE = 'drone_departure'
    BATTERY_LOADER = 'battery_loader'
    BATTERY_CHARGER = 'battery_charger'
    CARGO_LOADER = 'cargo_loader'
    CARGO_PUT_TAKE = 'cargo_put_take'


_errors_mapping = {
    ('', 0): AlertCodes.UNKNOWN,
    ('CLEPE', 1): AlertCodes.CLIMATE_CTRL,
    ('CLEPE', 2): AlertCodes.CLIMATE_CTRL,
    ('CLEPE', 3): AlertCodes.CLIMATE_ROOF_HEATING_WINTER,
    ('CLECC', 1): None,
    ('CLECC', 2): None,
    ('CLECC', 3): None,
    ('CLECC', 4): None,
    ('CLECC', 5): None,
    ('CLECC', 6): None,
    ('CLECC', 7): None,
    ('CLECC', 8): None,
    ('CLETE', 1): AlertCodes.CLIMATE_ROOF_HEATING_WINTER,
    ('CLETE', 2): AlertCodes.CLIMATE_ROOF_HEATING,
    ('CLETE', 3): AlertCodes.CLIMATE_CTRL,
    ('CLETE', 4): AlertCodes.CLIMATE_CTRL,
    ('CLETE', 5): AlertCodes.CLIMATE_CTRL,
    ('CLETE', 6): AlertCodes.CLIMATE_CTRL,
    ('RFEPE', 1): AlertCodes.ROOF_CTRL,
    ('RFECC', 1): AlertCodes.ROOF_CTRL,
    ('RFECC', 2): AlertCodes.ROOF_CTRL,
    ('CGEPE', 1): AlertCodes.CARGO_CTRL,
    ('CGEPE', 2): AlertCodes.CARGO_PUT_TAKE,
    ('CGECC', 1): AlertCodes.CARGO_PUT_TAKE,
    ('CGECC', 2): AlertCodes.CARGO_PUT_TAKE,
    ('CGECC', 3): AlertCodes.CARGO_CTRL,
    ('CGECC', 4): AlertCodes.CARGO_CTRL,
    ('PMEPE', 1): AlertCodes.HANGAR_PMS,
    ('PMETE', 1): AlertCodes.HANGAR_PMS,
    ('PMETE', 2): AlertCodes.HANGAR_PMS,
    ('PMECC', 1): AlertCodes.HANGAR_PMS,
    ('PMECC', 2): AlertCodes.HANGAR_PMS,
    ('PMECC', 3): AlertCodes.HANGAR_PMS,
    ('PMECC', 4): AlertCodes.HANGAR_PMS,
    ('PMECC', 5): AlertCodes.HANGAR_PMS,
    ('PMECC', 6): AlertCodes.HANGAR_PMS,
    ('PMECC', 7): AlertCodes.HANGAR_PMS,
    ('PMECC', 8): AlertCodes.HANGAR_PMS,
    ('PMECC', 9): AlertCodes.HANGAR_PMS,
    ('PMECC', 10): AlertCodes.HANGAR_PMS,
    ('PMECC', 11): AlertCodes.HANGAR_PMS,
    ('PMECC', 12): AlertCodes.HANGAR_PMS,
    ('PMECC', 13): AlertCodes.HANGAR_PMS,
    ('PMECC', 14): AlertCodes.HANGAR_PMS,
    ('UPEPE', 1): AlertCodes.USER_PANEL,
    ('MCEPE', 1): AlertCodes.BATTERY_CHARGING,
    ('MCEPE', 2): AlertCodes.BATTERY_IDENTIFICATION,
    ('MCETE', 1): AlertCodes.BATTERY_PARAMETERS,
    ('MCETE', 2): AlertCodes.BATTERY_PARAMETERS,
    ('MCETE', 3): AlertCodes.BATTERY_PARAMETERS,
    ('MCETE', 4): AlertCodes.BATTERY_PARAMETERS,
    ('MCETE', 5): AlertCodes.BATTERY_PARAMETERS,
    ('MCETE', 6): AlertCodes.BATTERY_PARAMETERS,
    ('MCETE', 7): AlertCodes.BATTERY_PARAMETERS,
    ('MCETE', 8): AlertCodes.BATTERY_PARAMETERS,
    ('MCECC', 1): AlertCodes.BATTERY_CHARGING,
    ('MCECC', 2): AlertCodes.BATTERY_CHARGING
}
errors_mapping: Dict[Tuple[str, int], Optional[AlertCodes]] = \
    frozendict(_errors_mapping)

_alerts_breakdowns = {
    AlertCodes.UNKNOWN: [],
    AlertCodes.CLIMATE_CTRL: [BreakdownZones.CARGO_PUT_TAKE,
                              BreakdownZones.BATTERY_CHARGER],
    AlertCodes.CLIMATE_ROOF_HEATING_WINTER: [BreakdownZones.DRONE_ARRIVAL,
                                             BreakdownZones.DRONE_DEPARTURE],
    AlertCodes.CLIMATE_ROOF_HEATING: [],
    AlertCodes.ROOF_CTRL: [BreakdownZones.DRONE_ARRIVAL,
                           BreakdownZones.DRONE_DEPARTURE],
    AlertCodes.CARGO_CTRL: [BreakdownZones.CARGO_PUT_TAKE,
                            BreakdownZones.CARGO_LOADER],
    AlertCodes.CARGO_PUT_TAKE: [BreakdownZones.CARGO_PUT_TAKE],
    AlertCodes.HANGAR_PMS: [BreakdownZones.CARGO_PUT_TAKE,
                            BreakdownZones.CARGO_LOADER,
                            BreakdownZones.BATTERY_LOADER,
                            BreakdownZones.DRONE_ARRIVAL,
                            BreakdownZones.DRONE_DEPARTURE],
    AlertCodes.USER_PANEL: [BreakdownZones.CARGO_PUT_TAKE],
    AlertCodes.BATTERY_CHARGING: [BreakdownZones.BATTERY_CHARGER],
    AlertCodes.BATTERY_IDENTIFICATION: [BreakdownZones.BATTERY_LOADER,
                                        BreakdownZones.BATTERY_CHARGER],
    AlertCodes.BATTERY_PARAMETERS: [BreakdownZones.BATTERY_CHARGER],
    # TODO: temporary unused
    # AlertCodes.BATTERY_CHARGER_CTRL: [BreakdownZones.BATTERY_LOADER,
    #                                   BreakdownZones.BATTERY_CHARGER],
}
alerts_breakdowns: Dict[AlertCodes, List[str]] = frozendict(
    {k: [zone.value for zone in v] for k, v in _alerts_breakdowns.items()}
)
