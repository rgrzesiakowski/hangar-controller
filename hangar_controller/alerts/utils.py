import re

from .alert_codes import AlertCodes


def parse_alert_to_string_code(alert: AlertCodes):
    return f'H{alert.value:07d}'


def parse_string_code_to_alert(alert_str: str):
    match = re.match(r'^[H]+\d{7}$', alert_str)  # find "H<7 digits>"
    if match:
        code_int = int(alert_str[1:])
        alert = AlertCodes(code_int)
    else:
        try:
            alert = AlertCodes[alert_str]
        except KeyError:
            alert = AlertCodes.UNKNOWN

    return alert
