import csv
import itertools
import logging
import os
from typing import List, Set, Iterable, Optional, Dict

from satella.coding.structures import Singleton

from common import get_timestamp_ms
from settings import AZURE_SEND_ALERTS_AND_LOGS
from settingsd.alerts.alerts_settings import PATH_EXISTING_ALERTS, \
    SAVE_ALERT_INTERVAL_MS
from .alert_codes import AlertCodes, alerts_breakdowns
from .utils import parse_string_code_to_alert, parse_alert_to_string_code
from ..azure_communication import AzureIoTHubClient
from ..azure_communication.messages import AlertMessage
from ..executor import HangarData

logger = logging.getLogger(__name__)


@Singleton
class AlertManager:
    def __init__(self):
        if not os.path.exists(PATH_EXISTING_ALERTS):
            with open(PATH_EXISTING_ALERTS, 'w'):
                pass  # Touch file

        self._last_alerts_timestamp: Dict[AlertCodes, int] = {}

        self.hangar_data = HangarData()

        self.existing_alerts: Set[AlertCodes] = self._load_existing_alerts()
        self.existing_breakdowns: List[str] = []

        self._update_breakdowns()

        if self.existing_alerts:
            logger.warning('Alert manager started with existing alerts.')

    def register(self, alert: AlertCodes, timestamp: Optional[int] = None):
        """Register new alert.

        The method will add new alert to the set, send Azure message and
        save the alert to the file if condition to save is met.

        Args:
            alert: AlertCodes enum
            timestamp: miliseconds timestamp

        Returns:
            True if registered successfully, False otherwise

        """
        alert_timestamp = timestamp or get_timestamp_ms()

        if alert not in self._last_alerts_timestamp:
            should_save = True
        else:
            last_registered_timestamp = self._last_alerts_timestamp[alert]
            timestamp_delta = alert_timestamp - last_registered_timestamp
            should_save = timestamp_delta >= SAVE_ALERT_INTERVAL_MS

        if not should_save:
            return False

        self._last_alerts_timestamp[alert] = alert_timestamp
        self.existing_alerts.add(alert)
        self._update_breakdowns()

        self._send_alert_message_to_cloud(alert, False)
        self._save_new_alert(alert, alert_timestamp)

    def repair(self, alert: AlertCodes) -> bool:
        """Remove the alert from csv file and send IoTHubMessage to
        cloud.
        Args:
            alert: AlertCodes enum

        Returns:
            bool: True if alert removed successfully

        """
        self.existing_alerts.discard(alert)
        self._update_breakdowns()

        self._send_alert_message_to_cloud(alert, True)
        self._erase_alert_type(alert)
        return True

    def _update_breakdowns(self):
        self.existing_breakdowns = self._get_existing_breakdowns(
            self.existing_alerts
        )

        if not AZURE_SEND_ALERTS_AND_LOGS:
            # Don't update hangar_data as it shouldn't forward
            # breakdowns to messages
            return

        with self.hangar_data:
            self.hangar_data.breakdowns = self.existing_breakdowns

    def _send_alert_message_to_cloud(self, alert: AlertCodes, repair: bool):
        if not AZURE_SEND_ALERTS_AND_LOGS:
            return

        alert_code = parse_alert_to_string_code(alert)
        breakdowns = self.existing_breakdowns if repair \
            else alerts_breakdowns[alert]

        alert_message = AlertMessage(alert_code, repair, breakdowns)
        AzureIoTHubClient().send_iothub_message(alert_message)

    @staticmethod
    def _save_new_alert(alert: AlertCodes, timestamp: int,
                        filepath: str = PATH_EXISTING_ALERTS) -> bool:
        """Save new alert at the end of the file; each alert in new row with
        its text representation and timestamp.

        Args:
            alert: AlertCodes enum
            timestamp: time of alert reporting
            filepath: absolute path to .csv file

        Returns:
            bool: True for success.

        Raises:
            OSError: If there is a problem with open() function

        """
        with open(filepath, 'a') as csvfile:
            csv.writer(csvfile).writerow(
                [parse_alert_to_string_code(alert), timestamp]
            )
        return True

    @staticmethod
    def _erase_alert_type(alert: AlertCodes,
                          filepath: str = PATH_EXISTING_ALERTS) -> bool:
        """Remove all alert occurrences in CSV file.

        Args:
            alert: AlertCodes enum to be erased from file
            filepath: absolute path to .csv file

        Returns:
            bool: True for success.

        Raises:
            OSError: If there is a problem with open() function

        """
        with open(filepath, 'r') as csvfile:
            reader = csv.reader(csvfile)
            rows_filtered = [
                row for row in reader
                if parse_string_code_to_alert(row[0]).name != alert.name
            ]
        with open(filepath, 'w') as csvfile:
            csv.writer(csvfile).writerows(rows_filtered)
        return True

    @staticmethod
    def _get_existing_breakdowns(alerts: Iterable[AlertCodes]) -> List[str]:
        """Get list of unique breakdowns for list of alerts.

        Args:
            alerts: Iterable of AlertCodes enums

        Returns:
            List of unique, 1D breakdowns list.

        Example:
            If there are more than one alerts with same breakdowns
            passed as a param (i.e.
            alert1_breakdowns = ["a", "b"]
            alert2_breakdowns = ["a", "c"]), the return of method will be
            ["a", "b", "c"]

        """
        breakdowns_nested = [alerts_breakdowns[alert] for alert in alerts]
        breakdowns_flat = itertools.chain.from_iterable(breakdowns_nested)
        return list(set(breakdowns_flat))

    @staticmethod
    def _load_existing_alerts(filepath: str = PATH_EXISTING_ALERTS
                              ) -> Set[AlertCodes]:
        """Get a set of saved alerts from csv file. The alerts could be
        either text representation or alert code.

        Args:
            filepath: absolute path to .csv file with text representation
                of alerts

        Returns:
            Set of AlertCodes enums

        """
        with open(filepath) as csvfile:
            reader = csv.reader(csvfile)
            alerts = set()
            for row in reader:
                if row:
                    alert_text = row[0]
                    alert = parse_string_code_to_alert(alert_text)
                    if alert == AlertCodes.UNKNOWN:
                        logger.error(f"Alert - {alert_text} - is not specified!")
                        continue
                    alerts.add(alert)
        return alerts
