import queue
import logging
from typing import NoReturn
from threading import Thread
from concurrent.futures.thread import ThreadPoolExecutor

import paho.mqtt.client as mqtt
from satella.coding.structures import Singleton

from .controller_components import Cargo, Roof, MasterCharger, Climate, \
    UserPanel, PowerManagementSystem
from .hangar_drivers_mqtt_client import HangarDriversMQTTClient
from .diagnostics.drivers_diagnostics import DriversDiagnostics
from .messages.abstract_message_manager import AbstractMessageManager
from .messages.commands.commands_manager import CommandsManager
from .messages.errors.error_manager import ErrorManager
from .messages.measurements.measurements_manager import MeasurementsManager
from .messages.responses.response_manager import ResponseManager
from .messages.state_readers.state_read_manager import StateReadManager
from .messages.warnings.warning_manager import WarningManager
from ..error_queues import ErrorQueue, EventLogQueue
from ..motors.motors import Motors
from ..motors.controller.task_queue.task_queue import TaskQueue

logger = logging.getLogger(__name__)


@Singleton
class DriversController(Thread):
    """Driver thread that sends messages to stm controllers.

    Driver is run automatically after starting motors.py.
    """

    def __init__(self, error_queue: queue.Queue = None,
                 error_warning_log_queue: queue.Queue = None):
        super().__init__()
        self._error_queue = error_queue
        if error_queue is None:
            self._error_queue = ErrorQueue()
        self._error_warning_log_queue = error_warning_log_queue
        if error_warning_log_queue is None:
            self._error_warning_log_queue = EventLogQueue()

        motors = Motors()
        wobit_diagnostics_reader = motors.diagnostics_reader
        self._mqtt_client = HangarDriversMQTTClient()
        self.send_message = self._mqtt_client.send_message
        self.message_queue = self._mqtt_client.messages_queue
        self._task_queue = TaskQueue()
        self._diagnostics = DriversDiagnostics()
        self.executor = ThreadPoolExecutor()
        self._measurements_manager = MeasurementsManager(self._diagnostics)
        self._response_manager = ResponseManager(self.executor)
        self._commands_manager = CommandsManager(self._response_manager,
                                                 self.send_message,
                                                 self._error_queue)
        self._state_read_manager = StateReadManager(wobit_diagnostics_reader,
                                                    self.send_message)
        self._error_manager = ErrorManager(self._error_queue,
                                           self._error_warning_log_queue)
        self._warning_manager = WarningManager(self._error_warning_log_queue)
        self.stop_controller = False

        self._cargo = Cargo(self._commands_manager)
        self._user_panel = UserPanel(self._commands_manager)
        self._climate = Climate(self._commands_manager)
        self._master_charger = MasterCharger(self._commands_manager)
        self._pms = PowerManagementSystem(self._commands_manager)
        self._roof = Roof(self._commands_manager)

    @property
    def diagnostics(self) -> DriversDiagnostics:
        """Property returning DriverDiagnostics object.

        That object contains all measurements sent from STM controllers.
        __repr__ from DriverDiagnostics is a string containing dictionary
        with format 'topic': value, where topic is mqtt topic that
        message was sent with.

        Returns:
            Driver Diagnostics object.
        """
        return self._diagnostics

    @property
    def error_manager(self):
        return self._error_manager

    @property
    def cargo(self):
        return self._cargo

    @property
    def user_panel(self):
        return self._user_panel

    @property
    def climate(self):
        return self._climate

    @property
    def master_charger(self):
        return self._master_charger

    @property
    def pms(self):
        return self._pms

    @property
    def roof(self):
        return self._roof

    def stop(self) -> None:
        """Stops drivers thread.

        Puts None on the queue to push it forwards.
        Stops all future responses that were waiting for response.

        Returns:
            None.
        """
        self.stop_controller = True
        self.message_queue.put(None)
        self._commands_manager.future_response_manager.stop()
        self._mqtt_client.close_connection()

    def _receive_message(self, message: mqtt.MQTTMessage) -> None:
        """This method is called after message arrives from STM.

        Message can either be:
            - a response to some command,
                that is registered in _response_manager
            - measurement, that is registered in _measurement_manager
            - error, that is registered in _error_manager
            - warning, that is registered in _warning_manager
        If message received is not registered, we ignore it.

        Note:
        Checking response needs to be executed before checking measurement
        because get_telemetry will register telemetry topics as responses,
        if we receive telemetry message as a measurement,
        command get_telemetry will raise Timeout Error.

        Note2:
        After receiving an error from Drivers, error is put on the error_queue,
        which should be read and decided upon by higher instance.

        Args:
            message: received from STM.

        Returns:
            None.
        """
        managers = [self._response_manager, self._measurements_manager,
                    self._state_read_manager, self._error_manager,
                    self._warning_manager]

        for manager in managers:
            assert (isinstance(manager, AbstractMessageManager))

            if manager.is_topic_registered(message.topic):
                manager.receive(message)
                break

    def run(self) -> NoReturn:
        """Starting main thread.

        This method is already executed in motors.start().

        Returns:
             NoReturn.
        """
        while not self.stop_controller:
            message = self.message_queue.get()
            if message is None:
                continue

            self._receive_message(message)

    def start_heartbeat(self) -> None:
        """Starts heartbeat sender thread.

        Sends heartbeat message to all STMs with specified frequency.

        Returns:
            None.
        """
        self._commands_manager.heartbeat_sender.start()

    def stop_heartbeat(self) -> None:
        """Stops heartbeat sender thread.

        Returns:
            None.
        """
        self._commands_manager.heartbeat_sender.stop()
