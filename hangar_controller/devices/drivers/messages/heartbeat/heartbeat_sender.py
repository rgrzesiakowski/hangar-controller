import logging
import time
from threading import Thread
from typing import Callable

from .heartbeat import Heartbeat
from ..responses import CollectiveResponseEvaluation
from settingsd.drivers.stm_communication_settings import \
    HEARTBEAT_TICK_TIME

logger = logging.getLogger(__name__)


class HeartbeatSender(Thread):
    def __init__(self, send_command_and_wait: Callable):
        super().__init__()
        self._send_command_and_wait = send_command_and_wait
        self._stop_sender = False
        self.heartbeat = Heartbeat()

    def run(self) -> None:
        while not self._stop_sender:
            response: CollectiveResponseEvaluation =\
                self._send_command_and_wait(self.heartbeat)
            if not response.evaluation:
                logger.warning(response)
            time.sleep(HEARTBEAT_TICK_TIME)

    def stop(self):
        self._stop_sender = True
