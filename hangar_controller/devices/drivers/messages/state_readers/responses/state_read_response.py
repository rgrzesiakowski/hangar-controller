import logging
from typing import Callable

from ...commands import AbstractCommand

logger = logging.getLogger(__name__)


class StateReadResponse(AbstractCommand):
    def __init__(self, topic: str, payload: str):
        super().__init__(topic, [], payload)
        self.log_creation()

    def publish(self, send_message: Callable):
        send_message(self.topic, self.payload)
        logger.info(f'Publishing StateReadResponse: TOPIC: {self.topic}, '
                    f'PAYLOAD: {self.payload}')

    def log_creation(self):
        logger.debug(f'Creating StateReadResponse with '
                     f'TOPIC: {self.topic}, \n'
                     f'and PAYLOAD: {self.payload}')
