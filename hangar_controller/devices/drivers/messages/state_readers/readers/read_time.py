import time
from typing import List

from .abstract_state_read import AbstractStateRead
from ..responses.state_read_response import StateReadResponse
from ...message_utils import value_topic, get_topic
from settingsd.drivers.stm_communication_settings import Quantity


class ReadTime(AbstractStateRead):
    TOPIC = f'{Quantity.TIME}'

    def __init__(self):
        super().__init__(get_topic(ReadTime.TOPIC))

    def get_responses(self) -> List[StateReadResponse]:
        payload = str(int(time.time()))
        topic = value_topic(ReadTime.TOPIC)
        responses = [StateReadResponse(topic, payload)]

        return responses
