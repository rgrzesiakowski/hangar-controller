from typing import Callable

import paho.mqtt.client as mqtt

from hangar_controller.devices.drivers.messages.abstract_message_manager import AbstractMessageManager
from hangar_controller.devices.drivers.messages.state_readers.readers.abstract_state_read import AbstractStateRead
from hangar_controller.devices.drivers.messages.state_readers.readers.read_lift_state import ReadLiftState
from hangar_controller.devices.drivers.messages.state_readers.readers.read_time import ReadTime
from hangar_controller.devices.motors.controller.diagnostics.wobit_diagnostics_reader import \
    WobitDiagnosticsReader


class StateReadManager(AbstractMessageManager):
    def __init__(self, wobit_diagnostics_reader: WobitDiagnosticsReader,
                 send_message_call: Callable):
        super().__init__()
        self.wobit_diagnostics_reader = wobit_diagnostics_reader
        self.send_message_call = send_message_call
        self._init_state_reads()

    def _init_state_reads(self) -> None:
        """Initialize all state reads here.

        Register them, so when the new message arrives,
        they will be processed by StateReadManager.

        Returns:
            None.
        """
        state_reads = [ReadLiftState(self.wobit_diagnostics_reader), ReadTime()]
        self.register_messages(state_reads)

    def receive(self, message: mqtt.MQTTMessage):
        state_read = self.get_message_by_topic(message.topic)

        assert (isinstance(state_read, AbstractStateRead))
        state_read.publish_responses(self.send_message_call)
