from ..measurement import Measurement
from ...message_utils import climate_measurement
from settingsd.drivers.stm_climate_settings import Measurements
from settingsd.drivers.stm_communication_settings import Quantity


class HangarLowerHumidity(Measurement):
    TOPIC = climate_measurement(Quantity.HUMIDITY, Measurements.HANGAR_LOWER)

    def __init__(self):
        super().__init__(HangarLowerHumidity.TOPIC)
