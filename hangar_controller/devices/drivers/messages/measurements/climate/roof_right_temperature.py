from ..measurement import Measurement
from ...message_utils import climate_measurement
from settingsd.drivers.stm_climate_settings import Measurements
from settingsd.drivers.stm_communication_settings import Quantity


class RoofRightTemperature(Measurement):
    TOPIC = climate_measurement(Quantity.TEMPERATURE, Measurements.ROOF_RIGHT)

    def __init__(self):
        super().__init__(RoofRightTemperature.TOPIC)
