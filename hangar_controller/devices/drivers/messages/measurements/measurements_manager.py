import paho.mqtt.client as mqtt

from hangar_controller.devices.drivers.diagnostics.drivers_diagnostics import DriversDiagnostics
from hangar_controller.devices.drivers.messages.abstract_message_manager import AbstractMessageManager
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_average_humidity import HangarAverageHumidity
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_average_temperature import \
    HangarAverageTemperature
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_lower_humidity import HangarLowerHumidity
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_lower_temperature import HangarLowerTemperature
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_upper_humidity import HangarUpperHumidity
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_upper_temperature import HangarUpperTemperature
from hangar_controller.devices.drivers.messages.measurements.climate.roof_left_temperature import RoofLeftTemperature
from hangar_controller.devices.drivers.messages.measurements.climate.roof_right_temperature import RoofRightTemperature
from hangar_controller.devices.drivers.messages.measurements.master_charger.battery_temperature import BatteryTemperature
from hangar_controller.devices.drivers.messages.measurements.master_charger.battery_uid import BatteryUID
from hangar_controller.devices.drivers.messages.measurements.master_charger.battery_voltage import BatteryVoltage
from hangar_controller.devices.drivers.messages.measurements.master_charger.charger_current import ChargerCurrent
from hangar_controller.devices.drivers.messages.measurements.master_charger.charger_temperature import ChargerTemperature
from hangar_controller.devices.drivers.messages.measurements.master_charger.charger_voltage import ChargerVoltage
from hangar_controller.devices.drivers.messages.measurements.measurement import Measurement

from settingsd.drivers.stm_communication_settings import SubControllers

MEASUREMENT_LIST = [HangarAverageHumidity(), HangarAverageTemperature(),
                    HangarLowerHumidity(), HangarLowerTemperature(),
                    HangarUpperHumidity(), HangarUpperTemperature(),
                    RoofLeftTemperature(), RoofRightTemperature(), ]

CHARGER_MEASUREMENTS_TYPES = [BatteryTemperature,
                              BatteryUID, BatteryVoltage, ChargerCurrent,
                              ChargerTemperature, ChargerVoltage]
CHARGER_LIST = []

for battery_id in SubControllers.BATTERIES:
    for measurement in CHARGER_MEASUREMENTS_TYPES:
        CHARGER_LIST.append(measurement(battery_id))

MEASUREMENT_LIST += CHARGER_LIST


class MeasurementsManager(AbstractMessageManager):
    def __init__(self, diagnostics: DriversDiagnostics):
        super().__init__()
        self.diagnostics = diagnostics
        self.register_messages(MEASUREMENT_LIST)

    def receive(self, message: mqtt.MQTTMessage):
        update = self.get_message_by_topic(message.topic)

        assert (isinstance(update, Measurement))
        self.diagnostics.update(**{update.topic: message.payload})
