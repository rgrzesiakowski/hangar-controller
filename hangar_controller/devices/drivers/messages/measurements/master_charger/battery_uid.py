from ..measurement import Measurement
from ...message_utils import battery_measurement
from settingsd.drivers.stm_communication_settings import SubControllers
from settingsd.drivers.stm_master_charger_settings import Measurements


class BatteryUID(Measurement):
    def __init__(self, battery_id: str):
        topic = battery_measurement(battery_id, SubControllers.BATTERY,
                                    Measurements.UID)

        super().__init__(topic)
