from typing import Type

from hangar_controller.devices.drivers.messages.abstract_message import AbstractMessage


class Measurement(AbstractMessage):
    def __init__(self, topic: str, type_: Type = float):
        super().__init__(topic)
        self.type_ = type_
