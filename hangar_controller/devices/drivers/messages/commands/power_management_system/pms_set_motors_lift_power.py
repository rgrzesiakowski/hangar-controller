from typing import Union

from .. import AbstractCommand
from ...message_utils import command_topic, call_topic, state_change_topic
from ...responses.expected_messages.acknowledgement import create_ack_response
from ...responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from ...responses import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSControlCommands, \
    PMSTimeouts, PMSStates


class PMSSetMotorsLiftPower(AbstractCommand):
    ENABLE = EnableDisableResponse.ENABLE
    DISABLE = EnableDisableResponse.DISABLE

    TOPIC = command_topic(Controllers.STM_PMS,
                          PMSControlCommands.MOTORS_LIFT_POWER)

    def __init__(self, value: Union[ENABLE, DISABLE]):
        ack_response = create_ack_response(PMSSetMotorsLiftPower.TOPIC)
        lift_response = Response(
            state_change_topic(Controllers.STM_PMS,
                               PMSStates.MOTORS_LIFT_POWER),
            PMSTimeouts.MOTORS_LIFT_POWER,
            EnableDisableResponse(value))

        responses = [ack_response, lift_response]

        super().__init__(call_topic(PMSSetMotorsLiftPower.TOPIC), responses,
                         value)
