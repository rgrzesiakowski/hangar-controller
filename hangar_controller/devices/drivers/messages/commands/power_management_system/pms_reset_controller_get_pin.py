from .. import AbstractCommand
from ...message_utils import command_topic, call_topic, pin_topic
from ...responses.expected_messages.value_response import ValueResponse
from ...responses import Response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, PIN_TIMEOUT


class PMSResetControllerGetPin(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_PMS, CommandMessage.CTRL_RESET)

    def __init__(self):
        responses = [
            Response(pin_topic(PMSResetControllerGetPin.TOPIC), PIN_TIMEOUT,
                     ValueResponse())]
        super().__init__(call_topic(PMSResetControllerGetPin.TOPIC),
                         responses, CommandMessage.RESET)
