from .. import AbstractCommand
from ...message_utils import create_response_list, command_topic, call_topic, \
    plc_io_topic, response_topic
from ...responses.expected_messages.acknowledgement import Acknowledgment
from ...responses.expected_messages.plc_io_response import PLCIOResponse
from settingsd.drivers.stm_communication_settings import Controllers, \
    ACK_TIMEOUT, CommandMessage


class PMSGetPLCIO(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_PMS, CommandMessage.GET_PLC_IO)

    RESPONSE_TOPICS = [
        response_topic(TOPIC),
        plc_io_topic(Controllers.STM_PMS, inputs=True),
        plc_io_topic(Controllers.STM_PMS, inputs=False),
    ]

    TIMEOUTS = [ACK_TIMEOUT, ] * len(RESPONSE_TOPICS)

    def __init__(self):
        expected_messages = [Acknowledgment(),
                             *[PLCIOResponse(), ] * (
                                     len(PMSGetPLCIO.RESPONSE_TOPICS) - 1)]

        responses = create_response_list(PMSGetPLCIO.RESPONSE_TOPICS,
                                         PMSGetPLCIO.TIMEOUTS,
                                         expected_messages)
        super().__init__(call_topic(PMSGetPLCIO.TOPIC), responses,
                         CommandMessage.GET)
