from .. import AbstractCommand
from ...message_utils import command_topic, call_topic
from ...responses.expected_messages.acknowledgement import create_ack_response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage


class PMSResetPLC(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_PMS, CommandMessage.PLC_RESET)

    def __init__(self):
        responses = [create_ack_response(PMSResetPLC.TOPIC)]
        super().__init__(call_topic(PMSResetPLC.TOPIC), responses,
                         CommandMessage.RESET)
