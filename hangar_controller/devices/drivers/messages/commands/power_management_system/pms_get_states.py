from .. import AbstractCommand
from ...message_utils import command_topic, response_topic, \
    state_change_topic, call_topic, create_response_list
from ...responses.expected_messages.acknowledgement import Acknowledgment
from ...responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, MULTIPLE_RESPONSE_TIMEOUT
from settingsd.drivers.stm_pms_settings import PMSStates


class PMSGetStates(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_PMS, CommandMessage.GET_STATES)
    PMS_STATES_LIST = [key for topic, key in PMSStates.__dict__.items() if
                       not topic.startswith('__')]

    RESPONSE_TOPICS = [
        response_topic(TOPIC),
        *[state_change_topic(Controllers.STM_PMS, state) for state in
          PMS_STATES_LIST]
    ]
    TIMEOUTS = [MULTIPLE_RESPONSE_TIMEOUT, ] * len(RESPONSE_TOPICS)

    def __init__(self):
        expected_messages = [
            Acknowledgment(),
            *[EnableDisableResponse(None), ] * len(
                PMSGetStates.PMS_STATES_LIST)
        ]

        responses = create_response_list(PMSGetStates.RESPONSE_TOPICS,
                                         PMSGetStates.TIMEOUTS,
                                         expected_messages)

        super().__init__(call_topic(PMSGetStates.TOPIC), responses,
                         CommandMessage.GET)
