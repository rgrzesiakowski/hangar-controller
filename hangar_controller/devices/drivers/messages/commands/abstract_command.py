import logging
from typing import List, Callable

from ..abstract_message import AbstractMessage
from ..responses.response import Response

logger = logging.getLogger(__name__)


class AbstractCommand(AbstractMessage):
    def __init__(self, topic: str, responses: List[Response], payload: str):
        super().__init__(topic)
        self.responses = responses
        self.payload = payload
        self.log_creation()

    def publish(self, send_message: Callable):
        send_message(self.topic, self.payload)
        logger.debug(f'Publishing Command:\n'
                     f'\tTOPIC: {self.topic};\n'
                     f'\tPAYLOAD: {self.payload}')

    def log_creation(self):
        logger.debug('Creating command:\n'
                     f'\tTOPIC: {self.topic};\n'
                     f'\tRESPONSES: {self.responses};\n'
                     f'\tPAYLOAD: {self.payload}')
