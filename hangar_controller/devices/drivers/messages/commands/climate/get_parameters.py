from .. import AbstractCommand
from ...message_utils import create_response_list, command_topic, call_topic, \
    response_topic, read_topic, parameter_topic
from ...responses.expected_messages.acknowledgement import Acknowledgment
from ...responses.expected_messages.value_response import ValueResponse
from settingsd.drivers.stm_climate_settings import ClimateControlCommands
from settingsd.drivers.stm_communication_settings import Controllers, \
    Quantity, CommandMessage, MULTIPLE_RESPONSE_TIMEOUT


class ClimateGetParameters(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CLIMATE,
                          CommandMessage.GET_PARAMETERS)
    RESPONSE_TOPICS = [
        response_topic(TOPIC),
        read_topic(
            parameter_topic(Controllers.STM_CLIMATE, Quantity.TEMPERATURE,
                            ClimateControlCommands.ROOF_HEAT)),
        read_topic(
            parameter_topic(Controllers.STM_CLIMATE, Quantity.TEMPERATURE,
                            ClimateControlCommands.ROOF_HYSTERESIS)),
        read_topic(
            parameter_topic(Controllers.STM_CLIMATE, Quantity.TEMPERATURE,
                            ClimateControlCommands.HANGAR_HEAT)),
        read_topic(
            parameter_topic(Controllers.STM_CLIMATE, Quantity.TEMPERATURE,
                            ClimateControlCommands.HANGAR_HYSTERESIS)),
        read_topic(parameter_topic(Controllers.STM_CLIMATE, Quantity.HUMIDITY,
                                   ClimateControlCommands.HANGAR_HYSTERESIS)),
        read_topic(parameter_topic(Controllers.STM_CLIMATE, Quantity.HUMIDITY,
                                   ClimateControlCommands.HANGAR_HUMIDITY)),
    ]
    TIMEOUTS = [MULTIPLE_RESPONSE_TIMEOUT, ] * len(RESPONSE_TOPICS)

    def __init__(self):
        expected_messages = [Acknowledgment(),
                             *[ValueResponse(), ] * 6]
        responses = create_response_list(ClimateGetParameters.RESPONSE_TOPICS,
                                         ClimateGetParameters.TIMEOUTS,
                                         expected_messages)
        super().__init__(call_topic(ClimateGetParameters.TOPIC), responses,
                         CommandMessage.GET)
