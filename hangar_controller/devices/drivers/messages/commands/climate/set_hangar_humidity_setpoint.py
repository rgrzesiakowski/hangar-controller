from .. import AbstractCommand
from ...message_utils import create_response_list, response_topic, set_topic, \
    parameter_topic, humidity_payload
from ...responses.expected_messages.acknowledgement import Acknowledgment
from settingsd.drivers.stm_climate_settings import ClimateControlCommands
from settingsd.drivers.stm_communication_settings import Controllers, \
    ACK_TIMEOUT, \
    Quantity


class SetHangarHumiditySetPoint(AbstractCommand):
    TOPIC = parameter_topic(Controllers.STM_CLIMATE, Quantity.HUMIDITY,
                            ClimateControlCommands.HANGAR_HUMIDITY)

    RESPONSE_TOPICS = [response_topic(TOPIC)]
    TIMEOUTS = [ACK_TIMEOUT, ]

    def __init__(self, humidity: float):
        """Args:
            humidity: float humidity value in % (0-100)
        """
        payload = humidity_payload(humidity)
        expected_messages = [Acknowledgment()]
        responses = create_response_list(
            SetHangarHumiditySetPoint.RESPONSE_TOPICS,
            SetHangarHumiditySetPoint.TIMEOUTS,
            expected_messages)

        super().__init__(set_topic(SetHangarHumiditySetPoint.TOPIC), responses,
                         payload)
