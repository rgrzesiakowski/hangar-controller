from .. import AbstractCommand
from ...message_utils import command_topic, call_topic
from ...responses.expected_messages.acknowledgement import create_ack_response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage


class ClimateResetPLC(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CLIMATE, CommandMessage.PLC_RESET)

    def __init__(self):
        responses = [create_ack_response(ClimateResetPLC.TOPIC)]
        super().__init__(call_topic(ClimateResetPLC.TOPIC), responses,
                         CommandMessage.RESET)
