from typing import Union

from .. import AbstractCommand
from ...message_utils import create_response_list, command_topic, call_topic, \
    response_topic, state_change_topic
from ...responses.expected_messages.acknowledgement import Acknowledgment
from ...responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from settingsd.drivers.stm_climate_settings import ClimateControlCommands, \
    ClimateControlTimeouts
from settingsd.drivers.stm_communication_settings import Controllers, \
    ACK_TIMEOUT, \
    ResponseMessage


class SetHangarHeater(AbstractCommand):
    ENABLE = ResponseMessage.ENABLE
    DISABLE = ResponseMessage.DISABLE

    TOPIC = command_topic(Controllers.STM_CLIMATE,
                          ClimateControlCommands.HANGAR_HEATER)

    RESPONSE_TOPICS = [response_topic(TOPIC),
                       state_change_topic(Controllers.STM_CLIMATE,
                                          ClimateControlCommands.HANGAR_HEATER)]
    TIMEOUTS = [ACK_TIMEOUT,
                ClimateControlTimeouts.HANGAR_HEATER]

    def __init__(self, enable: Union[ENABLE, DISABLE]):
        """Args:
            enable: enable hangar heater if TRUE, disable if FALSE
        """
        expected_messages = [Acknowledgment(),
                             EnableDisableResponse(enable)]
        responses = create_response_list(SetHangarHeater.RESPONSE_TOPICS,
                                         SetHangarHeater.TIMEOUTS,
                                         expected_messages)

        super().__init__(call_topic(SetHangarHeater.TOPIC), responses, enable)
