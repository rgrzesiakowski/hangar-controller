from .. import AbstractCommand
from ...message_utils import set_topic, parameter_topic, temperature_payload
from ...responses.expected_messages.acknowledgement import create_ack_response
from settingsd.drivers.stm_climate_settings import ClimateControlCommands
from settingsd.drivers.stm_communication_settings import Controllers, \
    Quantity


class SetHangarHeaterHysteresis(AbstractCommand):
    TOPIC = parameter_topic(Controllers.STM_CLIMATE, Quantity.TEMPERATURE,
                            ClimateControlCommands.HANGAR_HYSTERESIS)

    def __init__(self, temperature: float):
        """Args:
            temperature: float value of temperature set point in Kelvin
        """
        payload = temperature_payload(temperature)
        responses = [create_ack_response(SetHangarHeaterHysteresis.TOPIC)]

        super().__init__(set_topic(SetHangarHeaterHysteresis.TOPIC), responses,
                         payload)
