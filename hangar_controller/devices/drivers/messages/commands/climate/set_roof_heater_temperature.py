from .. import AbstractCommand
from ...message_utils import create_response_list, response_topic, set_topic, \
    parameter_topic, temperature_payload
from ...responses.expected_messages.acknowledgement import Acknowledgment
from settingsd.drivers.stm_climate_settings import ClimateControlCommands
from settingsd.drivers.stm_communication_settings import Controllers, \
    ACK_TIMEOUT, \
    Quantity


class SetRoofHeaterTemperature(AbstractCommand):
    TOPIC = parameter_topic(Controllers.STM_CLIMATE, Quantity.TEMPERATURE,
                            ClimateControlCommands.ROOF_HEAT)

    RESPONSE_TOPICS = [response_topic(TOPIC)]
    TIMEOUTS = [ACK_TIMEOUT, ]

    def __init__(self, temperature: float):
        """Args:
            temperature: roof heater set point in Kelvin
        """
        payload = temperature_payload(temperature)
        expected_messages = [Acknowledgment()]
        responses = create_response_list(
            SetRoofHeaterTemperature.RESPONSE_TOPICS,
            SetRoofHeaterTemperature.TIMEOUTS,
            expected_messages)

        super().__init__(set_topic(SetRoofHeaterTemperature.TOPIC), responses,
                         payload)
