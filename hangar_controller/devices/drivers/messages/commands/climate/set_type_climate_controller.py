from typing import Union

from .. import AbstractCommand
from ...message_utils import create_response_list, command_topic, call_topic, \
    response_topic, state_change_topic
from ...responses.expected_messages.acknowledgement import Acknowledgment
from ...responses.expected_messages.climate.auto_manual_response import \
    AutoManualResponse
from settingsd.drivers.stm_climate_settings import ClimateControlCommands, \
    ClimateControlTimeouts
from settingsd.drivers.stm_communication_settings import Controllers, \
    ACK_TIMEOUT


class SetTypeClimateController(AbstractCommand):
    AUTO = ClimateControlCommands.AUTO
    MANUAL = ClimateControlCommands.MANUAL

    TOPIC = command_topic(Controllers.STM_CLIMATE, ClimateControlCommands.TYPE)

    RESPONSE_TOPICS = [response_topic(TOPIC),
                       state_change_topic(Controllers.STM_CLIMATE,
                                          ClimateControlCommands.TYPE)]
    TIMEOUTS = [ACK_TIMEOUT,
                ClimateControlTimeouts.TYPE]

    def __init__(self, auto: Union[AUTO, MANUAL]):
        """Args:
            auto: Climate STM controller set to auto if AUTO,
                or manual if MANUAL
        """
        expected_messages = [Acknowledgment(),
                             AutoManualResponse(auto)]
        responses = create_response_list(
            SetTypeClimateController.RESPONSE_TOPICS,
            SetTypeClimateController.TIMEOUTS,
            expected_messages)

        super().__init__(call_topic(SetTypeClimateController.TOPIC), responses,
                         auto)
