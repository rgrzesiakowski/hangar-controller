from .. import AbstractCommand
from ...message_utils import command_topic, call_topic, state_change_topic
from ...responses.expected_messages.acknowledgement import create_ack_response
from ...responses.expected_messages.cargo.cargo_presence_response import \
    CargoPresenceResponse
from ...responses.response import Response
from settingsd.drivers.stm_cargo_settings import CargoControlCommands, \
    CargoTimeouts
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage


class CargoGetPresence(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO,
                          CargoControlCommands.CARGO_PRESENCE)
    STATE_CHANGE_TOPIC = state_change_topic(Controllers.STM_CARGO,
                                            CargoControlCommands.CARGO_PRESENCE)

    def __init__(self):
        ack_response = create_ack_response(CargoGetPresence.TOPIC)
        presence_response = Response(CargoGetPresence.STATE_CHANGE_TOPIC,
                                     CargoTimeouts.CARGO_PRESENCE,
                                     CargoPresenceResponse())

        responses = [ack_response, presence_response]
        super().__init__(call_topic(CargoGetPresence.TOPIC), responses,
                         CommandMessage.GET)
