from .. import AbstractCommand
from ...message_utils import command_topic, response_topic, \
    state_change_topic, call_topic
from ...responses.expected_messages.acknowledgement import Acknowledgment
from ...responses.expected_messages.cargo.shift_response import ShiftResponse
from ...responses import Response
from settingsd.drivers.stm_cargo_settings import CargoControlCommands, \
    CargoTimeouts, CargoStates
from settingsd.drivers.stm_communication_settings import Controllers, \
    ACK_TIMEOUT


class CargoWorkShiftCheckConditions(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO, CargoControlCommands.SHIFT)

    def __init__(self):
        ack_response = Response(response_topic(CargoWorkShiftCheckConditions.TOPIC),
                                ACK_TIMEOUT,
                                Acknowledgment())
        shift_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_WORK_CHECK_COND,
            ShiftResponse(ShiftResponse.WORK))
        idle_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_WORK_CHECK_COND,
            ShiftResponse(ShiftResponse.IDLE))
        responses = [ack_response, idle_response, shift_response]
        super().__init__(call_topic(CargoWorkShiftCheckConditions.TOPIC), responses,
                         CargoControlCommands.WORK_CONDITIONS)
