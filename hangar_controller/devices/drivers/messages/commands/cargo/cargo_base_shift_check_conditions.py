from .. import AbstractCommand
from ...message_utils import command_topic, state_change_topic, call_topic
from ...responses.expected_messages.acknowledgement import create_ack_response
from ...responses.expected_messages.cargo.shift_response import ShiftResponse
from ...responses import Response
from settingsd.drivers.stm_cargo_settings import CargoControlCommands, \
    CargoTimeouts, CargoStates
from settingsd.drivers.stm_communication_settings import Controllers


class CargoBaseShiftCheckConditions(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO, CargoControlCommands.SHIFT)

    def __init__(self):
        ack_response = create_ack_response(CargoBaseShiftCheckConditions.TOPIC)
        shift_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_BASE_CHECK_COND,
            ShiftResponse(ShiftResponse.BASE))
        idle_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_BASE_CHECK_COND,
            ShiftResponse(ShiftResponse.IDLE))
        responses = [ack_response, idle_response, shift_response]
        super().__init__(call_topic(CargoBaseShiftCheckConditions.TOPIC),
                         responses,
                         CargoControlCommands.BASE_CONDITIONS)
