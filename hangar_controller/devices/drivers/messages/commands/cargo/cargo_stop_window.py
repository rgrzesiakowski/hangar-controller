from .. import AbstractCommand
from ...message_utils import command_topic, state_change_topic, call_topic
from ...responses.expected_messages.acknowledgement import create_ack_response
from ...responses.expected_messages.cargo.window_response import WindowResponse
from ...responses import Response
from settingsd.drivers.stm_cargo_settings import CargoControlCommands, \
    CargoTimeouts
from settingsd.drivers.stm_communication_settings import Controllers


class CargoStopWindow(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO, CargoControlCommands.WINDOW)
    STATE_CHANGE_TOPIC = state_change_topic(TOPIC, CargoControlCommands.WINDOW)

    def __init__(self):
        responses = [
            create_ack_response(CargoStopWindow.TOPIC),
            Response(CargoStopWindow.STATE_CHANGE_TOPIC, CargoTimeouts.WINDOW_STOP,
                     WindowResponse(WindowResponse.STOP))
        ]
        super().__init__(call_topic(CargoStopWindow.TOPIC), responses,
                         CargoControlCommands.STOP)
