from .. import AbstractCommand
from ...message_utils import command_topic, call_topic, state_change_topic
from ...responses.expected_messages.value_response import ValueResponse
from ...responses.expected_messages.acknowledgement import create_ack_response
from ....messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage
from settingsd.drivers.stm_cargo_settings import CargoControlCommands, \
    CargoTimeouts


class CargoTare(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO,
                          CargoControlCommands.CARGO_WEIGHT)
    STATE_CHANGE_TOPIC = state_change_topic(Controllers.STM_CARGO,
                                            CargoControlCommands.CARGO_WEIGHT)

    def __init__(self, force: bool = False):
        ack_response = create_ack_response(CargoTare.TOPIC)
        weight_response = Response(CargoTare.STATE_CHANGE_TOPIC,
                                   CargoTimeouts.CARGO_WEIGHT, ValueResponse())

        responses = [ack_response, weight_response]
        command_payload = CommandMessage.TARE + (',f' if force else '')
        super().__init__(call_topic(CargoTare.TOPIC), responses, command_payload)
