from .. import AbstractCommand
from ...message_utils import command_topic, call_topic, pin_topic
from ...responses.expected_messages.value_response import ValueResponse
from ...responses import Response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, PIN_TIMEOUT


class CargoResetControllerGetPin(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO, CommandMessage.CTRL_RESET)

    def __init__(self):
        responses = [
            Response(pin_topic(CargoResetControllerGetPin.TOPIC), PIN_TIMEOUT,
                     ValueResponse())]
        super().__init__(call_topic(CargoResetControllerGetPin.TOPIC),
                         responses, CommandMessage.RESET)
