from .. import AbstractCommand
from ...message_utils import command_topic, response_topic, \
    state_change_topic, call_topic, create_response_list
from ...responses.expected_messages.acknowledgement import Acknowledgment
from ...responses.expected_messages.cargo.cargo_shift_state_response import \
    CargoShiftStateResponse
from ...responses.expected_messages.cargo.window_response import WindowResponse
from ...responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from settingsd.drivers.stm_cargo_settings import CargoStates
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, MULTIPLE_RESPONSE_TIMEOUT


class CargoGetStates(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO, CommandMessage.GET_STATES)
    CARGO_STATES_LIST = [topic for name, topic in CargoStates.__dict__.items()
                         if not name.startswith('__')]
    RESPONSE_TOPICS = [
        response_topic(TOPIC),
        *[state_change_topic(Controllers.STM_CARGO, state) for state in
          CARGO_STATES_LIST]
    ]
    TIMEOUTS = [MULTIPLE_RESPONSE_TIMEOUT, ] * len(RESPONSE_TOPICS)

    def __init__(self):
        expected_messages = [
            Acknowledgment(),
            WindowResponse(None),
            CargoShiftStateResponse(None),
            *[EnableDisableResponse(None), ] * 12,
        ]

        responses = create_response_list(CargoGetStates.RESPONSE_TOPICS,
                                         CargoGetStates.TIMEOUTS,
                                         expected_messages)
        super().__init__(call_topic(CargoGetStates.TOPIC), responses,
                         CommandMessage.GET)
