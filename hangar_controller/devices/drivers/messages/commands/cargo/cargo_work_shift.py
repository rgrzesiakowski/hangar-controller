from .. import AbstractCommand
from ...message_utils import command_topic, response_topic, \
    state_change_topic, call_topic
from ...responses.expected_messages.acknowledgement import Acknowledgment
from ...responses.expected_messages.cargo.shift_response import ShiftResponse
from ...responses import Response
from settingsd.drivers.stm_cargo_settings import CargoControlCommands, \
    CargoTimeouts, CargoStates
from settingsd.drivers.stm_communication_settings import Controllers, \
    ACK_TIMEOUT


class CargoWorkShift(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO, CargoControlCommands.SHIFT)

    def __init__(self):
        ack_response = Response(response_topic(CargoWorkShift.TOPIC),
                                ACK_TIMEOUT,
                                Acknowledgment())
        idle_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_WORK,
            ShiftResponse(ShiftResponse.IDLE))
        shift_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_WORK,
            ShiftResponse(ShiftResponse.WORK))

        responses = [ack_response, idle_response, shift_response]
        super().__init__(call_topic(CargoWorkShift.TOPIC), responses,
                         CargoControlCommands.WORK)
