from .. import AbstractCommand
from ...message_utils import command_topic, call_topic, state_change_topic
from ...responses.expected_messages.acknowledgement import create_ack_response
from ...responses.expected_messages.user_panel.view_response import ViewResponse
from ...responses import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_user_panel_settings import UserPanelStates, \
    UserPanelTimeouts, UserPanelViews, UserPanelControlCommands


class UserPanelSetView(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_USER_PANEL,
                          UserPanelControlCommands.VIEW)
    STATE_CHANGE = state_change_topic(Controllers.STM_USER_PANEL,
                                      UserPanelStates.VIEW)

    def __init__(self, view: UserPanelViews):
        """Args:
            view: view name that should user panel change to.
                Choose one from UserPanelViews
        """
        ack_response = create_ack_response(UserPanelSetView.TOPIC)
        state_change_response = Response(UserPanelSetView.STATE_CHANGE,
                                         UserPanelTimeouts.VIEW,
                                         ViewResponse(view))
        responses = [ack_response, state_change_response]
        super().__init__(call_topic(UserPanelSetView.TOPIC), responses,
                         str(view))
