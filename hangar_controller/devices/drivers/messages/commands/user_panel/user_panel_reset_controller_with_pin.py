from .. import AbstractCommand
from ...message_utils import command_topic, call_topic
from ...responses.expected_messages.acknowledgement import create_ack_response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage


class UserPanelResetControllerWithPin(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_USER_PANEL,
                          CommandMessage.CTRL_RESET)

    def __init__(self, pin: str):
        responses = [create_ack_response(UserPanelResetControllerWithPin.TOPIC)]
        super().__init__(call_topic(UserPanelResetControllerWithPin.TOPIC),
                         responses, pin)
