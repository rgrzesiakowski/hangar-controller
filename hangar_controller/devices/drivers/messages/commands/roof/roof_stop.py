from ..abstract_command import AbstractCommand
from ...message_utils import command_topic, call_topic, state_change_topic, \
    response_topic
from ...responses.expected_messages.acknowledgement import Acknowledgment
from ...responses.expected_messages.roof.roof_state_response import \
    RoofStateResponse
from ...responses import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_roof_settings import RoofControlCommands, \
    RoofControlTimeouts


class RoofStop(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_ROOF, RoofControlCommands.ROOF)
    STATE_CHANGE = state_change_topic(Controllers.STM_ROOF,
                                      RoofControlCommands.ROOF)

    def __init__(self):
        responses = [
            Response(response_topic(RoofStop.TOPIC),
                     RoofControlTimeouts.ROOF_ACK, Acknowledgment()),
            Response(RoofStop.STATE_CHANGE,
                     RoofControlTimeouts.ROOF_STOP,
                     RoofStateResponse(RoofStateResponse.IDLE))
        ]
        super().__init__(call_topic(RoofStop.TOPIC), responses,
                         RoofControlCommands.STOP)
