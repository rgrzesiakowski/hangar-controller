from .. import AbstractCommand
from ...message_utils import call_topic, command_topic, state_change_topic, \
    response_topic
from ...responses.expected_messages.acknowledgement import Acknowledgment
from ...responses.expected_messages.roof.roof_state_response import \
    RoofStateResponse
from ...responses import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_roof_settings import RoofControlCommands, \
    RoofControlTimeouts


class RoofClose(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_ROOF, RoofControlCommands.ROOF)
    STATE_CHANGE = state_change_topic(Controllers.STM_ROOF,
                                      RoofControlCommands.ROOF)

    def __init__(self):
        ack_response = Response(response_topic(RoofClose.TOPIC),
                                RoofControlTimeouts.ROOF_ACK, Acknowledgment())
        roof_closing = Response(RoofClose.STATE_CHANGE,
                                RoofControlTimeouts.ROOF_CLOSE,
                                RoofStateResponse(RoofStateResponse.IDLE))
        roof_state = Response(RoofClose.STATE_CHANGE,
                              RoofControlTimeouts.ROOF_CLOSE,
                              RoofStateResponse(RoofStateResponse.CLOSE))
        responses = [ack_response, roof_closing, roof_state]
        super().__init__(call_topic(RoofClose.TOPIC), responses,
                         RoofControlCommands.CLOSE)
