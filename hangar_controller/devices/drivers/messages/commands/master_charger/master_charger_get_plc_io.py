from .. import AbstractCommand
from ...message_utils import create_response_list, command_topic, call_topic, \
    plc_io_topic, response_topic
from ...responses.expected_messages.acknowledgement import Acknowledgment
from ...responses.expected_messages.plc_io_response import PLCIOResponse
from settingsd.drivers.stm_communication_settings import Controllers, \
    ACK_TIMEOUT, \
    CommandMessage


class MasterChargerGetPLCIO(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_MASTER_CHARGER,
                          CommandMessage.GET_PLC_IO)

    RESPONSE_TOPICS = [
        response_topic(TOPIC),
        plc_io_topic(Controllers.STM_MASTER_CHARGER, inputs=True),
        plc_io_topic(Controllers.STM_MASTER_CHARGER, inputs=False),
    ]

    TIMEOUTS = [ACK_TIMEOUT, ] * len(RESPONSE_TOPICS)

    def __init__(self):
        expected_messages = [Acknowledgment(),
                             *[PLCIOResponse(), ] * (len(
                                 MasterChargerGetPLCIO.RESPONSE_TOPICS) - 1)]
        responses = create_response_list(MasterChargerGetPLCIO.RESPONSE_TOPICS,
                                         MasterChargerGetPLCIO.TIMEOUTS,
                                         expected_messages)
        super().__init__(call_topic(MasterChargerGetPLCIO.TOPIC), responses,
                         CommandMessage.GET)
