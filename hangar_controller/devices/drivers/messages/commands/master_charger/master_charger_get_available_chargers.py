from .. import AbstractCommand
from ...message_utils import command_topic, call_topic, state_change_topic
from ...responses.expected_messages.acknowledgement import create_ack_response
from ...responses.expected_messages.master_charger.available_chargers_response import \
    AvailableChargersResponse
from ...responses import Response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, MULTIPLE_RESPONSE_TIMEOUT
from settingsd.drivers.stm_master_charger_settings import \
    MasterChargerStates


class MasterChargerGetAvailableChargers(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_MASTER_CHARGER,
                          CommandMessage.GET_AVAILABLE_CHARGERS)
    STATE_TOPIC = state_change_topic(
        Controllers.STM_MASTER_CHARGER,
        MasterChargerStates.AVAILABLE_CHARGERS)

    def __init__(self):
        ack_response = create_ack_response(
            MasterChargerGetAvailableChargers.TOPIC)
        chargers_list_response = Response(MasterChargerGetAvailableChargers.STATE_TOPIC,
                                          MULTIPLE_RESPONSE_TIMEOUT,
                                          AvailableChargersResponse())
        responses = [ack_response, chargers_list_response]
        super().__init__(call_topic(MasterChargerGetAvailableChargers.TOPIC),
                         responses, CommandMessage.GET)
