from .. import AbstractCommand
from ...message_utils import command_topic, call_topic
from ...responses.expected_messages.acknowledgement import create_ack_response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage


class MasterChargerResetControllerWithPin(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_MASTER_CHARGER,
                          CommandMessage.CTRL_RESET)

    def __init__(self, pin: str):
        responses = [
            create_ack_response(MasterChargerResetControllerWithPin.TOPIC)]
        super().__init__(call_topic(MasterChargerResetControllerWithPin.TOPIC),
                         responses, pin)
