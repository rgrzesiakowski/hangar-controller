import queue
import logging
from typing import Callable

import paho.mqtt.client as mqtt

from . import AbstractCommand
from ..abstract_message_manager import AbstractMessageManager
from ..responses import CollectiveResponseEvaluation, FutureResponse, \
    ResponseManager
from ..heartbeat.heartbeat_sender import HeartbeatSender
from ...future_response.future_response_manager import \
    FutureResponseManager

logger = logging.getLogger(__name__)


class CommandsManager(AbstractMessageManager):
    def __init__(self, response_manager: ResponseManager,
                 send_message_call: Callable, error_queue: queue.Queue):
        super().__init__()
        self._response_manager = response_manager
        self.send_message_callable = send_message_call
        self.heartbeat_sender = HeartbeatSender(self.send_command_and_wait)
        self.future_response_manager = FutureResponseManager()
        self.error_queue = error_queue

    def send_command(self, command: AbstractCommand) \
            -> FutureResponse:
        self._response_manager.register_messages(command.responses)
        future_response = self._response_manager.create_future_response(
            command)
        command.publish(self.send_message_callable)

        return future_response

    def check_evaluation_and_add_to_error_queue(
            self, evaluation: CollectiveResponseEvaluation):
        if not evaluation.evaluation:
            self.error_queue.put(evaluation)
            self._response_manager.unregister_timeout_topics(evaluation)
            logger.error(f"Command failed to execute. "
                         f"Topic: {evaluation.topic}, "
                         f"Reason: {evaluation.evaluation_reason}.")

    def send_command_and_wait(self, command: AbstractCommand) \
            -> CollectiveResponseEvaluation:
        future_response = self.send_command(command)
        evaluation = future_response.result()

        assert (isinstance(evaluation, CollectiveResponseEvaluation))
        self.check_evaluation_and_add_to_error_queue(evaluation)

        return evaluation

    def send_command_no_wait(self, command: AbstractCommand):
        future_response = self.send_command(command)
        self.future_response_manager.start_future_observer(future_response)

        return future_response

    def receive(self, message: mqtt.MQTTMessage):
        # Commands should never be received
        raise NotImplementedError
