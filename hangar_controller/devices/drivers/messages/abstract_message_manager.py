from typing import List
import paho.mqtt.client as mqtt

from hangar_controller.devices.drivers.messages.abstract_message import AbstractMessage


class AbstractMessageManager:
    def __init__(self):
        self.registered_messages = []

    @property
    def registered_topics(self):
        return [message.topic for message in self.registered_messages]

    def is_topic_registered(self, topic: str) -> bool:
        return topic in self.registered_topics

    def _register(self, message: AbstractMessage) -> None:
        self.registered_messages.append(message)

    def register_messages(self, message_list: List[AbstractMessage]) \
            -> None:
        for message in message_list:
            self._register(message)

    def unregister_message(self, message: AbstractMessage) -> None:
        self.registered_messages.remove(message)

    def unregister_message_by_topic(self, topic: str):
        for message in self.registered_messages:
            if message.topic == topic:
                self.unregister_message(message)

    def check_is_topic_registered(self, topic_name):
        if not self.is_topic_registered(topic_name):
            error_message = f'Topic {topic_name} not registered in {self}'
            raise ValueError(error_message)

    def get_messages_by_topic(self, topic_name: str) -> List[AbstractMessage]:
        self.check_is_topic_registered(topic_name)
        messages = [message for message in self.registered_messages
                    if message.topic == topic_name]

        return messages

    def get_message_by_topic(self, topic_name: str) -> AbstractMessage:
        messages = self.get_messages_by_topic(topic_name)

        return messages[0]

    def receive(self, message: mqtt.MQTTMessage):
        raise NotImplementedError
