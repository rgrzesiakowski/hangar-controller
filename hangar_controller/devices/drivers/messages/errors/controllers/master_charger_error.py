from ..error_message import ErrorMessage
from ...message_utils import error_topic
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers


class MasterChargerErrorMessage(ErrorMessage):
    def __init__(self):
        super().__init__(
            error_topic(Controllers.STM_MASTER_CHARGER),
            stm_communication_settings.ControllerErrorCodes.STM_MASTER_CHARGER)
