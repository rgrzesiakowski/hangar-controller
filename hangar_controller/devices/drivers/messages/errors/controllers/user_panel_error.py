from ..error_message import ErrorMessage
from ...message_utils import error_topic
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers

TOPIC = error_topic(Controllers.STM_USER_PANEL)


class UserPanelErrorMessage(ErrorMessage):
    def __init__(self):
        super().__init__(
            error_topic(Controllers.STM_USER_PANEL),
            stm_communication_settings.ControllerErrorCodes.STM_USER_PANEL)
