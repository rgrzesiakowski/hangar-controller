from typing import Optional


class ErrorCodes:
    def __init__(self, peripheral: Optional[str] = None,
                 command_control: Optional[str] = None,
                 telemetry: Optional[str] = None):
        self.peripheral = peripheral
        self.command_control = command_control
        self.telemetry = telemetry
