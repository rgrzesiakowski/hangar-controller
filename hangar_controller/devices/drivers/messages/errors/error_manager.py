import logging
import queue
import copy

import paho.mqtt.client as mqtt

from .controllers.cargo_error import CargoErrorMessage
from .controllers.climate_error import ClimateErrorMessage
from .controllers.master_charger_error import MasterChargerErrorMessage
from .controllers.pms_error import PMSErrorMessage
from .controllers.roof_error import RoofErrorMessage
from .controllers.user_panel_error import UserPanelErrorMessage
from .error_message import ErrorMessage, ErrorPayload
from ..abstract_message_manager import AbstractMessageManager

logger = logging.getLogger(__name__)


class ErrorManager(AbstractMessageManager):
    def __init__(self, error_queue: queue.Queue,
                 error_warning_log_queue: queue.Queue):
        super().__init__()
        self._error_queue = error_queue
        self._error_warning_log_queue = error_warning_log_queue
        self._init_error_messages()

    def _init_error_messages(self):
        error_list = [CargoErrorMessage(), ClimateErrorMessage(),
                      MasterChargerErrorMessage(), PMSErrorMessage(),
                      RoofErrorMessage(), UserPanelErrorMessage()]
        self.register_messages(error_list)

    def _decide_on_message(self, error: ErrorMessage, payload: str):
        parsed_payload = ErrorPayload()
        try:
            parsed_payload.parse(payload, error.error_code)
        except ValueError as e:
            logger.error(e)
            return
        error.parsed_payload = parsed_payload

        telemetry = error.is_telemetry(payload)
        peripheral = error.is_peripheral(payload)
        if telemetry or peripheral:
            self._error_queue.put(error)

    def receive(self, message: mqtt.MQTTMessage):
        error = self.get_message_by_topic(message.topic)
        error = copy.deepcopy(error)

        self._error_warning_log_queue.put(error)
        self._decide_on_message(error, message.payload.decode('utf-8'))

    def get_error(self):
        return self._error_queue.get_nowait()
