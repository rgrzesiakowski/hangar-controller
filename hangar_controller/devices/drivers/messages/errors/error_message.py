from typing import Tuple, Optional

from .error_code import ErrorCodes
from ..abstract_message import AbstractMessage


class ErrorPayload:
    def __init__(self):
        self.reset()

    def reset(self):
        self.is_initialized: bool = False
        self.is_peripheral: bool = False
        self.is_command_control: bool = False
        self.is_telemetry: bool = False
        self.code: Optional[str] = None
        self.subcode: Optional[int] = None
        self.bitflags: Optional[int] = None

    def parse(self, payload: str, error_code: ErrorCodes):
        self.reset()
        try:
            payload = self._parse_code(payload, error_code)
            self._parse_fields(payload)
        except ValueError as e:
            self.reset()
            raise e
        self.is_initialized = True

    def _parse_code(self, payload: str, error_code: ErrorCodes) -> str:
        if (error_code.peripheral is not None
                and payload.startswith(error_code.peripheral)):
            payload = payload[len(error_code.peripheral):]
            self.is_peripheral = True
            self.code = error_code.peripheral
        elif (error_code.command_control is not None
                  and payload.startswith(error_code.command_control)):
            payload = payload[len(error_code.command_control):]
            self.is_command_control = True
            self.code = error_code.command_control
        elif (error_code.telemetry is not None
                  and payload.startswith(error_code.telemetry)):
            payload = payload[len(error_code.telemetry):]
            self.is_telemetry = True
            self.code = error_code.telemetry
        else:
            raise ValueError(f'Error payload: {payload} has incorrect code')
        return payload

    def _parse_fields(self, payload: str) -> None:
        if (len(payload) == 0) or (not payload[0].isnumeric()):
            raise ValueError(f'Error payload: {self.code}{payload} '
                             f'does not have subcode')
        splits = payload.split(',')
        if len(splits) > 2:
            raise ValueError(f'Too many values in error payload: {payload}')
        else:
            try:
                self.subcode = int(splits[0])
                if len(splits) == 1:
                    self.bitflags = 0
                else:
                    self.bitflags = int(splits[1])
            except TypeError:
                raise ValueError(f'Error payload: {payload} cannot be cast '
                                 'into list of ints')


class ErrorMessage(AbstractMessage):
    def __init__(self, topic: str, error_code: ErrorCodes):
        super().__init__(topic)
        self.error_code = error_code
        self.parsed_payload = ErrorPayload()

    def is_telemetry(self, payload: str):
        telemetry = self.error_code.telemetry \
                    and self.error_code.telemetry in payload

        return telemetry

    def is_peripheral(self, payload: str):
        peripheral = self.error_code.peripheral \
                     and self.error_code.peripheral in payload

        return peripheral
