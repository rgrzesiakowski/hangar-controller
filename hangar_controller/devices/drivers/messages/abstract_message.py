class AbstractMessage:
    def __init__(self, topic: str):
        self.topic = topic

    def __eq__(self, other):
        return self.topic == other.topic
