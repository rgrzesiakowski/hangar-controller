from typing import List, Union

from ..responses.expected_messages.expected_message import ExpectedMessage
from ..responses import Response
from settingsd.drivers.stm_communication_settings import Common, \
    Controllers, SubControllers, CommandMessage


def create_response_list(topics: List[str], timeouts: List[float],
                         expected_messages: List[ExpectedMessage]) \
        -> List[Response]:
    assert (len(topics) == len(timeouts) == len(expected_messages))

    responses = []
    for topic, timeout, expected_message in zip(topics, timeouts,
                                                expected_messages):
        responses.append(Response(topic, timeout, expected_message))

    return responses


def command_topic(stm_topic: str, cmd_name: str):
    return f'{stm_topic}/{Common.COMMAND}/{cmd_name}'


def sub_controller_topic(topic: str, sub_controller: str):
    slash_position = topic.index('/')
    assert (slash_position > 0)
    topic = topic[:slash_position] + f'/{sub_controller}' + \
            topic[slash_position:]

    return topic


def call_topic(topic: str):
    return f'{topic}/{Common.CALL}'


def response_topic(topic: str):
    return f'{topic}/{Common.RESP}'


def value_topic(topic: str):
    return f'{topic}/{Common.VAL}'


def set_topic(topic: str):
    return f'{topic}/{Common.SET}'


def get_topic(topic: str):
    return f'{topic}/{CommandMessage.GET}'


def read_topic(topic: str):
    return f'{topic}/{Common.READ}'


def pin_topic(topic: str):
    return f'{topic}/{Common.PIN}'


def state_change_topic(stm_topic: str, state_name: str):
    return f'{stm_topic}/{Common.STATE}/{state_name}'


def parameter_topic(stm_topic: str, quantity: str, parameter_name: str):
    return f'{stm_topic}/{SubControllers.CLIMATE}/{Common.PARAMETERS}/' \
           f'{quantity}/{parameter_name}'


def climate_measurement(quantity: str, parameter_name: str):
    return f'{Controllers.STM_CLIMATE}/{SubControllers.CLIMATE}/' \
           f'{Common.TELEMETRY}/{quantity}/{parameter_name}/{Common.READ}'


def battery_measurement(
        charger_id: str,
        sub_controller: Union[SubControllers.BATTERY, SubControllers.CHARGER],
        parameter_name: str
        ):
    return f'{Controllers.STM_MASTER_CHARGER}/{charger_id}/{Common.TELEMETRY}/' \
           f'{sub_controller}/{parameter_name}/{Common.READ}'


def heartbeat_topic(stm_topic: str):
    return f'{stm_topic}/{Common.HEARTBEAT}'


def error_topic(stm_topic: str):
    return f'{stm_topic}/{Common.DIAGNOSTICS}/{Common.ERRORS}'


def warning_topic(stm_topic: str):
    return f'{stm_topic}/{Common.DIAGNOSTICS}/{Common.WARNINGS}'


def plc_io_topic(stm_topic: str, inputs: bool):
    command = Common.INPUT if inputs else Common.OUTPUT

    return f'{stm_topic}/{SubControllers.PLC}/{command}/{Common.READ}'


def read_state_topic(state_topic: str):
    return f'{Controllers.WOBIT}/{state_topic}'


def temperature_payload(value: float):
    return f'{value}'


def humidity_payload(value: float):
    return f'{value}'
