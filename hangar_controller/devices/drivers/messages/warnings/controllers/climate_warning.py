from ..warning_message import WarningMessage
from ...message_utils import warning_topic
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers


class ClimateWarningMessage(WarningMessage):
    def __init__(self):
        super().__init__(
            warning_topic(Controllers.STM_CLIMATE),
            stm_communication_settings.ControllerWarningCodes.STM_CLIMATE)
