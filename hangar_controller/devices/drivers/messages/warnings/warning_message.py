from hangar_controller.devices.drivers.messages.abstract_message import AbstractMessage
from hangar_controller.devices.drivers.messages.warnings.warning_code import WarningCodes


class WarningMessage(AbstractMessage):
    def __init__(self, topic: str, warning_code: WarningCodes):
        super().__init__(topic)
        self.warning_code = warning_code
