import queue

import paho.mqtt.client as mqtt

from hangar_controller.devices.drivers.messages.abstract_message_manager import AbstractMessageManager
from hangar_controller.devices.drivers.messages.warnings.controllers.climate_warning import ClimateWarningMessage
from hangar_controller.devices.drivers.messages.warnings.controllers.master_charger_warning import MasterChargerWarningMessage
from hangar_controller.devices.drivers.messages.warnings.controllers.pms_warning import PMSWarningMessage
from hangar_controller.devices.drivers.messages.warnings.controllers.roof_warning import RoofWarningMessage
from hangar_controller.devices.drivers.messages.warnings.controllers.user_panel_warning import UserPanelWarningMessage
from hangar_controller.devices.drivers.messages.warnings.warning_message import WarningMessage


class WarningManager(AbstractMessageManager):
    def __init__(self, error_warning_log_queue: queue.Queue):
        super().__init__()
        self._error_warning_log_queue = error_warning_log_queue
        self._init_warning_messages()

    def _init_warning_messages(self):
        warning_list = [ClimateWarningMessage(), MasterChargerWarningMessage(),
                        PMSWarningMessage(), RoofWarningMessage(),
                        UserPanelWarningMessage()]
        self.register_messages(warning_list)

    def receive(self, message: mqtt.MQTTMessage):
        warning = self.get_message_by_topic(message.topic)

        assert (isinstance(warning, WarningMessage))
        self._error_warning_log_queue.put(warning)
