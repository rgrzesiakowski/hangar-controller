from .expected_message import ExpectedMessage
from settingsd.drivers.stm_communication_settings import ResponseMessage


class PLCIOResponse(ExpectedMessage):
    def __init__(self):
        super().__init__(positive_messages=None)

    def evaluate_message(self, message: str):
        self.evaluation = True

        if not self.check_message_expected(message):
            self.evaluation = False
            self.evaluation_reason = ResponseMessage.UNEXPECTED_RESPONSE

    def check_message_expected(self, message: str) -> bool:
        inputs_length = 8
        inputs = message.split(' | ')

        correct_length = len(inputs) == inputs_length
        all_bin = all([i in ['0', '1'] for i in inputs])

        expected = correct_length and all_bin

        return expected
