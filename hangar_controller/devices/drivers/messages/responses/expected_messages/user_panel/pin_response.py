from ..expected_message import ExpectedMessage
from settingsd.drivers.stm_communication_settings import ResponseMessage
from settingsd.drivers.stm_user_panel_settings import UserPanelControlCommands


class PINResponse(ExpectedMessage):
    def __init__(self):
        super().__init__(positive_messages=None)

    def evaluate_message(self, message: str):
        self.evaluation = True

        if self.check_message_timeout(message):
            self.evaluation = False
            self.evaluation_reason = ResponseMessage.TIMEOUT
        elif not self.check_message_correct(message):
            self.evaluation = False
            self.evaluation_reason = ResponseMessage.UNEXPECTED_RESPONSE

    def check_message_expected(self, message: str) -> bool:
        timeout = self.check_message_timeout(message)
        positive = self.check_message_positive(message)

        expected = timeout or positive

        return expected

    def check_message_timeout(self, message: str) -> bool:
        if message == UserPanelControlCommands.PIN_TIMEOUT:
            return True

        return False

    def check_message_correct(self, message: str) -> bool:
        inputs_length = 6
        inputs = message.split(',')

        correct_length = len(inputs) == inputs_length
        all_digits = all([i.isdigit() for i in inputs])
        positive = correct_length and all_digits

        return positive
