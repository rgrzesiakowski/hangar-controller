from typing import Optional

from ..expected_message import ExpectedMessage
from settingsd.drivers.stm_user_panel_settings import UserPanelViews, \
    ConfirmationViews


class ViewResponse(ExpectedMessage):
    VIEWS = [value for view in (UserPanelViews, ConfirmationViews)
             for key, value in view.__dict__.items() if not key.startswith('__')]

    def __init__(self, positive_messages: Optional[UserPanelViews]):
        super().__init__(expected_messages=ViewResponse.VIEWS,
                         positive_messages=positive_messages)
