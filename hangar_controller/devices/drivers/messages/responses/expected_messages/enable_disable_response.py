from typing import Optional, Union

from .expected_message import ExpectedMessage
from settingsd.drivers.stm_communication_settings import CommandMessage


class EnableDisableResponse(ExpectedMessage):
    ENABLE = CommandMessage.ENABLE
    DISABLE = CommandMessage.DISABLE

    def __init__(self, positive_messages: Optional[Union[ENABLE, DISABLE]]):
        expected_messages = [EnableDisableResponse.ENABLE,
                             EnableDisableResponse.DISABLE]
        super().__init__(expected_messages=expected_messages,
                         positive_messages=positive_messages)
