from typing import Optional, Union

from ..expected_message import ExpectedMessage
from settingsd.drivers.stm_cargo_settings import CargoControlCommands


class CargoShiftStateResponse(ExpectedMessage):
    WORK = CargoControlCommands.WORK
    BASE = CargoControlCommands.BASE
    IDLE = CargoControlCommands.IDLE
    ERROR = CargoControlCommands.ERROR

    def __init__(self, positive_messages: Optional[Union[WORK, BASE, IDLE,
                                                         ERROR]]):
        expected_messages = [CargoControlCommands.WORK,
                             CargoControlCommands.BASE,
                             CargoControlCommands.IDLE,
                             CargoControlCommands.ERROR]
        super().__init__(expected_messages=expected_messages,
                         positive_messages=positive_messages)
