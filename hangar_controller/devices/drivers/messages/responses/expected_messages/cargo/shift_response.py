from typing import Optional, Union

from ..expected_message import ExpectedMessage
from settingsd.drivers.stm_cargo_settings import CargoControlCommands


class ShiftResponse(ExpectedMessage):
    WORK = CargoControlCommands.WORK
    BASE = CargoControlCommands.BASE
    IDLE = CargoControlCommands.IDLE

    def __init__(self, positive_messages: Optional[Union[WORK, BASE]]):
        super().__init__(positive_messages=positive_messages)
