from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import \
    ExpectedMessage
from settingsd.drivers.stm_cargo_settings import CargoPresenceStates


class CargoPresenceResponse(ExpectedMessage):
    PRESENT = CargoPresenceStates.PRESENT
    INCORRECT = CargoPresenceStates.INCORRECT
    ABSENT = CargoPresenceStates.ABSENT

    def __init__(self):
        _expected_messages = [CargoPresenceResponse.PRESENT,
                              CargoPresenceResponse.INCORRECT,
                              CargoPresenceResponse.ABSENT]
        super().__init__(positive_messages=CargoPresenceResponse.PRESENT,
                         expected_messages=_expected_messages)
