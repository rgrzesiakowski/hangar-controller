from typing import Optional, Union

from ..expected_message import ExpectedMessage
from settingsd.drivers.stm_climate_settings import ClimateControlCommands


class AutoManualResponse(ExpectedMessage):
    AUTO = ClimateControlCommands.AUTO
    MANUAL = ClimateControlCommands.MANUAL

    def __init__(self, positive_messages: Optional[Union[AUTO, MANUAL]]):
        expected_messages = [AutoManualResponse.AUTO,
                             AutoManualResponse.MANUAL]
        super().__init__(expected_messages=expected_messages,
                         positive_messages=positive_messages)
