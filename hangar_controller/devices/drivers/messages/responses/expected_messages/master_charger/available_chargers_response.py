from ..expected_message import ExpectedMessage


class AvailableChargersResponse(ExpectedMessage):
    def __init__(self):
        super().__init__(expected_messages=None, positive_messages=None)

    def check_message_positive(self, message: str) -> bool:
        return all(message.split(','))
