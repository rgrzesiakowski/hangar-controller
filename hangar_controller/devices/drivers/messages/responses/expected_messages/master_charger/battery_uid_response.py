from ..expected_message import ExpectedMessage


class BatteryUIDResponse(ExpectedMessage):
    def __init__(self):
        super().__init__(expected_messages=None, positive_messages=None)
