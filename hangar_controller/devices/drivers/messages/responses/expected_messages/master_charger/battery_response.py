from ..expected_message import ExpectedMessage
from settingsd.drivers.stm_master_charger_settings import \
    MasterChargerStates


class BatteryResponse(ExpectedMessage):
    CHARGING = MasterChargerStates.CHARGING
    CHARGED = MasterChargerStates.CHARGED
    UNCHARGED = MasterChargerStates.UNCHARGED
    UNPLUGGED = MasterChargerStates.UNPLUGGED

    def __init__(self, positive_messages):
        expected_messages = [BatteryResponse.CHARGED,
                             BatteryResponse.CHARGING,
                             BatteryResponse.UNCHARGED,
                             BatteryResponse.UNPLUGGED]
        super().__init__(expected_messages=expected_messages,
                         positive_messages=positive_messages)
