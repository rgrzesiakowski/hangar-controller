from ...message_utils import response_topic
from .expected_message import ExpectedMessage
from ..response import Response
from settingsd.drivers.stm_communication_settings import ResponseMessage, \
    ACK_TIMEOUT


class Acknowledgment(ExpectedMessage):
    ACK = ResponseMessage.ACK
    WRONG_VALUE = ResponseMessage.WRONG_VALUE
    REFUSED = ResponseMessage.REFUSED

    def __init__(self):
        expected_messages = [Acknowledgment.ACK,
                             Acknowledgment.WRONG_VALUE,
                             Acknowledgment.REFUSED]
        super().__init__(expected_messages=expected_messages,
                         positive_messages=Acknowledgment.ACK)

    def check_message_expected(self, message: str) -> bool:
        """Checks that any of expected_messages in the message.

        Acknowledgment may be sent with number after ack, e.g. 'ack/24',
        suggesting that it's 24th message, hence we need to update evaluation.

        Args:
            message: message to check.

        Returns:
            True if message contains
                one or more of self.expected_messages,
                False otherwise.

        """
        expected = [exp_message in message
                    for exp_message in self.expected_messages]

        return any(expected)

    def check_message_positive(self, message: str) -> bool:
        """Checks that the positive_messages in the message.

        Acknowledgment may be sent with number after ack, e.g. 'ack/24',
        suggesting that it's 24th message, hence we need to update evaluation.

        Args:
            message: message to check.

        Returns:
            True if self.positive_messages in message,
                False otherwise.
        """
        positive = self.positive_messages in message

        return positive

    def evaluate_message(self, message: str):
        super().evaluate_message(message)

        if self.evaluation:
            return
        elif Acknowledgment.WRONG_VALUE in message:
            self.evaluation_reason = ResponseMessage.WRONG_VALUE
        elif Acknowledgment.REFUSED in message:
            self.evaluation_reason = ResponseMessage.REFUSED


def create_ack_response(topic: str):
    ack_response = Response(response_topic(topic), ACK_TIMEOUT,
                            Acknowledgment())

    return ack_response
