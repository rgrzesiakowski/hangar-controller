from .expected_message import ExpectedMessage
from settingsd.drivers.stm_communication_settings import ResponseMessage


class ListResponse(ExpectedMessage):
    def __init__(self):
        super().__init__(positive_messages=None)

    def evaluate_message(self, message: str):
        self.evaluation = True
        if not self.check_message_expected(message):
            self.evaluation = False
            self.evaluation_reason = ResponseMessage.UNEXPECTED_RESPONSE

    def check_message_expected(self, message: str) -> bool:
        # TODO: Make sure that ListResponse can't have letters as payload
        for element in message.split(','):
            try:
                float(element)
            except ValueError:
                return False
        return True
