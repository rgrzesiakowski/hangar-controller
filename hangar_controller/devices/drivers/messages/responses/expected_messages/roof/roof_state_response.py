from typing import Optional, Union

from ..expected_message import ExpectedMessage
from settingsd.drivers.stm_roof_settings import RoofControlCommands


class RoofStateResponse(ExpectedMessage):
    OPEN = RoofControlCommands.OPEN
    CLOSE = RoofControlCommands.CLOSE
    IDLE = RoofControlCommands.IDLE
    ERROR = RoofControlCommands.ERROR

    def __init__(self, positive_messages: Optional[Union[OPEN, CLOSE, IDLE,
                                                         ERROR]]):
        expected_messages = [RoofStateResponse.OPEN,
                             RoofStateResponse.CLOSE,
                             RoofStateResponse.IDLE,
                             RoofStateResponse.ERROR]
        super().__init__(expected_messages=expected_messages,
                         positive_messages=positive_messages)
