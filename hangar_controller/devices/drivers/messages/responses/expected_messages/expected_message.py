from typing import List, Optional, Union

from settingsd.drivers.stm_communication_settings import ResponseMessage

MESSAGE_TYPE = Optional[Union[List[str], str]]


class ExpectedMessage:
    def __init__(self, positive_messages: MESSAGE_TYPE,
                 expected_messages: Optional[MESSAGE_TYPE] = None):
        self.positive_messages = positive_messages
        self.expected_messages = expected_messages if expected_messages else positive_messages
        self.evaluation: Optional[bool] = None
        self.evaluation_reason: Optional[str] = None

    def __repr__(self):
        expected = (f'expected_messages={self.expected_messages}'
                    if isinstance(self.expected_messages, list)
                    else f'expected_messages=\"{self.expected_messages}\"')
        positive = (f'positive_messages={self.positive_messages}'
                    if isinstance(self.positive_messages, list)
                    else f'positive_messages=\"{self.positive_messages}\"')
        class_name = self.__class__.__name__

        return f'<{class_name} {expected}; {positive}>'

    def check_message_expected(self, message: str) -> bool:
        if self.expected_messages is None:
            return True
        if isinstance(self.expected_messages, list):
            expected = message in self.expected_messages
        else:
            expected = message == self.expected_messages

        return expected

    def check_message_positive(self, message: str) -> bool:
        if self.positive_messages is None:
            return True
        elif isinstance(self.positive_messages, list):
            positive = message in self.positive_messages
        else:
            positive = message == self.positive_messages

        return positive

    def check_evaluation(self, message: str):
        evaluation = self.check_message_expected(message) \
                     and self.check_message_positive(message)

        return evaluation

    def evaluate_message(self, message: str):
        self.evaluation = self.check_evaluation(message)

        if not self.check_message_expected(message):
            self.evaluation_reason = ResponseMessage.UNEXPECTED_RESPONSE
        elif not self.check_message_positive(message):
            self.evaluation_reason = ResponseMessage.NEGATIVE_RESPONSE
