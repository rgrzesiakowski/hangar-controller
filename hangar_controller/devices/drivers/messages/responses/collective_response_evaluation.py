from typing import List

from .response_evaluation import ResponseEvaluation


class CollectiveResponseEvaluation(ResponseEvaluation):
    def __init__(self, topic, payload):
        super().__init__(True, topic, payload)
        self.responses: List[ResponseEvaluation] = []

    def __repr__(self):
        super_repr = super().__repr__().split(' ')
        super_repr[0] = f'<{self.__class__.__name__}'  # Replace class name
        super_repr[-1] = super_repr[-1][:-1]  # Remove closing bracket
        super_repr.append(f'responses={self.responses}>')

        return ' '.join(super_repr)

    def add_response(self, response: ResponseEvaluation):
        self.responses.append(response)

    def add_and_evaluate_response(self, response: ResponseEvaluation):
        self.add_response(response)
        if not self.evaluation or response.evaluation:
            return

        self.update_evaluation(response.evaluation)
        self.update_evaluation_reason(response.evaluation_reason)

    def get_response_by_topic(self, topic) -> ResponseEvaluation:
        for response in self.responses:
            if response.topic == topic:
                return response

        raise ValueError(f'In collective response evaluation: {self}, there '
                         f'was no response evaluation with topic: {topic}')
