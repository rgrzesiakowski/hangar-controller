from concurrent.futures import Future, ThreadPoolExecutor
from typing import List

from ..commands import AbstractCommand
from .collective_response_evaluation import CollectiveResponseEvaluation
from .response_evaluation import ResponseEvaluation


class FutureResponse(Future):
    """Class responsible for waiting for responses, receiving them,
    evaluating and stopping them if timeout appears.
    """
    def __init__(self, executor: ThreadPoolExecutor, command: AbstractCommand):
        super().__init__()
        self.executor = executor
        self.responses = command.responses
        self.futures = self.init_futures()
        self.amount_of_responses = len(self.responses)
        self.collective_response = CollectiveResponseEvaluation(command.topic,
                                                                command.payload)
        self.stopped = False

    def init_futures(self) -> List[Future]:
        """Create future for each pending response.

        Add callback when response is received.

        Returns:
            List of future responses.
        """
        futures = []
        for response in self.responses:
            future = self.executor.submit(response.wait_for_message)
            future.add_done_callback(self.receive)
            futures.append(future)

        return futures

    def _evaluate_response(self, response_result: ResponseEvaluation) -> None:
        """Add received response into collective response evaluation.

        Each response can change evaluation of the final result.

        Args:
            response_result: evaluation of received message.

        Returns:
            None.
        """
        self.collective_response.add_and_evaluate_response(response_result)

    def cancel_responses(self) -> None:
        """Cancel all pending responses.

        Set Evaluation of responses to False and set evaluation reason
        to cancelled.

        Returns:
            None.
        """
        for response in self.responses:
            response.cancel()

    def stop(self) -> None:
        """Stop waiting for responses.

        Stop waiting for responses in case, that some other response
        returned with False evaluation. Cancel all other.

        Returns:
            None.
        """
        if self.stopped:
            return

        self.stopped = True
        self.cancel_responses()

    def receive(self, future: Future):
        """Receive message that was in responses list.

        Args:
            future: Future object that contains response.

        Returns:
            None.
        """
        self.futures.remove(future)

        response_result: ResponseEvaluation = future.result()

        if not self.stopped:
            self._evaluate_response(response_result)
        else:
            self.collective_response.add_response(response_result)

        if not self.collective_response.evaluation and not self.stopped:
            self.stop()

        if len(self.futures) == 0:
            self.set_result(self.collective_response)

    def evaluate(self):
        if self.collective_response.evaluation:
            return True

        return False
