from typing import Optional


class ResponseEvaluation:
    def __init__(self, evaluation: bool, topic: str, payload: str):
        self.evaluation: bool = evaluation
        self.topic: str = topic
        self.payload: str = payload
        self.evaluation_reason: Optional[str] = None

    def __repr__(self):
        topic = f'topic=\"{self.topic}\"'
        payload = f'payload=\"{self.payload}\"'
        evaluation = f'evaluation={self.evaluation}'
        evaluation_reason = ('' if self.evaluation else
                             f' evaluation_reason=\"{self.evaluation_reason}\"')
        class_name = self.__class__.__name__

        return f'<{class_name} {topic}; {payload}; {evaluation}; ' \
               f'{evaluation_reason}>'

    def update_evaluation(self, evaluation: bool):
        self.evaluation = evaluation

    def update_evaluation_reason(self, evaluation_reason: str):
        self.evaluation_reason = evaluation_reason
