from threading import Lock


class ResponseTimer:
    def __init__(self, timeout: float):
        super().__init__()
        self.timeout = timeout
        self.lock = Lock()

    def stop(self):
        if self.lock.locked():
            self.lock.release()

    def wait(self) -> bool:
        self.lock.acquire()
        acquired = self.lock.acquire(timeout=self.timeout)

        try:
            self.lock.release()
        except RuntimeError:
            # functional tests race condition sometimes releasing lock twice
            pass

        return acquired
