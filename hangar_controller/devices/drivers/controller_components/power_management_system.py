import logging

from ..messages.message_utils import pin_topic
from ..messages.commands.power_management_system import *
from ..messages.commands.commands_manager import CommandsManager
from ..messages.responses.collective_response_evaluation import \
    CollectiveResponseEvaluation
from settingsd.drivers.stm_pms_settings import PMSComponent
from ..messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse

logger = logging.getLogger(__name__)


class PowerManagementSystem:
    def __init__(self, commands_manager: CommandsManager):
        self._commands_manager = commands_manager

        self._commands = {
            PMSComponent.TRAFO: PMSSetChargersPower,
            PMSComponent.POSITIONING_MOTORS: PMSSetMotorsPosPower,
            PMSComponent.MANIPULATOR_MOTORS: PMSSetMotorsManPower,
            PMSComponent.LIFT_MOTORS: PMSSetMotorsLiftPower,
            PMSComponent.PRESSURE: PMSSetPressurePower,
            PMSComponent.AIR_COMPRESSOR: PMSSetAirCompressorPower,
            PMSComponent.IR_LOCK: PMSSetIRLockPower
        }

    def set_component_power(self, component: PMSComponent, enable: bool
                            ) -> CollectiveResponseEvaluation:
        """Sends message to STM PMS controller to enable/disable
        component's power.

        Args:
            component: Single PMS component.
            enable: True if component's power should be enabled,
                False otherwise.

        Returns:
            Collective evaluation of responses.

        """
        power = (EnableDisableResponse.ENABLE if enable else
                 EnableDisableResponse.DISABLE)
        logger.info('[POWER_MANAGEMENT_SYSTEM] [START] '
                    f'Setting {component.value} power - {power}')
        command = self._commands[component](power)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[POWER_MANAGEMENT_SYSTEM] [END] '
                    f'Setting {component.value} power - {power}')
        return response

    def get_states(self) -> CollectiveResponseEvaluation:
        """Sends message to STM PMS to get states.

        Lock time = stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[POWER_MANAGEMENT_SYSTEM] [START] Getting states')
        command = PMSGetStates()
        states = self._commands_manager.send_command_and_wait(command)
        logger.info('[POWER_MANAGEMENT_SYSTEM] [END] Getting states')

        return states

    def get_plc_io(self) -> CollectiveResponseEvaluation:
        """Sends message to STM PMS to get PLC inputs and outputs.

        Lock time = stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[POWER_MANAGEMENT_SYSTEM] [START] Getting PLC I/O')
        command = PMSGetPLCIO()
        plc_io = self._commands_manager.send_command_and_wait(command)
        logger.info('[POWER_MANAGEMENT_SYSTEM] [END] Getting PLC I/O')

        return plc_io

    def reset_plc(self) -> CollectiveResponseEvaluation:
        """Sends message to STM PMS to reset PLC.

        Lock time = stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[POWER_MANAGEMENT_SYSTEM] [START] Resetting PLC')
        command = PMSResetPLC()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[POWER_MANAGEMENT_SYSTEM] [END] Resetting PLC')

        return response

    def reset_controller(self) -> CollectiveResponseEvaluation:
        """Reset controller using pin request, then sending reset
        with pin received.

        Lock time: stm_communication_settings.ACK_TIMEOUT
                   + stm_communication_settings.PIN_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[POWER_MANAGEMENT_SYSTEM] [START] Resetting controller')
        response = self._commands_manager.send_command_and_wait(
            PMSResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(PMSResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = PMSResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[POWER_MANAGEMENT_SYSTEM] [END] Resetting controller')

        return response
