import logging
from typing import Tuple

from ..messages.message_utils import pin_topic
from ..messages.commands.user_panel import *
from ..messages.commands.commands_manager import CommandsManager
from ..messages.responses.collective_response_evaluation import \
    CollectiveResponseEvaluation
from settingsd.drivers.stm_user_panel_settings import UserPanelViews, \
    ConfirmationViews

logger = logging.getLogger(__name__)


class UserPanel:
    def __init__(self, commands_manager: CommandsManager):
        self._commands_manager = commands_manager

    def get_pin(self) -> Tuple[CollectiveResponseEvaluation, str]:
        """Sends message to STM User Panel to change view of the
        screen and ask user for pin.

        This panel has a timeout.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        UserPanelTimeouts.AUTH_PIN).

        Returns:
            evaluation of responses.
        """
        logger.info('[USER_PANEL] [START] Getting PIN')
        command = UserPanelGetPin()
        response = self._commands_manager.send_command_and_wait(command)
        pin_response = response.get_response_by_topic(
            UserPanelGetPin.PIN_STATE)
        pin = pin_response.payload
        logger.info('[USER_PANEL] [END] Getting PIN')

        return response, pin

    def set_confirmation_view(self, view: ConfirmationViews) \
            -> CollectiveResponseEvaluation:
        """Sends message to STM User Panel to change view of the screen to
        specified one.

        View has a OK button that user needs to press in order to unlock
        this function.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        timeout_from_view),
        where timeout_from_view is one of timeouts based on the
        confirmation view that is set.

        Args:
            view: specific view to change.
                Select one from ConfirmationViews.

        Returns:
            evaluation of responses.
        """
        logger.info(f'[USER_PANEL] [START] Setting confirmation view ({view})')
        command = UserPanelSetConfirmationView(view)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info(f'[USER_PANEL] [END] Setting confirmation view ({view})')

        return response

    def set_view(self, view: UserPanelViews) -> CollectiveResponseEvaluation:
        """Sends message to STM User Panel to change view of the screen
        to specified one.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        UserPanelTimeouts.VIEW).

        Args:
            view: specific view to change.
                Select one from UserPanelViews.

        Returns:
            evaluation of responses.
        """
        logger.info(f'[USER_PANEL] [START] Setting view ({view})')
        command = UserPanelSetView(view)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info(f'[USER_PANEL] [END] Setting view ({view})')

        return response

    def reset_controller(self) -> CollectiveResponseEvaluation:
        """Reset controller using pin request, then sending reset
        with pin received.

        Lock time: stm_communication_settings.ACK_TIMEOUT
                   + stm_communication_settings.PIN_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[USER_PANEL] [START] Resetting controller')
        response = self._commands_manager.send_command_and_wait(
            UserPanelResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(UserPanelResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = UserPanelResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[USER_PANEL] [END] Resetting controller')

        return response

    def get_states(self) -> CollectiveResponseEvaluation:
        """Get current states (view) of user panel.

        Returns:
             evaluation of responses.
        """
        logger.info('[USER_PANEL] [START] Getting state')
        command = UserPanelGetStates()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[USER_PANEL] [END] Getting state')

        return response
