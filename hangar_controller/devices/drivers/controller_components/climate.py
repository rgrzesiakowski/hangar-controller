import logging

from ..messages.message_utils import pin_topic
from ..messages.commands.climate import *
from ..messages.commands.commands_manager import CommandsManager
from ..messages.responses.collective_response_evaluation import \
    CollectiveResponseEvaluation

logger = logging.getLogger(__name__)


class Climate:
    def __init__(self, commands_manager: CommandsManager):
        self._commands_manager = commands_manager

    def get_parameters(self) -> CollectiveResponseEvaluation:
        """Get parameters from Climate STM controller.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Getting parameters')
        command = ClimateGetParameters()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Getting parameters')

        return response

    def get_states(self) -> CollectiveResponseEvaluation:
        """Get states from Climate STM controller.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Getting state')
        command = ClimateGetStates()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Getting state')

        return response

    def get_plc_io(self) -> CollectiveResponseEvaluation:
        """Get plc_io values from Climate STM controller.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Getting PLC I/O')
        command = ClimateGetPLCIO()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Getting PLC I/O')

        return response

    def get_telemetry(self) -> CollectiveResponseEvaluation:
        """Get telemetry data from climate STM controller.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Getting telemetry')
        command = ClimateGetTelemetry()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Getting telemetry')

        return response

    def reset_plc(self):
        """Reset PLC without pin.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Resetting PLC')
        command = ClimateResetPLC()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Resetting PLC')

        return response

    def reset_controller(self) -> CollectiveResponseEvaluation:
        """Reset controller using pin request.

        After that send reset with pin received.

        Lock time: stm_communication_settings.ACK_TIMEOUT
                   + stm_communication_settings.PIN_TIMEOUT

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Resetting controller')
        response = self._commands_manager.send_command_and_wait(
            ClimateResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(ClimateResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = ClimateResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Resetting controller')

        return response

    def set_ac(self, enable: bool) -> CollectiveResponseEvaluation:
        """Set AC temperature to value specified.

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                       ClimateControlTimeouts.AC).

        Args:
            enable: enable if True, disable if False.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Setting up AC')
        enable_command = SetAC.ENABLE if enable else SetAC.DISABLE
        command = SetAC(enable_command)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Setting up AC')

        return response

    def set_fan(self, on: bool) -> CollectiveResponseEvaluation:
        """Set Fan to ON/OFF.

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                       ClimateControlTimeouts.FAN).

        Args:
            on: ON if True, OFF if False.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Setting up fan')
        message = SetFan.ENABLE if on else SetFan.DISABLE
        command = SetFan(message)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Setting up fan')

        return response

    def set_hangar_heater(self, on: bool) -> CollectiveResponseEvaluation:
        """Set hangar heater ON/OFF.

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                       ClimateControlTimeouts.HANGAR_HEATER).

        Args:
            on: ON if True, OFF if False.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Setting up hangar heater')
        message = SetHangarHeater.ENABLE if on else SetHangarHeater.DISABLE
        command = SetHangarHeater(message)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Setting up hangar heater')

        return response

    def set_hangar_heater_hysteresis(self, temperature: float) \
            -> CollectiveResponseEvaluation:
        """Set hangar heater hysteresis to temperature value in Kelvins.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Args:
            temperature: temperature in Kelvin.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Setting up hangar heater hysteresis')
        command = SetHangarHeaterHysteresis(temperature)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Setting up hangar heater hysteresis')

        return response

    def set_hangar_heater_temperature(self, temperature: float) \
            -> CollectiveResponseEvaluation:
        """Sets hangar heater temperature set point.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Args:
            temperature: temperature in Kelvin.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Setting up hangar heater temperature')
        command = SetHangarHeaterTemperature(temperature)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Setting up hangar heater temperature')

        return response

    def set_hangar_humidity_hysteresis(self, temperature: float) \
            -> CollectiveResponseEvaluation:
        """Sets hangar humidity hysteresis.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Args:
            temperature: temperature in Kelvin.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Setting up hangar humidity hysteresis')
        command = SetHangarHumidityHysteresis(temperature)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Setting up hangar humidity hysteresis')

        return response

    def set_hangar_humidity_set_point(self, humidity: float) \
            -> CollectiveResponseEvaluation:
        """Sets hangar humidifier set point.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Args:
            humidity: humidity set point in %.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Setting up hangar humidity set point')
        command = SetHangarHumiditySetPoint(humidity)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Setting up hangar humidity set point')

        return response

    def set_roof_heater(self, on: bool) -> CollectiveResponseEvaluation:
        """Sets roof heater ON/OFF.

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                       ClimateControlTimeouts.ROOF_HEATER).

        Args:
            on: ON if True, OFF if False.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Setting up roof heater')
        value = SetRoofHeater.ENABLE if on else SetRoofHeater.DISABLE
        command = SetRoofHeater(value)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Setting up roof heater')

        return response

    def set_roof_heater_hysteresis(self, temperature: float) \
            -> CollectiveResponseEvaluation:
        """Sets roof heater hysteresis temperature.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Args:
            temperature: temperature in Kelvin.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Setting up roof heater hysteresis')
        command = SetRoofHeaterHysteresis(temperature)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Setting up roof heater hysteresis')

        return response

    def set_roof_heater_temperature(self, temperature: float) \
            -> CollectiveResponseEvaluation:
        """Sets roof heater temperature set point.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Args:
            temperature: temperature in Kelvin.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Setting up roof heater temperature')
        command = SetRoofHeaterTemperature(temperature)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Setting up roof heater temperature')

        return response

    def set_type_climate_controller(self, auto: bool) \
            -> CollectiveResponseEvaluation:
        """Sets type of climate controller AUTO/MANUAL.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Args:
            auto: AUTO if True, MANUAL if False.

        Returns:
            evaluation of responses.
        """
        logger.info('[CLIMATE] [START] Setting up climate controller type')
        value = SetTypeClimateController.AUTO \
            if auto else SetTypeClimateController.MANUAL
        command = SetTypeClimateController(value)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CLIMATE] [END] Setting up climate controller type')

        return response
