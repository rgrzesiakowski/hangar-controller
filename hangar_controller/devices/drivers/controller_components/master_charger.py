import logging
from typing import Tuple, Dict, Union, List, Optional

from ..messages.commands.master_charger.master_charger_get_available_chargers import \
    MasterChargerGetAvailableChargers
from ..messages.message_utils import pin_topic
from ..messages.commands.master_charger import *
from ..messages.commands.commands_manager import CommandsManager
from ..messages.responses.collective_response_evaluation import \
    CollectiveResponseEvaluation

logger = logging.getLogger(__name__)

ChargerTelemetry = Dict[str, Dict[str, Union[str, list]]]


class MasterCharger:
    def __init__(self, commands_manager: CommandsManager):
        self._commands_manager = commands_manager

    def get_available_chargers(self) -> Tuple[CollectiveResponseEvaluation,
                                              List[str]]:
        """Send message to STM Master Charger to get list of available
        chargers.


        Returns:
            Tuple of evaluation of responses and list of charger ids
                that are available in hangar

        """
        command = MasterChargerGetAvailableChargers()
        response = self._commands_manager.send_command_and_wait(command)

        if not response.evaluation:
            return response, []

        chargers_response = response.get_response_by_topic(
            MasterChargerGetAvailableChargers.STATE_TOPIC)
        available_chargers = chargers_response.payload.split(',')

        return response, available_chargers

    def set_charger_power(self,
                          charger_id: str,
                          enabled: bool) -> CollectiveResponseEvaluation:
        """Send message to STM Master Charger to set charger power ON/OFF.

        STM Master Charger is specified by charger_id.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        MasterChargerTimeouts.POWER).

        Args:
            charger_id: name of charger_id.
            enabled: ON if True, OFF if False.

        Returns:
            evaluation of responses.
        """
        logger.info('[MASTER_CHARGER] [START] Setting power')
        value = MasterChargerSetPower.ENABLED if enabled \
            else MasterChargerSetPower.DISABLED
        command = MasterChargerSetPower(charger_id, value)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[MASTER_CHARGER] [END] Setting power')

        return response

    def get_states(self, charger_id: str) -> Tuple[CollectiveResponseEvaluation,
                                                   Optional[Dict[str, str]]]:
        """Reads states from STM Master Charger.

        Lock time = stm_communication_settings.MULTIPLE_RESPONSE_TIMEOUT.

        Args:
            charger_id: string id of charger

        Returns:
            Tuple of evaluation of responses and states values for
            specified charger. If states couldn't be fetched then
            states is None.

        """
        logger.info('[MASTER_CHARGER] [START] Getting state')
        command = MasterChargerGetStates(charger_id)
        collective_resp = self._commands_manager.send_command_and_wait(command)

        if not collective_resp.evaluation:
            return collective_resp, None

        response_power = collective_resp.get_response_by_topic(
            command.responses[1])
        response_battery = collective_resp.get_response_by_topic(
            command.responses[2])

        states = {'power': response_power.payload,
                  'battery': response_battery.payload}

        logger.info('[MASTER_CHARGER] [END] Getting state')
        return collective_resp, states

    def get_telemetry(self, charger_id: str
                      ) -> Tuple[CollectiveResponseEvaluation,
                                 Optional[ChargerTelemetry]]:
        """Read telemetry from STM Master Charger.

        Lock time = stm_communication_settings.MULTIPLE_RESPONSE_TIMEOUT.

        Args:
            charger_id: string id of charger.

        Returns:
            Tuple of evaluation of responses and telemetry values for
            specified charger. If telemetry couldn't be fetched then
            telemetry is None.

        """
        logger.info('[MASTER_CHARGER] [START] Getting telemetry')
        command = MasterChargerGetTelemetry(charger_id)
        collective_resp = self._commands_manager.send_command_and_wait(command)

        if not collective_resp.evaluation:
            return collective_resp, None

        battery_uid_payload = collective_resp.get_response_by_topic(
            command.responses[0]).payload  # battery uid
        battery_t_payload = collective_resp.get_response_by_topic(
            command.responses[1]).payload  # battery temperature
        battery_v_payload = collective_resp.get_response_by_topic(
            command.responses[2]).payload  # battery voltage
        charger_t_payload = collective_resp.get_response_by_topic(
            command.responses[3]).payload  # charger temperature
        charger_c_payload = collective_resp.get_response_by_topic(
            command.responses[4]).payload  # charger current

        telemetry = {
            'battery': {
                'uid': battery_uid_payload,
                'temperature': [int(i) for i in battery_t_payload.split(',')],
                'voltage': [float(i) for i in battery_v_payload.split(',')]
            },
            'charger': {
                'temperature': [int(i) for i in charger_t_payload.split(',')],
                'charge_current': [float(i) for i in charger_c_payload.split(',')]
            }
        }
        logger.info('[MASTER_CHARGER] [END] Getting telemetry')
        return collective_resp, telemetry

    def get_plc_io(self) -> CollectiveResponseEvaluation:
        """Read PLC inputs and outputs from STM Master Charger.

        Lock time = stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[MASTER_CHARGER] [START] Getting PLC I/O')
        command = MasterChargerGetPLCIO()
        plc_io = self._commands_manager.send_command_and_wait(command)
        logger.info('[MASTER_CHARGER] [END] Getting PLC I/O')

        return plc_io

    def reset_plc(self) -> CollectiveResponseEvaluation:
        """Reset PLC without pin.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[MASTER_CHARGER] [START] Resetting PLC')
        command = MasterChargerResetPLC()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[MASTER_CHARGER] [END] Resetting PLC')

        return response

    def reset_controller(self) -> CollectiveResponseEvaluation:
        """Reset controller using pin request, then sending reset
        with pin received.

        Lock time: stm_communication_settings.ACK_TIMEOUT
                   + stm_communication_settings.PIN_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[MASTER_CHARGER] [START] Resetting controller')
        response = self._commands_manager.send_command_and_wait(
            MasterChargerResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(MasterChargerResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = MasterChargerResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[MASTER_CHARGER] [END] Resetting controller')

        return response
