import logging

from ..messages.message_utils import pin_topic
from ..messages.commands.roof import *
from ..messages.commands.commands_manager import CommandsManager
from ..messages.responses.collective_response_evaluation import \
    CollectiveResponseEvaluation

logger = logging.getLogger(__name__)


class Roof:
    def __init__(self, commands_manager: CommandsManager):
        self._commands_manager = commands_manager

    def get_states(self) -> CollectiveResponseEvaluation:
        """Reads states from STM Roof controller.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[ROOF] [START] Getting states')
        command = RoofGetStates()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[ROOF] [END] Getting states')

        return response

    def get_plc_io(self) -> CollectiveResponseEvaluation:
        """Reads PLC inputs and outputs values.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[ROOF] [START] Getting PLC I/O')
        command = RoofGetPLCIO()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[ROOF] [END] Getting PLC I/O')

        return response

    def stop(self) -> CollectiveResponseEvaluation:
        """Stops roof if it was opening or closing.

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                       RoofControlTimeouts.ROOF_STOP).

        Returns:
            evaluation of responses.
        """
        logger.info('[ROOF] [START] Stopping')
        command = RoofStop()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[ROOF] [END] Stopping')

        return response

    def open(self) -> CollectiveResponseEvaluation:
        """Opens roof.

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                       RoofControlTimeouts.ROOF_OPEN).

        Returns:
            evaluation of responses.
        """
        logger.info('[ROOF] [START] Opening')
        command = RoofOpen()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[ROOF] [END] Opening')

        return response

    def close(self) -> CollectiveResponseEvaluation:
        """Closes roof.

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                       RoofControlTimeouts.ROOF_CLOSE).

        Returns:
            evaluation of responses.
        """
        logger.info('[ROOF] [START] Closing')
        command = RoofClose()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[ROOF] [END] Closing')

        return response

    def reset_plc(self) -> CollectiveResponseEvaluation:
        """Reset PLC without pin.

        Lock time: stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[ROOF] [START] Resetting PLC')
        command = RoofResetPLC()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[ROOF] [END] Resetting PLC')

        return response

    def reset_controller(self) -> CollectiveResponseEvaluation:
        """Reset controller using pin request.

        After that send reset with pin received.

        Lock time: stm_communication_settings.ACK_TIMEOUT
                   + stm_communication_settings.PIN_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[ROOF] [START] Resetting controller')
        response = self._commands_manager.send_command_and_wait(
            RoofResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(RoofResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = RoofResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[ROOF] [END] Resetting controller')

        return response
