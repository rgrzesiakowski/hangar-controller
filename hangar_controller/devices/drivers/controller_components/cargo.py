import logging
from typing import Tuple, Optional

from ..messages.message_utils import pin_topic
from ..messages.commands.cargo import *
from ..messages.commands.commands_manager import CommandsManager
from ..messages.responses.collective_response_evaluation import \
    CollectiveResponseEvaluation

logger = logging.getLogger(__name__)


class Cargo:
    def __init__(self, commands_manager: CommandsManager):
        self._commands_manager = commands_manager

    def open_window(self) -> CollectiveResponseEvaluation:
        """Sends message to STM Cargo to open window.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.WINDOW_OPEN).

        Returns:
            evaluation of responses.
        """
        logger.info('[CARGO] [START] Opening window')
        command = CargoOpenWindow()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CARGO] [END] Opening window')

        return response

    def close_window(self) -> CollectiveResponseEvaluation:
        """Sends message to STM Cargo to close window.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.WINDOW_CLOSE).

        Returns:
            evaluation of responses.
        """
        logger.info('[CARGO] [START] Closing window')
        command = CargoCloseWindow()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CARGO] [END] Closing window')

        return response

    def base_shift(self) -> CollectiveResponseEvaluation:
        """Sends message to STM Cargo to set shift to base position.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.SHIFT_BASE).

        Returns:
            evaluation of responses.
        """
        logger.info('[CARGO] [START] Shifting to base position')
        command = CargoBaseShift()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CARGO] [END] Shifting to base position')

        return response

    def base_shift_check_conditions(self) -> CollectiveResponseEvaluation:
        """Sends message to STM Cargo to set shift to base position.

        STM checks cargo weight and position before starting.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.SHIFT_BASE_CHECK_COND).

        Returns:
            evaluation of responses.
        """
        logger.info('[CARGO] [START] Checking conditions for '
                    'base position shift')
        command = CargoBaseShiftCheckConditions()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CARGO] [END] Checking conditions for base position shift')

        return response

    def work_shift(self) -> CollectiveResponseEvaluation:
        """Sends message to STM Cargo to set shift to work position.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.SHIFT_WORK).

        Returns:
            evaluation of responses.
        """
        logger.info('[CARGO] [START] Shifting to work position')
        command = CargoWorkShift()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CARGO] [END] Shifting to work position')

        return response

    def work_shift_check_conditions(self) -> CollectiveResponseEvaluation:
        """Sends message to STM Cargo to set shift to work position.

        STM checks cargo weight and position before starting.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.SHIFT_WORK_CHECK_COND).

        Returns:
            evaluation of responses.
        """
        logger.info('[CARGO] [START] Checking conditions for '
                    'work position shift')
        command = CargoWorkShiftCheckConditions()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CARGO] [END] Checking conditions for work position shift')

        return response

    def get_weight(self) -> Tuple[CollectiveResponseEvaluation, Optional[float]]:
        """Sends message to STM Cargo to get weight of cargo.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.CARGO_WEIGHT).

        Returns:
            evaluation of responses.
        """
        logger.info('[CARGO] [START] Getting weight')
        command = CargoGetWeight()
        response = self._commands_manager.send_command_and_wait(command)
        weight_response = response.get_response_by_topic(
            CargoGetWeight.STATE_CHANGE_TOPIC)
        weight = weight_response.payload

        if not response.evaluation:
            return response, None
        logger.info('[CARGO] [END] Getting weight')

        return response, float(weight)

    def get_presence(self) -> CollectiveResponseEvaluation:
        """Sends message to STM Cargo to get information if cargo is put
        in the cargo box.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.CARGO_PRESENCE).

        Returns:
            evaluation of responses.
            CollectiveResponseEvaluation.evaluation return True if cargo
            is present, False otherwise (can either be 'incorrect' or
            'absent')
        """
        logger.info('[CARGO] [START] Getting presence')
        command = CargoGetPresence()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CARGO] [END] Getting presence')
        return response

    def get_states(self) -> CollectiveResponseEvaluation:
        """Sends message to STM Cargo to get states.

        Lock time = stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[CARGO] [START] Getting states')
        command = CargoGetStates()
        states = self._commands_manager.send_command_and_wait(command)
        logger.info('[CARGO] [END] Getting states')

        return states

    def get_plc_io(self) -> CollectiveResponseEvaluation:
        """Sends message to STM Cargo to get plc inputs and outputs.

        Lock time = stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[CARGO] [START] Getting PLC I/O')
        command = CargoGetPLCIO()
        plc_io = self._commands_manager.send_command_and_wait(command)
        logger.info('[CARGO] [END] Getting PLC I/O')

        return plc_io

    def reset_plc(self):
        """Resets PLC.

        Lock time = stm_communication_settings.ACK_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[CARGO] [START] Resetting PLC')
        command = CargoResetPLC()
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CARGO] [END] Resetting PLC')

        return response

    def reset_controller(self) -> CollectiveResponseEvaluation:
        """Reset controller using pin request, then sending reset
        with pin received.

        Lock time: stm_communication_settings.ACK_TIMEOUT
                   + stm_communication_settings.PIN_TIMEOUT.

        Returns:
            evaluation of responses.
        """
        logger.info('[CARGO] [START] Resetting controller')
        response = self._commands_manager.send_command_and_wait(
            CargoResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(CargoResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = CargoResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)
        logger.info('[CARGO] [END] Resetting controller')

        return response

    def tare(self) -> CollectiveResponseEvaluation:
        """Tare cargo weight in hangar.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.CARGO_WEIGHT).

        Returns:
            evaluation of responses.
        """
        command = CargoTare()
        response = self._commands_manager.send_command_and_wait(command)

        return response
