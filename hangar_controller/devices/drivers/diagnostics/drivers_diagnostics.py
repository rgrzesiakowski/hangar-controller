class DriversDiagnostics:
    def __init__(self):
        self.values = {}

    def __repr__(self):
        values = f'values={self.values}'
        class_name = self.__class__.__name__

        return f'<{class_name} {values}>'

    def update(self, **kwargs):
        self.values.update(kwargs)
