from typing import List

from .future_response_observer import FutureResponseObserver
from ..messages.responses import FutureResponse


class FutureResponseManager:
    # Holds lists of future response observers. Can start observer
    # Can stop future response (observer stops when response stops)
    def __init__(self):
        self.future_response_observers: List[FutureResponseObserver] = []

    def start_future_observer(self, future_response: FutureResponse):
        observer = FutureResponseObserver(future_response, self.unregister_self)
        self.future_response_observers.append(observer)
        observer.start()

    def unregister_self(self, observer: FutureResponseObserver):
        # observer calls this function to remove himself and future response
        # from the list.
        self.future_response_observers.remove(observer)

    def stop(self):
        # stop all remaining future responses
        for observer in self.future_response_observers:
            observer.future_response.stop()
