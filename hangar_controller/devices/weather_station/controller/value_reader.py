from concurrent.futures import ThreadPoolExecutor, Future

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from settingsd.weather_station import modbus_constants
from settingsd.weather_station.modbus_constants import HoldingRegisters, RealRegisters


class ValueReader:
    """ValueReader is reading specific registers using ModbusIO and
    returns future with value.

    Before initializing, ModbusIO.connect() should be called to ensure
    that connection is present.
    """

    def __init__(self, modbus_io: ModbusIO,
                 thread_pool_executor: ThreadPoolExecutor):
        """Thread pool executor is used to ensure no tasks will be stuck
        for unknown amount of time.

        Every function returns future that will be returned in higher
        instance.

        Args:
            modbus_io: initialized and connected Modbus_io.
            thread_pool_executor: executor object.
        """
        self.executor = thread_pool_executor
        self.modbus_io = modbus_io

    def _read_holding_register(self, address: HoldingRegisters) -> Future:
        """Submit to executor reading holding register.

        Args:
            address: address of register.

        Returns:
            future with modbus response.
        """
        future = self.executor.submit(
            self.modbus_io.read_holding_registers,
            address
        )

        return future

    def _read_real_register(self, address: RealRegisters) -> Future:
        """Submit to executor reading real register.

        Args:
            address: address of first register.

        Returns:
            future with modbus response.
        """
        future = self.executor.submit(
            self.modbus_io.read_real_register,
            address
        )

        return future

    def read_register(self, address: modbus_constants.ADDRESS_TYPE) -> Future:
        """Find out if register is holding (one address - 16 bit) or
        real (two addresses - 32 bit).

        Args:
            address: starting address.

        Returns:
            future with reading register.
        """
        if address in HoldingRegisters.__dict__.values():
            future = self._read_holding_register(address)
        elif address in RealRegisters.__dict__.values():
            future = self._read_real_register(address)
        else:
            raise NotImplementedError('Address is not part of Real or Holding '
                                      'registers')

        return future
