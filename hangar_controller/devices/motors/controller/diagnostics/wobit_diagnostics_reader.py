from typing import List, Union

from hangar_controller.devices.motors.controller.motors_controllers.abstract_motor_controller import \
    AbstractMotorsController
from hangar_controller.devices.motors.controller.observers.motor_diagnostics import MotorDiagnostics
from hangar_controller.devices.motors.parameters.parameters_reader import ParametersReader
from settingsd.motors import modbus_constants as mc, parameters_settings
from settingsd.motors.modbus_constants import Wobit

MOTOR_NAME = Union[mc.Wobit.BatteriesMotorsNames,
                   mc.Wobit.PositioningMotorsNames]


class WobitDiagnosticsReader:
    def __init__(self, motor_controllers: List[AbstractMotorsController],
                 parameters_reader: ParametersReader):
        self.motor_controllers: List[AbstractMotorsController] \
            = motor_controllers
        self.parameters_reader: ParametersReader = parameters_reader

    def get_motor_diagnostics_by_name(self, motor_name: MOTOR_NAME) \
            -> MotorDiagnostics:
        """Return diagnostics from motor, by the motor name.

        Args:
            motor_name: either name from BatteryMotorsNames or
                PositioningMotorsNames.

        Returns:
            motor diagnostics.
        """
        for motor_controllers in self.motor_controllers:
            if motor_name not in motor_controllers.diagnostics.keys():
                continue

            diagnostics = motor_controllers. \
                get_diagnostics_by_motor_name(motor_name)

            return diagnostics

        raise ValueError(f'motor name {motor_name} not found in diagnostics '
                         f'reader')

    @staticmethod
    def compare_lift_position(position, up_position, down_position):
        if abs(position - down_position) < 0.01:
            return parameters_settings.RelativePositions.DOWN
        elif abs(position - up_position) < 0.01:
            return parameters_settings.RelativePositions.UP

        return parameters_settings.RelativePositions.MIDDLE

    def get_lift_position(self) -> parameters_settings.RelativePositions:
        """Get position of lift motor relative to up and down position
        specified in parameters.yaml.

        Returns:
            string 'up', 'down' or 'middle'.
        """
        lift_diagnostics = self.get_motor_diagnostics_by_name(
            Wobit.BatteriesMotorsNames.MOTOR_LIFT)

        lift_position = lift_diagnostics.position
        lift_down = self.parameters_reader.read_lift_down()
        lift_up = self.parameters_reader.read_lift_up()

        relative_position = self.compare_lift_position(lift_position, lift_up,
                                                       lift_down)

        return relative_position
