from typing import Optional


class MotorDiagnostics:
    def __init__(self):
        self._limit_switch_in: Optional[bool] = None
        self._limit_switch_out: Optional[bool] = None
        self._position: Optional[float] = None

    def __repr__(self):
        position = f'_position={self._position}'
        limit_in = f'limit_switch_in={self._limit_switch_in}'
        limit_out = f'limit_switch_out={self._limit_switch_out}'
        class_name = self.__class__.__name__

        return f'<{class_name} {position}; {limit_in}; {limit_out}>'

    @property
    def limit_switch_in(self):
        return self._limit_switch_in

    @property
    def limit_switch_out(self):
        return self._limit_switch_out

    @property
    def position(self):
        return self._position

    def update_limit_switch_in(self, limit_switch: bool):
        self._limit_switch_in = limit_switch

    def update_limit_switch_out(self, limit_switch: bool):
        self._limit_switch_out = limit_switch

    def update_position(self, position: float):
        self._position = position
