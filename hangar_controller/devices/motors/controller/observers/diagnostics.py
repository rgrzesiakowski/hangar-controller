import time
from threading import Thread
from typing import List, Dict, Callable

from hangar_controller.devices.motors.controller.observers.motor_diagnostics import \
    MotorDiagnostics
from hangar_controller.devices.motors.controller.wobit_motors.wobit_motor import WobitMotor
from hangar_controller.devices.motors.errors.error_handler import ErrorHandler
from hangar_controller.devices.motors.modbus_interface import modbus_utils
from settingsd.motors.modbus_constants import ModbusResponseStatus


class Diagnostics(Thread):
    def __init__(self, motors: List[WobitMotor], error_handler: ErrorHandler,
                 frequency: int = 1):
        super().__init__()
        self._frequency = frequency
        self._motors = motors
        self._stop_diagnostics = False
        self._error_handler = error_handler
        self._diagnostics: Dict[
            str, MotorDiagnostics] = self._init_diagnostics()

    @property
    def diagnostics(self):
        return self._diagnostics

    def _init_diagnostics(self) -> Dict[str, MotorDiagnostics]:
        diagnostics = {}
        for motor in self._motors:
            diagnostics[motor.motor_name] = MotorDiagnostics()

        return diagnostics

    def _check_modbus_response_status(self,
                                      status: ModbusResponseStatus) -> bool:
        if not modbus_utils.check_response_status(status):
            self._error_handler.modbus_error(status)
            return False

        return True

    def _update_motor_reading(self, motor_read: Callable,
                              diagnostic_update: Callable):
        status, motor_value = motor_read()
        if not self._check_modbus_response_status(status):
            return

        diagnostic_update(motor_value)

    def _update_motors_reading(self, motor):
        diagnostics: MotorDiagnostics = self._diagnostics[motor.motor_name]
        self._update_motor_reading(motor.read_limit_switch_in,
                                   diagnostics.update_limit_switch_in)
        self._update_motor_reading(motor.read_limit_switch_out,
                                   diagnostics.update_limit_switch_out)
        self._update_motor_reading(motor.read_position,
                                   diagnostics.update_position)

    def update_diagnostics(self):
        for motor in self._motors:
            self._update_motors_reading(motor)

    def stop(self):
        self._stop_diagnostics = True

    def run(self) -> None:
        while not self._stop_diagnostics:
            self.update_diagnostics()
            time.sleep(1 / self._frequency)
