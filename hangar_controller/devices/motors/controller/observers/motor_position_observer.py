import time
from typing import Callable, Optional, NoReturn

from hangar_controller.devices.motors.errors.error_handler import ErrorHandler
from hangar_controller.devices.motors.modbus_interface import modbus_utils
from settingsd.motors import modbus_constants
from settingsd.motors.modbus_constants import ModbusResponseStatus, MotorErrors


class MotorPositionObserver:
    def __init__(self, motor_position_check: Callable,
                 motor_status_check: Callable,
                 error_handler: ErrorHandler, frequency=1):
        super().__init__()
        self._motor_position_check = motor_position_check
        self._motor_status_check = motor_status_check
        self._end_position: Optional[float] = None
        self._error_handler: ErrorHandler = error_handler
        self._frequency = frequency
        self._stop_observer = False

    def _update_end_position(self, end_position: float):
        self._end_position = end_position

    def _stop(self) -> None:
        self._stop_observer = True

    def _connection_error(self) -> NoReturn:
        self._stop()
        self._error_handler.modbus_error(ModbusResponseStatus.CONNECTION_ERROR)

    def _motor_limit_error(self) -> NoReturn:
        self._stop()
        self._error_handler.motor_error(MotorErrors.MOTOR_LIMIT_REACHED)

    def _get_motor_position(self) -> float:
        """Reads motor current position.

        Returns:
            Motor position if there is all ok.

        Raises:
            ModbusError if there is communication problem.
        """
        response_status, position = self._motor_position_check()

        if not modbus_utils.check_response_status(response_status):
            self._connection_error()

        return position

    def _get_motor_status(self) -> int:
        """Reads motor status.

        Returns:
            Motor_status as a integer. Motor are listed in Wobit
                controller documentation.

        Raises:
            ModbusError if there is communication problem.
        """
        response_status, motor_status = self._motor_status_check()

        if not modbus_utils.check_response_status(response_status):
            self._connection_error()

        return motor_status

    def _check_position_reached(self) -> bool:
        motor_position = self._get_motor_position()

        try:
            position_difference = motor_position - self._end_position
        except TypeError:
            return False

        position_reached = abs(position_difference) < 0.01

        return position_reached

    @staticmethod
    def _check_motor_stopped(motor_status: int) -> bool:
        status_not_moving = motor_status not in [
            modbus_constants.Wobit.MotorStatus.POSITION_MODE,
            modbus_constants.Wobit.MotorStatus.VELOCITY_MODE
        ]

        return status_not_moving

    def _check_motor_limit(self, motor_status: int) -> bool:
        limits = [modbus_constants.Wobit.MotorStatus.R_LIMIT,
                  modbus_constants.Wobit.MotorStatus.L_LIMIT]
        motor_limit = motor_status in limits

        if motor_limit:
            self._motor_limit_error()

        return motor_limit

    def _check_stop_status_reached(self) -> bool:
        motor_status = self._get_motor_status()
        if motor_status is None:
            return False

        motor_stopped = self._check_motor_stopped(motor_status)
        self._check_motor_limit(motor_status)

        return motor_stopped

    def _check_stop_condition(self) -> bool:
        position_reached = self._check_position_reached()
        motor_stopped = self._check_stop_status_reached()
        return position_reached and motor_stopped

    def observe(self, end_position: float) -> bool:
        self._update_end_position(end_position)
        while not self._check_stop_condition():
            if self._stop_observer:
                return False

            time.sleep(1 / self._frequency)

        return True
