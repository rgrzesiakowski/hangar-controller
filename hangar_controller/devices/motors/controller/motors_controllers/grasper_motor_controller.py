import logging

from ...errors.error_handler import ErrorHandler
from ...modbus_interface.modbus_io import ModbusIO
from ...parameters.parameters_reader import ParametersReader
from ...controller.task_queue.task_queue import TaskQueue
from ...controller.motors_controllers.abstract_motor_controller \
    import AbstractMotorsController
from settingsd.motors import parameters_settings
from settingsd.motors.modbus_constants import ALL_ADDRESSES

logger = logging.getLogger(__name__)

MOTOR_GRASPER = parameters_settings.DrivesNames.GRASPER


class GrasperMotorController(AbstractMotorsController):
    def __init__(self, modbus_io: ModbusIO, error_handler: ErrorHandler,
                 task_queue: TaskQueue):
        grasper_motors = self._init_motors_addresses()
        super().__init__(grasper_motors, modbus_io, error_handler)
        self._task_queue = task_queue

    @staticmethod
    def _init_motors_addresses():
        parameters_reader = ParametersReader()
        wobit_id, motor_id = parameters_reader.read_grasper_motor_placement()
        assert (len(motor_id) == 1)

        address = ALL_ADDRESSES[motor_id[0]]

        return [(MOTOR_GRASPER, address)]

    def calibrate_grasper(self):
        """Rotate grasper back to the closest hole and then finish
        movement by calibrating it in the another direction.

        """
        logger.debug('[GRASPER] [START] Calibrate grasper')
        motor_lift_params_name = parameters_settings.DrivesNames.GRASPER
        calibration_speed = self._parameters_reader.read_calibration_speed(
            motor_lift_params_name
        )
        self._task_queue.add_task(self.calibrate,
                                  [[MOTOR_GRASPER], [calibration_speed]])
        self._task_queue.wait_until_done()
        logger.debug('[GRASPER] [END] Calibrate grasper')

    def release(self):
        """Release battery either on drone or on battery stash.

        Lock time: ~ 2s.

        """
        logger.info('[GRASPER] [START] Releasing')
        position = self._parameters_reader.read_grasp_position()
        self._task_queue.add_task(self.move_motors, [position])
        self._task_queue.wait_until_done()
        logger.info('[GRASPER] [END] Releasing')

    def grasp(self):
        """
        Grasp is alias for calibrate_grasper() because after each
        movement that ends up with vertical position motor position has
        to be reset, otherwise LIMIT_SWITCH error is going to be raised
        when you will want to release it again. Since calibration always
        ends up with vertical position and motor position reset,
        grasping objects are handled with calibration method.

        """
        logger.info('[GRASPER] [START] Grasping')
        self.calibrate_grasper()
        logger.info('[GRASPER] [END] Grasping')
