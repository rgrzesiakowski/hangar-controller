import time
import logging
from typing import List, Optional, Tuple, Type

from ..wobit_motors import motors_utils
from ..wobit_motors.wobit_motor import WobitMotor
from ..wobit_motors.master_motor import MasterMotor
from ..observers.diagnostics import Diagnostics
from ..observers.motor_diagnostics import MotorDiagnostics
from ...modbus_interface import modbus_utils
from ...modbus_interface.modbus_io import ModbusIO
from ...errors.error_handler import ErrorHandler
from ...utils.motors_position import MotorsPosition
from ...parameters.parameters_reader import ParametersReader
from settingsd.motors import modbus_constants

logger = logging.getLogger(__name__)

MotorsList = List[Tuple[modbus_constants.Wobit.PositioningMotorsNames,
                        Type[modbus_constants.WobitMotorAddresses]]]


class AbstractMotorsController:
    def __init__(self, motors: MotorsList, modbus_io: ModbusIO,
                 error_handler: ErrorHandler):
        self._modbus_io = modbus_io
        self._parameters_reader = ParametersReader()
        self._error_handler = error_handler
        self._motor_master = MasterMotor(modbus_io)
        self._motors: List[WobitMotor] = self.init_motors(motors)
        self._diagnostics = Diagnostics(self._motors, error_handler)

    @property
    def motors(self):
        return self._motors

    def start_diagnostics(self):
        logger.debug('Starting diagnostics')
        self._diagnostics.start()

    def stop_diagnostics(self):
        logger.debug('Stopping diagnostics')
        self._diagnostics.stop()

    @property
    def diagnostics(self):
        return self._diagnostics.diagnostics

    def get_diagnostics_by_motor_name(self, motor_name) \
            -> Optional[MotorDiagnostics]:
        if motor_name not in self.diagnostics.keys():
            raise ValueError(f'Motor name {motor_name} not present in '
                             f'diagnostics. Possible motors: '
                             f'{[motor.motor_name for motor in self.motors]}')

        diagnostics = self.diagnostics[motor_name]

        return diagnostics

    def init_motors(self, motors_list: MotorsList) -> List[WobitMotor]:
        logger.debug(f'Initializing motors:\n\t{motors_list}')
        motors = []
        for name, registers in motors_list:
            motor = WobitMotor(self._modbus_io, self._error_handler, name,
                               registers)
            motors.append(motor)

        return motors

    def _check_modbus_response_status(
            self, status: modbus_constants.ModbusResponseStatus) -> bool:
        if not modbus_utils.check_response_status(status):
            self._error_handler.modbus_error(status)
            return False

        return True

    def _update_motor_parameters(self, motor: WobitMotor):
        logger.debug(f'Updating motor parameters {motor.motor_name}')
        velocity = self._parameters_reader.read_max_velocity(motor.motor_name)
        status = motor.write_max_velocity(velocity)
        if not self._check_modbus_response_status(status):
            return False

        acceleration = self._parameters_reader.read_acceleration(
            motor.motor_name)
        status = motor.write_acceleration(acceleration)
        if not self._check_modbus_response_status(status):
            return False

        deceleration = self._parameters_reader.read_deceleration(
            motor.motor_name)
        status = motor.write_deceleration(deceleration)
        if not self._check_modbus_response_status(status):
            return False

        return True

    def update_motors_parameters(self) -> bool:
        logger.debug('Updating motors parameters')
        for motor in self.motors:
            if not self._update_motor_parameters(motor):
                return False

        return True

    def _set_direct_mode(self) -> bool:
        logger.debug('Setting direct mode')
        status = self._motor_master.write_axis_control_mode(
            modbus_constants.Wobit.AxisControlMode.DIRECT)
        return self._check_modbus_response_status(status)

    def _set_trigger_mode(self) -> bool:
        logger.debug('Setting trigger mode')
        status = self._motor_master.write_axis_control_mode(
            modbus_constants.Wobit.AxisControlMode.TRIGGER)
        return self._check_modbus_response_status(status)

    def _set_motors_targets(self, motors: List[WobitMotor],
                            positions: List[float]) -> bool:

        logger.debug(f'Setting motors targets {motors}, {positions}')
        for motor, position in zip(motors, positions):
            status = motor.write_absolute_position(position)

            if not self._check_modbus_response_status(status):
                return False

        return True

    def _trigger_movements(self, motors: List[WobitMotor]) -> bool:
        logger.debug(f'Triggering movements {motors}')
        status = self._motor_master.trigger_absolute_position([
            motor.wobit_id for motor in motors])

        return self._check_modbus_response_status(status)

    def _start_position_observers(self, motors: List[WobitMotor],
                                  positions: List[float]) -> bool:
        logger.debug(f'Starting positions observers {motors}, {positions}')
        for motor, position in zip(motors, positions):
            status = motor.position_observer.observe(position)

            if not status:
                return False

        return True

    def move_motors(self, positions: List[MotorsPosition]):
        logger.debug(f'Moving motors {positions}')
        status_ok = self._set_trigger_mode()
        if not status_ok:
            return False

        # set targets
        motors_names = [position.motor_name for position in positions]
        positions = [position.position for position in positions]
        motors = motors_utils.get_motors_by_names(self._motors, motors_names)

        status_ok = self._set_motors_targets(motors, positions)
        if not status_ok:
            return False

        time.sleep(0.1)

        # trigger movement
        status_ok = self._trigger_movements(motors)
        if not status_ok:
            return False

        # wait until positions are reached
        status_ok = self._start_position_observers(motors, positions)
        if not status_ok:
            return False

        return True

    def get_calibration_current_position(self, speed):
        actual_position = 1 if speed > 0 else -1
        return actual_position

    def calibrate(self, motors_names: List[modbus_constants.Wobit.MotorNames],
                  speeds: List[float]) -> bool:
        logger.debug(f'Calibrating {motors_names} with speeds {speeds}')
        motors = motors_utils.get_motors_by_names(self._motors, motors_names)

        status_ok = self._set_direct_mode()
        if not status_ok:
            return False

        for motor, speed in zip(motors, speeds):
            actual_position = self.get_calibration_current_position(speed)
            status = motor.write_actual_position(actual_position)
            if not self._check_modbus_response_status(status):
                return False

            time.sleep(0.1)
            status = motor.move_home(speed)
            if not self._check_modbus_response_status(status):
                return False

        for motor in motors:
            status = motor.position_observer.observe(end_position=0)
            if not status:
                return False

        return True

    def stop_all(self) -> bool:
        logger.debug('Stopping all motors')
        status = self._motor_master.stop_all_motors()
        return self._check_modbus_response_status(status)

    def filter_positions(self, positions, motor_name, equal=True):
        if equal:
            movement = [position for position in positions if
                        position.motor_name == motor_name]
        else:
            movement = [position for position in positions if
                        position.motor_name != motor_name]

        return movement
