import logging

from ...errors.error_handler import ErrorHandler
from ...modbus_interface.modbus_io import ModbusIO
from ...parameters.parameters_reader import ParametersReader
from ...controller.task_queue.task_queue import TaskQueue
from ...controller.motors_controllers.abstract_motor_controller \
    import AbstractMotorsController
from settingsd.motors import parameters_settings
from settingsd.motors.modbus_constants import ALL_ADDRESSES
from settingsd.motors.parameters_settings import DrivesNames

logger = logging.getLogger(__name__)

X_PLATFORM = parameters_settings.DrivesNames.X_PLATFORM
Y_PLATFORM = parameters_settings.DrivesNames.Y_PLATFORM


class PositioningMotorController(AbstractMotorsController):
    def __init__(self, modbus_io: ModbusIO, error_handler: ErrorHandler,
                 task_queue: TaskQueue):
        positioning_motors = self._init_motors_addresses()
        super().__init__(positioning_motors, modbus_io, error_handler)
        self.task_queue = task_queue

    def _init_motors_addresses(self):
        parameters_reader = ParametersReader()
        wobit_id, motor_id = parameters_reader.read_positioning_motors_placement()
        assert (wobit_id[0] == wobit_id[1])
        motors = [X_PLATFORM, Y_PLATFORM]

        addresses = [ALL_ADDRESSES[i] for i in motor_id]

        return list(zip(motors, addresses))

    def calibrate_x(self):
        logger.info('[MOTORS] [START] Calibrating x-axis positioning motors')
        motor_x_params_name = parameters_settings.DrivesNames.X_PLATFORM
        calibration_speed = self._parameters_reader.read_calibration_speed(
            motor_x_params_name
        )
        self.task_queue.add_task(self.calibrate,
                                 [[X_PLATFORM], [calibration_speed]])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Calibrating x-axis positioning motors')

    def calibrate_y(self):
        logger.info('[MOTORS] [START] Calibrating y-axis positioning motors')
        motor_y_params_name = parameters_settings.DrivesNames.Y_PLATFORM
        calibration_speed = self._parameters_reader.read_calibration_speed(
            motor_y_params_name
        )
        self.task_queue.add_task(self.calibrate,
                                 [[Y_PLATFORM], [calibration_speed]])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Calibrating y-axis positioning motors')

    def calibrate_all(self):
        """Move to the limits and reset positions to 0.

        Returns:
            None.
        """
        logger.info('[MOTORS] [START] Calibrating positioning motors')
        motor_x_params_name = parameters_settings.DrivesNames.X_PLATFORM
        motor_y_params_name = parameters_settings.DrivesNames.Y_PLATFORM
        calibration_speed_x = self._parameters_reader.read_calibration_speed(
            motor_x_params_name
        )
        calibration_speed_y = self._parameters_reader.read_calibration_speed(
            motor_y_params_name
        )

        self.task_queue.add_task(
            self.calibrate,
            [[X_PLATFORM, Y_PLATFORM],
             [calibration_speed_x, calibration_speed_y]])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Calibrating positioning motors')

    def position_drone(self):
        """Move in and position drone.

        Returns:
            None.
        """
        logger.info('[MOTORS] [START] Positioning drone')
        self.calibrate_all()
        positions = self._parameters_reader.read_platform_middle()
        first_movement = self.filter_positions(positions,
                                               DrivesNames.Y_PLATFORM)
        self.task_queue.add_task(self.move_motors,
                                 [first_movement])
        second_movement = self.filter_positions(positions,
                                                DrivesNames.X_PLATFORM)
        self.task_queue.add_task(self.move_motors,
                                 [second_movement])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Positioning drone')

    def release_drone(self):
        """Move out and release drone.

        Lock time: ~ 10s.

        Returns:
            None.
        """
        logger.info('[MOTORS] [START] Releasing drone')
        positions = self._parameters_reader.read_platform_home()
        self.task_queue.add_task(self.move_motors,
                                 [positions])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Releasing drone')

    def calibrate_positioning(self):
        self.calibrate_all()
