import logging
from typing import List, Union

from ...errors.error_handler import ErrorHandler
from ...modbus_interface.modbus_io import ModbusIO
from ...utils.motors_position import MotorsPosition
from ...parameters.parameters_reader import ParametersReader
from ...controller.zones.zone import Zone
from ...controller.zones.hub_zones import HubZones
from ...controller.task_queue.task_queue import TaskQueue
from ...controller.zones.zone_entry_point import ZoneEntryPositions
from ...controller.motors_controllers.abstract_motor_controller \
    import AbstractMotorsController
from settingsd.motors import parameters_settings
from settingsd.motors.modbus_constants import ALL_ADDRESSES
from settingsd.motors.parameters_settings import DrivesNames, Positions

logger = logging.getLogger(__name__)

MOTOR_X = parameters_settings.DrivesNames.X
MOTOR_Y = parameters_settings.DrivesNames.Y
MOTOR_Z = parameters_settings.DrivesNames.Z


class ManipulatorMotorController(AbstractMotorsController):
    def __init__(self, modbus_io: ModbusIO, error_handler: ErrorHandler,
                 task_queue: TaskQueue):
        manipulator_motors = self._init_motors_addresses()
        super().__init__(manipulator_motors, modbus_io, error_handler)
        self.current_zone = HubZones.ENTRY_ZONE
        self.task_queue = task_queue

    def _init_motors_addresses(self):
        parameters_reader = ParametersReader()
        wobit_id, motor_id = parameters_reader.read_manipulator_motors_placement()
        assert (wobit_id[0] == wobit_id[1] == wobit_id[2])
        motors = [MOTOR_X, MOTOR_Y, MOTOR_Z]

        addresses = [ALL_ADDRESSES[i] for i in motor_id]

        return list(zip(motors, addresses))

    def _update_current_zone(self, zone: Zone):
        logger.debug(f'Updating current zone: {zone}')
        self.current_zone = zone

    def _move_by_path(self, movement_path: List[ZoneEntryPositions]):
        logger.debug(f'Moving by path {movement_path}')
        for entry_position in movement_path:
            if entry_position == ZoneEntryPositions.CARGO_WINDOW_PICK_UP:
                self._pick_up_cargo()
            elif entry_position == ZoneEntryPositions.MOTOR_Y_BACK:
                self._move_y_motor_back()
            elif entry_position == ZoneEntryPositions.CARGO_SAFE_HEIGHT:
                self._move_z_motor_safe_height()
            else:
                raise ValueError('Unspecified home location')

    def _exit_current_zone(self):
        logger.debug(f'Exiting current zone {self.current_zone} '
                     f'by path {self.current_zone.exit_path}')
        self._move_by_path(self.current_zone.exit_path)

    def _move_motors_xz_y_position(self, position: List[MotorsPosition]):
        first_movement = self.filter_positions(position, DrivesNames.Y, False)
        second_movement = self.filter_positions(position, DrivesNames.Y)
        self.task_queue.add_task(self.move_motors, [first_movement])
        self.task_queue.add_task(self.move_motors, [second_movement])

    def _move_motors_x_z_y_position(self, position: List[MotorsPosition]):
        first_movement = self.filter_positions(position, DrivesNames.X)
        second_movement = self.filter_positions(position, DrivesNames.Z)
        third_movement = self.filter_positions(position, DrivesNames.Y)
        self.task_queue.add_task(self.move_motors, [first_movement])
        self.task_queue.add_task(self.move_motors, [second_movement])
        self.task_queue.add_task(self.move_motors, [third_movement])

    def _move_motors_z_x_y_position(self, position: List[MotorsPosition]):
        first_movement = self.filter_positions(position, DrivesNames.Z)
        second_movement = self.filter_positions(position, DrivesNames.X)
        third_movement = self.filter_positions(position, DrivesNames.Y)
        self.task_queue.add_task(self.move_motors, [first_movement])
        self.task_queue.add_task(self.move_motors, [second_movement])
        self.task_queue.add_task(self.move_motors, [third_movement])

    def _move_home(self):
        logger.debug('Moving to the home position')
        position = self._parameters_reader.read_home_position()
        # By this time, motor y should already be full back, this is to be sure
        self._move_y_motor_back()
        # This command will move only x, then z position, y should be back
        self._move_motors_x_z_y_position(position)
        self.task_queue.add_task(self._update_current_zone, [HubZones.HOME])

    def _pick_up_cargo(self):
        logger.debug('Picking up a cargo')
        position = self._parameters_reader.read_cargo_window_pick_up()
        movement = self.filter_positions(position, DrivesNames.Z)
        self.task_queue.add_task(self.move_motors, [movement])

    def _move_y_motor_back(self):
        logger.debug('Moving manipulator back')
        position = self._parameters_reader.read_home_position()
        movement = self.filter_positions(position, DrivesNames.Y)
        self.task_queue.add_task(self.move_motors, [movement])

    def _move_z_motor_safe_height(self):
        logger.debug('Moving manipulator to safe height')
        position = self._parameters_reader.read_cargo_safe_move_height()
        self.task_queue.add_task(self.move_motors, [position])

    def calibrate_x(self):
        logger.info('[MOTORS] [START] Running manipulator x-axis calibration')
        motor_x_params_name = parameters_settings.DrivesNames.X
        calibration_speed = self._parameters_reader.read_calibration_speed(
            motor_x_params_name
        )
        self.task_queue.add_task(self.calibrate,
                                 [[MOTOR_X], [calibration_speed]])
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CALIBRATION])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [START] Running manipulator x-axis calibration')

    def calibrate_y(self):
        logger.info('[MOTORS] [START] Running manipulator y-axis calibration')
        motor_y_params_name = parameters_settings.DrivesNames.Y
        calibration_speed = self._parameters_reader.read_calibration_speed(
            motor_y_params_name
        )
        self.task_queue.add_task(self.calibrate,
                                 [[MOTOR_Y], [calibration_speed]])
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CALIBRATION])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Running manipulator y-axis calibration')

    def calibrate_z(self):
        logger.info('[MOTORS] [START] Running manipulator z-axis calibration')
        calibration_speed = self._parameters_reader.read_calibration_speed(
            parameters_settings.DrivesNames.Z
        )
        self.task_queue.add_task(self.calibrate,
                                 [[MOTOR_Z], [calibration_speed]])
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CALIBRATION])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Running manipulator z-axis calibration')

    def calibrate_safe_sequence(self):
        """Sends signal to manipulator motors to calibrate.

        I.e. go to home position and reset position to 0.
        Lock time: ~ 10s.

        Returns:
            None.
        """
        logger.info('[MOTORS] [START] Running manipulator calibration sequence')
        self.calibrate_y()
        self.calibrate_z()
        self._move_z_motor_safe_height()
        self.calibrate_x()
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CALIBRATION])
        logger.info('[MOTORS] [END] Running manipulator calibration sequence')

    def calibrate_all(self):
        logger.info('[MOTORS] [START] Running manipulator calibration sequence')
        calibration_speed_x = self._parameters_reader.read_calibration_speed(
            parameters_settings.DrivesNames.X
        )
        calibration_speed_y = self._parameters_reader.read_calibration_speed(
            parameters_settings.DrivesNames.Y
        )
        calibration_speed_z = self._parameters_reader.read_calibration_speed(
            parameters_settings.DrivesNames.Z
        )
        self.task_queue.add_task(
            self.calibrate,
            [[MOTOR_X, MOTOR_Y, MOTOR_Z],
             [calibration_speed_x, calibration_speed_y, calibration_speed_z]]
        )
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CALIBRATION])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Running manipulator  calibration sequence')

    def go_to_battery_slot(self, slot_number: int):
        """Go to battery slot, either to pick up battery or leave one.

        If picking up, battery there is no need for safety, as long as
        the manipulator is moving on the back wall. However, if it's
        holding a battery from drone, after it moves back, it has to go
        straight down, then x and then y.

        Args:
            slot_number: battery slot number.

        Returns:
            None.
        """
        logger.info('[MOTORS] [START] Moving manipulator to the battery slot '
                    f'{slot_number}')
        self._exit_current_zone()
        position = self._parameters_reader.read_battery_slot_position(
            slot_number)
        self._move_motors_z_x_y_position(position)
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.BATTERY_SLOTS])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Moving manipulator to the battery slot '
                    f'{slot_number}')

    def go_home(self):
        """Move arm to safe position called home.

        Home position can be set in parameters.yaml file in main folder.

        Lock time: ~ 5s.

        Returns:
            None.
        """
        logger.info('[MOTORS] [START] Moving manipulator to the home position')
        self._exit_current_zone()
        self._move_home()
        self.calibrate_all()
        self._move_home()
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Moving manipulator to the home position')

    def go_cargo_window_pick_up(self):
        """Move manipulator to position of cargo, assuming cargo is there
        to pick it up.

        Manipulator stops with grasper in the cargo and next command
        should be to grasp cargo.

        Returns:
            None.
        """
        logger.info('[MOTORS] [START] Moving manipulator to the cargo pick-up')
        self._exit_current_zone()
        position = self._parameters_reader.read_cargo_window()
        self._move_motors_x_z_y_position(position)
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CARGO_WINDOW_FOR_PICKING_UP])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Moving manipulator to the cargo pick-up')

    def go_cargo_window_put_down(self):
        """Move manipulator to put down cargo.

        First move above the cargo position, then put it down.
        The next command should be to release cargo.

        Returns:
            None.
        """
        logger.info('[MOTORS] [START] Moving manipulator to the cargo drop')
        self._exit_current_zone()
        # if cargo is taken from drone, first move horizontally not to catch
        # propellers, then go up, then to the front
        position = self._parameters_reader.read_cargo_window_pick_up()
        self._move_motors_x_z_y_position(position)
        # position of cargo window should be directly below current point
        position = self._parameters_reader.read_cargo_window()
        self._move_motors_xz_y_position(position)
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CARGO_WINDOW_PUT_DOWN])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Moving manipulator to the cargo drop')

    def go_cargo_drone(self):
        """Move arm to cargo drone either to get the cargo from drone,
        or to put cargo into drone.

        Lock time: ~ 5s.

        Returns:
            None.
        """
        logger.info('[MOTORS] [START] Moving cargo to the drone')
        self._exit_current_zone()
        position = self._parameters_reader.read_cargo_drone()
        # if cargo is taken from window, first move it horizontally
        # not to catch the propellers
        self._move_motors_x_z_y_position(position)
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CARGO_DRONE])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Moving cargo to the drone')

    def go_battery_drone(self,
                         position_name: Union[Positions.BATTERY_DRONE_PUT,
                                              Positions.BATTERY_DRONE_TAKE]):
        """Go to battery drone.

        Returns:
            None.
        """
        logger.info('[MOTORS] [START] Moving battery to the drone')
        self._exit_current_zone()
        position = self._parameters_reader.read_battery_drone(position_name)
        self._move_motors_x_z_y_position(position)
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.BATTERY_DRONE])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Moving battery to the drone')

    def calibrate_manipulator_motors(self):
        self.calibrate_safe_sequence()
