import logging

from ...errors.error_handler import ErrorHandler
from ...modbus_interface.modbus_io import ModbusIO
from ...parameters.parameters_reader import ParametersReader
from ...controller.task_queue.task_queue import TaskQueue
from ...controller.motors_controllers.abstract_motor_controller \
    import AbstractMotorsController
from settingsd.motors import parameters_settings
from settingsd.motors.modbus_constants import ALL_ADDRESSES

logger = logging.getLogger(__name__)

MOTOR_LIFT = parameters_settings.DrivesNames.LIFT


class LiftMotorController(AbstractMotorsController):
    def __init__(self, modbus_io: ModbusIO, error_handler: ErrorHandler,
                 task_queue: TaskQueue):
        lift_motors = self._init_motors_addresses()
        super().__init__(lift_motors, modbus_io, error_handler)
        self.task_queue = task_queue

    def _init_motors_addresses(self):
        parameters_reader = ParametersReader()
        wobit_id, motor_id = parameters_reader.read_lift_motor_placement()
        assert (len(motor_id) == 1)

        address = ALL_ADDRESSES[motor_id[0]]

        return [(MOTOR_LIFT, address)]

    def calibrate_lift(self):
        """Calibrate lift motor.

        I.e. go to home position and reset position to 0.

        Lock time: ~ 10s

        Returns:
            None.
        """
        logger.info('[MOTORS] [START] Calibrating lift')
        calibration_speed = self._parameters_reader.read_calibration_speed(
            parameters_settings.DrivesNames.LIFT
        )
        self.task_queue.add_task(self.calibrate,
                                 [[MOTOR_LIFT], [calibration_speed]])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Calibrating lift')

    def go_up(self):
        logger.info('[MOTORS] [START] Moving lift up')
        positions = self._parameters_reader.read_lift_up()
        self.task_queue.add_task(self.move_motors, [positions])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Moving lift up')

    def go_down(self):
        logger.info('[MOTORS] [START] Moving lift down')
        positions = self._parameters_reader.read_lift_down()
        self.task_queue.add_task(self.move_motors, [positions])
        self.task_queue.wait_until_done()
        logger.info('[MOTORS] [END] Moving lift down')
