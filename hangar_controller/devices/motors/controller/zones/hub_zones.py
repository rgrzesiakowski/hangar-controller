from hangar_controller.devices.motors.controller.zones.zone import Zone
from hangar_controller.devices.motors.controller.zones.zone_entry_point import ZoneEntryPositions


class HubZones:
    BATTERY_SLOTS = Zone(
        'Battery Slots',
        exit_path=[ZoneEntryPositions.MOTOR_Y_BACK]
    )
    CARGO_WINDOW_FOR_PICKING_UP = Zone(
        'Cargo window',
        exit_path=[ZoneEntryPositions.CARGO_WINDOW_PICK_UP,
                   ZoneEntryPositions.MOTOR_Y_BACK,
                   ZoneEntryPositions.CARGO_SAFE_HEIGHT]
    )
    CARGO_WINDOW_PUT_DOWN = Zone(
        'Cargo window',
        exit_path=[ZoneEntryPositions.MOTOR_Y_BACK,
                   ZoneEntryPositions.CARGO_SAFE_HEIGHT]
    )
    CARGO_DRONE = Zone(
        'Cargo drone',
        exit_path=[ZoneEntryPositions.MOTOR_Y_BACK,
                   ZoneEntryPositions.CARGO_SAFE_HEIGHT]
    )
    BATTERY_DRONE = Zone(
        'Battery drone',
        exit_path=[ZoneEntryPositions.MOTOR_Y_BACK,
                   ZoneEntryPositions.CARGO_SAFE_HEIGHT]
    )
    HOME = Zone('Home', exit_path=[ZoneEntryPositions.CARGO_SAFE_HEIGHT])
    ENTRY_ZONE = Zone('Entry zone', exit_path=[])
    CALIBRATION = Zone('Calibration', exit_path=[])
