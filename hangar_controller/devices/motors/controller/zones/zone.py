from typing import List

from hangar_controller.devices.motors.controller.zones.zone_entry_point import ZoneEntryPositions


class Zone:
    def __init__(self, name, exit_path: List[ZoneEntryPositions]):
        self.name = name
        self.exit_path = exit_path

    def __repr__(self):
        name = f'name=\"{self.name}\"'
        exit_path = f'exit_path={self.exit_path}'
        class_name = self.__class__.__name__

        return f'<{class_name} {name}; {exit_path}>'
