from typing import List, Optional

from hangar_controller.devices.motors.controller.wobit_motors.wobit_motor import \
    WobitMotor
from settingsd.motors import modbus_constants


def get_motor_by_name(motors: List[WobitMotor],
                      name: modbus_constants.Wobit.MotorNames) \
        -> Optional[WobitMotor]:
    for motor in motors:
        if motor.motor_name == name:
            return motor

    raise ValueError(f'Motor named {name} not found in list {motors}')


def get_motors_by_names(motors_list: List[WobitMotor],
                        names: List[modbus_constants.Wobit.MotorNames]) \
        -> List[WobitMotor]:
    motors = []
    for name in names:
        motors.append(get_motor_by_name(motors_list, name))

    return motors
