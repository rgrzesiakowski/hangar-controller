from hangar_controller.devices.motors.controller.task_queue.task import Task


def empty_callback():
    pass


class EmptyTask(Task):
    def __init__(self):
        super().__init__(empty_callback)
