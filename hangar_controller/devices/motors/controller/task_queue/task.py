from threading import Thread
from typing import Callable, Optional


class Task(Thread):
    def __init__(self, callback, args: Optional[list] = None,
                 lock_release: Optional[Callable] = None):
        super().__init__()
        self._callback: Callable = callback
        self._args: list = args
        self._lock_release: Optional[Callable] = lock_release

        if args is None:
            self._args = []

    @property
    def queue_locking(self) -> bool:
        return self._lock_release is not None

    def run(self) -> None:
        # hang here until process is done
        self._callback(*self._args)

        # unlock task_queue if lock was set
        if self.queue_locking:
            self._lock_release()
