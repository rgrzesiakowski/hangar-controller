import queue
import logging
from threading import Thread
from typing import NoReturn

from satella.coding.structures import Singleton

from .errors.error_handler import ErrorHandler
from .parameters.parameters_reader import ParametersReader
from .controller.task_queue.task_queue import TaskQueue
from .controller.motors_controllers.grasper_motor_controller import \
    GrasperMotorController
from .controller.motors_controllers.lift_motor_controller import \
    LiftMotorController
from .controller.motors_controllers.manipulator_motor_controller import \
    ManipulatorMotorController
from .controller.motors_controllers.positioning_motors_controller import \
    PositioningMotorController
from .controller.diagnostics.wobit_diagnostics_reader import \
    WobitDiagnosticsReader
from ..modbus_io_manager import ModbusIOManager
from ...devices.error_queues import ErrorQueue, EventLogQueue

logger = logging.getLogger(__name__)


@Singleton
class Motors(Thread):
    """Main thread that controls all hangar functions.

    Method start() needs to be called when running motors.
    If we are using the real motors, we need to update their parameters
    to match the ones saved in parameters.yaml. To do this:
    update_motor_parameters(). If user wants to observe motors
    diagnostics (e.g. position), he needs to call start_diagnostics()
    as well. To stop diagnostics call stop_diagnostics().
    To stop motors thread call stop().

    Raises:
        MotorError: when motor hits the position limiter.
        ModbusError: when modbus connection has been broken.
    """
    def __init__(self, error_queue: queue.Queue = None,
                 error_warning_log_queue: queue.Queue = None):
        super().__init__()
        self._error_queue = error_queue
        self._error_warning_log_queue = error_warning_log_queue
        if error_queue is None:
            self._error_queue = ErrorQueue()
        if error_warning_log_queue is None:
            self._error_warning_log_queue = EventLogQueue()
        self._stop_controller = False
        self._task_queue = TaskQueue()
        self._error_handler = ErrorHandler()
        self._modbus_io_manager = ModbusIOManager()
        self._parameters_reader = ParametersReader()
        self._manipulator = ManipulatorMotorController(
            self._modbus_io_manager.batteries, self._error_handler,
            self._task_queue
        )
        self._grasper = GrasperMotorController(
            self._modbus_io_manager.batteries, self._error_handler,
            self._task_queue
        )
        self._lift = LiftMotorController(
            self._modbus_io_manager.positioning, self._error_handler,
            self._task_queue
        )
        self._positioning = PositioningMotorController(
            self._modbus_io_manager.positioning, self._error_handler,
            self._task_queue
        )
        self._controllers = [
            self._lift,
            self._positioning,
            self._grasper,
            self._manipulator
        ]
        self._diagnostics_reader = WobitDiagnosticsReader(
            self.controllers, self._parameters_reader
        )

    @property
    def manipulator(self):
        return self._manipulator

    @property
    def grasper(self):
        return self._grasper

    @property
    def lift(self):
        return self._lift

    @property
    def positioning(self):
        return self._positioning

    @property
    def diagnostics_reader(self):
        return self._diagnostics_reader

    @property
    def controllers(self):
        return self._controllers

    def stop(self) -> None:
        """Stops main controller thread.

        Stops diagnostics thread that updates diagnostic messages from stm.
        Sets stop flag. Exit procedure stops queue safely.

        Returns:
            None.

        Raises:
            Look for 'raises' definition in the class header.
        """
        self.stop_diagnostics()
        self._stop_controller = True
        self._task_queue.exit_procedure()

    def run(self) -> NoReturn:
        """Runs this thread and starts processing tasks from the motors queue.

        Runs drivers controller thread. Stops after executing self.stop().

        Raises:
            Look for 'raises' definition in the class header.
        """
        logger.info('Starting controller')
        while not self._stop_controller:
            self._task_queue.process_task()

    def update_motors_parameters(self) -> bool:
        """Reads motors parameters (acceleration, deceleration, speed)
        from yaml file and sends modbus message to each motor,
        with parameters set.

        Returns:
            Status if there was no modbus error during update.

        Raises:
            Look for 'raises' definition in the class header.
        """
        for controller in self._controllers:
            if not controller.update_motors_parameters():
                return False
        return True

    def start_diagnostics(self):
        """Starting diagnostic thread.

        Wobit diagnostics sends modbus message to get response about
        motor position, and other parameters. This is being sent every
        second, or faster if specified otherwise.

        Raises:
            Look for 'raises' definition in the class header.
        """
        for controller in self._controllers:
            controller.start_diagnostics()

    def stop_diagnostics(self):
        """Stops diagnostics thread.

        Raises:
            Look for 'raises' definition in the class header.
        """
        for controller in self._controllers:
            controller.stop_diagnostics()
