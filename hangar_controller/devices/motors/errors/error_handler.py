from hangar_controller.devices.motors.errors.modbus_error import ModbusError
from hangar_controller.devices.motors.errors.motor_error import MotorError
from settingsd.motors.modbus_constants import ModbusResponseStatus, MotorErrors


class ErrorHandler:
    def __init__(self):
        self._status = True

    def modbus_error(self, error: ModbusResponseStatus):
        raise ModbusError(str(error))

    def motor_error(self, error: MotorErrors):
        raise MotorError(str(error))

    def get_status(self):
        current_status = self._status
        self._status = True

        return current_status
