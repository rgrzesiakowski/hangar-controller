from typing import Optional


class MotorError(Exception):
    def __init__(self, message: Optional[str] = None):
        super().__init__(message)
