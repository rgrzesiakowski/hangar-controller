class MotorsPosition:
    def __init__(self, motor_name, position):
        self.motor_name = motor_name
        self.position = position

    def __repr__(self):
        name = f'motor_name=\"{self.motor_name}\"'
        position = f'position={self.position}'
        class_name = self.__class__.__name__

        return f'<{class_name} {name}; {position}>'
