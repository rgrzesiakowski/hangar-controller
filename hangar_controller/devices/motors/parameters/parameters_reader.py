import logging
from typing import Type, Dict, Optional, List, Union

import yaml
from satella.coding.structures import Singleton

from hangar_controller.devices.motors.utils.motors_position import \
    MotorsPosition
from settingsd.motors.parameters_settings import Positions, FILE_PATH, \
    Parameters, Coordinates, DrivesNames, Motor

logger = logging.getLogger(__name__)


@Singleton
class ParametersReader:
    def __init__(self):
        self._parameters: Optional[Dict] = None
        self._positions: Optional[Dict] = None
        self._file_path = FILE_PATH
        self._update_parameters()
        self._frequent_update_parameters = True

    def _update_parameters(self):
        logger.debug('Updating parameters')
        with open(self._file_path, 'r') as file:
            self._parameters = yaml.safe_load(file)
            self._positions = self._parameters[Parameters.POSITION]

    def _check_update_positions(self):
        logger.debug('Checking if updating parameters')
        if self._frequent_update_parameters:
            self._update_parameters()

    def read_motor_placement(self, motor_names):
        logger.debug(f'Reading motors {motor_names} placement')
        wobit_ids = [self.read_wobit_id(name) for name in motor_names]
        motor_ids = [self.read_motor_id(name) for name in motor_names]

        return wobit_ids, motor_ids

    def read_manipulator_motors_placement(self):
        logger.debug('Reading manipulator motor placement')
        motor_names = [DrivesNames.X, DrivesNames.Y, DrivesNames.Z]
        placement = self.read_motor_placement(motor_names)

        return placement

    def read_lift_motor_placement(self):
        logger.debug('Reading lift motor placement')
        name = DrivesNames.LIFT
        placement = self.read_motor_placement([name])

        return placement

    def read_grasper_motor_placement(self):
        logger.debug('Reading grasper motor placement')
        name = DrivesNames.GRASPER
        placement = self.read_motor_placement([name])

        return placement

    def read_positioning_motors_placement(self):
        logger.debug('Reading positioning motors placement')
        motor_names = [DrivesNames.X_PLATFORM, DrivesNames.Y_PLATFORM]
        placement = self.read_motor_placement(motor_names)

        return placement

    def read_battery_slot_position(self, slot_number: int) \
            -> List[MotorsPosition]:
        logger.debug(f'Reading {slot_number} battery slot position')
        self._check_update_positions()
        x_array = self._positions[Positions.SLOTS][Coordinates.X]
        y_array = self._positions[Positions.SLOTS][Coordinates.Y]
        z_array = self._positions[Positions.SLOTS][Coordinates.Z]

        try:
            x_position = MotorsPosition(DrivesNames.X, x_array[slot_number])
            y_position = MotorsPosition(DrivesNames.Y, y_array[slot_number])
            z_position = MotorsPosition(DrivesNames.Z, z_array[slot_number])

            return [x_position, y_position, z_position]

        except IndexError:
            raise IndexError(f'Wrong index of battery slot, got {slot_number},'
                             f'Expecting index from 0 to {len(x_array)}')

    def _read_motors_position(self, element_path: Type[Positions]) \
            -> List[MotorsPosition]:
        """Read element position and return tuple describing motors to
        move and positions.

        Args:
            element_path: name of element to position motors to.

        Returns:
            List of MotorsPosition objects.
        """
        logger.debug('Reading motors position')
        self._check_update_positions()
        position_dict = self._positions[element_path]
        motors_position = []
        for motor_name, position in position_dict.items():
            position = MotorsPosition(motor_name, position)
            motors_position.append(position)

        return motors_position

    def read_cargo_drone(self) -> List[MotorsPosition]:
        logger.debug('Reading cargo drone position')
        self._check_update_positions()
        cargo_drone = self._read_motors_position(Positions.CARGO_DRONE)

        return cargo_drone

    def read_battery_drone(self,
                           position: Union[Positions.BATTERY_DRONE_TAKE,
                                           Positions.BATTERY_DRONE_PUT]
                           ) -> List[MotorsPosition]:
        logger.debug('Reading battery drone position')
        self._check_update_positions()
        battery_drone = self._read_motors_position(position)

        return battery_drone

    def read_cargo_window(self) -> List[MotorsPosition]:
        logger.debug('Reading cargo window position')
        self._check_update_positions()
        cargo_window = self._read_motors_position(Positions.CARGO_WINDOW)

        return cargo_window

    def read_home_position(self) -> List[MotorsPosition]:
        logger.debug('Reading home position')
        self._check_update_positions()
        home = self._read_motors_position(Positions.HOME)

        return home

    def read_cargo_window_pick_up(self) -> List[MotorsPosition]:
        logger.debug('Reading cargo window pick up position')
        self._check_update_positions()
        cargo_window_home = self._read_motors_position(
            Positions.CARGO_WINDOW_PICK_UP)

        return cargo_window_home

    def read_cargo_safe_move_height(self) -> List[MotorsPosition]:
        logger.debug('Reading cargo safe move position')
        self._check_update_positions()
        cargo_safe_move = self._read_motors_position(
            Positions.CARGO_SAFE_MOVE_HEIGHT)

        return cargo_safe_move

    def read_lift_up(self) -> List[MotorsPosition]:
        logger.debug('Reading lift up position')
        self._check_update_positions()
        lift_up = self._read_motors_position(Positions.LIFT_UP)

        return lift_up

    def read_lift_down(self) -> List[MotorsPosition]:
        self._check_update_positions()
        lift_down = self._read_motors_position(Positions.LIFT_DOWN)

        return lift_down

    def read_platform_middle(self) -> List[MotorsPosition]:
        self._check_update_positions()
        position = self._read_motors_position(Positions.PLATFORM_MIDDLE)

        return position

    def read_platform_home(self) -> List[MotorsPosition]:
        self._check_update_positions()
        position = self._read_motors_position(Positions.PLATFORM_HOME)

        return position

    def read_wobit_id(self, motor_name: DrivesNames) -> int:
        wobit_id = self._parameters[Parameters.DRIVES][motor_name][
            Motor.WOBIT_ID]

        return wobit_id

    def read_motor_id(self, motor_name: DrivesNames) -> int:
        motor_id = self._parameters[Parameters.DRIVES][motor_name][
            Motor.MOTOR_ID]

        return motor_id

    def read_calibration_speed(self, motor_name: DrivesNames) -> float:
        self._check_update_positions()
        calibration_speed = self._parameters[Parameters.DRIVES][motor_name][
            Motor.CALIBRATION_SPEED]

        return calibration_speed

    def read_acceleration(self, motor_name: DrivesNames) -> float:
        self._check_update_positions()
        acceleration = self._parameters[Parameters.DRIVES][motor_name][
            Motor.ACCELERATION]

        return acceleration

    def read_deceleration(self, motor_name: DrivesNames) -> float:
        self._check_update_positions()
        deceleration = self._parameters[Parameters.DRIVES][motor_name][
            Motor.DECELERATION]

        return deceleration

    def read_max_velocity(self, motor_name: DrivesNames) -> float:
        self._check_update_positions()
        max_velocity = self._parameters[Parameters.DRIVES][motor_name][
            Motor.MAX_VELOCITY]

        return max_velocity

    def read_grasp_position(self) -> List[MotorsPosition]:
        self._check_update_positions()
        grasper_close = self._read_motors_position(Positions.GRASP)

        return grasper_close

    def read_release_position(self) -> List[MotorsPosition]:
        self._check_update_positions()
        grasper_open = self._read_motors_position(Positions.RELEASE)

        return grasper_open
