import logging

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.writable_io_address import \
    WritableIOAddress
from settingsd.motors.modbus_constants import ModbusResponseStatus

logger = logging.getLogger(__name__)


class DiscreteOutput(WritableIOAddress):
    def __init__(self, name: str, address: int):
        super().__init__(name, address)
        logger.debug(f'DiscreteOutput {self.name} with address '
                     f'0x{self.address:04X} created')

    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        return modbus_io.read_coils(self._address, count)

    def write_value(self, modbus_io: ModbusIO,
                    value: bool) -> ModbusResponseStatus:
        status, _ = modbus_io.write_coil(self.address, value)

        return status
