import logging

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.writable_io_address import \
    WritableIOAddress
from settingsd.motors.modbus_constants import ModbusResponseStatus

logger = logging.getLogger(__name__)


class HoldingRegister(WritableIOAddress):
    def __init__(self, name: str, address: int):
        super().__init__(name, address)
        logger.debug(f'HoldingRegister {name} with address '
                     f'0x{address:04X} created')

    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        return modbus_io.read_holding_registers(self._address, count)

    def write_value(self, modbus_io: ModbusIO, value: int) \
            -> ModbusResponseStatus:

        status, _ = modbus_io.write_holding_register(self.address, value)

        return status
