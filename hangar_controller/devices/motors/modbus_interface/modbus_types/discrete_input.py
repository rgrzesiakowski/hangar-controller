import logging

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress

logger = logging.getLogger(__name__)


class DiscreteInput(AbstractIOAddress):
    def __init__(self, name: str, address: int):
        super().__init__(name, address)
        logger.debug(f'DiscreteInput {name} with address '
                     f'0x{address:04X} created')

    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        """Read value of this address, or consecutive addresses
        starting from this one.

        Returns:
            Status and list of values.
        """
        return modbus_io.read_discrete_inputs(self._address, count)
