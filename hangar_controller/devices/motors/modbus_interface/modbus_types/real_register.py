import logging
from typing import List

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.writable_io_address import \
    WritableIOAddress
from settingsd.motors.modbus_constants import ModbusResponseStatus

logger = logging.getLogger(__name__)


class RealRegister(WritableIOAddress):
    def __init__(self, name: str, address: int):
        super().__init__(name, address)
        logger.debug(f'RealRegister {name} with address '
                     f'0x{address:04X} created')

    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        status, registers = modbus_io.read_real_register(
            self._address, count=count)
        if registers is None:
            return status, registers

        values_out: List[float] = []

        while len(registers) != 0:
            values = [registers.pop(0), registers.pop(0)]
            values_out.append(modbus_utils.registers_to_float(values))

        return status, values_out

    def write_value(self, modbus_io: ModbusIO, value: float) \
            -> ModbusResponseStatus:

        status, _ = modbus_io.write_real_register(self.address, value)

        return status
