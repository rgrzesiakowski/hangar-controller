import logging

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress

logger = logging.getLogger(__name__)


class InputRegister(AbstractIOAddress):
    def __init__(self, name: str, address: int):
        super().__init__(name, address)
        logger.debug(f'InputRegister {name} with address '
                     f'0x{address:04X} created')

    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        return modbus_io.read_input_registers(self.address, count)
