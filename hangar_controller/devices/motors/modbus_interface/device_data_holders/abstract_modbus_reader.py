import logging
from abc import abstractmethod
from typing import List, Tuple, Type, Optional, Union, Dict

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from hangar_controller.devices.motors.modbus_interface.device_data_holders.abstract_modbus_data \
    import AbstractModbusData
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress
from settingsd.motors import modbus_constants
from settingsd.motors.modbus_constants import ModbusResponseStatus

logger = logging.getLogger(__name__)


class AbstractModbusReader(AbstractModbusData):
    @abstractmethod
    def __init__(self, address_list: modbus_constants.ADDRESS_LIST_TYPE,
                 address_type: Type[AbstractIOAddress],
                 modbus_io: ModbusIO):
        super().__init__(address_list, address_type)
        self.modbus_io = modbus_io

    def update_values(self) -> Optional[ModbusResponseStatus]:
        status = None
        logger.debug(f'Updating grouped values for {self.get_type()}')

        for group in self.address_groups:
            count = len(group)
            first_io_address = self.data_list[group[0]]
            status, values = first_io_address.read_values(self.modbus_io,
                                                          count)
            if not modbus_utils.check_response_status(status):
                logger.error('Response status INVALID, when updating '
                             f'values of type {self.get_type()}')
                break

            for index, value in zip(group, values):
                self.data_list[index].value = value

        return status

    def read_io_addresses(self) -> Tuple[ModbusResponseStatus,
                                         Optional[List[AbstractIOAddress]]]:
        status = self.update_values()
        logger.debug(f'Reading values for {self.get_type()}')

        if modbus_utils.check_response_status(status):
            return status, self.data_list

        logger.error('Response status INVALID, when reading '
                     f'values of type {self.get_type()}')
        return status, None

    def read_values(self) -> \
            Tuple[ModbusResponseStatus,
                  Optional[Dict[str, Union[bool, int, float]]]]:
        status, data_list = self.read_io_addresses()
        if data_list is None:
            return status, None

        values = {}
        for io_address in data_list:
            values[io_address.name] = io_address.value

        return status, values

    def read_by_channel_name(self, name: str) \
            -> Tuple[ModbusResponseStatus,
                     Union[bool, int, float]]:
        address_io = self.get_io_address_by_name(name)

        return address_io.read_updated_value(self.modbus_io)
