import logging

from hangar_controller.devices.motors.modbus_interface.device_data_holders. \
    abstract_modbus_controller import AbstractModbusController
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.discrete_output import \
    DiscreteOutput
from settingsd.motors import modbus_constants

logger = logging.getLogger(__name__)


class DiscreteOutputController(AbstractModbusController):
    def __init__(self, modbus_io: ModbusIO,
                 address_list: modbus_constants.ADDRESS_LIST_TYPE):

        logger.debug('Initializing Discrete Output values')
        super().__init__(address_list, DiscreteOutput, modbus_io)
