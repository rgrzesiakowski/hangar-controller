import logging
from abc import ABC, abstractmethod
from typing import Type, Optional, Union, List

from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress
from hangar_controller.devices.motors.modbus_interface.modbus_types.writable_io_address import \
    WritableIOAddress
from settingsd.motors import modbus_constants

logger = logging.getLogger(__name__)


class AbstractModbusData(ABC):
    """Given list of tuples (name: str, address: int),
    class transforms it into a list of specific classes of type type_
    (e.g. DiscreteInput).
    """
    @abstractmethod
    def __init__(self, address_list: modbus_constants.ADDRESS_LIST_TYPE,
                 address_type: Type[AbstractIOAddress]):
        self.data_type = address_type
        self.data_list = self.init_addresses(address_list, address_type)
        self.address_groups = self.group_addresses()

    def init_addresses(self, addresses: modbus_constants.ADDRESS_LIST_TYPE,
                       address_type: Type[AbstractIOAddress]) \
            -> List[AbstractIOAddress]:
        logger.debug(f'Initializing address list of type: {self.get_type()}')
        address_types = []
        for name, address in addresses:
            address_types.append(address_type(name, address))

        return address_types

    def append_addresses(self, addresses: modbus_constants.ADDRESS_LIST_TYPE):
        additional_addresses = self.init_addresses(addresses, self.data_type)
        self.data_list += additional_addresses
        self.address_groups = self.group_addresses()

    def group_addresses(self) -> List[List[int]]:
        """Group addresses together if they have consecutive addresses.

        Addresses could be read in one modbus query to save some time.
        """
        if len(self.data_list) == 0:
            return [[]]

        if len(self.data_list) == 1:
            # return one group with one element in group
            return [[0]]

        groups = []
        current_group = []
        for index, io_address in enumerate(self.data_list[:-1]):
            if len(current_group) == 0:
                current_group.append(index)

            next_io_address = self.data_list[index + 1]
            if io_address.address + 1 == next_io_address.address:
                current_group.append(index + 1)

            else:
                groups.append(current_group.copy())
                current_group = []

        # add the last address if it didn't fall into last group
        if len(current_group) == 0:
            groups.append([len(self.data_list) - 1])
        else:
            groups.append(current_group)

        return groups

    def get_io_address_by_name(self, name: str) \
            -> Optional[Union[WritableIOAddress, AbstractIOAddress]]:
        logger.debug(f'Getting io address object by name: {name}')
        for io_address in self.data_list:
            if io_address.name == name:
                return io_address

        error_message = f'Name not found when getting address by name: ' \
                        f'{name} of type: {self.get_type()}'
        logger.error(error_message)
        raise ValueError(error_message)

    def get_io_address_by_address(self, address: int) \
            -> Optional[Union[WritableIOAddress, AbstractIOAddress]]:
        logger.debug(f'Getting io address object by address, {address}')
        for io_address in self.data_list:
            if io_address.address == address:
                return io_address

        error_message = f'Address not found when getting io address object: ' \
                        f'{self.get_type()} by address: {address}'
        logger.error(error_message)
        raise ValueError(error_message)

    def get_type(self) -> str:
        """Returns type.

        Returns:
            type of format <'foo.bar'>. We don't need last 2 characters,
                hence [:-2].
        """
        return str(self.data_type).split('.')[-1][:-2]
