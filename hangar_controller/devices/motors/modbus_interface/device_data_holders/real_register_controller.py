import logging

from hangar_controller.devices.motors.modbus_interface.device_data_holders.abstract_modbus_controller \
    import AbstractModbusController
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.real_register import RealRegister
from settingsd.motors import modbus_constants

logger = logging.getLogger(__name__)


class RealRegisterController(AbstractModbusController):
    def __init__(self, modbus_io: ModbusIO,
                 address_list: modbus_constants.ADDRESS_LIST_TYPE):

        logger.debug('Initializing Real Registers values')
        super().__init__(address_list, RealRegister, modbus_io)
