import logging

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO

logger = logging.getLogger(__name__)


class ModbusConnector:
    def __init__(self, connection: modbus_utils.Connection,
                 testing_discrete_input: int):
        self._modbus_io: ModbusIO = ModbusIO(connection,
                                             testing_discrete_input)
        self._connected: bool = False

    @property
    def modbus_io(self) -> ModbusIO:
        return self._modbus_io

    @property
    def connected(self) -> bool:
        return self._connected

    def connect(self) -> None:
        logger.debug('Connecting to device via modbus')
        connection_status = self._modbus_io.connect_and_test()
        self._connected = modbus_utils.check_response_status(connection_status)

    def check_connection(self) -> None:
        logger.debug('Checking connection to device')
        self._connected = self._modbus_io.check_connection()
