import logging
from typing import Optional, Callable, Tuple, Any, List

from pymodbus.client.sync import ModbusSerialClient
from pymodbus.exceptions import ConnectionException

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from settingsd.motors.modbus_constants import ModbusResponseStatus, Wobit, WobitMotorAddresses

logger = logging.getLogger(__name__)


def connection_check(wrapped_function: Callable):
    """Decorator checks if there is modbus connection exception thrown
    on read/write register.
    """

    def wrapper(*args, **kwargs) -> \
            Tuple[ModbusResponseStatus, Any]:
        function_name = str(wrapped_function).split()[1]
        logger.debug(f'Connection check for {function_name}')
        try:
            response = wrapped_function(*args, **kwargs)
            return ModbusResponseStatus.STATUS_OK, response
        except ConnectionException:
            logger.error('Connection exception when executing function '
                         f'{function_name}')
            return ModbusResponseStatus.CONNECTION_ERROR, None

    return wrapper


class ModbusIO:
    def __init__(self, connection: modbus_utils.Connection,
                 testing_register: int):
        self._client: Optional[ModbusSerialClient] = None
        self._connected: bool = False
        self.connection = connection
        self._testing_register: int = testing_register

    @property
    def client(self) -> Optional[ModbusSerialClient]:
        return self._client

    @property
    def connected(self) -> bool:
        return self._connected

    def create_client(self):
        logger.debug('Creating modbus client')
        port = self.connection.port
        baud = self.connection.baudrate
        stop_bits = self.connection.stop_bits
        parity = self.connection.parity
        timeout = self.connection.timeout

        self._client = ModbusSerialClient(method='rtu', port=port,
                                          baudrate=baud, stopbits=stop_bits,
                                          parity=parity, timeout=timeout)

    def connect_and_test(self) -> ModbusResponseStatus:
        self.create_client()
        connection_status = self.check_connection()

        return connection_status

    def disconnect(self) -> None:
        logger.debug('Closing modbus client')
        self._client.close()

    def check_connection(self) -> ModbusResponseStatus:
        """Try to read some inputs.

        If controller is not connected, register response will not have
        .bits attribute, hence AttributeError.
        """
        logger.debug('Checking modbus connection')
        connection_success = self._client.connect()

        if not connection_success:
            logger.error("Modbus client couldn't be created")
            return ModbusResponseStatus.PARAMETERS_ERROR
        reading_status, _ = self.test_reading_value()

        return reading_status

    def test_reading_value(self) -> Tuple[ModbusResponseStatus, list]:
        reading_status, value = self.read_holding_registers(
            self._testing_register)
        logger.debug('Reading single digital input status: '
                     f"{str(reading_status).split('.')[-1]}")

        return reading_status, value

    @connection_check
    def read_discrete_inputs(self, channel: int, count: int = 1) \
            -> List[bool]:
        logger.debug(f'Reading {count} discrete input(s) '
                     f'starting from address: 0x{channel:04X}')

        response = self._client.read_discrete_inputs(
            address=channel,
            count=count,
            unit=self.connection.device_address
        )

        bits = modbus_utils.try_reading_bits(response)

        return bits

    @connection_check
    def read_coils(self, channel: int, count: int = 1) \
            -> List[bool]:
        logger.debug(f'Reading {count} coil(s) from address 0x{channel:04X}')
        response = self.client.read_coils(
            address=channel,
            count=count,
            unit=self.connection.device_address
        )

        bits = modbus_utils.try_reading_bits(response)

        return bits

    @connection_check
    def write_coil(self, channel: int, value: bool):
        logger.debug(f'Writing coil value {value} to address 0x{channel:04X}')
        self._client.write_coil(channel, value,
                                unit=self.connection.device_address)

    @connection_check
    def read_input_registers(self, channel: int, count: int = 1) \
            -> List[int]:
        logger.debug(f'Reading input register from address 0x{channel:04X}')
        response = self.client.read_input_registers(
            address=channel,
            count=count,
            unit=self.connection.device_address
        )

        registers = modbus_utils.try_reading_registers(response)

        return registers

    @connection_check
    def read_holding_registers(self, channel: int, count: int = 1) -> List[int]:
        logger.debug(f'Reading holding register from address 0x{channel:04X}')
        response = self.client.read_holding_registers(
            address=channel,
            count=count,
            unit=self.connection.device_address
        )

        registers = modbus_utils.try_reading_registers(response)

        return registers

    @connection_check
    def write_holding_register(self, channel: int, value: int):
        logger.debug(f'Writing holding register value {value} '
                     f'to addressed 0x{channel:04X}')
        self.client.write_register(
            address=channel,
            value=value,
            unit=self.connection.device_address
        )

    @connection_check
    def read_real_register(self, channel: int, count: int = 1) -> List[int]:
        logger.debug(f'Reading real register from address 0x{channel:04X}')
        response = self.client.read_holding_registers(
            address=channel,
            count=count * 2,
            unit=self.connection.device_address
        )

        registers = modbus_utils.try_reading_registers(response)

        return registers

    @connection_check
    def write_real_register(self, channel: int, value: float):
        logger.debug(f'Writing real register values {value} '
                     f'to addressed 0x{channel:04X}')
        self.client.write_registers(
            address=channel,
            values=modbus_utils.float_to_2_registers(value),
            unit=self.connection.device_address
        )

    def write_user_registers(self):
        self._client.write_coil(WobitMotorAddresses.USER_REG_SAVE, 1)

    def load_user_registers(self):
        self._client.write_coil(WobitMotorAddresses.USER_REG_READ, 1)
