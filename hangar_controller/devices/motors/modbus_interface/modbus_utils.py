import logging
import struct
from typing import Optional, List, Any

import numpy as np
from pymodbus.exceptions import ConnectionException

from settingsd.motors.modbus_constants import ModbusResponseStatus

logger = logging.getLogger(__name__)


class Connection:
    def __init__(self, baudrate: int, stop_bits: int, parity: str,
                 timeout: float, device_address: int, port: str):
        self.baudrate = baudrate
        self.stop_bits = stop_bits
        self.parity = parity
        self.timeout = timeout
        self.device_address = device_address
        self.port = port

    def __eq__(self, other):  # used for unit testing
        equality = self.baudrate == other.baudrate \
                   and self.stop_bits == other.stop_bits \
                   and self.parity == other.parity \
                   and self.timeout == other.timeout \
                   and self.device_address == other.device_address \
                   and self.port == other.port

        return equality


def try_reading_bits(response) -> Optional[List]:
    logger.debug(f'Trying to read bits from response: {response}')
    try:
        return response.bits
    except AttributeError:
        logger.error(
            "Response for discrete read doesn't have 'bits' attribute. "
            "Probable Connection Exception, or address does not exists")
        raise ConnectionException


def try_reading_registers(response) -> Optional[List]:
    logger.debug(f'Trying to read registers from response: {response}')
    try:
        return response.registers
    except AttributeError:
        logger.error(
            "Response for registers read doesn't have 'registers' attribute. "
            "Probable Connection Exception, or address does not exists")
        raise ConnectionException


def check_response_status(status: ModbusResponseStatus) -> bool:
    return status == ModbusResponseStatus.STATUS_OK


def filter_response_by_status(status: ModbusResponseStatus,
                              value: Any) -> Optional[Any]:
    if check_response_status(status):
        return value
    logger.warning(f'Response "{value}" filtered because of the status {status}')
    return None


def decimal_to_binary(n, length=-1) -> str:
    """Represent number as binary.

    Add leading zeros to return string with correct length.
    If length is -1, don't add zeros.

    Args:
        n: number.
        length: length of output number.

    Returns:
        string representation of binary number.
    """

    binary_number = bin(n).replace("0b", "")
    if length > len(binary_number):
        zeros = '0' * (length - len(binary_number))
        binary_number = zeros + binary_number

    return binary_number


def float_to_2_registers(value: float) -> List[int]:
    """Convert float number to 2, 16-bit registers.

    Args:
        value: value to convert.

    Returns:
        List of two registers.
    """
    bytes_ = struct.pack('f', value)
    reg_x0 = bytes_[0] + (bytes_[1] << 8)
    reg_x1 = bytes_[2] + (bytes_[3] << 8)
    return [reg_x0, reg_x1]


def registers_to_float(reg: list) -> float:
    """Convert 2 registers to float number.

    Args:
        reg: list contains two registers.

    Returns:
        Float number from two registers.
    """
    if len(reg) != 2:
        raise ValueError('register should be list of 2 16-bits values')
    packed = struct.pack('>HH', reg[1], reg[0])
    return struct.unpack('>f', packed)[0]


def id_list_to_control_word(id_list: list):
    """Convert motor id list into control word.

    Args:
        id_list: list of motors id (e.g. [1, 2] is wobit_motors 1 and 2).

    Returns:
        Control word (eg. (1 * 2**0)
                          + (1 * 2**1)
                          + (0 * 2**2)
                          + (0 * 2**3)
                          = 3 = b'0011')
    """
    if not id_list:
        return 0
    id_bits = np.array(id_list) - 1
    control_4_bits = sum(np.power(2, id_bits))
    return control_4_bits
