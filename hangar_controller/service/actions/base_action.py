from abc import abstractmethod

from hangar_controller.devices.drivers.drivers_controller import \
    DriversController
from hangar_controller.devices.motors.motors import Motors
from hangar_controller.devices.weather_station.weather_station_controller import \
    WeatherStationController
from hangar_controller.executor.actions import BaseAction


class BaseCrumbAction(BaseAction):
    def __init__(self):
        super(BaseCrumbAction, self).__init__()
        self.motors = Motors()
        self.drivers = DriversController()
        self.weather_station = WeatherStationController()

    @abstractmethod
    def run(self):
        pass

    def can_run(self) -> bool:
        return True
