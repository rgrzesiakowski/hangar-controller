import logging

from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class CalibrateMechanismsAction(BaseCrumbAction):
    def run(self):
        logger.debug(self.motors.grasper.calibrate_grasper())
        logger.debug(self.motors.grasper.release())
        logger.debug(self.motors.manipulator.calibrate_manipulator_motors())
        logger.debug(self.motors.positioning.calibrate_positioning())
        logger.debug(self.motors.positioning.position_drone())
        logger.debug(self.drivers.cargo.base_shift())
        logger.debug(self.motors.lift.calibrate_lift())
        logger.debug(self.motors.lift.go_down())
        logger.debug(self.drivers.cargo.close_window())
        logger.debug(self.drivers.roof.close())

    def can_run(self) -> bool:
        # TODO: Check that can run
        return True
