import logging

from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class UnloadCargoAction(BaseCrumbAction):
    def run(self):
        logger.debug(self.drivers.cargo.work_shift())
        logger.debug(self.motors.manipulator.go_cargo_drone())
        logger.debug(self.motors.grasper.grasp())
        logger.debug(self.motors.manipulator.go_cargo_window_put_down())
        logger.debug(self.motors.grasper.release())
        logger.debug(self.motors.manipulator.go_home())
        logger.debug(self.drivers.cargo.base_shift())

    def can_run(self) -> bool:
        # TODO: Check that can run
        return True
