import logging

from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class CloseRoofAction(BaseCrumbAction):
    def run(self):
        logger.debug(self.drivers.roof.close())

    def can_run(self) -> bool:
        # TODO: Check that can run
        return True
