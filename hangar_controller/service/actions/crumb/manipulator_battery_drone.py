import logging

from hangar_controller.service.actions.base_action import \
    BaseCrumbAction
from settingsd.motors.parameters_settings import Positions

logger = logging.getLogger(__name__)


class ManipulatorBatteryDroneAction(BaseCrumbAction):
    def __init__(self, position):
        super().__init__()

        if position == "put":
            self.position = Positions.BATTERY_DRONE_PUT
        elif position == 'take':
            self.position = Positions.BATTERY_DRONE_TAKE
        else:
            self.position = None

    def run(self):
        if self.position not in (Positions.BATTERY_DRONE_PUT, Positions.BATTERY_DRONE_TAKE):
            logger.error("Wrong position for battery drone action")
            return
        logger.debug(self.motors.manipulator.go_battery_drone(self.position))

    def can_run(self) -> bool:
        # TODO: Check that can run
        return True
