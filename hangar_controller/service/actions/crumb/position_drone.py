import logging

from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class PositionDroneAction(BaseCrumbAction):
    def run(self):
        logger.debug(self.motors.positioning.position_drone())

    def can_run(self) -> bool:
        # TODO: Check that can run
        return True
