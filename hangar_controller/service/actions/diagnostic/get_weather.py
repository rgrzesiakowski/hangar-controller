import logging

from hangar_controller.service.actions.base_action import \
    BaseCrumbAction
from hangar_controller.service.service_hangar_client import ServiceHangarClient

logger = logging.getLogger(__name__)


class GetWeatherAction(BaseCrumbAction):
    def run(self):
        logger.debug("Getting weather")
        weather_info = {
            'temperature': self.weather_station.read_temperature(),
            'wind_speed': self.weather_station.read_wind_speed(),
            'wind_direction': self.weather_station.read_wind_direction(),
            'humidity': self.weather_station.read_humidity(),
            'pressure': self.weather_station.read_pressure()
        }

        ServiceHangarClient().send_message(weather_info)

    def can_run(self) -> bool:
        # TODO: Check that can run
        return True
