import logging

from hangar_controller.executor.state_machine import StateMachine
from hangar_controller.hangar_state import HangarState
from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class SetHangarState(BaseCrumbAction):
    def __init__(self, state: str):
        super().__init__()

        self.state = HangarState(state)

    def run(self):
        StateMachine().transition(self.state)

    def can_run(self) -> bool:
        return True
