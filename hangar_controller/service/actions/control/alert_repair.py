import logging

from hangar_controller.alerts import AlertManager
from hangar_controller.alerts.utils import parse_string_code_to_alert
from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class AlertRepairAction(BaseCrumbAction):
    def __init__(self, alert: str):
        super().__init__()
        self.alert_code = parse_string_code_to_alert(alert)

    def run(self):
        AlertManager().repair(self.alert_code)

    def can_run(self) -> bool:
        return True
