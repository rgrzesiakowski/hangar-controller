import logging

from hangar_controller.battery_chargers_supervisor import BatteryChargersSupervisor
from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class SetForcedBatterySlotAction(BaseCrumbAction):
    def __init__(self, slot='0'):
        super().__init__()

        self.slot = slot

    def run(self):
        slot = None if self.slot.upper() == 'NONE' else self.slot
        BatteryChargersSupervisor().forced_slot = slot

    def can_run(self) -> bool:
        return 'Mock' in str(BatteryChargersSupervisor())  # only allowed in mock
