import logging
import typing as tp

from hangar_controller.battery_chargers_supervisor import BatteryChargersSupervisor
from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class SetBatteryToChargeAction(BaseCrumbAction):
    CAN_EXECUTE_CONCURRENTLY_WITH = [True]

    def __init__(self, slot: tp.Optional[str] = None):
        super().__init__()

        self.slot = slot or '0'

    def run(self):
        BatteryChargersSupervisor().change_battery_state_to_charged(self.slot)

    def can_run(self) -> bool:
        return 'Mock' in str(BatteryChargersSupervisor())  # only allowed in mock
