import logging

from hangar_controller.executor.state_machine import StateMachine
from hangar_controller.hangar_state import HangarState
from hangar_controller.service.actions.base_action import \
    BaseCrumbAction
from hangar_controller.azure_communication import \
    AzureIoTHubClient
from hangar_controller.azure_communication.messages import HangarHeartBeat

logger = logging.getLogger(__name__)


class EnableServiceModeAction(BaseCrumbAction):
    def run(self):
        if StateMachine().state == HangarState.SERVICE_MODE:
            logger.info("Service mode already enabled")
        else:
            logger.info("Enabling service mode")

            StateMachine().transition(HangarState.SERVICE_MODE)
            AzureIoTHubClient().send_iothub_message(HangarHeartBeat())

        # TODO: Abort other actions

    def can_run(self) -> bool:
        return True
