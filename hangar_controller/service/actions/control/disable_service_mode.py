import logging

from hangar_controller.executor.state_machine import StateMachine
from hangar_controller.hangar_state import HangarState
from hangar_controller.service.actions.base_action import \
    BaseCrumbAction
from hangar_controller.azure_communication import \
    AzureIoTHubClient
from hangar_controller.azure_communication.messages import HangarHeartBeat

logger = logging.getLogger(__name__)


class DisableServiceModeAction(BaseCrumbAction):
    def __init__(self, new_state: str = 'hangar_empty'):
        super().__init__()
        self.new_state = new_state

    def run(self):
        if StateMachine().state == HangarState.SERVICE_MODE:
            logger.info("Disabling service mode")

            new_state = HangarState(self.new_state)

            StateMachine().transition(new_state)
            AzureIoTHubClient().send_iothub_message(HangarHeartBeat())
        else:
            logger.info("Service mode not active")

    def can_run(self) -> bool:
        return True
