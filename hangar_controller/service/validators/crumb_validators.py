import typing as tp

from hangar_controller.service.validators.utils import _validate_command_name, \
    _validate_parameters_schema

KNOWN_COMMAND = ('open_roof', 'close_roof', 'open_cargo_window',
                 'close_cargo_window', 'cargo_feeder_window',
                 'cargo_feeder_lift', 'position_drone', 'release_drone',
                 'lift_up', 'lift_down', 'grasper_grasp', 'grasper_release',
                 'manipulator_cargo_drone', 'manipulator_cargo_feeder_pick_up',
                 'manipulator_cargo_feeder_put_down',
                 'manipulator_battery_drone', 'manipulator_battery_slot',
                 'manipulator_home'
                 )


@_validate_command_name('open_roof')
def _validate_open_roof_action(command: dict) -> bool:
    return True


@_validate_command_name('close_roof')
def _validate_close_roof_action(command: dict) -> bool:
    return True


@_validate_command_name('open_cargo_window')
def _validate_open_cargo_window_action(command: dict) -> bool:
    return True


@_validate_command_name('close_cargo_window')
def _validate_close_cargo_window_action(command: dict) -> bool:
    return True


@_validate_command_name('cargo_feeder_window')
def _validate_cargo_feeder_window_action(command: dict) -> bool:
    return True


@_validate_command_name('cargo_feeder_lift')
def _validate_cargo_feeder_lift_action(command: dict) -> bool:
    return True


@_validate_command_name('position_drone')
def _validate_position_drone_action(command: dict) -> bool:
    return True


@_validate_command_name('release_drone')
def _validate_release_drone_action(command: dict) -> bool:
    return True


@_validate_command_name('lift_up')
def _validate_lift_up_action(command: dict) -> bool:
    return True


@_validate_command_name('lift_down')
def _validate_lift_down_action(command: dict) -> bool:
    return True


@_validate_command_name('grasper_grasp')
def _validate_grasper_grasp_action(command: dict) -> bool:
    return True


@_validate_command_name('grasper_release')
def _validate_grasper_release_action(command: dict) -> bool:
    return True


@_validate_command_name('manipulator_cargo_drone')
def _validate_manipulator_cargo_drone_action(command: dict) -> bool:
    return True


@_validate_command_name('manipulator_cargo_feeder_pick_up')
def _validate_manipulator_cargo_feeder_pick_up_action(command: dict) -> bool:
    return True


@_validate_command_name('manipulator_cargo_feeder_put_down')
def _validate_manipulator_cargo_feeder_put_down_action(command: dict) -> bool:
    return True


@_validate_command_name('manipulator_battery_drone')
@_validate_parameters_schema({"position": str})
def _validate_manipulator_battery_drone_action(command: dict) -> bool:
    return True


@_validate_command_name('manipulator_battery_slot')
@_validate_parameters_schema({"battery_slot_number": int})
def _validate_manipulator_battery_slot_action(command: dict) -> bool:
    return True


@_validate_command_name('manipulator_home')
def _validate_manipulator_home_action(command: dict) -> bool:
    return True


validators: tp.List[tp.Callable[[dict], bool]] = [
    _validate_open_roof_action,
    _validate_close_roof_action,
    _validate_open_cargo_window_action,
    _validate_close_cargo_window_action,
    _validate_cargo_feeder_window_action,
    _validate_cargo_feeder_lift_action,
    _validate_position_drone_action,
    _validate_release_drone_action,
    _validate_lift_up_action,
    _validate_lift_down_action,
    _validate_grasper_grasp_action,
    _validate_grasper_release_action,
    _validate_manipulator_cargo_drone_action,
    _validate_manipulator_cargo_feeder_pick_up_action,
    _validate_manipulator_cargo_feeder_put_down_action,
    _validate_manipulator_battery_drone_action,
    _validate_manipulator_battery_slot_action,
    _validate_manipulator_home_action
]
