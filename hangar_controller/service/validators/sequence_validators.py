import typing as tp

from hangar_controller.service.validators.utils import _validate_command_name

KNOWN_COMMAND = ('calibrate_mechanisms', 'load_cargo', 'unload_cargo')


@_validate_command_name('calibrate_mechanisms')
def _validate_calibrate_mechanisms_action(command: dict) -> bool:
    return True


@_validate_command_name('load_cargo')
def _validate_load_cargo_action(command: dict) -> bool:
    return True


@_validate_command_name('unload_cargo')
def _validate_unload_cargo_action(command: dict) -> bool:
    return True


validators: tp.List[tp.Callable[[dict], bool]] = [
    _validate_calibrate_mechanisms_action,
    _validate_load_cargo_action,
    _validate_unload_cargo_action
]
