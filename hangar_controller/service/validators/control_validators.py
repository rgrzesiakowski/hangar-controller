import typing as tp

from hangar_controller.alerts import AlertCodes
from hangar_controller.alerts.utils import parse_string_code_to_alert
from hangar_controller.hangar_state import HangarState
from hangar_controller.service.validators.utils import _validate_command_name, \
    _validate_parameters_schema

KNOWN_COMMAND = ('alert_repair', 'enable_service_mode', 'disable_service_mode',
                 'set_battery_to_charge', 'set_forced_battery_slot',
                 'set_hangar_state')


@_validate_command_name('set_hangar_state')
@_validate_parameters_schema({'state': str})
def _validate_set_hangar_state_action(command: dict) -> bool:
    requested_state = command['parameters']['state']
    try:
        HangarState(requested_state)
    except ValueError:
        return False
    return True


@_validate_command_name('set_battery_to_charge')
def _validate_set_battery_to_charge_action(command: dict) -> bool:
    return True


@_validate_command_name('set_forced_battery_slot')
def _validate_set_forced_battery_slot(command: dict) -> bool:
    return True


@_validate_command_name('alert_repair')
@_validate_parameters_schema({'alert': str})
def _validate_alert_repair_action(command: dict) -> bool:
    alert_code = parse_string_code_to_alert(command['parameters']['alert'])

    if alert_code == AlertCodes.UNKNOWN:
        return False

    return True


@_validate_command_name('enable_service_mode')
def _validate_enable_service_mode_action(command: dict) -> bool:
    return True


@_validate_command_name('disable_service_mode')
@_validate_parameters_schema({"new_state": str})
def _validate_disable_service_mode_action(command: dict) -> bool:
    new_state_parameters = command['parameters']['new_state']

    try:
        new_state = HangarState(new_state_parameters)
    except ValueError:
        return False

    allowed_stated = (HangarState.HANGAR_FULL, HangarState.HANGAR_EMPTY,
                      HangarState.SHUTDOWN)

    if new_state not in allowed_stated:
        return False

    return True


validators: tp.List[tp.Callable[[dict], bool]] = [
    _validate_alert_repair_action,
    _validate_enable_service_mode_action,
    _validate_disable_service_mode_action,
    _validate_set_battery_to_charge_action,
    _validate_set_forced_battery_slot,
    _validate_set_hangar_state_action,
]
