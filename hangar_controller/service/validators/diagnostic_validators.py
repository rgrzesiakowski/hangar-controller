import typing as tp

from hangar_controller.service.validators.utils import _validate_command_name

KNOWN_COMMAND = ('get_weather', 'cargo_tare',)


@_validate_command_name('get_weather')
def _validate_get_weather_action(command: dict) -> bool:
    return True


@_validate_command_name('cargo_tare')
def _validate_cargo_tare_action(command: dict) -> bool:
    return True


validators: tp.List[tp.Callable[[dict], bool]] = [
    _validate_get_weather_action,
    _validate_cargo_tare_action,
]
