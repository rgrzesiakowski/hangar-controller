import logging
import typing as tp

from hangar_controller.service.actions.crumb import *
from hangar_controller.service.handlers.utils import must_have_command_name
from hangar_controller.service.handlers.utils import handler_factory

logger = logging.getLogger(__name__)


@must_have_command_name('manipulator_battery_slot')
def handle_manipulator_battery_slot(cmd: dict) -> bool:
    return ManipulatorBatterySlotAction(
        cmd['parameters']['battery_slot_number']).schedule()


@must_have_command_name('manipulator_battery_drone')
def handle_manipulator_battery_drone(cmd: dict) -> bool:
    return ManipulatorBatteryDroneAction(
        cmd['parameters']['position']).schedule()


handlers: tp.List[tp.Callable[[dict], bool]] = [
    handler_factory('open_roof', OpenRoofAction),
    handler_factory('close_roof', CloseRoofAction),
    handler_factory('open_cargo_window', OpenCargoWindowAction),
    handler_factory('close_cargo_window', CloseCargoWindowAction),
    handler_factory('cargo_feeder_window', CargoFeederWindowAction),
    handler_factory('cargo_feeder_lift', CargoFeederLiftAction),
    handler_factory('position_drone', PositionDroneAction),
    handler_factory('release_drone', ReleaseDroneAction),
    handler_factory('lift_up', LiftUpAction),
    handler_factory('lift_down', LiftDownAction),
    handler_factory('grasper_grasp', GrasperGraspAction),
    handler_factory('grasper_release', GrasperReleaseAction),
    handler_factory('manipulator_cargo_drone', ManipulatorCargoDroneAction),
    handler_factory('manipulator_cargo_feeder_pick_up',
                    ManipulatorCargoFeederPickUpAction),
    handler_factory('manipulator_cargo_feeder_put_down',
                    ManipulatorCargoFeederPutDownAction),
    handle_manipulator_battery_slot,
    handle_manipulator_battery_drone,
    handler_factory('manipulator_home', ManipulatorHomeAction),
]
