import logging
import typing as tp

from hangar_controller.service.actions.diagnostic import *
from hangar_controller.service.handlers.utils import handler_factory

logger = logging.getLogger(__name__)

handlers: tp.List[tp.Callable[[dict], bool]] = [
    handler_factory('get_weather', GetWeatherAction),
    handler_factory('cargo_tare', CargoTareAction),
]
