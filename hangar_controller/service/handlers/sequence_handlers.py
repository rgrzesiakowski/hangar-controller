import logging
import typing as tp

from hangar_controller.service.actions.sequence import *
from hangar_controller.service.handlers.utils import handler_factory

logger = logging.getLogger(__name__)

handlers: tp.List[tp.Callable[[dict], bool]] = [
    handler_factory('calibrate_mechanisms', CalibrateMechanismsAction),
    handler_factory('load_cargo', LoadCargoAction),
    handler_factory('unload_cargo', UnloadCargoAction)
]
