import logging
import typing as tp

from hangar_controller.service.actions.control import *
from hangar_controller.service.handlers.utils import handler_factory, \
    must_have_command_name

logger = logging.getLogger(__name__)


@must_have_command_name('set_hangar_state')
def handle_set_hangar_state(cmd: dict) -> bool:
    return SetHangarState(cmd['parameters']['state']).schedule()


@must_have_command_name('alert_repair')
def handle_alert_repair(cmd: dict) -> bool:
    return AlertRepairAction(cmd['parameters']['alert']).schedule()


@must_have_command_name('disable_service_mode')
def handle_disable_service_mode(cmd: dict) -> bool:
    return DisableServiceModeAction(cmd['parameters']['new_state']).schedule()


@must_have_command_name('set_battery_to_charge')
def handle_set_battery_to_charge(cmd: dict) -> bool:
    return SetBatteryToChargeAction(cmd['parameters']['slot']).schedule()


@must_have_command_name('set_forced_battery_slot')
def handle_set_forced_battery_slot(cmd: dict) -> bool:
    return SetForcedBatterySlotAction(cmd['parameters']['slot']).schedule()


handlers: tp.List[tp.Callable[[dict], bool]] = [
    handle_alert_repair,
    handler_factory('enable_service_mode', EnableServiceModeAction),
    handle_disable_service_mode,
    handle_set_battery_to_charge,
    handle_set_forced_battery_slot,
    handle_set_hangar_state,
]
