import logging
import time

from hangar_controller.azure_communication.messages import CargoActionObjectType
from hangar_controller.devices.drivers.drivers_controller import \
    DriversController
from hangar_controller.executor.actions.cargo import TakeFromHangar, PutToHangar
from hangar_controller.put_and_take_to_hangar_backend.abstract_backend import \
    AbstractBackend
from settingsd.drivers.stm_communication_settings import ResponseMessage
from settingsd.drivers.stm_user_panel_settings import ConfirmationViews, \
    UserPanelViews

logger = logging.getLogger(__name__)


class UserPanelBackend(AbstractBackend):
    def __init__(self):
        super(UserPanelBackend, self).__init__()
        self.drivers = DriversController()

    def run(self):
        while not self._terminating:
            self.loop_tick()
            time.sleep(0.1)

    def loop_tick(self):
        if self._is_put_and_take_locked():
            time.sleep(10)
            return

        wait_for_wakeup = True
        while wait_for_wakeup:
            response = self.drivers.user_panel.set_confirmation_view(
                ConfirmationViews.HOME)

            if response.evaluation:
                wait_for_wakeup = False
                continue

            if response.evaluation_reason != ResponseMessage.TIMEOUT:
                # TODO: Show ERROR on user panel
                pass

        caot = CargoActionObjectType.CLIENT

        with self.hangar_data:
            hangar_cargo_id = self.hangar_data.hangar_cargo_id

        if hangar_cargo_id is None:
            action = 'PUT_TO_HANGAR'
            try:
                response, pin = self.drivers.user_panel.get_pin()

                if not response.evaluation:
                    if response.evaluation_reason != ResponseMessage.TIMEOUT:
                        # TODO: Show ERROR on user panel
                        return

                    self.drivers.user_panel.set_view(
                        UserPanelViews.CARGO_PIN_TIMEOUT
                    )
                    time.sleep(5)
                    self.drivers.user_panel.set_view(
                        UserPanelViews.THANK_YOU
                    )
                    time.sleep(5)
                    return

                can_run = PutToHangar(None, caot, pin).schedule()
            except ValueError as e:
                logger.error(e)
                return
        else:
            action = 'TAKE_FROM_HANGAR'
            can_run = TakeFromHangar(None, caot).schedule()

        if not can_run:
            logger.warning(
                f'Action is illegal now. Cannot perform "{action}" action'
            )
            # TODO: Show ERROR on user panel
            time.sleep(5)
