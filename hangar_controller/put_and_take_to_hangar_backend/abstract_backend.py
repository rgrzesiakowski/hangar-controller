from abc import ABC
from threading import Thread

from hangar_controller.executor import HangarData


class AbstractBackend(ABC, Thread):
    def __init__(self):
        super(AbstractBackend, self).__init__()

        self.hangar_data = HangarData()
        self._terminating = False

    # region Thread specific
    def loop_tick(self):
        """Should implement one loop tick when object run as thread"""

    def run(self):
        """
        When object should work as thread that method must be overridden

        >>> def run(self):
        >>>     while not self._terminating:
        >>>         self.loop_tick()
        >>>         time.sleep(1)
        """

    def terminate(self):
        self._terminating = True

        return self

    # endregion

    def _is_put_and_take_locked(self):
        with self.hangar_data:
            return self.hangar_data.sync_put_and_take_locked
