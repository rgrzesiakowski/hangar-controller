import logging
import time
import typing as tp
from enum import Enum

import requests

import settings
from hangar_controller.azure_communication.messages import CargoActionObjectType
from hangar_controller.executor.actions.cargo import TakeFromHangar, PutToHangar
from hangar_controller.put_and_take_to_hangar_backend.abstract_backend import \
    AbstractBackend

logger = logging.getLogger(__name__)


class Actions(Enum):
    NONE = 'none'
    TAKE_FROM_HANGAR = 'take_from_hangar'
    PUT_TO_HANGAR = 'put_to_hangar'


class WrongStatusCode(Exception):
    ...


class WrongSchema(Exception):
    ...


class UnknownAction(Exception):
    ...


class APIBackend(AbstractBackend):
    def __init__(self):
        super(APIBackend, self).__init__()
        self._expected_keys = ("action", "user_type", "pin")

    def run(self):
        while not self._terminating:
            self.loop_tick()
            time.sleep(10)

    def loop_tick(self):
        if self._is_put_and_take_locked():
            time.sleep(10)
            return

        try:
            action, user_type, pin = self._get_expected_cargo_user_info()
            logger.debug(f'ACTION: {action}, USER_TYPE: {user_type}, '
                         f'PIN: {pin}')
        except (WrongStatusCode, WrongSchema, UnknownAction) as e:
            logger.error(e)

            time.sleep(10)
            return

        if action is Actions.NONE:
            time.sleep(10)
            return

        try:
            caot = CargoActionObjectType(user_type)
        except ValueError as e:
            logger.error(e)

            time.sleep(10)
            return

        if action is Actions.TAKE_FROM_HANGAR:
            can_run = TakeFromHangar(None, caot).schedule()
        elif action is Actions.PUT_TO_HANGAR:
            can_run = PutToHangar(None, caot, pin=pin).schedule()
        else:
            logger.warning(f'Unknown action: {action}')
            return

        if not can_run:
            logger.warning(f'Received invalid command from cloud. '
                           f'Cannot perform "{action}" action')

    def _get_expected_cargo_user_info(self) -> \
            tp.Tuple[Actions, tp.Optional[str], tp.Optional[str]]:

        url = settings.PENTACOMP_API_CARGO_USER_INFO
        payload = {'deviceid': settings.HANGAR_ID}

        r = requests.get(url, payload)

        if r.status_code != 200:
            raise WrongStatusCode(f"CargoUserInfo: {r.status_code} status code")

        result = r.json()
        logger.debug(f'CargoUserInfo RESPONSE: {result}')

        if "action" not in result.keys():
            raise WrongSchema("CargoUserInfo: incorrect schema")

        try:
            action = Actions(result['action'])
        except ValueError:
            raise UnknownAction('CargoUserInfo: unknown action')

        action_has_correct_schema = all(
            [k in self._expected_keys for k in result.keys()]
        )

        if action is not Actions.NONE and not action_has_correct_schema:
            raise WrongSchema("CargoUserInfo: incorrect schema")

        user_type = result.get('user_type', None)
        pin = result.get('pin', None)

        return action, user_type, pin
