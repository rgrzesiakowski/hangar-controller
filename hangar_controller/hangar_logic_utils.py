import logging
import signal
import sys
import time

from satella.exception_handling import DumpToFileHandler
from satella.instrumentation import install_dump_frames_on

from .azure_communication.messages import BaseIoTHubMessage
from .azure_communication.messages import HangarHeartBeat

import settings
from .azure_communication import AzureIoTHubClient
from .azure_communication.messages.battery_state import BatteryState
from .battery_chargers_supervisor import BatteryChargersSupervisor
from .executor.state_machine import StateMachine
from .executor import HangarData
from .executor import ExecutorThread

from .drone_communication import HangarDroneMQTTClient
from hangar_controller.service.service_hangar_client import ServiceHangarClient

from .azure_communication.validators import validate_azure_method_name
from .azure_communication.validators import validators as azure_validators
from .azure_communication.handlers import handlers as azure_handlers
from .drone_communication.handlers import handlers as drone_mqtt_client_handlers
from .drone_communication.validators import \
    validators as drone_mqtt_client_validators
from hangar_controller.service.validators import \
    validators as service_validators
from hangar_controller.service.handlers import handlers as service_handlers

from hangar_controller.put_and_take_to_hangar_backend.api_backend import \
    APIBackend

from .logs_dump import STMLogsDump
from .logs_dump import DailyFileHandler

from hangar_controller.hangar_state import HangarState
from .logs_dump.logs_uploader import LogsUploader
from .put_and_take_to_hangar_backend.user_panel_backend import UserPanelBackend

logger = logging.getLogger(__name__)


def setup_loggers():
    logging.basicConfig(level=settings.LOGGING_LEVEL,
                        handlers=[DailyFileHandler(), logging.StreamHandler()])

    logging.getLogger('urllib3').setLevel(logging.WARNING)
    logging.getLogger('pymodbus').setLevel(logging.WARNING)


def prepare_hangar():
    logger.info('[LAUNCHING] [START] Prepare hangar')

    DumpToFileHandler([sys.stderr]).install()
    install_dump_frames_on(signal.SIGUSR2)

    # StateMachine object with AzureIoTHubClient object as a argument
    # must be created during hangar start and before other StateMachine
    # object creation (e.g. inside HangarData)
    StateMachine(AzureIoTHubClient())

    with HangarData():
        HangarData().launch_timestamp = int(time.time() * 1000)

    logger.info('[LAUNCHING] [END] Prepare hangar')


def add_validators_and_handlers():
    logger.info('[LAUNCHING] [START] Add validators and handlers')

    AzureIoTHubClient().register_azure_method_name_validator(
        validate_azure_method_name)

    for validator in azure_validators:
        AzureIoTHubClient().add_validator(validator)
    for handler in azure_handlers:
        AzureIoTHubClient().add_handler(handler)
    for validator in drone_mqtt_client_validators:
        HangarDroneMQTTClient().add_validator(validator)
    for handler in drone_mqtt_client_handlers:
        HangarDroneMQTTClient().add_handler(handler)
    for validator in service_validators:
        ServiceHangarClient().add_validator(validator)
    for handler in service_handlers:
        ServiceHangarClient().add_handler(handler)

    logger.info('[LAUNCHING] [END] Add validators and handlers')


def register_getters():
    logger.info('[LAUNCHING] [START] Register getter')

    BaseIoTHubMessage.set_hangar_type_getter(HangarData().hangar_type_getter)
    BaseIoTHubMessage.set_flight_id_getter(HangarData().flight_id_getter)
    HangarHeartBeat.set_system_info_getter(HangarData().system_info_getter)
    HangarHeartBeat.set_weather_info_getter(HangarData().weather_info_getter)
    HangarHeartBeat.set_breakdowns_getter(HangarData().breakdowns_getter)
    BatteryState.set_battery_info_getter(HangarData().battery_info_getter)
    logger.info('[LAUNCHING] [END] Register getter')


def enable_hangar():
    logger.info('[LAUNCHING] [START] Enable hangar')

    executor_thread = ExecutorThread()
    executor_thread.start()

    bcs_thread = BatteryChargersSupervisor()
    bcs_thread.start()

    iothub_client = AzureIoTHubClient()
    drone_client = HangarDroneMQTTClient()
    service_client = ServiceHangarClient()

    if settings.AZURE_SEND_ALERTS_AND_LOGS:
        logs_uploader_thread = LogsUploader()
        logs_uploader_thread.start()

    # TODO: Disable functionalities here
    StateMachine().transition(settings.HANGAR_INIT_STATE)
    iothub_client.send_iothub_message(HangarHeartBeat())
    time.sleep(2)

    logger.info('[LAUNCHING] [END] Enable hangar')

    return executor_thread, iothub_client, drone_client, service_client


def enable_cargo_backend() -> APIBackend:
    logger.info('[LAUNCHING] [START] Enable cargo backend')

    settings_cargo_backend = settings.CARGO_BACKEND

    if settings_cargo_backend == "API":
        cargo_backend = APIBackend()
    elif settings_cargo_backend == "USER_PANEL":
        cargo_backend = UserPanelBackend()
    else:
        logger.error(f"Unknown cargo backend: {settings_cargo_backend}")
        raise RuntimeError

    cargo_backend.start()

    logger.info('[LAUNCHING] [END] Enable cargo backend')

    return cargo_backend


def lock_actions():
    logger.info('[DISABLING] [START] Lock actions')

    HangarData().lock_put_and_take_actions()

    logger.info('[DISABLING] [END] Lock actions')


def disable_hangar(api_cargo_backend: APIBackend):
    logger.info('[DISABLING] [START] Disable hangar')

    StateMachine().transition(HangarState.SHUTDOWN)
    time.sleep(2)

    ExecutorThread().terminate().join()

    api_cargo_backend.terminate().join()

    STMLogsDump().terminate().join()

    BatteryChargersSupervisor().terminate().join()

    if settings.AZURE_SEND_ALERTS_AND_LOGS:
        LogsUploader().terminate().join()

    logger.info('[DISABLING] [END] Disable hangar')
