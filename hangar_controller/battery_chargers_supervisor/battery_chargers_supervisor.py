from typing import List, Tuple

from satella.coding.structures import Singleton
from satella.coding.concurrent import TerminableThread

from hangar_controller.azure_communication import AzureIoTHubClient
from hangar_controller.azure_communication.messages.battery_state import \
    BatteryState
from hangar_controller.devices.drivers.drivers_controller import DriversController
from hangar_controller.executor.hangar_data import HangarData
from settingsd.drivers.stm_master_charger_settings import \
    MasterChargerStates


class NoChargerError(Exception):
    """Raised when there is no charger available for requested state"""


@Singleton
class BatteryChargersSupervisor(TerminableThread):
    """Class that stores and manages batteries state with additional
    methods that allows direct update of these states.

    """
    def __init__(self):
        super().__init__()
        self._master_charger = DriversController().master_charger
        self.hangar_data = HangarData()
        self.azure_client = AzureIoTHubClient()

        _, chargers = self._master_charger.get_available_chargers()
        self.available_chargers: List[str] = chargers

        self.charged_batteries_slots: List[str] = []

        self.update_battery_data()

    def loop(self):
        self.safe_sleep(60)

        _, chargers = self._master_charger.get_available_chargers()
        self.available_chargers = chargers

        self.update_battery_data()

    def update_battery_data(self):
        """Update amount of charged batteries and all batteries uids
        that are present in the hangar.

        """
        charged_battery_slots = []
        batteries = []

        for charger_id in self.available_chargers:
            _, states = self._master_charger.get_states(charger_id)
            if states is None:
                continue

            bat_state = states['battery']
            if bat_state == MasterChargerStates.UNPLUGGED:
                continue

            _, telemetry = self._master_charger.get_telemetry(charger_id)
            if telemetry is None:
                continue

            if bat_state == MasterChargerStates.CHARGED:
                charged_battery_slots.append(charger_id)
            battery_uid = telemetry['battery']['uid']
            batteries.append(battery_uid)

        old_charged_slots = self.charged_batteries_slots.copy()
        self.charged_batteries_slots = charged_battery_slots

        with self.hangar_data:
            self.hangar_data.available_battery = len(charged_battery_slots)
            old_batteries = self.hangar_data.batteries.copy()
            self.hangar_data.batteries = batteries

        slot_changed = set(old_charged_slots) != set(charged_battery_slots)
        batteries_changed = set(old_batteries) != set(batteries)
        if slot_changed or batteries_changed:
            self.azure_client.send_iothub_message(BatteryState())

    def get_charged_battery(self) -> Tuple[str, str]:
        """Pop charger id that contains charged battery which is going
        to be placed in drone.

        Returns:
            Tuple of battery slot and battery's uid from this slot

        Raises:
            IndexError: When there is no battery available
            KeyError: When battery's telemetry is not available

        """
        charger_id = self.charged_batteries_slots.pop()

        _, charger_telemetry = self._master_charger.get_telemetry(charger_id)
        battery_uid = charger_telemetry['battery']['uid']

        return charger_id, battery_uid

    def get_charger_id_by_state(self, state: MasterChargerStates) -> str:
        """Get first available charger for requested state.

        Returns:
            First charger_id that has requested battery state

        Raises:
            NoChargerError: When any charger doesn't have requested state.

        """
        for charger_id in self.available_chargers:
            _, states = self._master_charger.get_states(charger_id)
            if states and states['battery'] == state:
                return charger_id
        raise NoChargerError

    def add_battery_to_slot(self, charger_id: str) -> str:
        """Register battery in free slot. The battery should already be
        physically placed in the slot when this method is called,
        because the telemetry of battery has to be read.

        Args:
            charger_id: ID of charger where the battery will be stored

        Returns:
            UID of battery that had been put down to the slot.

        """
        _, telemetry = self._master_charger.get_telemetry(charger_id)
        uid = telemetry['battery']['uid'] if telemetry else None

        with self.hangar_data:
            if uid and uid not in self.hangar_data.batteries:
                self.hangar_data.batteries.append(uid)

        return uid

    def remove_battery_from_hangar(self, battery_uid: str):
        """Remove battery from charged batteries list. From now the
        battery is considered as not present in the hangar.
        This method should be called AFTER the battery is picked by
        manipulator and is, in fact, already in drone.

        Args:
            charger_id: ID of charger where the battery is stored
            battery_uid: UID of the battery

        """
        available_battery = len(self.charged_batteries_slots)

        with self.hangar_data:
            self.hangar_data.available_battery = available_battery
            if battery_uid in self.hangar_data.batteries:
                self.hangar_data.batteries.remove(battery_uid)
