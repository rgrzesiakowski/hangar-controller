import heapq
import logging
import time
from collections import defaultdict
import datetime
from typing import DefaultDict, Dict, List, Optional, Tuple, TextIO

from satella.coding.concurrent import TerminableThread
from satella.coding.structures import Singleton
from serial.serialposix import Serial, SerialException

from settingsd.logs_dump.stm_dump_settings import DEVICES, BAUD_RATE, \
    NEXT_CONN_RETRY_DELAY, READ_TIMEOUT, DUMP_SIZE, LOG_TIMESTAMP_REGEX, \
    LOG_REGEX, DUMP_FOLDER, LOG_COLORS

logger = logging.getLogger(__name__)


@Singleton
class STMLogsDump(TerminableThread):
    def __init__(self):
        super().__init__()

        self._logs_heap: List[Tuple[int, str, str]] = []

        self._serial: Dict[str, Optional[Serial]] = \
            defaultdict(lambda: None)
        self._serial_next_conn_retry: DefaultDict[str, int] = \
            defaultdict(lambda: 0)
        self._serial_next_conn_retry_delay: DefaultDict[str, int] = \
            defaultdict(lambda: 0)

        for device in DEVICES:
            self._connect(device)

    def loop(self) -> None:
        for device in DEVICES:
            if not self._serial[device]:
                self._connect(device)

            if self._serial[device]:
                self._read_logs(device)

        self._dump_logs()

    def cleanup(self) -> None:
        for device in DEVICES:
            if self._serial[device]:
                self._serial[device].close()
        self._dump_logs(flush=True)

    def _connect(self, device: str) -> None:
        if self._serial_next_conn_retry[device] > time.time():
            return

        try:
            serial = Serial(f'/dev/{device}', BAUD_RATE, timeout=READ_TIMEOUT)
        except SerialException as err:
            self._unbind_serial(device, str(err))
            return

        self._bind_serial(device, serial)

    def _bind_serial(self, device: str, serial: Serial):
        self._serial[device] = serial
        self._serial_next_conn_retry[device] = 0
        self._serial_next_conn_retry_delay[device] = 0
        logger.debug(f'Connected to /dev/{device}')

    def _unbind_serial(self, device: str, reason: str):
        curr_delay = self._serial_next_conn_retry_delay[device]
        next_delay = NEXT_CONN_RETRY_DELAY[curr_delay]
        self._serial[device] = None
        self._serial_next_conn_retry[device] = int(time.time() + next_delay)
        self._serial_next_conn_retry_delay[device] = next_delay
        logger.error(f'{reason}. Next retry after {next_delay} seconds')

    def _read_logs(self, device: str) -> None:
        try:
            if self._serial[device].in_waiting:
                row = self._serial[device].read_until(b'\r\n')
            else:
                return
        except (IOError, SerialException):
            self._unbind_serial(device, f'Port /dev/{device} disconnected')
            return

        try:
            timestamp, record = self._parse_log(row)
        except TypeError:
            return

        heapq.heappush(self._logs_heap, (timestamp, device, record))

    @staticmethod
    def _parse_log(log: bytes) -> Optional[Tuple[int, str]]:
        log = log.decode('utf-8')[:-2]
        for color in LOG_COLORS:
            log = log.replace(color, '')

        if LOG_REGEX.match(log):
            timestamp_str = LOG_TIMESTAMP_REGEX.match(log)[0]
            timestamp = int(timestamp_str[1:-1])
            record = log.replace(timestamp_str, '').strip()
            return timestamp, record
        else:
            logger.error(f'Unparsable log received: {log}')

    def _dump_logs(self, flush: bool = False) -> None:
        if flush:
            logger.debug('Flushing logs dump')
        elif len(self._logs_heap) < DUMP_SIZE:
            return

        while self._logs_heap:
            first_log_ts = self._logs_heap[0][0]
            begin_ts, end_ts, date = self._get_ts_range_and_date(first_log_ts)
            log_name = date.strftime('%Y%m%d')
            with open(f'{DUMP_FOLDER}stm-{log_name}.log', 'a') as logs_dump:
                self._dump_logs_until_ts(logs_dump, log_name, end_ts)

    @staticmethod
    def _get_ts_range_and_date(timestamp: int) -> Tuple[int, int,
                                                        datetime.datetime]:
        date = datetime.datetime.fromtimestamp(timestamp)
        date = datetime.datetime(date.year, date.month, date.day)
        begin_ts = int(date.timestamp())
        end_ts = int((date + datetime.timedelta(days=1)).timestamp())
        return begin_ts, end_ts, date

    def _dump_logs_until_ts(self, file_handler: TextIO,
                            log_name: str, end_ts: int) -> None:
        while self._logs_heap:
            log_ts = self._logs_heap[0][0]
            if log_ts < end_ts:
                timestamp, device, log = heapq.heappop(self._logs_heap)
                file_handler.write(f'{timestamp} {device}: {log}\n')
            else:
                logger.debug(f'Logs dump: hangar-{log_name}.log completed')
                return
