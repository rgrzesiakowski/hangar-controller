import logging
import os
from datetime import datetime, timedelta
from time import sleep
from typing import List

from azure.storage.blob import BlockBlobService
from satella.coding.concurrent import TerminableThread
from satella.coding.structures import Singleton

from settings import AZURE_DEVICE_ID, AZURE_STORAGE_ACCOUNT, \
    AZURE_STORAGE_KEY, AZURE_STORAGE_KWARGS, AZURE_LOGS_CONTAINER
from settingsd.logs_dump.logs_uploader_settings import SUPERVISED_FOLDERS, \
    TERMINATION_WATCHDOG_INTERVAL, UPLOAD_RETRIES_NUM, UPLOAD_RETRY_INTERVAL, \
    UPLOAD_AT_DELAY

logger = logging.getLogger(__name__)


@Singleton
class LogsUploader(TerminableThread):
    def __init__(self):
        super().__init__()
        self._storage_service = BlockBlobService(AZURE_STORAGE_ACCOUNT,
                                                 AZURE_STORAGE_KEY,
                                                 **AZURE_STORAGE_KWARGS)
        self._next_send_date = datetime.utcnow().date() + timedelta(days=1)

    def loop(self):
        if self._next_send_date == datetime.utcnow().date():
            # Wait to omit race condition with logs producers
            if self.safe_sleep(UPLOAD_AT_DELAY, TERMINATION_WATCHDOG_INTERVAL):
                return

            collected_files = self._collect_files()
            for f_path in collected_files:
                self._upload_file_with_retries(f_path)
            self._next_send_date += timedelta(days=1)
        else:
            # Wakeup more often than necessary to check if terminating
            sleep(TERMINATION_WATCHDOG_INTERVAL)

    def _collect_files(self) -> List[str]:
        previous_day_date = self._next_send_date - timedelta(days=1)
        previous_day_date_str = previous_day_date.strftime('%Y%m%d')

        collected_files = []

        for dir_name in SUPERVISED_FOLDERS:
            unfiltered_files = os.listdir(dir_name)
            unfiltered_files = [os.path.join(dir_name, f_name)
                                for f_name in unfiltered_files]
            filtered_files = [f_name for f_name in unfiltered_files
                              if previous_day_date_str in f_name]
            collected_files.extend(filtered_files)

        return collected_files

    def _upload_file_with_retries(self, f_path):
        if self.terminating:
            return

        for retry in range(1, UPLOAD_RETRIES_NUM + 1):
            try:
                self._upload_file(f_path)
                return
            # Catch all exceptions as create_blob_from_path
            # doesn't list thrown exceptions
            except Exception as e:
                logger.error(f'File {f_path} upload error\n'
                             f'\tReason: {e}\n'
                             f'\tRetry: {retry}')
                if not self._sleep_until_next_retry(retry):
                    return

    def _upload_file(self, f_path):
        blob_name = f'{AZURE_DEVICE_ID}-{os.path.basename(f_path)}'

        self._storage_service.create_blob_from_path(AZURE_LOGS_CONTAINER,
                                                    blob_name, f_path)
        logger.debug(f'Log file {f_path} saved as {blob_name}')

    def _sleep_until_next_retry(self, retry: int) -> bool:
        if retry < UPLOAD_RETRIES_NUM:
            if self.safe_sleep(UPLOAD_RETRY_INTERVAL,
                               TERMINATION_WATCHDOG_INTERVAL):
                return False  # Hangar terminating
            else:
                return True  # Sleep completed
        else:
            return False  # Retries number exceeded

