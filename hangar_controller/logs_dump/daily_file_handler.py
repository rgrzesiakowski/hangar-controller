import time
import os
from logging import Formatter
from logging.handlers import TimedRotatingFileHandler

from settingsd.logs_dump.hangar_dump_settings import LOG_FORMAT, TIME_FORMAT, \
    DUMP_FOLDER


class DailyFileHandler(TimedRotatingFileHandler):
    def __init__(self, logs_dir=DUMP_FOLDER):
        self.logs_dir = logs_dir
        super().__init__(self.output_filename, when='midnight',
                         utc=True, interval=1)
        self.setFormatter(Formatter(LOG_FORMAT, TIME_FORMAT))

    def doRollover(self) -> None:
        self.stream.close()
        self.baseFilename = self.output_filename
        self.stream = open(self.baseFilename, 'w', encoding=self.encoding)
        self.rolloverAt = self.computeRollover(int(time.time()))

    @property
    def output_filename(self):
        date_str = time.strftime('%Y%m%d')
        return os.path.join(self.logs_dir, f'hangar-{date_str}.log')
