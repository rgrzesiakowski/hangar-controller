# RUNTIME
FROM git.cervirobotics.com:4567/airvein/hangar/tools/hangar-baseimage-build:18.04-py3.6-lb1.65 AS runtime

ENV PIP_INDEX_URL=https://pip.cervirobotics.com/packages/stable/+simple

ADD requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

WORKDIR /app
ADD . /app

CMD ["python3", "-m", "hangar_controller.run"]

# TESTS
FROM runtime AS tests

RUN pip3 install coverage

## UNIT
FROM tests AS unittests

RUN chmod ugo+x /app/tests/run_unit_tests.sh
CMD ["/app/tests/run_unit_tests.sh"]

## INTEGRATION
FROM tests AS integration

RUN chmod ugo+x /app/tests/run_integration_tests.sh
CMD ["/app/tests/run_integration_tests.sh"]

# DEBUGMODE
FROM runtime AS debugmode
CMD ["python3", "-m", "tests.dry_run.dry_run"]

# PSCI API MOCK
FROM python:3.6 AS pentacomp-api-mock
ADD tests/dry_run/requirements-dryrun.txt /tmp/requirements-dryrun.txt
RUN pip3 install -r /tmp/requirements-dryrun.txt

WORKDIR /app
ADD tests/dry_run/pentacomp_api_mock.py /app/pentacomp_api_mock.py

CMD ["python3", "-m", "pentacomp_api_mock"]
